﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class GameEventAndResponse
{
    public GameEvent Event;
    public UnityEvent Response;
    public void OnEventRaised()
    {
        Response.Invoke();
    }
}
public class GameEventListener : MonoBehaviour
{
    public List<GameEventAndResponse> Events = new List<GameEventAndResponse>();
    void OnEnable()
    {
        foreach(GameEventAndResponse EventAndResponse in Events)
        {
            EventAndResponse.Event.RegisterListener(this);
        }
        
    }
    void OnDisable()
    {
        foreach (GameEventAndResponse EventAndResponse in Events)
        {
            EventAndResponse.Event.UnregisterListener(this);
        }
    }
    public void OnEventRaised(GameEvent gameEvent)
    {
        foreach (GameEventAndResponse EventAndResponse in Events)
        {
            if(EventAndResponse.Event == gameEvent)
            {
                EventAndResponse.Response.Invoke();
            }
        }
    }
}
