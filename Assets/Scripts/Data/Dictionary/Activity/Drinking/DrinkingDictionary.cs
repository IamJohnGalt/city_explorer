﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Dictionary/Drinking")]
public class DrinkingDictionary : GenericDataDict<DrinkingNode>
{
    public override void DebugCount()
    {
        Debug.Log("Total of " + UIDList.Count + " nodes are added to current Drinking Dictionary");

    }
    public override void DebugPrint()
    {
        foreach (KeyValuePair<string, DrinkingNode> pair in DataDict)
        {
            pair.Value.DebugPrint();
        }
    }
}
