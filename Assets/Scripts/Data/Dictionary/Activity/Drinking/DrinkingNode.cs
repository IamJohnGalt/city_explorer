﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class DrinkingInfo
{
    public string cardUID;
    public float weight;
    public int unlockTier;
    public dConstants.Drinking.InfoType Type;
    public string Description;
    public DrinkingInfo()
    {
        cardUID = "";
        weight = 0;
        unlockTier = 0;
        Type = dConstants.Drinking.InfoType.generic;
        Description = "";
    }
    public DrinkingInfo(DrinkingInfo copyInfo)
    {
        cardUID = copyInfo.cardUID;
        weight = copyInfo.weight;
        unlockTier = copyInfo.unlockTier;
        Type = copyInfo.Type;
        Description = copyInfo.Description;
    }
    public void DebugPrint()
    {
        Debug.Log("Drinking Info Desc: " + Description + " with weight of " + weight);
    }
}
[System.Serializable]
public class DrinkingNode
{
    public string UID;
    public List<int> costByTier;
    public List<DrinkingInfo> infoList;

    public DrinkingNode()
    {
        UID = "";
        costByTier = new List<int>();
        infoList = new List<DrinkingInfo>();
    }
    public DrinkingNode(DrinkingNode copyNode)
    {
        UID = copyNode.UID;
        costByTier = new List<int>(copyNode.costByTier);
        infoList = new List<DrinkingInfo>(copyNode.infoList);
    }

    public void DebugPrint()
    {
        Debug.Log("Node UID: " + UID + " contains " + infoList.Count + " drinking info");
        foreach (DrinkingInfo info in infoList)
        {
            info.DebugPrint();
        }
    }
}
