﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TastingFood
{
    public string cardUID;
    public string releaseGate;
    public bool signatured;
    public float weight;
    public int unlockTier;
    public TastingFood()
    {
        cardUID = "";
        releaseGate = "";
        signatured = false;
        weight = 0;
        unlockTier = 0;
    }
    public TastingFood(TastingFood copyItem)
    {
        cardUID = copyItem.cardUID;
        releaseGate = copyItem.releaseGate;
        signatured = copyItem.signatured;
        weight = copyItem.weight;
        unlockTier = copyItem.unlockTier;
    }
    public void DebugPrint()
    {
        Debug.Log("Card UID: " + cardUID + " with weight of " + weight);
    }
}
[System.Serializable]
public class TastingNode
{
    public string UID;
    public string releaseGate;
    public int counts;
    public int cost;
    public List<TastingFood> foodList;

    public TastingNode()
    {
        UID = "";
        releaseGate = "";
        counts = 0;
        cost = 0;
        foodList = new List<TastingFood>();
    }
    public TastingNode(TastingNode copyNode)
    {
        UID = copyNode.UID;
        releaseGate = copyNode.releaseGate;
        counts = copyNode.counts;
        cost = copyNode.cost;
        foodList = new List<TastingFood>(copyNode.foodList);
    }
    public void DebugPrint()
    {
        Debug.Log("Node UID: " + UID + " contains " + foodList.Count + " food");
        foreach (TastingFood item in foodList)
        {
            item.DebugPrint();
        }
    }
}
