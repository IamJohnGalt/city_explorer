﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Dictionary/Tasting")]
public class TastingDictionary : GenericDataDict<TastingNode>
{
    public override void DebugCount()
    {
        Debug.Log("Total of " + UIDList.Count + " nodes are added to current Tasting Dictionary");

    }
    public override void DebugPrint()
    {
        foreach (KeyValuePair<string, TastingNode> pair in DataDict)
        {
            pair.Value.DebugPrint();
        }
    }
}
