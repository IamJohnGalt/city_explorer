﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[System.Serializable]
public class GenericDataDict<T> : ScriptableObject
    //, ISerializationCallbackReceiver
{
    public Dictionary<string, T> DataDict = new Dictionary<string, T>();
    public List<string> UIDList = new List<string>();
    public void Reset()
    {
        DataDict.Clear();
        UIDList.Clear();
    }
    public bool Contains(string UID)
    {
        if (UIDList.Contains(UID))
        {
            return true;
        }
        return false;
    }
    public bool Add(string UID, T item)
    {
        if (UIDList.Contains(UID))
        {
            Debug.LogError("Duplicated UID found: " + UID);
            return false;
        }
        else
        {
            UIDList.Add(UID);
            DataDict.Add(UID, item);
            return true;
        }
    }
    public T GetFromUID(string UID)
    {
        T item = default(T);
        if (UIDList.Contains(UID))
        {
            DataDict.TryGetValue(UID, out item);
            return item;
        }
        else
        {
            Debug.LogError("Cannot find UID: " + UID);
            return item;
        }

    }
    public List<T> GetFullList()
    {
        return DataDict.Values.ToList();
    }
    public virtual void DebugCount()
    {
        Debug.Log("Total of " + UIDList.Count + " entries are added to current Dictionary");
        
    }
    public virtual void DebugPrint()
    {
        
    }
}
