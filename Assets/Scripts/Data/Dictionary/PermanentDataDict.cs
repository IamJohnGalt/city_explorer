﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PermanentDataDict<T> : ScriptableObject, ISerializationCallbackReceiver
{
    public Dictionary<string, T> DataDict = new Dictionary<string, T>();
    public List<string> UIDList = new List<string>();
    public void OnAfterDeserialize()
    {
        Reset();
    }

    public void OnBeforeSerialize()
    {

    }
    public void Reset()
    {
        DataDict.Clear();
        UIDList.Clear();
    }
    public bool Add(string UID, T item)
    {
        if (UIDList.Contains(UID))
        {
            Debug.LogError("Duplicated UID found: " + UID);
            return false;
        }
        else
        {
            UIDList.Add(UID);
            DataDict.Add(UID, item);
            return true;
        }
    }
    public bool Set(string UID, T item)
    {
        if (UIDList.Contains(UID))
        {
            DataDict[UID] = item;
            return true;
        }
        else
        {
            Debug.LogError("No UID found: " + UID);
            return false;
            
        }
    }
    public T GetFromUID(string UID)
    {
        T item = default(T);
        if (UIDList.Contains(UID))
        {
            DataDict.TryGetValue(UID, out item);
            return item;
        }
        else
        {
            //Debug.LogError("Cannot find UID: " + UID);
            return item;
        }

    }
    public virtual void DebugCount()
    {
        Debug.Log("Total of " + UIDList.Count + " entries are added to current Dictionary");

    }
    public virtual void DebugPrint()
    {

    }
}
