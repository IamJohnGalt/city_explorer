﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataReader_KeywordNRules : GenericDataReader
{
    public KeywordOrderList keywords;
    public KeywordRuleList keywordRuleset;
    public override void LoadFromXLS()
    {
        keywords.list.Clear();
        keywordRuleset.list.Clear();
        LoadKeywordData("keyword_list_exporter");
        LoadKeywordRuleData("keyword_rules_exporter");
        FinishLoading();
    }
    void LoadKeywordData(string filename)
    {
        string[,] string_data = CSVReader.SplitCsvGrid(Resources.Load<TextAsset>("XLSData/" + filename).text);
        if (keywords == null)
        {
            Debug.Log("Null Ref on Data Dictionary");
        }
        string curWord;
        for (int iRow = 1; iRow < string_data.GetUpperBound(1); iRow++)
        {
            
            curWord = string_data[0, iRow];
            if (!keywords.Contains(curWord))
            {
                KeywordOrder keywordOrder = new KeywordOrder();
                keywordOrder.word = string_data[0, iRow];
                int.TryParse(string_data[1, iRow], out keywordOrder.orderIndex);
                int.TryParse(string_data[2, iRow], out keywordOrder.typeIndex);
                keywords.list.Add(keywordOrder);
            }
            else
            {
                Debug.Log("duplicated keyword adding attempts");
            }
        }
    }
    void LoadKeywordRuleData(string filename)
    {
        string[,] string_data = CSVReader.SplitCsvGrid(Resources.Load<TextAsset>("XLSData/" + filename).text);
        if (keywordRuleset == null)
        {
            Debug.Log("Null Ref on Data Dictionary");
        }
        for (int iRow = 1; iRow < string_data.GetUpperBound(1); iRow++)
        {

            KeywordRule rule = new KeywordRule();
            rule.UID = string_data[0, iRow];
            if (!keywordRuleset.Contains(rule.UID))
            {
                rule.keyword = string_data[1, iRow];
                rule.formula = string_data[2, iRow];
                float.TryParse(string_data[3, iRow], out rule.value);
                int.TryParse(string_data[4, iRow], out rule.reqCount);
                rule.specialType = string_data[5, iRow];
                keywordRuleset.list.Add(rule);
            }
            else
            {
                Debug.Log(string.Format("duplicated rule adding attempts as UID {0}", rule.UID));
            }
        }
        if (!keywordRuleset.ReadRewardLvByTier())
        {
            Debug.LogError("missing reward level by tier rule");
        }
    }
}
