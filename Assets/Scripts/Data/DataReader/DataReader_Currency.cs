﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataReader_Currency : GenericDataReader
{
    public CurrencyDefinitionDictionary currencyDict;

    public override void LoadFromXLS()
    {
        currencyDict.Reset();
        LoadCurrencyDefinitionData("Currency - currency_exporter");
        FinishLoading();
    }
    public void LoadCurrencyDefinitionData(string filename)
    {
        string[,] string_data = CSVReader.SplitCsvGrid(Resources.Load<TextAsset>("XLSData/" + filename).text);
        if (string_data[0, 0] != "UID")
        {
            Debug.LogError("Invalid File (No UID found): " + filename);
            return;
        }
        if (currencyDict == null)
        {
            Debug.Log("Null Ref on Dictionary");
        }
        for (int iRow = 1; iRow < string_data.GetUpperBound(1); iRow++)
        {
            Currency cur = new Currency();
            cur.UID = string_data[0, iRow];
            cur.CurrencyName = string_data[1, iRow];
            cur.artAsset.objectArt = string_data[2, iRow];

            currencyDict.Add(cur.UID, cur);
        }
        currencyDict.DebugCount();
    }
}
