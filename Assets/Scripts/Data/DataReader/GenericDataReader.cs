﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class GenericDataReader : MonoBehaviour
{
    public bool finished;
    //public GameEvent loadingFinished;
    private void OnEnable()
    {
        finished = false;
    }
    public void FinishLoading()
    {
        finished = true;
        GetComponent<dDataHub>().CheckDataLoadingProgress();
        //loadingFinished.Raise();
    }
    public virtual void LoadFromXLS()
    {

    }
    public static List<string> SplitString(string original, char spliter)
    {
        List<string> result = new List<string>();
        if(original != null)
        {
            result = original.Split(spliter).ToList();
            for (int i = 0; i < result.Count; i++)
            {
                string oldString = result[i];
                result[i] = result[i].Replace(" ", "");
                /*if (oldString != result[i])
                {
                    Debug.Log(string.Format("replace ({0}) with ({1})", oldString, result[i]));
                }*/  
            }
            result.Remove("");
        }
        return result;
    }
}
