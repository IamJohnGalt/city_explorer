﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class uiElement : MonoBehaviour
{
    public virtual void UIUpdate()
    {
        Debug.Log(string.Format("vitural func UIUpdate() called"));
    }
    public virtual void UIClose()
    {
        Debug.Log(string.Format("vitural func UIClose() called"));
    }
    public virtual void UIInit()
    {
        Debug.Log(string.Format("vitural func UIInit() called"));
    }
}
