﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "Utility/PopupBox")]
public class dataPopupBox : ScriptableObject
{
    public string title;
    public string desc;
    public Sprite btnSprite;
    public string btnLabel;

    public delegate void btn_hit_func();
    public btn_hit_func btn_popup_confirm;

    public void Reset()
    {
        title = "";
        desc = "";
        btnLabel = "OK";
        btn_popup_confirm = null;
    }
}
