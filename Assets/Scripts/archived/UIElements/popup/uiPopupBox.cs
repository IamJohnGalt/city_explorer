﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class uiPopupBox : uiElement
{
    [Header("data")]
    public dataPopupBox data;
    [Header("content group")]
    [SerializeField] GameObject content;
    [Header("children objects")]
    [SerializeField] TextMeshProUGUI title_tmp;
    [SerializeField] TextMeshProUGUI desc_tmp;
    [SerializeField] TextMeshProUGUI btn_tmp;
    [SerializeField] Image btn_sprite;
    public override void UIInit()
    {
        UIUpdate();
        content.SetActive(true);
    }
    public override void UIUpdate()
    {
        title_tmp.text = data.title;
        title_tmp.gameObject.SetActive(data.title != "");
        desc_tmp.text = data.desc;
        desc_tmp.gameObject.SetActive(data.desc != "");
        btn_tmp.text = data.btnLabel;
        btn_tmp.gameObject.SetActive(data.btnLabel != "");
        btn_sprite.sprite = data.btnSprite;
    }
    public void ConfirmClicked()
    {
        data.btn_popup_confirm?.Invoke();
    }
    public override void UIClose()
    {
        content.SetActive(false);
    }
}
