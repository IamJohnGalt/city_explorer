﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIKeyElements : MonoBehaviour {

    public static UIKeyElements locator = null;

    public Transform Journal;
    public Transform TraitMenu;
    public Transform EquipmentMenu;
    public Transform Coin;
    public Transform Avatar;
    public List<Transform> Equipements;
    public Transform StaminaIcon;
    public Transform StaminaBar;
    public Transform Progress;
    public Transform NextNodeArrival;
    public Transform DangerCounter;
    public Transform CollectibleCounter;
    void Awake()
    {
        if (locator == null)
        {
            locator = this;
        }
        else if (locator != null)
        {
            Destroy(gameObject);
        }
    }
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
