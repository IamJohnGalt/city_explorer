﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UICanvas : MonoBehaviour {
    public static UICanvas canvas = null;

    public Transform WindowLayer;
    public Transform WindowPopupLayer;
    public Transform StaticLayer;
    public Transform ConstantMenuLayer;
    public Transform EffectLayer;
    public Transform ScreenTopLayer;

    //public GameObject topUI;
    // Use this for initialization
    void Awake()
    {
        if (canvas == null)
        {
            canvas = this;
        }
        else if (canvas != null)
        {
            Destroy(gameObject);
        }
    }
    void Start()
    {
    }
	// Update is called once per frame
	void Update () {
		
	}
    /*public void setTopUI(GameObject ui)
    {
        Destroy(topUI);
        topUI = ui;
    }*/
}
