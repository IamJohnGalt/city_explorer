﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class uiScrollableGroup : MonoBehaviour
{
    [Header("Objects")]
    [SerializeField] RectTransform scrollableGroup;
    [SerializeField] RectTransform scrollableSizeReference;
    [SerializeField] float startingPos;
    [SerializeField] bool bottomUp;
    //[SerializeField] bool leftRight;
    public float curTargetPos;

    [Header("Control")]
    [SerializeField] float scrollableDelta;
    [SerializeField] float scrollSpeedMax;
    //[SerializeField] float actualScrollSpeed;

    public void ForceReset()
    {
        scrollableGroup.anchoredPosition = new Vector2(scrollableGroup.anchoredPosition.x, startingPos);
    }
    public void AnimReset()
    {
        curTargetPos = startingPos;
    }
    public void SetTo(float posY)
    {
        scrollableGroup.anchoredPosition = new Vector2(scrollableGroup.anchoredPosition.x, posY);
    }
    private void Awake()
    {
        ForceReset();
        curTargetPos = startingPos;
    }
    private void Update()
    {
        //read mouse wheel
        float scrolling = Input.GetAxis("Mouse ScrollWheel");
        if (scrolling != 0)
        {
            curTargetPos += (scrolling > 0 ? -scrollSpeedMax : scrollSpeedMax);
            if (bottomUp)
            {
                curTargetPos = Mathf.Min(startingPos, Mathf.Max(curTargetPos, -(scrollableSizeReference.sizeDelta.y * scrollableSizeReference.localScale.y + scrollableDelta)));
            }
            else
            {
                curTargetPos = Mathf.Max(startingPos, Mathf.Min(curTargetPos, scrollableSizeReference.sizeDelta.y * scrollableSizeReference.localScale.y + scrollableDelta));
            }
            //Debug.Log(string.Format("current scrolling target pos is {0}", curTargetPos));
        }
        //anim to pos
        if(curTargetPos != scrollableGroup.anchoredPosition.y)
        {
            float stepDelta = Mathf.Min(scrollSpeedMax, Mathf.Abs(scrollableGroup.anchoredPosition.y - curTargetPos)) * (scrollableGroup.anchoredPosition.y < curTargetPos ? 1 : -1);
            //Debug.Log(string.Format("current stepDelta is {0}", stepDelta));
            scrollableGroup.anchoredPosition = new Vector2(scrollableGroup.anchoredPosition.x, scrollableGroup.anchoredPosition.y + stepDelta);
        }
    }
}
