﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class uiScrollableGroupHorizontal : MonoBehaviour
{
    [Header("Objects")]
    [SerializeField] RectTransform scrollableGroup;
    [SerializeField] RectTransform scrollableSizeReference;
    [SerializeField] float startingPos;
    [SerializeField] bool leftRight;
    //[SerializeField] bool leftRight;
    public float curTargetPos;

    [Header("Control")]
    [SerializeField] float scrollableDelta;
    [SerializeField] float scrollSpeedMax;
    //[SerializeField] float actualScrollSpeed;

    public void ForceReset()
    {
        scrollableGroup.anchoredPosition = new Vector2(startingPos, scrollableGroup.anchoredPosition.y);
    }
    public void AnimReset()
    {
        curTargetPos = startingPos;
    }
    public void SetTo(float posX)
    {
        //scrollableGroup.anchoredPosition = new Vector2(posX, scrollableGroup.anchoredPosition.y);
        curTargetPos = posX;
    }
    private void Awake()
    {
        //ForceReset();
        curTargetPos = scrollableGroup.anchoredPosition.x;
    }
    private void Update()
    {
        //read mouse wheel
        float scrolling = Input.GetAxis("Mouse ScrollWheel");
        if (scrolling != 0)
        {
            curTargetPos += (scrolling > 0 ? -scrollSpeedMax : scrollSpeedMax);
            if (leftRight)
            {
                curTargetPos = Mathf.Min(startingPos, Mathf.Max(curTargetPos, -(scrollableSizeReference.sizeDelta.x * scrollableSizeReference.localScale.x + scrollableDelta)));
            }
            else
            {
                curTargetPos = Mathf.Max(startingPos, Mathf.Min(curTargetPos, scrollableSizeReference.sizeDelta.x * scrollableSizeReference.localScale.x + scrollableDelta));
            }
            //Debug.Log(string.Format("current scrolling target pos is {0}", curTargetPos));
        }
        //anim to pos
        if(curTargetPos != scrollableGroup.anchoredPosition.y)
        {
            float stepDelta = Mathf.Min(scrollSpeedMax, Mathf.Abs(scrollableGroup.anchoredPosition.x - curTargetPos)) * (scrollableGroup.anchoredPosition.x < curTargetPos ? 1 : -1);
            //Debug.Log(string.Format("current stepDelta is {0}", stepDelta));
            scrollableGroup.anchoredPosition = new Vector2(scrollableGroup.anchoredPosition.x + stepDelta, scrollableGroup.anchoredPosition.y);
        }
    }
}
