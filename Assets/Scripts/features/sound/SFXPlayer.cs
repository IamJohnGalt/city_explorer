﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXPlayer : MonoBehaviour
{
    public static SFXPlayer mp3;
    [SerializeField] dataSetting setting;
    //[SerializeField] int currentPlayingSourceIndex = 0;

    [Header("Sound Resource")]
    [SerializeField] SoundAsset asset;

    [Header("Children Objects")]
    [SerializeField] List<AudioSource> sources;
    //[SerializeField] AudioMixer mixer;

    void Awake()
    {
        if (mp3 == null)
        {
            mp3 = this;
        }
        else if (mp3 != null)
        {
            Destroy(gameObject);
        }
        //DontDestroyOnLoad(hub);
        if (PlayerPrefs.HasKey("SFXVol"))
        {
            // set the volume to saved volume
            setting.sfxVolume = PlayerPrefs.GetFloat("SFXVol");
        }
        else
        {
            setting.sfxVolume = 0.5f;
            PlayerPrefs.SetFloat("SFXVol", setting.musicVolume);
        }
        UpdateVolume();
    }
    public void UpdateVolume()
    {
        for (int i = 0; i < sources.Count; i++)
        {
            sources[i].volume = dConstants.Sound.MASTER_SFX_VOLUME * setting.sfxVolume;
        }
        PlayerPrefs.SetFloat("SFXVol", setting.sfxVolume);
    }
    public void PlaySFX(string sfxName)
    {
        //Debug.Log(string.Format("SFX ({0}) playing", sfxName));
        for(int i = 0; i < sources.Count; i++)
        {
            if (!sources[i].isPlaying)
            {
                sources[i].PlayOneShot(asset.GetAsset(sfxName));
                return;
            }
        }
        Debug.LogError(string.Format("unable to play SFX ({0}) since all source are in use", sfxName));
    }
    public void StopAllSFX()
    {
        for (int i = 0; i < sources.Count; i++)
        {
            sources[i].Stop();
        }
    }
}
