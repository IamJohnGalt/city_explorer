﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using DG.Tweening;

public class BackgroundMusicPlayer : MonoBehaviour
{
    public static BackgroundMusicPlayer mp3;

    [SerializeField] int currentPlayingSourceIndex = 0;
    [SerializeField] dataSetting setting;

    [Header("Sound Resource")]
    [SerializeField] SoundAsset asset;

    [Header("Children Objects")]
    [SerializeField] List<AudioSource> sources;
    [SerializeField] AudioMixer mixer;

    void Awake()
    {
        if (mp3 == null)
        {
            mp3 = this;
        }
        else if (mp3 != null)
        {
            Destroy(gameObject);
        }
        if (PlayerPrefs.HasKey("MusicVol"))
        {
            // set the volume to saved volume
            setting.musicVolume = PlayerPrefs.GetFloat("MusicVol");
        }
        else
        {
            setting.musicVolume = 0.5f;
            PlayerPrefs.SetFloat("MusicVol", setting.musicVolume);
        }
        UpdateVolume();
    }
    public void UpdateVolume()
    {
        sources[currentPlayingSourceIndex].volume = dConstants.Sound.MASTER_BG_VOLUME * setting.musicVolume;
        /*for (int i = 0; i < sources.Count; i++)
        {
            sources[i].volume = dConstants.Sound.MASTER_BG_VOLUME * setting.musicVolume;
        }*/
        PlayerPrefs.SetFloat("MusicVol", setting.musicVolume);
        //PlayMusic("guqin_opening");
    }
    public void PlayMusic(string musicName)
    {
        sources[1 - currentPlayingSourceIndex].clip = asset.GetAsset(musicName);
        //smooth transition
        sources[currentPlayingSourceIndex].DOFade(0f, 3f);
        sources[1 - currentPlayingSourceIndex].DOFade(dConstants.Sound.MASTER_BG_VOLUME * setting.musicVolume, 5f).SetDelay(2f);
        
        sources[1 - currentPlayingSourceIndex].Play();
        currentPlayingSourceIndex = 1 - currentPlayingSourceIndex;
    }
}
