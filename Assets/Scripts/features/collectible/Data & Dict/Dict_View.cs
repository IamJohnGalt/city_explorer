﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Dictionary/View")]
public class Dict_View : GenericDataDict<View>
{
    public override void DebugCount()
    {
        Debug.Log("Total of " + UIDList.Count + " cards are added to current View Dictionary");
    }
    public List<PFEntrywGate> GetPFEntrywGateList()
    {
        List<PFEntrywGate> result = new List<PFEntrywGate>();
        List<View> fullList = GetFullList();
        foreach (View item in fullList)
        {
            result.Add(item);
        }
        return result;
    }
    public List<PFEntrywGate> GetModifiedPFEntrywGateList()
    {
        List<PFEntrywGate> result = new List<PFEntrywGate>();
        List<View> fullList = GetFullList();

        foreach (View item in fullList)
        {
            View modifiedItem = new View(item);
            float modifiedWeight = item.weight;
            //process the deck with weight modified
            if (modifiedItem.all_keywords.Contains("natural"))
            {
                TotalBuffValueSet weightNaturalBVS = ActiveBuffs.controller.RetrieveValueSetByAction("collectible_weight_natural");
                modifiedWeight = ActiveBuffs.ModifyByBVS(modifiedWeight, weightNaturalBVS);
                //Debug.Log(string.Format("a natrual card{0}'s drop rate is increased due to buff at ({1})", modifiedItem.UID, modifiedWeight));
            }
            if (modifiedItem.all_keywords.Contains("animal"))
            {
                TotalBuffValueSet weightAnimalBVS = ActiveBuffs.controller.RetrieveValueSetByAction("collectible_weight_animal");
                modifiedWeight = ActiveBuffs.ModifyByBVS(modifiedWeight, weightAnimalBVS);
                //Debug.Log(string.Format("a animal card{0}'s drop rate is increased due to buff at ({1})", modifiedItem.UID, modifiedWeight));
            }
            if (modifiedItem.all_keywords.Contains("structure"))
            {
                TotalBuffValueSet weightStructureBVS = ActiveBuffs.controller.RetrieveValueSetByAction("collectible_weight_strcuture");
                modifiedWeight = ActiveBuffs.ModifyByBVS(modifiedWeight, weightStructureBVS);
                //Debug.Log(string.Format("a structure card{0}'s drop rate is increased due to buff at ({1})", modifiedItem.UID, modifiedWeight));
            }
            if (modifiedItem.all_keywords.Contains("spring"))
            {
                TotalBuffValueSet weightStructureBVS = ActiveBuffs.controller.RetrieveValueSetByAction("collectible_weight_spring");
                modifiedWeight = ActiveBuffs.ModifyByBVS(modifiedWeight, weightStructureBVS);
            }
            if (modifiedItem.all_keywords.Contains("summer"))
            {
                TotalBuffValueSet weightStructureBVS = ActiveBuffs.controller.RetrieveValueSetByAction("collectible_weight_summer");
                modifiedWeight = ActiveBuffs.ModifyByBVS(modifiedWeight, weightStructureBVS);
            }
            if (modifiedItem.all_keywords.Contains("autumn"))
            {
                TotalBuffValueSet weightStructureBVS = ActiveBuffs.controller.RetrieveValueSetByAction("collectible_weight_autumn");
                modifiedWeight = ActiveBuffs.ModifyByBVS(modifiedWeight, weightStructureBVS);
            }
            if (modifiedItem.all_keywords.Contains("winter"))
            {
                TotalBuffValueSet weightStructureBVS = ActiveBuffs.controller.RetrieveValueSetByAction("collectible_weight_winter");
                modifiedWeight = ActiveBuffs.ModifyByBVS(modifiedWeight, weightStructureBVS);
            }
            for (int i = 0; i < modifiedItem.all_keywords.Count; i++)
            {
                if (dDataHub.hub.allKeywords.KeywordIsGroup(item.all_keywords[i], "geo"))
                {
                    TotalBuffValueSet weightStructureBVS = ActiveBuffs.controller.RetrieveValueSetByAction("collectible_weight_geo");
                    modifiedWeight = ActiveBuffs.ModifyByBVS(modifiedWeight, weightStructureBVS);
                    break;
                }
            }
            modifiedItem.weight = modifiedWeight;
            //added to result
            result.Add(modifiedItem);
        }
        return result;
    }
    public List<string> GetALLViewIcons()
    {
        List<string> result = new List<string>();
        List<View> fullList = GetFullList();
        foreach (View item in fullList)
        {
            if (ActiveBuffs.controller.CheckFeatureGate(item.release))
            {
                result.Add(item.artAsset.objectArt);
            }
        }
        return result;
    }
}
