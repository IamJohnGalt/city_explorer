﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Dictionary/Collectible")]
public class Dict_Collectible : GenericDataDict<Collectible>
{
    public override void DebugCount()
    {
        Debug.Log("Total of " + UIDList.Count + " cards are added to current Collectible Dictionary");
    }
    public List<PFEntrywGate> GetPFEntrywGateList(List<string> UIDList = null)
    {
        List<PFEntrywGate> result = new List<PFEntrywGate>();
        List<Collectible> fullList = GetFullList();
        foreach (Collectible item in fullList)
        {
            if(UIDList != null)
            {
                if (UIDList.Contains(item.UID))
                {
                    result.Add(item);
                }
            }
            else
            {
                result.Add(item);
            }
        }
        return result;
    }
    public List<PFEntrywGate> GetModifiedPFEntrywGateList()
    {
        List<PFEntrywGate> result = new List<PFEntrywGate>();
        List<Collectible> fullList = GetFullList();

        foreach (Collectible item in fullList)
        {
            Collectible modifiedItem = new Collectible(item);
            float modifiedWeight = modifiedItem.weight;
            //process the deck with weight modified
            if (modifiedItem.keywords.Contains("natural"))
            {
                TotalBuffValueSet weightNaturalBVS = ActiveBuffs.controller.RetrieveValueSetByAction("collectible_weight_natural");
                modifiedWeight = ActiveBuffs.ModifyByBVS(modifiedWeight, weightNaturalBVS);
                //Debug.Log(string.Format("a natrual card{0}'s drop rate is increased due to buff", item.UID));
            }
            if (modifiedItem.keywords.Contains("animal"))
            {
                TotalBuffValueSet weightAnimalBVS = ActiveBuffs.controller.RetrieveValueSetByAction("collectible_weight_animal");
                modifiedWeight = ActiveBuffs.ModifyByBVS(modifiedWeight, weightAnimalBVS);
            }
            if (modifiedItem.keywords.Contains("structure"))
            {
                TotalBuffValueSet weightStructureBVS = ActiveBuffs.controller.RetrieveValueSetByAction("collectible_weight_strcuture");
                modifiedWeight = ActiveBuffs.ModifyByBVS(modifiedWeight, weightStructureBVS);
            }
            if (modifiedItem.keywords.Contains("spring"))
            {
                TotalBuffValueSet weightStructureBVS = ActiveBuffs.controller.RetrieveValueSetByAction("collectible_weight_spring");
                modifiedWeight = ActiveBuffs.ModifyByBVS(modifiedWeight, weightStructureBVS);
            }
            if (modifiedItem.keywords.Contains("summer"))
            {
                TotalBuffValueSet weightStructureBVS = ActiveBuffs.controller.RetrieveValueSetByAction("collectible_weight_summer");
                modifiedWeight = ActiveBuffs.ModifyByBVS(modifiedWeight, weightStructureBVS);
            }
            if (modifiedItem.keywords.Contains("autumn"))
            {
                TotalBuffValueSet weightStructureBVS = ActiveBuffs.controller.RetrieveValueSetByAction("collectible_weight_autumn");
                modifiedWeight = ActiveBuffs.ModifyByBVS(modifiedWeight, weightStructureBVS);
            }
            if (modifiedItem.keywords.Contains("winter"))
            {
                TotalBuffValueSet weightStructureBVS = ActiveBuffs.controller.RetrieveValueSetByAction("collectible_weight_winter");
                modifiedWeight = ActiveBuffs.ModifyByBVS(modifiedWeight, weightStructureBVS);
            }
            for (int i = 0; i < modifiedItem.keywords.Count; i++)
            {
                if (dDataHub.hub.allKeywords.KeywordIsGroup(modifiedItem.keywords[i], "geo"))
                {
                    TotalBuffValueSet weightStructureBVS = ActiveBuffs.controller.RetrieveValueSetByAction("collectible_weight_geo");
                    modifiedWeight = ActiveBuffs.ModifyByBVS(modifiedWeight, weightStructureBVS);
                    break;
                }
            }
            modifiedItem.weight = modifiedWeight;
            //added to result
            result.Add(modifiedItem);
        }
        return result;
    }
    public List<PFEntrywGate> GetPFEntrywGateListWithTag(string targetTag)
    {
        List<PFEntrywGate> result = new List<PFEntrywGate>();
        List<Collectible> fullList = GetFullList();
        foreach (Collectible item in fullList)
        {
            if (UIDList != null)
            {
                if (UIDList.Contains(item.UID))
                {
                    result.Add(item);
                }
            }
            else
            {
                if (item.keywords.Contains(targetTag))
                {
                    result.Add(item);
                }
            }
        }
        return result;
    }
}
