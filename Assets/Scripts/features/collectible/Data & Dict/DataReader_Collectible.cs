﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataReader_Collectible : GenericDataReader
{
    public Dict_View viewDict;
    public Dict_Collectible collectibleDict;

    public override void LoadFromXLS()
    {
        viewDict.Reset();
        collectibleDict.Reset();
        LoadViewData("Card - view_exporter");
        LoadCollectibleData("Card - collectible_exporter");
        FinishLoading();
    }
    public void LoadViewData(string filename)
    {
        string[,] string_data = CSVReader.SplitCsvGrid(Resources.Load<TextAsset>("XLSData/" + filename).text);
        if (string_data[0, 0] != "UID")
        {
            Debug.LogError("Invalid File (No UID found): " + filename);
            return;
        }
        if (collectibleDict == null)
        {
            Debug.Log("Null Ref on Dictionary");
        }
        for (int iRow = 1; iRow < string_data.GetUpperBound(1); iRow++)
        {
            View card = new View();
            card.UID = string_data[0, iRow];
            card.title = string_data[1, iRow];
            card.desc = string_data[2, iRow];
            card.artAsset.objectArt = string_data[3, iRow];
            card.release = string_data[4, iRow];
            card.tags = SplitString(string_data[5, iRow], ',');
            int.TryParse(string_data[6, iRow], out card.tier);
            float.TryParse(string_data[7, iRow], out card.weight);
            card.collectibleOptions = SplitString(string_data[8, iRow], ',');
            //int.TryParse(string_data[9, iRow], out card.starValue);
            card.preview_keywords = SplitString(string_data[9, iRow], ',');
            card.all_keywords = SplitString(string_data[10, iRow], ',');
            viewDict.Add(card.UID, card);
        }
        viewDict.DebugCount();
    }
    public void LoadCollectibleData(string filename)
    {
        string[,] string_data = CSVReader.SplitCsvGrid(Resources.Load<TextAsset>("XLSData/" + filename).text);
        if (string_data[0, 0] != "UID")
        {
            Debug.LogError("Invalid File (No UID found): " + filename);
            return;
        }
        if (collectibleDict == null)
        {
            Debug.Log("Null Ref on Dictionary");
        }
        for (int iRow = 1; iRow < string_data.GetUpperBound(1); iRow++)
        {
            Collectible card = new Collectible();
            card.UID = string_data[0, iRow];
            card.title = string_data[1, iRow];
            card.desc = string_data[2, iRow];
            card.artAsset.objectArt = string_data[3, iRow];
            card.release = string_data[4, iRow];
            card.tags = SplitString(string_data[5, iRow], ',');
            int.TryParse(string_data[6, iRow], out card.tier);
            float.TryParse(string_data[7, iRow], out card.weight);
            card.viewSource = string_data[8, iRow];
            int.TryParse(string_data[9, iRow], out card.starValue);
            card.keywords = SplitString(string_data[10, iRow], ',');
            card.narrativeBits = SplitString(string_data[11, iRow], ',');
            collectibleDict.Add(card.UID, card);
        }
        collectibleDict.DebugCount();
    }
}
