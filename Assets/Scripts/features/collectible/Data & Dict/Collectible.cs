﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class View : PFEntrywGate
{
    public CardArt artAsset;
    public List<string> collectibleOptions;
    //public int starValue;
    public List<string> preview_keywords = new List<string>();
    public List<string> all_keywords = new List<string>();
    public View() : base()
    {
        artAsset = new CardArt();
        collectibleOptions = new List<string>();
        //starValue = 0;
        preview_keywords = new List<string>();
        all_keywords = new List<string>();
    }
    public View(View copyItem) : base(copyItem)
    {
        artAsset = new CardArt();
        artAsset.objectArt = copyItem.artAsset.objectArt;
        artAsset.backgroundArt = copyItem.artAsset.backgroundArt;
        collectibleOptions = new List<string>();
        collectibleOptions.AddRange(copyItem.collectibleOptions);
        //starValue = 0;
        preview_keywords = new List<string>();
        preview_keywords.AddRange(copyItem.preview_keywords);
        all_keywords = new List<string>();
        all_keywords.AddRange(copyItem.preview_keywords);
    }
}

public class Collectible : PFEntrywGate
{
    public CardArt artAsset;
    public string viewSource;
    public int starValue;
    public List<string> keywords = new List<string>();
    public List<string> narrativeBits = new List<string>();
    public string GetRandomNarrative
    {
        get
        {
            if (narrativeBits.Count > 0)
            {
                int rng = Random.Range(0, narrativeBits.Count);
                return narrativeBits[rng];
            }
            return "";
        }
    }
    public Collectible() : base()
    {
        artAsset = new CardArt();
        viewSource = "";
        //starValue = 0;
        keywords = new List<string>();
        narrativeBits = new List<string>();
    }
    public Collectible(Collectible copyItem) : base(copyItem)
    {
        artAsset = new CardArt();
        artAsset.objectArt = copyItem.artAsset.objectArt;
        artAsset.backgroundArt = copyItem.artAsset.backgroundArt;
        viewSource = copyItem.viewSource;
        starValue = copyItem.starValue;
        //starValue = 0;
        keywords = new List<string>();
        keywords.AddRange(copyItem.keywords);
        narrativeBits = new List<string>();
        narrativeBits.AddRange(copyItem.narrativeBits);
    }
}
