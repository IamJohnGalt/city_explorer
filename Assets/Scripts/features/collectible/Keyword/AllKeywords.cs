﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Generic/All Keywords")]
public class AllKeywords : ScriptableObject
{
    public List<Keyword> list;

    public Keyword FindKeyword(string word)
    {
        //Keyword result = null;
        foreach(Keyword kw in list)
        {
            if(kw.tagKeyword == word)
            {
                return kw;
            }
        }
        return null;
    }
    public bool KeywordIsGroup(string word, string group)
    {
        return FindKeyword(word).group == group;
    }
    public string GetARandomKeyword()
    {
        int index = Random.Range(0, list.Count);
        return list[index].tagKeyword;
    }
}
