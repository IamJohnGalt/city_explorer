﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Keyword
{
    //public string UID;
    public string tagKeyword;
    public string group;
    public int sortOrder;
}
