﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ParkBuffViewer : MonoBehaviour
{
    [Header("Data")]
    [SerializeField] dataBuffDisplay displayList;
    [SerializeField] dataActiveBuffList currentBuffs;
    [SerializeField] List<string> eligibleBuffUIDs;

    [Header("Event")]
    [SerializeField] GameEvent ParkBuffChanged;

    public void RetrieveAllParkBuffs()
    {
        displayList.buffDisplays.Clear();
        string targetBuffUID = "";
        for(int i=0;i< eligibleBuffUIDs.Count; i++)
        {
            targetBuffUID = eligibleBuffUIDs[i];
            if (!dDataHub.hub.buffDict.Contains(targetBuffUID))
            {
                Debug.LogError(string.Format("BuffDef UID ({0}) is not existed in Buff Def Dict", targetBuffUID));
                return;
            }
            BuffDefinition buffDef = new BuffDefinition(dDataHub.hub.buffDict.GetFromUID(targetBuffUID));
            if (buffDef == null)
            {
                Debug.LogError(string.Format("failed to get BuffDef from UID ({0})", targetBuffUID));
                return;
            }
            for (int j=0;j< currentBuffs.list.Count; j++)
            {
                if(currentBuffs.list[j].sourceUID != "" && currentBuffs.list[j].buffUID == targetBuffUID)
                {
                    //find an eligible buff,start generating a display for it
                    BuffDisplayItem displayItem = new BuffDisplayItem();
                    Buff buffIns = currentBuffs.list[j];
                    //modify for special buff types
                    if(buffDef.UID == "buff_park_danger_base_add" || buffDef.UID == "buff_park_collectible_base_add")
                    {
                        buffDef.formula = "multi";
                    }
                    displayItem.iconName = buffDef.buffIcon;
                    displayItem.operatorName = InterperateOperator(buffDef.formula, buffDef.type, buffIns.buffValue);
                    displayItem.desc = string.Format("{0} {1}{2}{3}", 
                        buffDef.buffName, 
                        InterperateBuffValue(buffDef.formula, buffIns.buffValue), 
                        InterperateDuration(buffIns.providerUID, buffIns.buffDuration), 
                        InterperateSource(buffIns.sourceUID));
                    displayList.buffDisplays.Add(displayItem);
                }
            }
        }
        ParkBuffChanged.Raise();
    }
    string InterperateOperator(string formula, string type, float value)
    {
        if(formula == "add" || formula == "multi")
        {
            if(type == "buff")
            {
                if(value > 0)
                {
                    return "plus_green";
                }
                else if(value < 0)
                {
                    return "minus_red";
                }
                else
                {
                    Debug.LogError(string.Format("invalid buff value as 0", formula));
                }
            }
            else if(type == "penalty")
            {
                if (value > 0)
                {
                    return "plus_red";
                }
                else if (value < 0)
                {
                    return "minus_green";
                }
                else
                {
                    Debug.LogError(string.Format("invalid buff value as 0", formula));
                }
            }
            else
            {
                Debug.LogError(string.Format("invalid buff type ({0}) on operator interpratation", type));
            }
        }
        else if(formula == "override")
        {
            return "";
        }
        else
        {
            Debug.LogError(string.Format("invalid formula type ({0}) on operator interpratation", formula));
        }
        return "";
    }
    string InterperateBuffValue(string formula, float value)
    {
        if (formula == "add")
        {
            if (value > 0)
            {
                return string.Format("+{0}", Mathf.RoundToInt(value));
            }
            else if (value < 0)
            {
                return string.Format("{0}", Mathf.RoundToInt(value));
            }
            else
            {
                Debug.LogError(string.Format("invalid buff value as 0", formula));
            }
        }
        else if (formula == "multi")
        {
            if (value > 0)
            {
                return string.Format("+{0}%", Mathf.RoundToInt(value*100));
            }
            else if (value < 0)
            {
                return string.Format("{0}%", Mathf.RoundToInt(value*100));
            }
            else
            {
                Debug.LogError(string.Format("invalid buff value as 0", formula));
            }
        }
        else if (formula == "override")
        {
            return string.Format("设为{0}", Mathf.RoundToInt(value));
        }
        else
        {
            Debug.LogError(string.Format("invalid formula type ({0}) on operator interpratation", formula));
        }
        return "";
    }
    string InterperateDuration(string scopeUID, int duration)
    {
        if (!dDataHub.hub.scopeDict.Contains(scopeUID))
        {
            return "";
        }
        ScopeDefinition scope = dDataHub.hub.scopeDict.GetFromUID(scopeUID);
        if(scope.displayName == "EMPTY" || duration == -1)
        {
            return "";
        }
        else if (duration == 0)
        {
            Debug.LogError("some buff last for 0 duration which is not expected");
            return "";
        }
        else if(duration > 0)
        {
            return string.Format("，持续{0}个{1}", duration, scope.displayName);
        }
        else
        {
            Debug.LogError(string.Format("invalid duration ({0}) on scope ({1}) interpratation", duration, scopeUID));
        }
        return "";
    }
    string InterperateSource(string SourceUID)
    {
        if (SourceUID == "")
        {
            return "";
        }
        //Debug.Log("SourceUID: " + SourceUID);
        string UIDType = SourceUID.Substring(0, SourceUID.IndexOf('_'));
        string sourceType = "";
        string sourceName = "";
        switch (UIDType)
        {
            case "solarterm":
                sourceType = "节气";
                ParkSolarTerm solar = dDataHub.hub.dParkSolarTerm.GetFromUID(SourceUID);
                sourceName = solar.title;
                break;
            case "park":
                sourceType = "景点";
                Park park = dDataHub.hub.dPark.GetFromUID(SourceUID);
                sourceName = park.title;
                break;
            case "parknode":
                sourceType = "节点";
                ParkNode node = dDataHub.hub.dParkNode.GetFromUID(SourceUID);
                sourceName = node.title;
                break;
            case "parkpath":
                sourceType = "路径";
                ParkPath path = dDataHub.hub.dParkPath.GetFromUID(SourceUID);
                sourceName = path.title;
                break;
            case "parkreward":
                sourceType = "节点";
                ParkReward park_reward = dDataHub.hub.dParkReward.GetFromUID(SourceUID);
                sourceName = park_reward.title;
                break;
            case "parkaction":
                sourceType = "行动";
                ParkPathAction action = dDataHub.hub.dParkAction.GetFromUID(SourceUID);
                sourceName = action.title;
                break;
            case "equipment":
                sourceType = "装备";
                Equipment equ = dDataHub.hub.equipmentDict.GetFromUID(SourceUID);
                sourceName = equ.title;
                break;
            case "trait":
                sourceType = "特质";
                Trait trait = dDataHub.hub.traitDict.GetFromUID(SourceUID);
                sourceName = trait.title;
                break;
            case "playerlevel":
                sourceType = "等级";
                PlayerLevel lv = dDataHub.hub.plevelDict.GetFromUID(SourceUID);
                sourceName = lv.title;
                break;
            case "character":
                sourceType = "人物";
                CharacterStat stat = dDataHub.hub.dCharacterStats.GetFromUID(SourceUID);
                sourceName = stat.title;
                break;
            case "activity":
                sourceType = "城镇";
                ChronicleActivity act = dDataHub.hub.dChronicleActivity.GetFromUID(SourceUID);
                sourceName = act.title;
                break;
            default:
                return "";
        }
        return string.Format("（{0}-{1}）", sourceType, sourceName);
    }
}
