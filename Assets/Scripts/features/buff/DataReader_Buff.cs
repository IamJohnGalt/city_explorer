﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataReader_Buff : GenericDataReader
{
    public BuffDefDictionary buffDefDict;
    public ScopeDefDictionary scopeDefDict;

    public override void LoadFromXLS()
    {
        buffDefDict.Reset();
        scopeDefDict.Reset();
        LoadFeatureGateData("Buffs - feature_gate_exporter");
        LoadBuffDefinitionData("Buffs - buff_exporter");
        LoadScopeDefinitionData("Buffs - scope_exporter");
        FinishLoading();
    }

    void LoadFeatureGateData(string filename)
    {
        string[,] string_data = CSVReader.SplitCsvGrid(Resources.Load<TextAsset>("XLSData/" + filename).text);
        if (string_data[0, 0] != "UID")
        {
            Debug.LogError("Invalid File (No UID found): " + filename);
            return;
        }
        if (buffDefDict == null)
        {
            Debug.Log("Null Ref on Dictionary");
        }
        for (int iRow = 1; iRow < string_data.GetUpperBound(1); iRow++)
        {
            BuffDefinition buffDef = new BuffDefinition();
            buffDef.UID = string_data[0, iRow];
            buffDef.buffName = string_data[1, iRow];
            buffDef.buffIcon = string_data[2, iRow];
            buffDef.type = string_data[3, iRow];
            if(buffDef.type != "gate")
            {
                Debug.LogError(string.Format("this ({0}) is not a feature gate", buffDef.UID));
                break;
            }
            int temp_int = 0;
            int.TryParse(string_data[4, iRow], out temp_int);
            buffDef.exposed = temp_int > 0;
            buffDef.action = string_data[5, iRow];
            buffDef.formula = string_data[6, iRow];
            int.TryParse(string_data[7, iRow], out temp_int);
            buffDef.isDefault = temp_int > 0;
            buffDef.buffDesc = string_data[8, iRow];
            //card.DebugPrint();
            buffDefDict.Add(buffDef.UID, buffDef);
        }
        buffDefDict.DebugCount();
    }
    void LoadBuffDefinitionData(string filename)
    {
        string[,] string_data = CSVReader.SplitCsvGrid(Resources.Load<TextAsset>("XLSData/" + filename).text);
        if (string_data[0, 0] != "UID")
        {
            Debug.LogError("Invalid File (No UID found): " + filename);
            return;
        }
        if (buffDefDict == null)
        {
            Debug.Log("Null Ref on Dictionary");
        }
        for (int iRow = 1; iRow < string_data.GetUpperBound(1); iRow++)
        {
            BuffDefinition buffDef = new BuffDefinition();
            buffDef.UID = string_data[0, iRow];
            buffDef.buffName = string_data[1, iRow];
            buffDef.buffIcon = string_data[2, iRow];
            buffDef.type = string_data[3, iRow];
            if (!(buffDef.type == "buff" || buffDef.type == "penalty"))
            {
                Debug.LogError(string.Format("this ({0}) is not a buff/penalty", buffDef.UID));
                break;
            }
            int temp_int = 0;
            int.TryParse(string_data[4, iRow], out temp_int);
            buffDef.exposed = temp_int > 0;
            buffDef.action = string_data[5, iRow];
            buffDef.formula = string_data[6, iRow];
            int.TryParse(string_data[7, iRow], out temp_int);
            buffDef.isDefault = temp_int > 0;
            buffDef.buffDesc = string_data[8, iRow];
            //card.DebugPrint();
            buffDefDict.Add(buffDef.UID, buffDef);
        }
        buffDefDict.DebugCount();
    }
    void LoadScopeDefinitionData(string filename)
    {
        string[,] string_data = CSVReader.SplitCsvGrid(Resources.Load<TextAsset>("XLSData/" + filename).text);
        if (string_data[0, 0] != "UID")
        {
            Debug.LogError("Invalid File (No UID found): " + filename);
            return;
        }
        if (scopeDefDict == null)
        {
            Debug.Log("Null Ref on Dictionary");
        }
        for (int iRow = 1; iRow < string_data.GetUpperBound(1); iRow++)
        {
            ScopeDefinition scope = new ScopeDefinition();
            scope.UID = string_data[0, iRow];
            scope.displayName = string_data[1, iRow];
            
            scopeDefDict.Add(scope.UID, scope);
        }
        scopeDefDict.DebugCount();
    }
}
