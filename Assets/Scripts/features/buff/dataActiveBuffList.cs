﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Buff
{
    //this should be renmaed to scopeUID;
    public string providerUID;

    public string buffUID;
    public float buffValue;
    public int buffDuration;
    //presentation data
    public string sourceUID;

    public Buff()
    {
        providerUID = "";
        buffUID = "";
        buffValue = 0;
        buffDuration = -1;
        sourceUID = "";
    }

    public Buff(Buff copyBuff)
    {
        providerUID = copyBuff.providerUID;
        buffUID = copyBuff.buffUID;
        buffValue = copyBuff.buffValue;
        buffDuration = copyBuff.buffDuration;
        sourceUID = copyBuff.sourceUID;
    }
}
[CreateAssetMenu(menuName = "Gameplay/Active Buff List")]
public class dataActiveBuffList : ScriptableObject
{
    public List<Buff> list;
}
