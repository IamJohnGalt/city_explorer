﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.ExceptionServices;
using UnityEngine;

[CreateAssetMenu(menuName = "Dictionary/BuffDefinition")]
public class BuffDefDictionary : GenericDataDict<BuffDefinition>
{
    public override void DebugCount()
    {
        Debug.Log("Total of " + UIDList.Count + " buff definitions are added to current Buff Def Dictionary");
    }
    public List<string> RetriveAllDefaultGates()
    {
        List<string> result = new List<string>();
        List<BuffDefinition> fullList = GetFullList();
        foreach (BuffDefinition def in fullList)
        {
            if(def.isDefault && def.type == "gate")
            {
                result.Add(def.UID);
            }
        }
        return result;
    }
}
