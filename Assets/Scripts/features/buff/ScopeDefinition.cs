﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ScopeDefinition
{
    public string UID;
    public string displayName;

    public ScopeDefinition()
    {
        UID = "";
        displayName = "";
    }
}
