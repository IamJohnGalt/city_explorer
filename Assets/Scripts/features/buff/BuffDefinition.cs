﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BuffDefinition
{
    public string UID;
    public string type;
    public bool exposed;
    public string action;
    public string formula;
    public string buffName;
    public string buffIcon;
    public bool isDefault;
    public string buffDesc;

    public BuffDefinition()
    {
        UID = "";
        type = "";
        exposed = false;
        action = "";
        formula = "";
        buffName = "";
        buffIcon = "";
        isDefault = false;
        buffDesc = "";
    }
    public BuffDefinition(BuffDefinition copyDef)
    {
        UID = copyDef.UID;
        type = copyDef.type;
        exposed = copyDef.exposed;
        action = copyDef.action;
        formula = copyDef.formula;
        buffName = copyDef.buffName;
        buffIcon = copyDef.buffIcon;
        isDefault = copyDef.isDefault;
        buffDesc = copyDef.buffDesc;
    }
}
