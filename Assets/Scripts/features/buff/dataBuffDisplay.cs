﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BuffDisplayItem
{
    public string iconName;
    public string operatorName;
    public string desc;
    public BuffDisplayItem()
    {
        iconName = "";
        operatorName = "";
        desc = "";
    }
}

[CreateAssetMenu(menuName = "Gameplay/Buff Display List")]
public class dataBuffDisplay : ScriptableObject
{
    public List<BuffDisplayItem> buffDisplays;
}
