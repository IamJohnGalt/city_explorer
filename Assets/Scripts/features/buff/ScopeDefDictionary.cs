﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Dictionary/ScopeDefinition")]
public class ScopeDefDictionary : GenericDataDict<ScopeDefinition>
{
    public override void DebugCount()
    {
        Debug.Log("Total of " + UIDList.Count + " buff scope definitions are added to current Scope Def Dictionary");
    }
}
