﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
//using UnityScript.Scripting.Pipeline;

[System.Serializable]
public class TotalBuffValueSet
{
    public bool override_enabled = false;
    public float override_value = 0;
    public float add_value = 0;
    public float multi_value = 0;

    public TotalBuffValueSet()
    {
        override_enabled = false;
        override_value = 0;
        add_value = 0;
        multi_value = 0;
    }
}
public class ActiveBuffs : MonoBehaviour
{
    public static ActiveBuffs controller;
    void Awake()
    {
        if (controller == null)
        {
            controller = this;
        }
        else if (controller != null)
        {
            Destroy(gameObject);
        }
    }
    [Header("Data")]
    [SerializeField] BuffDefDictionary buffDict;
    [SerializeField] dataActiveBuffList buffList;

    [Header("Children Sciprts")]
    [SerializeField] ParkBuffViewer parkViewer;
    
    public void BuffInit()
    {
        buffList.list.Clear();
        //activate all default feature gates
        List<string> gates2Add = dDataHub.hub.buffDict.RetriveAllDefaultGates();
        for(int i = 0; i < gates2Add.Count; i++)
        {
            ToggleFeatureGate(gates2Add[i], true);
        }
    }
    public void ActivateBuff(string sourceUID, string providerUID, string buffUID, float buffValue, int buffDuration = -1)
    {
        if (buffUID.Contains("feature_gate") && !providerUID.Contains("park"))
        {
            ToggleFeatureGate(buffUID, buffValue > 0);
        }
        else
        {
            Buff newBuff = new Buff();
            newBuff.providerUID = providerUID;
            newBuff.buffUID = buffUID;
            newBuff.buffValue = buffValue;
            newBuff.buffDuration = buffDuration;
            newBuff.sourceUID = sourceUID;
            if (!InstantBuffApplier(newBuff))
            {
                buffList.list.Add(newBuff);
            }
            else
            {
                //Debug.Log(string.Format("instant Buff {0} applied with a value of {1}", newBuff.buffUID, newBuff.buffValue));
            }
        }
        UpdateAllViewers();
    }
    public void DeactivateFrom(string removeingProvider, string buffUID = "")
    {
        List<Buff> newActiveBuffs = new List<Buff>();
        foreach (Buff bf in buffList.list)
        {
            if(bf.providerUID != removeingProvider)
            {
                newActiveBuffs.Add(bf);
            }
            else
            {
                if (buffUID != "")
                {
                    if (bf.buffUID != buffUID)
                    {
                        newActiveBuffs.Add(bf);
                    }
                }
            }
        }
        buffList.list = newActiveBuffs;
        UpdateAllViewers();
    }
    public void TimerCountByProvider(string providerUID, int countDown = 1)
    {
        List<Buff> newActiveBuffs = new List<Buff>();
        foreach (Buff bf in buffList.list)
        {
            Buff newBf = new Buff(bf);
            if (newBf.providerUID != providerUID)
            {
                newActiveBuffs.Add(newBf);
            }
            else
            {
                if (newBf.buffDuration > 0)
                {
                    if (newBf.buffDuration > countDown)
                    {
                        newBf.buffDuration = newBf.buffDuration - countDown;
                        newActiveBuffs.Add(newBf);
                    }
                }
                else
                {
                    newActiveBuffs.Add(newBf);
                }
            }
        }
        buffList.list = newActiveBuffs;
        UpdateAllViewers();
    }
    public float RetrieveBuffValue(string targetBuffUID)
    {
        float totalValue = 0;
        foreach (Buff bf in buffList.list)
        {
            if(bf.buffUID == targetBuffUID)
            {
                totalValue += bf.buffValue;
            }
        }
        return totalValue;
    }
    public bool CheckBuff(string targetBuffUID)
    {
        foreach (Buff bf in buffList.list)
        {
            if (bf.buffUID == targetBuffUID)
            {
                return true;
            }
        }
        return false;
    }
    public TotalBuffValueSet RetrieveValueSetByAction(string actionUID)
    {
        TotalBuffValueSet bfValueSet = new TotalBuffValueSet();
        List<string> relatedBuffUIDs = (from bfUID in buffDict.UIDList where buffDict.GetFromUID(bfUID).action == actionUID select bfUID).ToList();
        foreach(string bfUID in relatedBuffUIDs)
        {
            switch (buffDict.GetFromUID(bfUID).formula)
            {
                case "override":
                    bfValueSet.override_enabled = CheckBuff(bfUID);
                    bfValueSet.override_value = RetrieveBuffValue(bfUID);
                    break;
                case "add":
                    bfValueSet.add_value += RetrieveBuffValue(bfUID);
                    break;
                case "multi":
                    bfValueSet.multi_value += RetrieveBuffValue(bfUID);
                    break;
                default:
                    Debug.LogError(string.Format("unknown formula type on buff UID: {0}", bfUID));
                    break;
            }
        }
        return bfValueSet;
    }
    public void ToggleFeatureGate(string featureGateUID, bool isOn)
    {
        bool existed = false;
        foreach (Buff bf in buffList.list)
        {
            if (bf.buffUID == featureGateUID)
            {
                bf.buffValue = isOn ? 1 : 0;
                existed = true;
            }
        }
        if (!existed)
        {
            Buff newBuff = new Buff();
            newBuff.providerUID = "feature_gate";
            newBuff.buffUID = featureGateUID;
            newBuff.buffValue = isOn ? 1 : 0;
            newBuff.buffDuration = -1;
            buffList.list.Add(newBuff);
        }
        //UpdateAllViewers();
    }
    public bool CheckFeatureGate(string featureGateUID)
    {
        if(featureGateUID == "" || featureGateUID == null)
        {
            return true;
        }
        else
        {
            foreach (Buff bf in buffList.list)
            {
                if (bf.buffUID == featureGateUID)
                {
                    return bf.buffValue > 0;
                }
            }
            return false;
        }
    }
    bool InstantBuffApplier(Buff buff)
    {
        switch (buff.buffUID)
        {
            case "buff_instant_currency_coin_add":
                if ((int)buff.buffValue > 0)
                {
                    CurrencyManagement.wallet.AddCurrency("currency_coin", (int)buff.buffValue);
                }
                else
                {
                    CurrencyManagement.wallet.RemoveCurrency("currency_coin", -(int)buff.buffValue);
                }
                return true;
            case "buff_instant_currency_fame_add":
                if ((int)buff.buffValue > 0)
                {
                    CurrencyManagement.wallet.AddCurrency("currency_fame", (int)buff.buffValue);
                }
                else
                {
                    CurrencyManagement.wallet.RemoveCurrency("currency_fame", -(int)buff.buffValue);
                }
                return true;
            case "buff_instant_currency_trait_add":
                if ((int)buff.buffValue > 0)
                {
                    CurrencyManagement.wallet.AddCurrency("currency_trait", (int)buff.buffValue);
                }
                else
                {
                    CurrencyManagement.wallet.RemoveCurrency("currency_trait", -(int)buff.buffValue);
                }
                return true;
            case "buff_instant_currency_xp_add":
                if ((int)buff.buffValue > 0)
                {
                    CurrencyManagement.wallet.AddCurrency("currency_xp", (int)buff.buffValue);
                }
                else
                {
                    CurrencyManagement.wallet.RemoveCurrency("currency_xp", -(int)buff.buffValue);
                }
                return true;
            case "buff_instant_currency_star_add":
                if ((int)buff.buffValue > 0)
                {
                    CurrencyManagement.wallet.AddCurrency("currency_star", (int)buff.buffValue);
                }
                else
                {
                    CurrencyManagement.wallet.RemoveCurrency("currency_star", -(int)buff.buffValue);
                }
                return true;
            case "buff_instant_equipment_add":
                Debug.LogError(string.Format("deprecated buff buff_instant_equipment_add is called"));
                return true;
            case "buff_instant_equipment_fixed_tier_add":
                Debug.LogError(string.Format("deprecated buff buff_instant_equipment_fixed_tier_add is called"));
                return true;
            case "buff_instant_equipment_acquire_add":
                if((int)buff.buffValue > 1)
                {
                    EquipmentManagement.equipment.AcquireUpgradedRandomEquiupment();
                }
                else
                {
                    EquipmentManagement.equipment.AcquireNewRandomEquiupment();
                }
                return true;
            case "buff_instant_equipment_improve_add":
                EquipmentManagement.equipment.ImproveRandomEquipment();
                return true;
            case "buff_instant_equipment_upgrade_add":
                EquipmentManagement.equipment.UpgradeRandomEquiupment();
                return true;
            case "buff_instant_random_trait_add":
                TraitManagement.controller.AddRandomTraitByTier((int)buff.buffValue);
                return true;
            case "buff_instant_currency_token_charac2_add":
                CharacterAcquisition.controller.AcquisitionCheck_Character2();
                return true;
            case "buff_instant_collectible_add":
                if ((int)buff.buffValue > 0)
                {
                    Debug.LogError("unfinished function to add random collectible");
                }
                return true;
            case "buff_instant_return_month_add":
                CalendarManagement.calendar.AddTravelMonth((int)buff.buffValue);
                return true;
            case "buff_instant_season_removal_override":
                CalendarManagement.calendar.DisableSeasons();
                return true;
            default:
                return false;
        }
    }
    void UpdateAllViewers()
    {
        parkViewer.RetrieveAllParkBuffs();
    }

    static public int ModifyByBVS(int input, TotalBuffValueSet bvs)
    {
        int output;
        output = Mathf.Max(0, Mathf.CeilToInt(bvs.override_enabled ?
            bvs.override_value :
            ((input + bvs.add_value) * (1 + bvs.multi_value))));
        return output;
    }
    static public float ModifyByBVS(float input, TotalBuffValueSet bvs)
    {
        float output;
        output = Mathf.Max(0, bvs.override_enabled ?
            bvs.override_value :
            ((input + bvs.add_value) * (1 + bvs.multi_value)));
        return output;
    }
    static public int ModifyByBVSNegative(int input, TotalBuffValueSet bvs)
    {
        int output;
        output = Mathf.CeilToInt(bvs.override_enabled ?
            bvs.override_value :
            ((input + bvs.add_value) * (1 + bvs.multi_value)));
        return output;
    }
    static public float ModifyByBVSNegative(float input, TotalBuffValueSet bvs)
    {
        float output;
        output = bvs.override_enabled ?
            bvs.override_value :
            ((input + bvs.add_value) * (1 + bvs.multi_value));
        return output;
    }
}
