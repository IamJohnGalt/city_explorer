﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dStatistics : MonoBehaviour {

    // this class should be migrated to actionMonitor
    public static dStatistics stats;
    void Awake()
    {
        if (stats == null)
        {
            stats = this;
        }
        else if (stats != null)
        {
            Destroy(gameObject);
        }
    }

    public dataStats data;
    [SerializeField] GameEvent StatsLoaded;
    public void StartNewSession()
    {
        data.Reset();
    }
    public void LoadPlayStats()
    {
        //load from save manager
        int.TryParse(SaveManager.controller.Inquire(string.Format("stats_{0}", "totalFame")), out data.totalFame);
        int.TryParse(SaveManager.controller.Inquire(string.Format("stats_{0}", "totalSession")), out data.totalSession);
        int.TryParse(SaveManager.controller.Inquire(string.Format("stats_{0}", "totalJournal")), out data.totalJournal);

        StatsLoaded.Raise();
    }
    public void SavePlayStats()
    {
        //add in session to total
        data.totalFame += CurrencyManagement.wallet.CheckCurrency("currency_fame");
        data.totalSession += 1;
        data.totalJournal += data.inSessionJournalLogs.Count;
        //save to save manager
        SaveManager.controller.Insert(string.Format("stats_{0}", "totalFame"), data.totalFame.ToString());
        SaveManager.controller.Insert(string.Format("stats_{0}", "totalSession"), data.totalSession.ToString());
        SaveManager.controller.Insert(string.Format("stats_{0}", "totalJournal"), data.totalJournal.ToString());
    }
    public void JournalSubmitted(string journalUID, int totalStars)
    {
        //data.inSessionJournal += 1;
        JournalLog log = new JournalLog();
        log.UID = journalUID;
        log.totalStars = totalStars;
        log.submittedDate = CalendarManagement.calendar.GetCurDate();
        data.inSessionJournalLogs.Add(log);
    }
    public int GetJournalCount()
    {
        return data.totalJournal + data.inSessionJournalLogs.Count;
    }
    public void PatkVisited(string parkUID)
    {
        //data.inSessionJournal += 1;
        data.inSessionParks.Add(parkUID);
    }
}
