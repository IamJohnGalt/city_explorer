﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Utility/Game Stats")]
public class dataStats : ScriptableObject
{
    [Header("Perm Stats")]
    public int totalJournal;
    public int totalSession;
    public int totalFame;

    [Header("In-Session Stats")]
    //public int inSessionFame;
    public List<JournalLog> inSessionJournalLogs;
    public List<string> inSessionParks;

    public void Reset()
    {
        //inSessionFame = 0;
        inSessionJournalLogs = new List<JournalLog>();
        inSessionParks = new List<string>();
    }
}
