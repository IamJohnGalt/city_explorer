﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Utility/Loading Page")]
public class dataLoadingPage : ScriptableObject
{
    public string flavorText;
    public float progression;

}
