﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class uiLoadingPage : uiElement
{
    [Header("Data")]
    public dataLoadingPage data;

    [Header("Children Objects")]
    public GameObject frame;
    public TextMeshProUGUI flavorText;
    public Image progressBar;

    private void Awake()
    {
        frame.SetActive(true);
    }
    override public void UIInit()
    {
        
    }

    override public void UIUpdate()
    {
        flavorText.text = data.flavorText;
        progressBar.fillAmount = data.progression;

        if(data.progression == 1f)
        {
            frame.SetActive(false);
        }
    }
}
