﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class prop_cloud : MonoBehaviour
{
    [SerializeField] Image cloud;
    [SerializeField] SpriteAsset cloudSpriteLib;
    public void Init()
    {
        Sprite sprt = cloudSpriteLib.Assets[Random.Range(0, cloudSpriteLib.Assets.Count)];
        cloud.sprite = sprt;
    }
}
