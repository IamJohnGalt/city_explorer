﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class cloud_generator : MonoBehaviour
{
    [SerializeField] GameObject cloud_group;
    [SerializeField] prop_cloud cloud_template_left;
    [SerializeField] prop_cloud cloud_template_right;
    [SerializeField] List<GameObject> cloudsOnDuty;

    static float floatingDuration = 50f;
    static float maxHeight = 1080f;
    static float maxWidth = 2400f;
    static int maxClouds = 5;

    public void InitClouds()
    {
        for (int i = 0; i < maxClouds; i++)
        {
            GenerateCloud(1f + i * Random.Range(10f, 20f));
        }
    }
    public void GenerateCloud(float delay = 0f)
    {
        if(cloudsOnDuty.Count >= maxClouds)
        {
            return;
        }
        if(delay == 0f)
        {
            delay = Random.Range(floatingDuration * 0.2f, floatingDuration * 0.4f);
        }
        int rng = Random.Range(0, 2);
        if(rng == 0)
        {
            GameObject newCloud = Instantiate(cloud_template_left.gameObject, cloud_group.transform);
            RectTransform rect = newCloud.GetComponent<RectTransform>();
            //rect.anchorMin= new Vector2(1f, 0f);
            //rect.anchorMax = new Vector2(1f, 0f);
            //rect.pivot = new Vector2(0f, 0f);
            rect.anchoredPosition = new Vector2(200f, Random.Range(0.1f, 0.9f) * maxHeight);
            newCloud.GetComponent<prop_cloud>().Init();
            rect.DOAnchorPosX(-maxWidth, Random.Range(0.8f, 1.2f) * floatingDuration)
                .SetEase(Ease.Linear)
                .SetRelative(true)
                .SetDelay(delay)
                //.OnComplete(() => GenerateCloud())
                .OnComplete(() => RemoveCloud(newCloud));
            AddCloud(newCloud);
        }
        else if(rng == 1)
        {
            GameObject newCloud = Instantiate(cloud_template_right.gameObject, cloud_group.transform);
            RectTransform rect = newCloud.GetComponent<RectTransform>();
            //rect.anchorMin = new Vector2(0f, 0f);
            //rect.anchorMax = new Vector2(0f, 0f);
            //rect.pivot = new Vector2(1f, 0f);
            rect.anchoredPosition = new Vector2(-200f, Random.Range(0.1f, 0.9f) * maxHeight);
            newCloud.GetComponent<prop_cloud>().Init();
            rect.DOAnchorPosX(maxWidth, Random.Range(0.8f, 1.2f) * floatingDuration)
                .SetEase(Ease.Linear)
                .SetRelative(true)
                .SetDelay(delay)
                //.OnComplete(() => GenerateCloud())
                .OnComplete(()=> RemoveCloud(newCloud));
            AddCloud(newCloud);
        }
    }
    public void AddCloud(GameObject cloud)
    {
        cloudsOnDuty.Add(cloud);
    }
    public void RemoveCloud(GameObject cloud)
    {
        cloudsOnDuty.Remove(cloud);
        Destroy(cloud);
        GenerateCloud();
    }
}
