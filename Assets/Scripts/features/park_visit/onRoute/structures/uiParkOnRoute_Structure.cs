﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class uiParkOnRoute_Structure : MonoBehaviour
{
    public List<uiParkActionToken> stay_tokens;
    public List<uiParkActionToken> forward_tokens;
    public List<uiParkActionToken> danger_tokens;
    public List<uiParkActionToken> major_danger_tokens;
    public List<GameObject> stay_paths;
    public List<GameObject> forward_paths;
    public delegate void btn_hit_func(int index);
    //public btn_hit_func ref_stay_hit;
    //public btn_hit_func ref_forward_hit;
    public uiParkOnRoute_Character character;

    /*
    public void btn_stay_hit(int index)
    {
        ref_stay_hit?.Invoke(index);
    }
    public void btn_forward_hit(int index)
    {
        ref_forward_hit?.Invoke(index);
    }*/

}
