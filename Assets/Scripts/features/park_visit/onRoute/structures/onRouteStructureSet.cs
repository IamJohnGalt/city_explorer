﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class onRouteStructureOption
{
    public string structureUID;
    public GameObject pfb_structure = null;
    //public List<string> terrains = new List<string>();
    //public string releaseGate = "";
}
[CreateAssetMenu(menuName = "Park/OnRouteUI/Structure Set")]
public class onRouteStructureSet : ScriptableObject
{
    public List<onRouteStructureOption> structures;

    public onRouteStructureOption DrawValidStructure(string excludingUID = "")
    {
        int loopTimes = 0;
        int rngIndex = 0;
        while (loopTimes < dConstants.MAX_LOOP_TIMES)
        {
            rngIndex = Random.Range(0, structures.Count);
            if(structures[rngIndex].structureUID != excludingUID)
            {
                return structures[rngIndex];
            }
        }
        Debug.LogError("can't find a valid structure option, default structure used");
        return structures[0];
    }
}
