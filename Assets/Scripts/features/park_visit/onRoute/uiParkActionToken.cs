﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class uiParkActionToken : uipfb_atom
{
    public enum ActionTokenStatus { playable, played, blocked, hidden};

    public delegate void btn_hit_func(int index);
    public btn_hit_func ref_action_hit_func;
    public btn_hit_func ref_action_hover_on;

    public delegate void btn_hoveroff_func();
    public btn_hoveroff_func ref_action_hover_off;

    [Header("Data")]
    public string CurrentaActionUID;
    public ActionTokenStatus status;
    public bool collectibleTriggered;
    public bool dangerTriggered;

    [Header("Transportation")]
    public int stop_main_index;
    public List<Vector2> stop_branch;

    [Header("Children Objects")]
    [SerializeField] GameObject content;
    [SerializeField] GameObject group_tags;
    [SerializeField] GameObject interactiveZone;
    [SerializeField] Image frame;
    [SerializeField] Image art;
    [SerializeField] Image mask;
    public ui_icon_text_chance danger_base;
    public ui_icon_text_chance collectible_base;
    [SerializeField] GameObject desc_group;
    [SerializeField] GameObject desc_placeholder;
    [SerializeField] ui_text desc_text;
    [SerializeField] action_number stamina;
    [SerializeField] action_number progress;
    [SerializeField] ui_text title;
    [SerializeField] ui_icon sign;
    [SerializeField] ui_icon forward_arrow;
    [SerializeField] GameObject blocker;
    [SerializeField] GameObject buffTooltipArea;
    [SerializeField] ui_icon tooltipEntry;
    //[SerializeField] ui_icon pin;

    [Header("Asset")]
    [SerializeField] SpriteAsset typeTagAssets;

    private bool isForwardAction = false;
    private Sequence anim_arrow;

    private static float ANIM_PLAY_DURATION = 0.3f;

    public void SetTokenByUID(string tokenUID)
    {
        if (!dDataHub.hub.dParkAction.Contains(tokenUID))
        {
            SetVisible(false);
            Debug.LogError(string.Format("token({0}) is set to invisible due to missing token data by its UID({1})", gameObject.name, tokenUID));
            return;
        }
        CurrentaActionUID = tokenUID;
        ParkPathAction action = dDataHub.hub.dParkAction.GetFromUID(tokenUID);
        isForwardAction = action.forward;
        //set art
        art.sprite = uAssetReader.controller.GetByName(action.artAsset.objectArt);
        float adjustedScale = 1f;
        if(action.collectibleTrigger || action.dangerTrigger)
        {
            adjustedScale = 0.7f;
        }
        else if(action.desc != "")
        {
            adjustedScale = 0.8f;
        }
        art.transform.localScale = Vector3.one * adjustedScale;
        //set stamina & progress
        stamina.SetNumber(ReadStaminaRange(action.modifiedStaminaRange), action.modifiedStaminaRange.maxInt - action.staminaRange.maxInt);
        progress.SetNumber(ReadProgressRange(action.modifiedProgressRange), action.modifiedProgressRange.maxInt - action.progressRange.maxInt);
        stamina.SetColor(CharacterManagement.controller.GetCurrentCharacter().mainColor);
        stamina.SetWaterMark("dynamic_current_stamina_shape");

        //danger_trigger.SetVisible(action.dangerTrigger);
        if (action.dangerTrigger)
        {
            danger_base.majorStr.SetText(string.Format("{0}%", Mathf.RoundToInt(action.modifiedDangerBase * 100)));
            //Debug.Log(string.Format("danger delta: {0}", Mathf.RoundToInt(action.modifiedDangerBase * 100) - Mathf.RoundToInt(action.dangerBase * 100)));
            danger_base.indicator.SetIndicator(false, Mathf.RoundToInt(action.modifiedDangerBase * 100) - Mathf.RoundToInt(action.dangerBase*100));
            danger_base.gameObject.SetActive(true);
        }
        else
        {
            danger_base.gameObject.SetActive(false);
        }
        if (action.collectibleTrigger)
        {
            collectible_base.majorStr.SetText(string.Format("{0}%", Mathf.RoundToInt(action.modifiedCollectibleBase * 100)));
            collectible_base.indicator.SetIndicator(true, Mathf.RoundToInt(action.modifiedCollectibleBase * 100) - Mathf.RoundToInt(action.collectibleBase * 100));
            //Debug.Log(string.Format("collectible delta: {0}", Mathf.RoundToInt(action.modifiedCollectibleBase * 100) - Mathf.RoundToInt(action.collectibleBase * 100)));
            collectible_base.gameObject.SetActive(true);
        }
        else
        {
            collectible_base.gameObject.SetActive(false);
        }
        title.SetText(action.title);
        //desc_text.gameObject.SetActive(action.desc != "");
        desc_group.gameObject.SetActive(action.desc != "");
        desc_placeholder.gameObject.SetActive(action.desc != "");
        desc_text.SetText(string.Format(action.desc));
        interactiveZone.SetActive(status == ActionTokenStatus.playable);
        if (status != ActionTokenStatus.played)
        {
            ResetFade();
        }
        //sign
        if (action.signAsset.objectArt != "EMPTY")
        {
            sign.SetIcon(action.signAsset.objectArt);
            sign.gameObject.SetActive(true);
        }
        else
        {
            sign.gameObject.SetActive(false);
        }
        

        blocker.SetActive(status == ActionTokenStatus.blocked);
        //tooltip section
        tooltipEntry.gameObject.SetActive(HasAvailableBuffTootips());
        ClearAllBuffTooltips();

        //SetType(action.forward);
        HoverOff();
        
        gameObject.SetActive(true);
    }
    public void SetStatus(bool played, bool inDanger)
    {
        
        if(played)
        {
            status = ActionTokenStatus.played;
        }
        else if (inDanger)
        {
            status = ActionTokenStatus.blocked;
        }
        else
        {
            status = ActionTokenStatus.playable;
        }
        blocker.SetActive(status == ActionTokenStatus.blocked);
    }
    public void SetHidden()
    {
        status = ActionTokenStatus.hidden;
        SetVisible(false);
    }
    string ReadStaminaRange(intRange range)
    {
        string result = "";
        //Check if it is 0
        if (range.isZero() || range.minInt < 0 || range.maxInt < 0)
        {
            return result;
        }
        if (range.minInt == range.maxInt)
        {
            result += Mathf.Abs(range.minInt).ToString();
        }
        else
        {
            result += string.Format("{0}~{1}", Mathf.Abs(range.minInt), Mathf.Abs(range.maxInt));
        }
        return result;
    }
    string ReadProgressRange(intRange range)
    {
        string result = "";
        if (range.isZero())
        {
            return result;
        }
        if (range.minInt < 0)
        {
            result = "-";
        }
        else
        {
            result = "";
        }
        if (range.minInt == range.maxInt)
        {
            result += Mathf.Abs(range.minInt).ToString();
        }
        else
        {
            result += string.Format("{0}~{1}", Mathf.Abs(range.minInt), Mathf.Abs(range.maxInt));
        }
        return result;
    }
    string ReadDangerChance(float actionbDangerBase)
    {
        float modifiedDangerChance = 0;
        TotalBuffValueSet bvs = ActiveBuffs.controller.RetrieveValueSetByAction("park_danger_base");
        modifiedDangerChance = Mathf.Min(1, Mathf.Max(0, ActiveBuffs.ModifyByBVS(actionbDangerBase, bvs)));
        return string.Format("{0}%", Mathf.RoundToInt(modifiedDangerChance * 100));
    }
    string ReadCollectibleChance(float actionbCollectibleBase)
    {
        float modifiedCollectibleChance = 0;
        TotalBuffValueSet bvs = ActiveBuffs.controller.RetrieveValueSetByAction("park_collectible_base");
        modifiedCollectibleChance = Mathf.Min(1, Mathf.Max(0, ActiveBuffs.ModifyByBVS(actionbCollectibleBase, bvs)));
        return string.Format("{0}%", Mathf.RoundToInt(modifiedCollectibleChance * 100));
    }
    public void OnTokenClicked(int index)
    {
        switch (status)
        {
            case ActionTokenStatus.playable:
                ref_action_hit_func?.Invoke(index);
                status = ActionTokenStatus.played;
                SFXPlayer.mp3.PlaySFX("button_click");
                HoverOff();
                AnimSet_PlayAction();
                break;
            case ActionTokenStatus.played:
                Anim_Shake();
                break;
            case ActionTokenStatus.blocked:
                Anim_BlockerUp();
                break;
            default:
                break;
        }
        ClearAllBuffTooltips();
    }
    public void OnTokenHoverOn(int index)
    {
        switch (status)
        {
            case ActionTokenStatus.playable:
                ref_action_hover_on?.Invoke(index);
                break;
            default:
                break;
        }
    }
    public void AnimSet_PlayAction()
    {
        Sequence seq = DOTween.Sequence();
        seq.AppendCallback(Anim_MoveDown);
        seq.AppendCallback(Anim_FadeOut);
        seq.AppendInterval(0.5f);
        if (danger_base.majorStr.str.text != "0%")
        {
            danger_base.dice.Anim_Roll();
        }
        if (collectible_base.majorStr.str.text != "0%")
        {
            collectible_base.dice.Anim_Roll();
        }
        //move to uiParkOnRoute
        /*if (danger_base.gameObject.activeSelf && danger_base.majorStr.str.text != "0%")
        {
            seq.AppendCallback(() => danger_base.icon.Anim_Check());
            seq.AppendInterval(0.5f);
        }
        if (collectible_base.gameObject.activeSelf && collectible_base.majorStr.str.text != "0%")
        {
            seq.AppendCallback(() => collectible_base.icon.Anim_Check());
            seq.AppendInterval(0.5f);
        }
        */
        //ClearBuffTooltips();
    }
    /*public void ToggleBuffTooltips()
    {
        bool currentlyOpening = false;
        for (int i = 0; i < buffTooltips.Count; i++)
        {
            if (buffTooltips[i].gameObject.activeSelf)
            {
                currentlyOpening = true;
            }
        }
        if (currentlyOpening)
        {
            ClearBuffTooltips();
            return;
        }
        if (!dDataHub.hub.dParkAction.Contains(tokenUID))
        {
            return;
        }
        ParkPathAction action = dDataHub.hub.dParkAction.GetFromUID(tokenUID);
        for (int i = 0; i < buffTooltips.Count; i++)
        {
            if(i < action.buffUID.Count)
            {
                string buffUID = action.buffUID[i];
                if (dDataHub.hub.buffDict.Contains(buffUID))
                {
                    BuffDefinition bDef = dDataHub.hub.buffDict.GetFromUID(buffUID);
                    if(bDef.buffDesc != "")
                    {
                        buffTooltips[i].SetText(string.Format("<b>{0}</b<sprite name=\"{1}\"><br>{2}",
                            bDef.buffName,
                            bDef.buffIcon,
                            bDef.buffDesc));
                        buffTooltips[i].gameObject.SetActive(true);
                    }
                }
            }
        }
    }
    public void ClearBuffTooltips()
    {
        for(int i = 0; i < buffTooltips.Count; i++)
        {
            buffTooltips[i].gameObject.SetActive(false);
        }
    }
    */
    bool HasAvailableBuffTootips()
    {
        bool result = false;
        if (!dDataHub.hub.dParkAction.Contains(CurrentaActionUID))
        {
            return result;
        }
        ParkPathAction action = dDataHub.hub.dParkAction.GetFromUID(CurrentaActionUID);
        for (int i = 0; i < action.buffUID.Count; i++)
        {
            string buffUID = action.buffUID[i];
            if (dDataHub.hub.buffDict.Contains(buffUID))
            {
                BuffDefinition bDef = dDataHub.hub.buffDict.GetFromUID(buffUID);
                if (bDef.buffDesc != "")
                {
                    result = true;
                    return result;
                }
            }
        }
        return result;
    }
    public void CallBuffTooltips()
    {
        if (!dDataHub.hub.dParkAction.Contains(CurrentaActionUID))
        {
            return;
        }
        ParkPathAction action = dDataHub.hub.dParkAction.GetFromUID(CurrentaActionUID);
        buff_tooltip_popup.controller.CallTooltip(buffTooltipArea.transform.position, action.buffUID);
    }
    public void ClearAllBuffTooltips()
    {
        buff_tooltip_popup.controller.DismissAllTooltips();
    }
    public void OnTokenHoverOff()
    {
        ref_action_hover_off?.Invoke();
    }
    public void HoverOn()
    {
        mask.gameObject.SetActive(true);
        if (isForwardAction)
        {
            forward_arrow.gameObject.SetActive(true);
            Anim_ForwardArrow();
        }
        SFXPlayer.mp3.PlaySFX("button_hover");
    }
    public void HoverOff()
    {
        mask.gameObject.SetActive(false);
        forward_arrow.gameObject.SetActive(false);
    }
    public void Anim_Spawn()
    {
        transform.DOScale(0f, 0.5f).SetEase(Ease.OutBack).From();
    }
    public void Anim_ScaleUp()
    {
        art.transform.DOScale(Vector3.one * 1.05f, 0.3f).SetEase(Ease.OutBack);
    }
    public void Anim_ScaleReset()
    {
        art.transform.DOScale(Vector3.one, 0.3f);
    }
    public void Anim_DangerTokenCheck()
    {
        //danger_base.dice.Anim_Roll();
        danger_base.icon.Anim_Check();
    }
    public void Anim_DangerTokenPop()
    {
        //danger_base.dice.Anim_Roll();
        danger_base.icon.Anim_Pop();
    }
    public void Anim_CollectibleTokenCheck()
    {
        //collectible_base.dice.Anim_Roll();
        collectible_base.icon.Anim_Check();
    }
    public void Anim_DangerTokenAcquire(int count)
    {
        //Debug.Log(string.Format("danger trigger animation needed with {0}", count));
        CollectEffect.Collect(
            danger_base.icon.gameObject,
            count,
            new Vector2(3f, 0f),
            UICanvas.canvas.EffectLayer,
            danger_base.icon.transform,
            UIKeyElements.locator.DangerCounter);
    }
    public void Anim_CollectibleTokenAcquire(int count)
    {
        //Debug.Log(string.Format("collectible trigger animation needed with {0}", count));
        Sequence seq = DOTween.Sequence();
        seq.AppendCallback(() => collectible_base.icon.Anim_Pop());
        seq.AppendInterval(0.3f).AppendCallback(() => CollectEffect.Collect(
            collectible_base.icon.gameObject,
            count,
            new Vector2(3f, 0f),
            UICanvas.canvas.EffectLayer,
            collectible_base.icon.transform,
            UIKeyElements.locator.CollectibleCounter));
    }
    public void Anim_ProgressAdded(int amount)
    {
        //Debug.Log(string.Format("progress added animation needed with {0}", amount));
        FloatTextEffect.controller.Float(progress.transform, "icon_progress", amount.ToString(), 1f);
        
        CollectEffect.Collect(
            UIKeyElements.locator.Progress.gameObject, 
            Mathf.Max(1, Mathf.FloorToInt(amount/10f)), 
            new Vector2(1f, 0f),
            UICanvas.canvas.EffectLayer,
            progress.transform, 
            UIKeyElements.locator.Progress);
    }
    public void Anim_StaminaConsumed(int amount)
    {
        //Debug.Log(string.Format("stamina added animation needed with {0}", amount));
        FloatTextEffect.controller.Float(stamina.transform, "dynamic_current_stamina", amount.ToString(), 1f);
    }
    public void Anim_StaminaAdded(int amount)
    {
        //Debug.Log(string.Format("stamina added animation needed with {0}", amount));
        //FloatTextEffect.controller.Float(progress.transform, "dynamic_current_stamina", amount.ToString(), 1f);

        CollectEffect.Collect(
            UIKeyElements.locator.StaminaIcon.gameObject,
            amount,
            new Vector2(1f, 0f),
            UICanvas.canvas.EffectLayer,
            desc_text.transform,
            UIKeyElements.locator.StaminaBar);
    }

    public void Anim_MoveDown()
    {
        //pin.Anim_Pin(0.3f);
        DOTween.To(() => frame.GetComponent<Shadow>().effectDistance, x => frame.GetComponent<Shadow>().effectDistance = x, new Vector2(3f, -3f), ANIM_PLAY_DURATION);
        content.GetComponent<RectTransform>().DOAnchorPos(new Vector2(7f, -7f), ANIM_PLAY_DURATION);
        //group_tags.GetComponent<RectTransform>().DOAnchorPos(new Vector2(-7f, 7f), 0.3f);
    }
    public void Anim_FadeOut()
    {
        frame.DOFade(0.7f, ANIM_PLAY_DURATION);
        //art.DOFade(0.5f, ANIM_PLAY_DURATION);
    }
    void ResetFade()
    {
        frame.color = new Color(frame.color.r, frame.color.g, frame.color.b, 1f);
        //art.color = new Color(art.color.r, art.color.g, art.color.b, 1f);
    }
    public void Anim_DangerAppear(Transform startPos)
    {
        Sequence seq = DOTween.Sequence();
        seq.AppendInterval(0.3f).AppendCallback(() => CollectEffect.Collect(
            danger_base.icon.gameObject,
            1,
            new Vector2(3f, 0f),
            UICanvas.canvas.EffectLayer,
            startPos,
            transform));
        transform.DOScale(0f, 0.5f).From().SetDelay(1.2f);
    }
    public void Anim_ForwardArrow()
    {
        //Debug.Log("Anim_ForwardArrow called");
        if (forward_arrow.gameObject.activeSelf)
        {
            forward_arrow.GetComponent<RectTransform>().anchoredPosition = new Vector2(40f, 0f);
            forward_arrow.SetFade(1f);
            anim_arrow.Kill();
            anim_arrow = DOTween.Sequence();
            anim_arrow.Append(forward_arrow.GetComponent<RectTransform>().DOAnchorPosX(50, 0.8f));
            anim_arrow.InsertCallback(0f, () => forward_arrow.Anim_FadeTo(0f, 1f));
            anim_arrow.AppendInterval(0.3f);
            anim_arrow.AppendCallback(() => Anim_ForwardArrow());
        }
    }
    public void Anim_BlockerUp()
    {
        //
        blocker.transform.DOScale(Vector3.one * 1.1f, 1f).SetEase(Ease.InSine).SetLoops(1, LoopType.Yoyo).SetRelative();
    }
    public void Anim_Shake()
    {
        //
    }
}
