﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Park/Dict/Path")]
public class Dict_ParkPath : GenericDataDict<ParkPath>
{
    public override void DebugCount()
    {
        Debug.Log("Total of " + UIDList.Count + " park paths are added to Park Path Dictionary");
    }
    public List<PFEntrywGate> GetPFEntrywGateList()
    {
        List<PFEntrywGate> result = new List<PFEntrywGate>();
        List<ParkPath> fullList = GetFullList();
        foreach (ParkPath item in fullList)
        {
            result.Add(item);
        }
        return result;
    }
}
