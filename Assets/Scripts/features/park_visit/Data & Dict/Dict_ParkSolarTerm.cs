﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Park/Dict/SolarTerm")]
public class Dict_ParkSolarTerm : GenericDataDict<ParkSolarTerm>
{
    public override void DebugCount()
    {
        Debug.Log("Total of " + UIDList.Count + " park solar terms are added to Park Solar Term Dictionary");
    }
    public List<PFEntrywGate> GetPFEntrywGateList()
    {
        List<PFEntrywGate> result = new List<PFEntrywGate>();
        List<ParkSolarTerm> fullList = GetFullList();
        foreach (ParkSolarTerm item in fullList)
        {
            result.Add(item);
        }
        return result;
    }
}
