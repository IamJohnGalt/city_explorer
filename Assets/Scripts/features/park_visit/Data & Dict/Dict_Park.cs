﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Park/Dict/Park")]
public class Dict_Park : GenericDataDict<Park>
{
    public override void DebugCount()
    {
        Debug.Log("Total of " + UIDList.Count + " parks are added to Park Dictionary");
    }
    public List<PFEntrywGate> GetPFEntrywGateList()
    {
        List<PFEntrywGate> result = new List<PFEntrywGate>();
        List<Park> fullList = GetFullList();
        foreach (Park item in fullList)
        {
            result.Add(item);
        }
        return result;
    }
}
