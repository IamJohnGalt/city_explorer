﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataReader_ParkVisit : GenericDataReader
{
    public Dict_Park dPark;
    public Dict_ParkNode dParkNode;
    public Dict_ParkPath dParkPath;
    public Dict_ParkReward dParkReward;
    public Dict_ParkPathAction dParkAction;
    public Dict_ParkPathAction dParkRegularAction;
    public Dict_ParkPathAction dParkStayAction;
    public Dict_ParkPathAction dParkForwardAction;
    public Dict_ParkPathAction dParkDangerAction;
    public Dict_ParkSolarTerm dParkSolarTerm;

    public override void LoadFromXLS()
    {
        dPark.Reset();
        dParkNode.Reset();
        dParkPath.Reset();
        dParkReward.Reset();
        dParkAction.Reset();
        dParkRegularAction.Reset();
        dParkStayAction.Reset();
        dParkForwardAction.Reset();
        dParkDangerAction.Reset();
        dParkSolarTerm.Reset();

        LoadParkData("Park - park_exporter");
        LoadNodeData("Park - node_exporter");
        LoadPathData("Park - path_exporter");
        LoadRewardData("Park - reward_exporter");
        LoadRegularActionData("Park - action_exporter");
        LoadDangerActionData("Park - danger_exporter");
        LoadSolarTermData("Park - term_exporter");

        FinishLoading();
    }
    public void LoadParkData(string filename)
    {
        string[,] string_data = CSVReader.SplitCsvGrid(Resources.Load<TextAsset>("XLSData/" + filename).text);
        if (string_data[0, 0] != "UID")
        {
            Debug.LogError("Invalid File (No UID found): " + filename);
            return;
        }
        if (dPark == null)
        {
            Debug.Log("Null Ref on Data Dictionary");
        }
        for (int iRow = 1; iRow < string_data.GetUpperBound(1); iRow++)
        {
            Park item = new Park();

            item.UID = string_data[0, iRow];
            item.title = string_data[1, iRow];
            item.desc = string_data[2, iRow];
            item.regionText = string_data[3, iRow];
            item.release = string_data[4, iRow];
            item.tags = SplitString(string_data[5, iRow], ',');
            int.TryParse(string_data[6, iRow], out item.tier);
            float.TryParse(string_data[7, iRow], out item.weight);

            item.artAsset.objectArt = string_data[8, iRow];
            item.regionUID = string_data[9, iRow];
            int.TryParse(string_data[10, iRow], out item.coinCost);
            int.TryParse(string_data[11, iRow], out item.timeCost);
            int.TryParse(string_data[12, iRow], out item.maxTier);
            item.terrains = SplitString(string_data[13, iRow], ',');

            int temp_int = 0;
            //buff 1
            item.buffScope.Add(string_data[14, iRow]);
            item.buffUID.Add(string_data[15, iRow]);
            item.buffValue.Add(string_data[16, iRow]);
            int.TryParse(string_data[17, iRow], out temp_int);
            item.buffDuration.Add(temp_int);

            //buff 2
            item.buffScope.Add(string_data[18, iRow]);
            item.buffUID.Add(string_data[19, iRow]);
            item.buffValue.Add(string_data[20, iRow]);
            int.TryParse(string_data[21, iRow], out temp_int);
            item.buffDuration.Add(temp_int);

            item.musicUID = string_data[22, iRow];

            dPark.Add(item.UID, item);
        }
        dPark.DebugCount();
    }
    public void LoadNodeData(string filename)
    {
        string[,] string_data = CSVReader.SplitCsvGrid(Resources.Load<TextAsset>("XLSData/" + filename).text);
        if (string_data[0, 0] != "UID")
        {
            Debug.LogError("Invalid File (No UID found): " + filename);
            return;
        }
        if (dParkNode == null)
        {
            Debug.Log("Null Ref on Data Dictionary");
        }
        for (int iRow = 1; iRow < string_data.GetUpperBound(1); iRow++)
        {
            ParkNode item = new ParkNode();

            item.UID = string_data[0, iRow];
            item.title = string_data[1, iRow];
            item.desc = string_data[2, iRow];
            item.release = string_data[3, iRow];
            item.tags = SplitString(string_data[4, iRow], ',');
            int.TryParse(string_data[5, iRow], out item.tier);
            float.TryParse(string_data[6, iRow], out item.weight);

            item.nodeType = string_data[7, iRow];
            int.TryParse(string_data[8, iRow], out item.collectibleCount);
            int temp_min = 0;
            int.TryParse(string_data[9, iRow], out temp_min);
            int temp_max = 0;
            int.TryParse(string_data[10, iRow], out temp_max);
            item.collectibleLv = new intRange(temp_min, temp_max);
            int.TryParse(string_data[11, iRow], out item.rewardCount);
            int.TryParse(string_data[12, iRow], out item.pathDiff);
            item.artAsset.objectArt = string_data[13, iRow];
            item.narrativeBits = SplitString(string_data[14, iRow], ',');
            //Debug.Log(string.Format("get narrative bit of {0}", item.narrativeBits.Count));
            dParkNode.Add(item.UID, item);
        }
        dParkNode.DebugCount();
    }
    public void LoadPathData(string filename)
    {
        string[,] string_data = CSVReader.SplitCsvGrid(Resources.Load<TextAsset>("XLSData/" + filename).text);
        if (string_data[0, 0] != "UID")
        {
            Debug.LogError("Invalid File (No UID found): " + filename);
            return;
        }
        if (dParkPath == null)
        {
            Debug.Log("Null Ref on Data Dictionary");
        }
        for (int iRow = 1; iRow < string_data.GetUpperBound(1); iRow++)
        {
            ParkPath item = new ParkPath();

            item.UID = string_data[0, iRow];
            item.title = string_data[1, iRow];
            item.desc = string_data[2, iRow];
            item.flavor = string_data[3, iRow];
            item.release = string_data[4, iRow];
            item.tags = SplitString(string_data[5, iRow], ',');
            int.TryParse(string_data[6, iRow], out item.tier);
            float.TryParse(string_data[7, iRow], out item.weight);

            int.TryParse(string_data[8, iRow], out item.progressReq);
            
            int temp_int = 0;
            int.TryParse(string_data[9, iRow], out temp_int);
            item.randomProgress = temp_int > 0;
            //buff 1
            item.buffScope.Add(string_data[10, iRow]);
            item.buffUID.Add(string_data[11, iRow]);
            item.buffValue.Add(string_data[12, iRow]);
            int.TryParse(string_data[13, iRow], out temp_int);
            item.buffDuration.Add(temp_int);

            //buff 2
            item.buffScope.Add(string_data[14, iRow]);
            item.buffUID.Add(string_data[15, iRow]);
            item.buffValue.Add(string_data[16, iRow]);
            int.TryParse(string_data[17, iRow], out temp_int);
            item.buffDuration.Add(temp_int);
            item.narrativeBits = SplitString(string_data[18, iRow], ',');
            dParkPath.Add(item.UID, item);
        }
        dParkPath.DebugCount();
    }
    public void LoadRewardData(string filename)
    {
        string[,] string_data = CSVReader.SplitCsvGrid(Resources.Load<TextAsset>("XLSData/" + filename).text);
        if (string_data[0, 0] != "UID")
        {
            Debug.LogError("Invalid File (No UID found): " + filename);
            return;
        }
        if (dParkReward == null)
        {
            Debug.Log("Null Ref on Data Dictionary");
        }
        for (int iRow = 1; iRow < string_data.GetUpperBound(1); iRow++)
        {
            ParkReward item = new ParkReward();
            item.UID = string_data[0, iRow];
            item.title = string_data[1, iRow];
            item.desc = string_data[2, iRow];
            item.release = string_data[3, iRow];
            item.tags = SplitString(string_data[4, iRow], ',');
            int.TryParse(string_data[5, iRow], out item.tier);
            float.TryParse(string_data[6, iRow], out item.weight);

            int temp_int = 0;
            //buff 1
            item.buffScope.Add(string_data[7, iRow]);
            item.buffUID.Add(string_data[8, iRow]);
            item.buffValue.Add(string_data[9, iRow]);
            int.TryParse(string_data[10, iRow], out temp_int);
            item.buffDuration.Add(temp_int);

            //buff 2
            item.buffScope.Add(string_data[11, iRow]);
            item.buffUID.Add(string_data[12, iRow]);
            item.buffValue.Add(string_data[13, iRow]);
            int.TryParse(string_data[14, iRow], out temp_int);
            item.buffDuration.Add(temp_int);

            item.artAsset.objectArt = string_data[15, iRow];
            item.narrativeBits = SplitString(string_data[16, iRow], ',');
            dParkReward.Add(item.UID, item);
            //Debug.Log("reward tier: " + item.tier);
        }
        dParkReward.DebugCount();
    }
    public void LoadRegularActionData(string filename)
    {
        string[,] string_data = CSVReader.SplitCsvGrid(Resources.Load<TextAsset>("XLSData/" + filename).text);
        if (string_data[0, 0] != "UID")
        {
            Debug.LogError("Invalid File (No UID found): " + filename);
            return;
        }
        if (dParkRegularAction == null)
        {
            Debug.Log("Null Ref on Data Dictionary");
        }
        for (int iRow = 1; iRow < string_data.GetUpperBound(1); iRow++)
        {
            ParkPathAction item = new ParkPathAction();

            item.UID = string_data[0, iRow];
            item.title = string_data[1, iRow];
            item.desc = string_data[2, iRow];
            item.release = string_data[3, iRow];
            item.tags = SplitString(string_data[4, iRow], ',');
            int.TryParse(string_data[5, iRow], out item.tier);
            float.TryParse(string_data[6, iRow], out item.weight);

            int.TryParse(string_data[7, iRow], out item.maxCount);
            int.TryParse(string_data[8, iRow], out item.staminaRange.minInt);
            int.TryParse(string_data[9, iRow], out item.staminaRange.maxInt);
            int.TryParse(string_data[10, iRow], out item.progressRange.minInt);
            int.TryParse(string_data[11, iRow], out item.progressRange.maxInt);
            int temp_int = 0;
            float temp_float = 0;
            int.TryParse(string_data[12, iRow], out temp_int);
            item.forward = temp_int > 0;
            float.TryParse(string_data[13, iRow], out temp_float);
            item.dangerTrigger = temp_float > 0;
            item.dangerBase = temp_float;
            float.TryParse(string_data[14, iRow], out temp_float);
            item.collectibleTrigger = temp_float > 0;
            item.collectibleBase = temp_float;
            item.specialTriggerUID = string_data[15, iRow];
            float.TryParse(string_data[16, iRow], out temp_float);
            item.specialTrigger = temp_float > 0;
            item.specialBase = temp_float;

            float.TryParse(string_data[17, iRow], out item.buffChance);
            //buff 1
            item.buffScope.Add(string_data[18, iRow]);
            item.buffUID.Add(string_data[19, iRow]);
            item.buffValue.Add(string_data[20, iRow]);
            int.TryParse(string_data[21, iRow], out temp_int);
            item.buffDuration.Add(temp_int);

            //buff 2
            item.buffScope.Add(string_data[22, iRow]);
            item.buffUID.Add(string_data[23, iRow]);
            item.buffValue.Add(string_data[24, iRow]);
            int.TryParse(string_data[25, iRow], out temp_int);
            item.buffDuration.Add(temp_int);

            //art
            item.artAsset.objectArt = string_data[26, iRow];
            item.signAsset.objectArt = string_data[27, iRow];

            dParkAction.Add(item.UID, item);
            dParkRegularAction.Add(item.UID, item);
            if (item.forward)
            {
                dParkForwardAction.Add(item.UID, item);
            }
            else
            {
                dParkStayAction.Add(item.UID, item);
            }
        }
        dParkRegularAction.DebugCount();
    }
    public void LoadDangerActionData(string filename)
    {
        string[,] string_data = CSVReader.SplitCsvGrid(Resources.Load<TextAsset>("XLSData/" + filename).text);
        if (string_data[0, 0] != "UID")
        {
            Debug.LogError("Invalid File (No UID found): " + filename);
            return;
        }
        if (dParkDangerAction == null)
        {
            Debug.Log("Null Ref on Data Dictionary");
        }
        for (int iRow = 1; iRow < string_data.GetUpperBound(1); iRow++)
        {
            ParkPathAction item = new ParkPathAction();

            item.UID = string_data[0, iRow];
            item.title = string_data[1, iRow];
            item.desc = string_data[2, iRow];
            item.release = string_data[3, iRow];
            item.tags = SplitString(string_data[4, iRow], ',');
            int.TryParse(string_data[5, iRow], out item.tier);
            float.TryParse(string_data[6, iRow], out item.weight);

            int.TryParse(string_data[7, iRow], out item.maxCount);
            int.TryParse(string_data[8, iRow], out item.staminaRange.minInt);
            int.TryParse(string_data[9, iRow], out item.staminaRange.maxInt);
            int.TryParse(string_data[10, iRow], out item.progressRange.minInt);
            int.TryParse(string_data[11, iRow], out item.progressRange.maxInt);
            int temp_int = 0;
            float temp_float = 0;
            int.TryParse(string_data[12, iRow], out temp_int);
            item.forward = temp_int > 0;
            float.TryParse(string_data[13, iRow], out temp_float);
            item.dangerTrigger = temp_float > 0;
            item.dangerBase = temp_float;
            float.TryParse(string_data[14, iRow], out temp_float);
            item.collectibleTrigger = temp_float > 0;
            item.collectibleBase = temp_float;
            item.specialTriggerUID = string_data[15, iRow];
            float.TryParse(string_data[16, iRow], out temp_float);
            item.specialTrigger = temp_float > 0;
            item.specialBase = temp_float;

            float.TryParse(string_data[17, iRow], out item.buffChance);
            //buff 1
            item.buffScope.Add(string_data[18, iRow]);
            item.buffUID.Add(string_data[19, iRow]);
            item.buffValue.Add(string_data[20, iRow]);
            int.TryParse(string_data[21, iRow], out temp_int);
            item.buffDuration.Add(temp_int);

            //buff 2
            item.buffScope.Add(string_data[22, iRow]);
            item.buffUID.Add(string_data[23, iRow]);
            item.buffValue.Add(string_data[24, iRow]);
            int.TryParse(string_data[25, iRow], out temp_int);
            item.buffDuration.Add(temp_int);

            //art
            item.artAsset.objectArt = string_data[26, iRow];
            item.signAsset.objectArt = string_data[27, iRow];

            item.isDanger = true;

            dParkAction.Add(item.UID, item);
            dParkDangerAction.Add(item.UID, item);
        }
        dParkDangerAction.DebugCount();
    }
    public void LoadSolarTermData(string filename)
    {
        string[,] string_data = CSVReader.SplitCsvGrid(Resources.Load<TextAsset>("XLSData/" + filename).text);
        if (string_data[0, 0] != "UID")
        {
            Debug.LogError("Invalid File (No UID found): " + filename);
            return;
        }
        if (dParkReward == null)
        {
            Debug.Log("Null Ref on Data Dictionary");
        }
        for (int iRow = 1; iRow < string_data.GetUpperBound(1); iRow++)
        {
            ParkSolarTerm item = new ParkSolarTerm();
            item.UID = string_data[0, iRow];
            item.title = string_data[1, iRow];
            item.desc = string_data[2, iRow];
            item.release = string_data[3, iRow];
            item.tags = SplitString(string_data[4, iRow], ',');
            int.TryParse(string_data[5, iRow], out item.tier);
            float.TryParse(string_data[6, iRow], out item.weight);

            item.artAsset.objectArt = string_data[7, iRow];
            int temp_int = 0;
            //buff 1
            item.buffScope.Add(string_data[8, iRow]);
            item.buffUID.Add(string_data[9, iRow]);
            item.buffValue.Add(string_data[10, iRow]);
            int.TryParse(string_data[11, iRow], out temp_int);
            item.buffDuration.Add(temp_int);
            item.narrativeBits = SplitString(string_data[12, iRow], ',');

            dParkSolarTerm.Add(item.UID, item);
        }
        dParkSolarTerm.DebugCount();
    }
}
