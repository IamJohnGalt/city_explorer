﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Park/Dict/Node")]
public class Dict_ParkNode : GenericDataDict<ParkNode>
{
    public override void DebugCount()
    {
        Debug.Log("Total of " + UIDList.Count + " park nodes are added to Park Node Dictionary");
    }
    public List<PFEntrywGate> GetPFEntrywGateList()
    {
        List<PFEntrywGate> result = new List<PFEntrywGate>();
        List<ParkNode> fullList = GetFullList();
        foreach (ParkNode item in fullList)
        {
            result.Add(item);
        }
        return result;
    }
}
