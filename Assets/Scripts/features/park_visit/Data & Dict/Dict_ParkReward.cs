﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Park/Dict/Reward")]
public class Dict_ParkReward : GenericDataDict<ParkReward>
{
    public override void DebugCount()
    {
        Debug.Log("Total of " + UIDList.Count + " park rewards are added to Park Reward Dictionary");
    }
    public List<PFEntrywGate> GetPFEntrywGateList()
    {
        List<PFEntrywGate> result = new List<PFEntrywGate>();
        List<ParkReward> fullList = GetFullList();
        foreach (ParkReward item in fullList)
        {
            result.Add(item);
        }
        return result;
    }
}
