﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Park : PFEntrywGate
{
    public string regionText;
    public CardArt artAsset;
    public string regionUID;
    public int coinCost;
    public int modifiedCoinCost
    {
        get
        {
            TotalBuffValueSet coinBVS = ActiveBuffs.controller.RetrieveValueSetByAction("coin_cost_on_chronicle");
            return ActiveBuffs.ModifyByBVS(coinCost, coinBVS);
        }
    }
    public int timeCost;
    public int modifiedTimeCost
    {
        get
        {
            TotalBuffValueSet timeBVS = ActiveBuffs.controller.RetrieveValueSetByAction("time_cost_on_chronicle");
            return ActiveBuffs.ModifyByBVS(timeCost, timeBVS);
        }
    }
    public int maxTier;
    public List<string> terrains;

    //buff
    public List<string> buffScope;
    public List<string> buffUID;
    public List<string> buffValue;
    public List<int> buffDuration;

    //music
    public string musicUID;

    public Park() : base()
    {
        regionText = "未知区域";
        artAsset = new CardArt();
        regionUID = "";
        coinCost = 0;
        timeCost = 0;
        maxTier = 0;
        terrains = new List<string>();
        entryType = "Park";

        buffScope = new List<string>();
        buffUID = new List<string>();
        buffValue = new List<string>();
        buffDuration = new List<int>();

        musicUID = "EMPTY";
    }

    public string GetTerrainsString
    {
        get
        {
            List<string> locResult = new List<string>();
            for(int i = 0; i < terrains.Count; i++)
            {
                locResult.Add(dLocalHub.hub.keywords.GetAsset(terrains[i]));
            }
            return string.Format("地势：{0}", string.Join(",", locResult));
        }
    }

}
[System.Serializable]
public class ParkNode : PFEntrywGate
{
    public string nodeType;
    public int collectibleCount;
    public intRange collectibleLv;
    public int rewardCount;
    public int pathDiff;
    public CardArt artAsset;
    public List<string> narrativeBits;
    public string GetRandomNarrative
    {
        get
        {
            if (narrativeBits.Count > 0)
            {
                int rng = Random.Range(0, narrativeBits.Count);
                return narrativeBits[rng];
            }
            return "";
        }
    }
    public ParkNode() : base()
    {
        nodeType = "default";
        collectibleCount = 0;
        collectibleLv = new intRange(0, 0);
        rewardCount = 0;
        pathDiff = 0;
        entryType = "Park Node";
        artAsset = new CardArt();
        narrativeBits = new List<string>();
    }
}
[System.Serializable]
public class ParkPath : PFEntrywGate
{
    public int progressReq;
    public bool randomProgress;
    public int modifiedProgressReq
    {
        get
        {
            TotalBuffValueSet progressBVS = ActiveBuffs.controller.RetrieveValueSetByAction("path_progress_req");
            return ActiveBuffs.ModifyByBVS(progressReq, progressBVS);
        }
    }
    //buff
    public List<string> buffScope;
    public List<string> buffUID;
    public List<string> buffValue;
    public List<int> buffDuration;
    public List<string> narrativeBits;
    public string GetRandomNarrative
    {
        get
        {
            if(narrativeBits.Count > 0)
            {
                int rng = Random.Range(0, narrativeBits.Count);
                return narrativeBits[rng];
            }
            return "";
            
        }
    }

    public ParkPath() : base()
    {
        progressReq = 0;
        randomProgress = false;
        //dangerBase = 0f;
        entryType = "Park Path";

        buffScope = new List<string>();
        buffUID = new List<string>();
        buffValue = new List<string>();
        buffDuration = new List<int>();
        narrativeBits = new List<string>();
    }
}
[System.Serializable]
public class ParkReward : PFEntrywGate
{
    public List<string> buffScope;
    public List<string> buffUID;
    public List<string> buffValue;
    public List<int> buffDuration;
    public CardArt artAsset;
    public List<string> narrativeBits;
    public string GetRandomNarrative
    {
        get
        {
            if (narrativeBits.Count > 0)
            {
                int rng = Random.Range(0, narrativeBits.Count);
                return narrativeBits[rng];
            }
            return "";
        }
    }
    public ParkReward() : base()
    {
        buffScope = new List<string>();
        buffUID = new List<string>();
        buffValue = new List<string>();
        buffDuration = new List<int>();
        entryType = "Park Reward";
        artAsset = new CardArt();
        narrativeBits = new List<string>();
    }
}
[System.Serializable]
public class ParkPathAction : PFEntrywGate
{

    public int maxCount;
    public intRange staminaRange;
    public intRange progressRange;
    public bool isDanger;
    public bool dangerTrigger;
    public float dangerBase;
    public bool collectibleTrigger;
    public float collectibleBase;
    public bool specialTrigger;
    public string specialTriggerUID;
    public float specialBase;

    public bool forward;
    public float buffChance;
    public List<string> buffScope;
    public List<string> buffUID;
    public List<string> buffValue;
    public List<int> buffDuration;
    public CardArt artAsset;
    public CardArt signAsset;

    public ParkPathAction() : base()
    {
        maxCount = 0;
        staminaRange = new intRange();
        progressRange = new intRange();
        isDanger = false;
        dangerTrigger = false;
        dangerBase = 0;
        collectibleTrigger = false;
        collectibleBase = 0;
        specialTrigger = false;
        specialTriggerUID = "";
        specialBase = 0;
        forward = false;
        buffChance = 1;
        buffScope = new List<string>();
        buffUID = new List<string>();
        buffValue = new List<string>();
        buffDuration = new List<int>();
        artAsset = new CardArt();
        signAsset = new CardArt();
        entryType = "Park Action";
    }
    public intRange modifiedStaminaRange
    {
        get
        {
            if (isDanger)
            {
                //skip modification if it is a danger
                return staminaRange;
            }
            else if (staminaRange.minInt + staminaRange.maxInt < 0)
            {
                //return modified value as restore stamina
                intRange modifiedRange = new intRange();
                TotalBuffValueSet staminaBVS = ActiveBuffs.controller.RetrieveValueSetByAction("action_restore_stamina");
                modifiedRange.minInt = -ActiveBuffs.ModifyByBVS(-staminaRange.minInt, staminaBVS);
                modifiedRange.maxInt = -ActiveBuffs.ModifyByBVS(-staminaRange.maxInt, staminaBVS);
                return modifiedRange;
            }
            else if(staminaRange.minInt == 0 && staminaRange.maxInt == 0)
            {
                //do not modify if action do not cost stamina
                return staminaRange;
            }
            else
            {
                //return modified value as cost of stamina
                intRange modifiedRange = new intRange();
                TotalBuffValueSet staminaBVS = ActiveBuffs.controller.RetrieveValueSetByAction("action_stamina");
                TotalBuffValueSet maxRangeBVS = ActiveBuffs.controller.RetrieveValueSetByAction("action_stamina_range_max");
                modifiedRange.minInt = ActiveBuffs.ModifyByBVS(staminaRange.minInt, staminaBVS);
                modifiedRange.maxInt = ActiveBuffs.ModifyByBVS(ActiveBuffs.ModifyByBVS(staminaRange.maxInt, staminaBVS), maxRangeBVS);
                if (ActiveBuffs.controller.CheckFeatureGate("feature_gate_fame_max_stamina_limit_5"))
                {
                    modifiedRange.minInt = Mathf.Min(5, modifiedRange.minInt);
                    modifiedRange.maxInt = Mathf.Min(5, modifiedRange.maxInt);
                }
                return modifiedRange;
            }
        }
    }
    public intRange modifiedProgressRange
    {
        get
        {
            if (isDanger)
            {
                //skip modification if it is a danger
                return progressRange;
            }
            if (progressRange.minInt + progressRange.maxInt <= 0)
            {
                //return unmodified value if it decreases progress instead of increasing
                return progressRange;
            }
            else
            {
                intRange modifiedRange = new intRange();
                TotalBuffValueSet progressBVS = ActiveBuffs.controller.RetrieveValueSetByAction("action_progress");
                TotalBuffValueSet maxRangeBVS = ActiveBuffs.controller.RetrieveValueSetByAction("action_progress_range_max");
                modifiedRange.minInt = ActiveBuffs.ModifyByBVS(progressRange.minInt, progressBVS);
                modifiedRange.maxInt = ActiveBuffs.ModifyByBVS(ActiveBuffs.ModifyByBVS(progressRange.maxInt, progressBVS), maxRangeBVS);
                return modifiedRange;
            }
        }
    }
    public float modifiedDangerBase
    {
        get
        {
            float modifiedDangerChance = 0;
            TotalBuffValueSet bvs = ActiveBuffs.controller.RetrieveValueSetByAction("park_danger_base");
            modifiedDangerChance = Mathf.Min(1, Mathf.Max(0, ActiveBuffs.ModifyByBVS(dangerBase, bvs)));
            return modifiedDangerChance;
        }
    }
    public float modifiedCollectibleBase
    {
        get
        {
            float modifiedCollectibleChance = 0;
            TotalBuffValueSet bvs = ActiveBuffs.controller.RetrieveValueSetByAction("park_collectible_base");
            modifiedCollectibleChance = Mathf.Min(1, Mathf.Max(0, ActiveBuffs.ModifyByBVS(collectibleBase, bvs)));
            return modifiedCollectibleChance;
        }
    }
}
public class ParkSolarTerm : PFEntrywGate
{
    public CardArt artAsset;
    public List<string> buffScope;
    public List<string> buffUID;
    public List<string> buffValue;
    public List<int> buffDuration;
    public List<string> narrativeBits;
    public string GetRandomNarrative
    {
        get
        {
            if (narrativeBits.Count > 0)
            {
                int rng = Random.Range(0, narrativeBits.Count);
                return narrativeBits[rng];
            }
            return "";
        }
    }
    public ParkSolarTerm() : base()
    {
        buffScope = new List<string>();
        buffUID = new List<string>();
        buffValue = new List<string>();
        buffDuration = new List<int>();
        artAsset = new CardArt();
        narrativeBits = new List<string>();
    }
}