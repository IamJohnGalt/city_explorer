﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Park/Dict/PathAction")]
public class Dict_ParkPathAction : GenericDataDict<ParkPathAction>
{
    public override void DebugCount()
    {
        Debug.Log("Total of " + UIDList.Count + " park actions are added to Park Action Dictionary");
    }
    public List<PFEntrywGate> GetPFEntrywGateList()
    {
        List<PFEntrywGate> result = new List<PFEntrywGate>();
        List<ParkPathAction> fullList = GetFullList();
        foreach (ParkPathAction item in fullList)
        {
            result.Add(item);
        }
        return result;
    }
}
