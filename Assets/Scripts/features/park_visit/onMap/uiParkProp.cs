﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class uiParkProp : uipfb_atom
{

    [Header("Data")]
    public string collectibleUID;
    public bool discovered = false;

    [Header("Structure Info")]
    public List<uiParkNode> attachedNodes;
    public List<uiParkNode> blockingNodes;

    [Header("Children Objects")]
    [SerializeField] GameObject content;
    [SerializeField] Image art;

    public void SetStatus(List<string> visitNodePfbs)
    {
        bool discoveryStatus = false;
        for (int i = 0; i < attachedNodes.Count; i++)
        {
            if (visitNodePfbs.Contains(attachedNodes[i].pfbUID))
            {
                discoveryStatus = true;
                break;
            }
        }
        for (int i = 0; i < blockingNodes.Count; i++)
        {
            if (visitNodePfbs.Contains(blockingNodes[i].pfbUID))
            {
                discoveryStatus = false;
                break;
            }
        }
        discovered = discoveryStatus;
    }
    public void SetCollectible(string cUID)
    {
        collectibleUID = cUID;
        Collectible view = dDataHub.hub.dCollectible.GetFromUID(collectibleUID);
        art.sprite = uAssetReader.controller.GetByName(view.artAsset.objectArt);
    }
    public void StatusUpdate()
    {
        SetVisible(discovered && collectibleUID != "");
    }
}
