﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class uiParkOnMap_Structure : MonoBehaviour
{
    [SerializeField] Transform groupProps;
    [SerializeField] Transform groupNodes;
    [SerializeField] Transform groupRoutes;

    public Image backgroundImage;
    public List<uiParkNode> nodes;
    public List<uiParkRoute> routes;
    public List<uiParkProp> props;
    public delegate void btn_hit_func();
    public btn_hit_func ref_first_node_hit_func;
    public uiParkOnMap_Character character;

    public SpriteAsset backgroundAssets;
    public void OnParkFirstNodeClicked()
    {
        ref_first_node_hit_func?.Invoke();
    }
    public void InitAllActiveChildren()
    {
        nodes = groupNodes.GetComponentsInChildren<uiParkNode>(false).ToList();
        routes = groupRoutes.GetComponentsInChildren<uiParkRoute>(false).ToList();
        //props = groupProps.GetComponentsInChildren<uiParkProp>(false).ToList();

        //remove inactive links
        for (int i = 0; i < nodes.Count; i++)
        {
            List<uiParkRoute> newList = new List<uiParkRoute>();
            for (int j = 0; j < nodes[i].inRoutes.Count; j++)
            {
                if (nodes[i].inRoutes[j].gameObject.activeSelf)
                {
                    newList.Add(nodes[i].inRoutes[j]);
                }
            }
            nodes[i].inRoutes = newList;
            newList = new List<uiParkRoute>();
            for (int j = 0; j < nodes[i].outRoutes.Count; j++)
            {
                if (nodes[i].outRoutes[j].gameObject.activeSelf)
                {
                    newList.Add(nodes[i].outRoutes[j]);
                }
            }
            nodes[i].outRoutes = newList;
        }
        for (int i = 0; i < routes.Count; i++)
        {
            if(routes[i].inNode != null)
            {
                if (!routes[i].inNode.gameObject.activeSelf)
                {
                    routes[i].inNode = null;
                }
            }
            if (routes[i].outNode != null)
            {
                if (!routes[i].outNode.gameObject.activeSelf)
                {
                    routes[i].outNode = null;
                }
            }
        }
    }
    public void SetBackground(List<string> terrains)
    {
        for(int i = 0; i < terrains.Count; i++)
        {
            Sprite sprt = backgroundAssets.GetAsset(terrains[i]);
            if(sprt != null)
            {
                backgroundImage.sprite = sprt;
                return;
            }
        }
        Debug.LogError(string.Format("unable to find a valid background for terrain set ({0})", terrains.ToString()));
        
    }
    public uiParkNode RetrieveNodeByPfbUID(string pfbUID)
    {
        foreach(uiParkNode node in nodes)
        {
            if(node.pfbUID == pfbUID)
            {
                return node;
            }
        }
        return null;
    }
    public uiParkRoute RetrievePathByPfbUID(string pfbUID)
    {
        foreach (uiParkRoute route in routes)
        {
            if (route.pfbUID == pfbUID)
            {
                return route;
            }
        }
        return null;
    }
    public uiParkNode GetStartNode()
    {
        return RetrieveNodeByPfbUID("pfb_parkNode_t0_1");
    }
}
