﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class onMapStructureOption
{
    public string structureUID;
    public GameObject pfb_structure = null;
    public string terrain = "";
    public int maxTier = 1;
    //public string releaseGate = "";
}
[CreateAssetMenu(menuName = "Park/OnMapUI/Structure Set")]
public class onMapStructureSet : ScriptableObject
{
    public List<onMapStructureOption> structures;

    public onMapStructureOption DrawValidStructure(string targetTerrain, int targetNodeTier, string excludingUID = "")
    {
        //int loopTimes = 0;
        
        List<onMapStructureOption> availableStructures = new List<onMapStructureOption>();
        for (int i=0; i< structures.Count; i++)
        {
            if((targetTerrain == structures[i].terrain || structures[i].terrain == "shared") && (structures[i].maxTier == targetNodeTier) && structures[i].structureUID != excludingUID)
            {
                availableStructures.Add(structures[i]);
            }
        }
        if(availableStructures.Count == 0)
        {
            Debug.LogError("can't find a valid structure option, default structure used");
            return structures[0];
        }
        int rngIndex = Random.Range(0, availableStructures.Count);
        //availableStructures.Sort((a, b) => Random.value.CompareTo(Random.value));
        return availableStructures[rngIndex];
    }
}
