﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using DG.Tweening;
using UnityEditor;

public class uiParkRoute : uipfb_atom
{
    public enum RouteStatus { visited, passed, current, next, hidden};
    public delegate void btn_hit_func(string pathpfbUID);
    public btn_hit_func ref_path_hit_func;
    [SerializeField] dataParkVisit gameplayData;

    [Header("Data")]
    public string pathUID;
    //public int pathPoolIndex;
    //public int nodePoolIndex;
    public RouteStatus status;
    [SerializeField] string statusStr;

    [Header("Structure Info")]
    public string pfbUID;
    public int difficulty;
    public uiParkNode inNode;
    public uiParkNode outNode;

    [Header("Assets")]
    public SpriteAsset pathFrames;

    [Header("Children Objects")]
    [SerializeField] GameObject content;
    [SerializeField] Image regularSprite;
    [SerializeField] Image highlightSprite;
    public ui_icon label_sign;
    [SerializeField] TextMeshProUGUI progress_req;
    public Transform popup_pos;

    [Header("Game Event")]
    [SerializeField] GameEvent ClearPathHighlight;

    private Tween animTween;

    public void OnPathClicked()
    {
        if(status == RouteStatus.current && gameplayData.curStatus == dConstants.ParkConst.ParkStatus.nextNode)
        {
            outNode.OnNodeClicked();
            ClearPathHighlight.Raise();
            Anim_ShowPathHighlight();
        }
    }

    public void SetPath(string pathDataUID)
    {
        if (!dDataHub.hub.dParkPath.Contains(pathDataUID))
        {
            SetVisible(false);
            Debug.LogError(string.Format("node({0}) is set to invisible due to missing node data by its UID({1})", gameObject.name, pathDataUID));
            return;
        }
        ParkPath path = dDataHub.hub.dParkPath.GetFromUID(pathDataUID);
        pathUID = pathDataUID;

        progress_req.SetText(path.randomProgress ? "???" : path.modifiedProgressReq.ToString());
        SetVisible(true);

        highlightSprite.fillAmount = 0;
    }
    public void SetStatus(string curNodePfb, string curPathPfb, List<string> visitedNodePfbs, List<string> visitedPathPfbs, dConstants.ParkConst.ParkStatus gameplayStatus)
    {
        if (pathUID != "")
        {
            SetPath(pathUID);
        }
        bool passed = false;
        foreach (string pfbUID in visitedNodePfbs)
        {
            if (IsSourcedFrom(pfbUID))
            {
                passed = true;
            }
        }
        bool next = IsSourcedFrom(curNodePfb);
        if (curPathPfb == pfbUID)
        {
            status = RouteStatus.visited;
            statusStr = "visited";
        }
        else if (visitedPathPfbs.Contains(pfbUID))
        {
            status = RouteStatus.visited;
            statusStr = "visited";
        }
        else if (passed)
        {
            status = RouteStatus.passed;
            statusStr = "passed";
        }
        else if (next)
        {
            if (gameplayStatus == dConstants.ParkConst.ParkStatus.nextNode)
            {
                status = RouteStatus.current;
                statusStr = "current";
            }
            else
            {
                status = RouteStatus.next;
                statusStr = "next";
            }            
        }
        else if (inNode.status == uiParkNode.NodeStatus.next && outNode.isRevealed)
        {
            status = RouteStatus.next;
            statusStr = "next";
        }
        else
        {
            status = RouteStatus.hidden;
            statusStr = "hidden";
        }
    }
    public void StatusUpdate()
    {
        label_sign.SetFrame(pathFrames.GetAsset(statusStr));
        label_sign.SetInteractive(status == RouteStatus.next || status == RouteStatus.current);
        label_sign.gameObject.SetActive((status == RouteStatus.next || status == RouteStatus.current || status == RouteStatus.visited) && pathUID != "");
        regularSprite.color = new Color(1f, 1f, 1f, status == RouteStatus.passed ? 0.3f : 1f);
        //label_danger.SetVisible((status == RouteStatus.next || status == RouteStatus.current) && label_danger.curStr != "" && pathUID != "");
        content.SetActive(status != RouteStatus.hidden);
    }
    /*
    public void PathTaken()
    {
        switch (status)
        {
            case RouteStatus.visited:
                break;
            case RouteStatus.passed:
                break;
            case RouteStatus.current:
                ref_path_hit_func?.Invoke(pfbUID);
                break;
            case RouteStatus.next:
                //ref_path_hit_func?.Invoke(pathPoolIndex, nodePoolIndex);
                break;
            case RouteStatus.hidden:
                break;
            default:
                break;
        }
    }
    public void OnSignClicked()
    {
        switch (status)
        {
            case RouteStatus.visited:
                break;
            case RouteStatus.passed:
                break;
            case RouteStatus.current:
                //detailedPath.Expand();
                //NextNodePreview.Raise();
                //ref_path_hit_func?.Invoke(pfbUID);
                break;
            case RouteStatus.next:
                //ref_path_hit_func?.Invoke(pathPoolIndex, nodePoolIndex);
                break;
            case RouteStatus.hidden:
                break;
            default:
                break;
        }
    }
    */
    bool isDirectedTo(string nodePfb)
    {
        return outNode.pfbUID == nodePfb;
    }
    bool IsSourcedFrom(string nodePfb)
    {
        return inNode.pfbUID == nodePfb;
    }
    public void Anim_ShowPathHighlight()
    {
        highlightSprite.fillAmount = 0;
        animTween = DOTween.To(() => highlightSprite.fillAmount, x => highlightSprite.fillAmount = x, 1f, 0.3f);
        outNode.Anim_ScaleUp();
        highlightSprite.gameObject.SetActive(true);
    }
    public void Anim_ClearPathHighlight()
    {
        DOTween.Kill(animTween);
        highlightSprite.fillAmount = 0;
        highlightSprite.gameObject.SetActive(false);
        //DOTween.To(() => highlightSprite.fillAmount, x => highlightSprite.fillAmount = x, 1f, 2f);
    }
    public void Anim_ScaleUp()
    {
        label_sign.transform.DOScale(Vector3.one * 1.05f, 0.3f).SetEase(Ease.OutBack);
    }
    public void Anim_ScaleReset()
    {
        label_sign.transform.DOScale(Vector3.one, 0.3f);
    }
    //------------Debug--------------------
#if UNITY_EDITOR
    void OnDrawGizmos()
    {
        if(pfbUID != "" && pfbUID != null)
        {
            GUIStyle style = new GUIStyle();
            style.normal.textColor = Color.yellow;
            string displayTxt = pfbUID.Replace("pfb_parkRoute_", "");
            Handles.Label(transform.position, displayTxt, style);
        }
    }
#endif
}
