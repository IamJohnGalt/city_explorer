﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class uiParkRewardToken : uipfb_atom
{
    [Header("Data")]
    public string tokenUID;
    public dConstants.ParkConst.TokenStatus status;

    [Header("Children Objects")]
    [SerializeField] GameObject content;
    [SerializeField] uipfb_art art;
    [SerializeField] uipfb_label rewardName;
    [SerializeField] uipfb_label number;
    [SerializeField] uipfb_label description;
    [SerializeField] uipfb_pin pin;

    public void SetTokenByUID(string rewardUID, dConstants.ParkConst.TokenStatus _status = dConstants.ParkConst.TokenStatus.hidden)
    {
        //set visual
        if (status == dConstants.ParkConst.TokenStatus.playable && _status == dConstants.ParkConst.TokenStatus.played)
        {
            pin.Anim_Pin();
            //Anim_MoveDown();
        }
        else if(_status == dConstants.ParkConst.TokenStatus.played)
        {
            pin.SetPin();
        }
        else
        {
            pin.ResetPin();
        }

        tokenUID = rewardUID;
        status = _status;
        if (!dDataHub.hub.dParkReward.Contains(rewardUID))
        {
            SetVisible(false);
            Debug.LogError(string.Format("token({0}) is set to invisible due to missing token data by its UID({1})", gameObject.name, rewardUID));
            return;
        }
        
        if(_status == dConstants.ParkConst.TokenStatus.preview)
        {
            art.SetSprite(string.Format("Art/token/{0}", "token_question"));
            rewardName.SetText("未知奖励");
            rewardName.SetFade(0f);
            number.SetText("");
            description.SetText("");
        }
        else
        {
            ParkReward reward = dDataHub.hub.dParkReward.GetFromUID(rewardUID);
            art.SetSprite(string.Format("Art/token/{0}", reward.artAsset.objectArt));
            rewardName.SetText(reward.title);
            description.SetText(string.Format("{0}", reward.desc));
            if (reward.desc == "")
            {
                number.SetText(string.Format("+{0}", reward.buffValue));
            }
            else
            {
                number.SetText("");
            }
        }
        
        SetVisible(status != dConstants.ParkConst.TokenStatus.hidden);
    }
    public void Anim_MoveDown()
    {
        DOTween.To(() => art.img.GetComponent<Shadow>().effectDistance, x => art.img.GetComponent<Shadow>().effectDistance = x, new Vector2(3f, -3f), 0.2f);
        content.GetComponent<RectTransform>().DOAnchorPos(new Vector2(7f, -7f), 0.2f);
    }
}
