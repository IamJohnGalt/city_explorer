﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gParkVisit : MonoBehaviour
{
    public static gParkVisit controller;

    //public gJournal journalGameplay;
    public dataParkVisit gameplayData;
    public dataJournal journalData;
    public dataPlayerEquipment equipmentData;
    //public uiParkOnMap_Structure currentMap;

    [Header("Game Event")]
    [SerializeField] GameEvent ParkVisitInit;
    [SerializeField] GameEvent ParkVisitStart;
    [SerializeField] GameEvent ParkVisitEnd;
    //[SerializeField] GameEvent OnRouteStart;
    [SerializeField] GameEvent OnRouteEnd;
    //[SerializeField] GameEvent OnMapResume;
    [SerializeField] GameEvent OnSumamryStart;
    [SerializeField] GameEvent OnMapChanged;
    [SerializeField] GameEvent OnNodeChanged;
    [SerializeField] GameEvent OnRouteChanged;
    [SerializeField] GameEvent OnNodeEnd;
    [SerializeField] GameEvent OnNextNodeEnd;
    [SerializeField] GameEvent OnTermStart;

    [SerializeField] GameEvent ActionPreviewStart;
    [SerializeField] GameEvent ActionPreviewEnd;
    //[SerializeField] GameEvent OnNodeStart;
    //[SerializeField] GameEvent OnTermEnd;

    private void Awake()
    {
        if (controller == null)
        {
            controller = this;
        }
        else if (controller != null)
        {
            Destroy(gameObject);
        }
    }

    void OnEnable()
    {
        if (gameplayData == null)
        {
            gameplayData = ScriptableObject.CreateInstance<dataParkVisit>();
        }
        gameplayData.Reset();
    }
    public void ViewSolarTerm()
    {
        OnTermStart.Raise();
    }
    public void CardHoverOnStayPool(int index)
    {
        if (gameplayData.stayActionPlayed.Count <= index)
        {
            return;
        }
        if (gameplayData.stayActionPlayed[index])
        {
            return;
        }
        PreviewAction(gameplayData.stayActionPool[index]);
        ActionPreviewStart.Raise();
    }
    public void CardHoverOnForwardPool(int index)
    {
        if (gameplayData.forwardActionPlayed.Count <= index)
        {
            return;
        }
        if (gameplayData.forwardActionPlayed[index])
        {
            return;
        }
        PreviewAction(gameplayData.forwardActionPool[index]);
        ActionPreviewStart.Raise();
    }
    public void CardHoverOnDangerPool(int index)
    {
        if (gameplayData.dangerActionPlayed.Count <= index)
        {
            return;
        }
        if (gameplayData.dangerActionPlayed[index])
        {
            return;
        }
        PreviewAction(gameplayData.dangerActionPool[index]);
        ActionPreviewStart.Raise();
    }
    public void CardHoverOff()
    {
        gameplayData.staminaPreview = gameplayData.staminaCur;
        gameplayData.progressPreview = gameplayData.progressCur;
        gameplayData.previewDangerToken = gameplayData.curDangerToken;
        gameplayData.previewCollectibleToken = gameplayData.curCollectibleToken;
        ActionPreviewEnd.Raise();
    }
    public void CardPlayedOnStayPool(int index)
    {
        //Debug.Log(string.Format("CardPlayedOnStayPool @ {0}", index));
        string cardUID = gameplayData.stayActionPool[index];
        if (gameplayData.stayActionPlayed[index])
        {
            return;
        }
        ResetLatestActionResult();
        gameplayData.stayActionPlayed[index] = true;
        gameplayData.totalCards += 1;
        if(cardUID == "parkaction_arrive")
        {
            //SFXPlayer.mp3.PlaySFX("guqin_end");
            EnterNode();
        }
        else
        {
            //gameplayData.acquiredActions.Add(cardUID);
            LogPlayedAction(cardUID);
            PlayAction(cardUID);
        }
        CardHoverOff();
        if (gameplayData.parkUID == dConstants.ParkConst.INTRO_PARK_UID)
        {
            gIntroParkScript.controller.ClearDialogAndMask();
        }
    }
    public void CardPlayedOnDangerPool(int index)
    {
        //Debug.Log(string.Format("CardPlayedOnStayPool @ {0}", index));
        string cardUID = gameplayData.dangerActionPool[index];
        if (gameplayData.dangerActionPlayed[index])
        {
            return;
        }
        ResetLatestActionResult();
        gameplayData.dangerActionPlayed[index] = true;
        gameplayData.totalCards += 1;
        //chekc if it is a marjor danger
        ParkPathAction cardAction = dDataHub.hub.dParkAction.GetFromUID(cardUID);
        if (cardAction.isDanger && cardAction.tier == 0 && ActiveBuffs.controller.CheckFeatureGate("feature_gate_persona_star_major_danger_resolve"))
        {
            CharacterPersona.controller.Persona_Add_Star_Major_Danger();
        }
        LogPlayedAction(cardUID);
        PlayAction(cardUID);
        CardHoverOff();
    }
    public void CardPlayedOnForwardPool(int index)
    {
        //Debug.Log(string.Format("CardPlayedOnForwardPool @ {0}", index));
        string cardUID = gameplayData.forwardActionPool[index];
        if (gameplayData.forwardActionPlayed[index])
        {
            return;
        }
        ResetLatestActionResult();
        gameplayData.forwardActionPlayed[index] = true;
        gameplayData.totalCards += 1;
        if (cardUID == "parkaction_end")
        {
            FinishPark();
        }
        else if (cardUID == "parkaction_arrive")
        {
            EnterNode();
        }
        else
        {
            //gameplayData.acquiredActions.Add(cardUID);
            LogPlayedAction(cardUID);
            PlayAction(cardUID);
        }
        CardHoverOff();
        if (gameplayData.parkUID == dConstants.ParkConst.INTRO_PARK_UID)
        {
            gIntroParkScript.controller.ClearDialogAndMask();
        }
    }
    public void NodeVisitedOnMap(string nodePfbUID)
    {
        //check if it is teh first time enter a node
        if(nodePfbUID == gameplayData.curNodePfbUID && !gameplayData.currentNodeViewed)
        {
            gameplayData.currentNodeViewed = true;
            //trigger help check
            ParkNode node = dDataHub.hub.dParkNode.GetFromUID(gameplayData.curNodeDataUID);
            //CharacterHelp.controller.HelpCheck_Character2_HelpLv3(node.nodeType);
        }
    }
    public void NodePreviewOnMap(string nodePfbUID)
    {
        //nothing needed yet.
    }
    public void Enable2ndRewardOnRewardPool()
    {
        CurrencyManagement.wallet.RemoveCurrency("currency_coin", dConstants.ParkConst.SECOND_REWARD_COIN_COST_VILLAGE);
        gameplayData.In2ndReward = true;
        gameplayData.Allow2ndReward = false;
    }
    public void RedrawCollectibleByAbility()
    {
        RemoveStamina(dConstants.ParkConst.REDRAW_COLLECTIBLE_COST);
        ApplyBuff("", "", "redraw_collectible", "", 0);
        gameplayData.AllowCollectibleRedraw = false;
    }
    public void CardPlayedOnRewardPool(int index)
    {
        //Debug.Log(string.Format("CardPlayedOnRewardPool @ {0}", index));
        if (gameplayData.CurrentNodeObj.rewardClaimed[index])
        {
            Debug.LogError("Error on claiming a reward that already claimed");
        }
        gameplayData.CurrentNodeObj.rewardClaimed[index] = true;
        string cardUID = gameplayData.CurrentNodeObj.rewardUIDs[index];
        if (dDataHub.hub.dParkReward.Contains(cardUID))
        {
            ParkReward reward = dDataHub.hub.dParkReward.GetFromUID(cardUID);
            for(int i = 0; i < reward.buffUID.Count; i++)
            {
                if(reward.buffUID[i] != "")
                {
                    ApplyBuff(reward.UID, reward.buffScope[i], reward.buffUID[i], reward.buffValue[i], reward.buffDuration[i]);
                }
            }
            //add narrative
            string narrative = reward.GetRandomNarrative;
            if (narrative != "")
            {
                gameplayData.CurrentNodeObj.AddNarrative(narrative);
            }
        }

        if (gameplayData.CurrentNodeResolved)
        {
            if (gameplayData.inFinalNode)
            {
                FinishPark();
            }
            else
            {
                EnableRouteSelection();
            }
        }
        //OnNodeChanged.Raise();
    }
    public void EquipmentPlayed(int index)
    {
        if(gameplayData.equipmentCount[index] > 0)
        {

            if (ActiveBuffs.controller.CheckFeatureGate("feature_gate_persona_refill_equipment") && gameplayData.playedEquipments.Count == 0)
            {
                //do not consume count
                CharacterPersona.controller.Persona_Refill_Equipment_FirstUse();
            }
            else
            {
                gameplayData.equipmentCount[index] -= 1;
            }
            Equipment equ = dDataHub.hub.equipmentDict.GetFromUID(equipmentData.activeEquipment[index]);
            for (int i = 0; i < equ.buffs.Count; i++)
            {
                ApplyBuff(equ.UID, equ.buffs[i].scope, equ.buffs[i].uid, equ.buffs[i].value, equ.buffs[i].duration);
            }
            gameplayData.playedEquipments.Add(equipmentData.activeEquipment[index]);
        }
        gameplayData.itemUsedCurRound += 1;
        if (ActiveBuffs.controller.CheckFeatureGate("feature_gate_help_use_equipment"))
        {
            CharacterAcquisition.controller.AcquisitionCheck_Use_Equipment();
        }
        
        OnRouteChanged.Raise();
    }
    public void CardPlayedOnViewPool(int index)
    {
        //generate collectibles
        if (gameplayData.CurrentNodeObj.viewClaimed[index])
        {
            Debug.LogError("Error on claiming a view that already claimed");
        }
        gameplayData.CurrentNodeObj.viewClaimed[index] = true;
        string viewUID = gameplayData.CurrentNodeObj.viewUIDs[index];
        if (dDataHub.hub.dView.Contains(viewUID))
        {
            List<string> collectibleOptions = ShowView(viewUID);
            gameplayData.CurrentNodeObj.SetCollectibleOnNode(collectibleOptions);
        }
        //OnNodeChanged.Raise();
    }
    public void CardPlayedOnCollectiblePool(int index)
    {
        if (gameplayData.CurrentNodeObj.collectibleClaimed[index])
        {
            Debug.LogError("Error on claiming a collectible that already claimed");
        }
        gameplayData.CurrentNodeObj.collectibleClaimed[index] = true;
        string cardUID = gameplayData.CurrentNodeObj.collectibleUIDs[index];
        if (dDataHub.hub.dCollectible.Contains(cardUID))
        {
            GetCollectible(cardUID);
            Collectible col = dDataHub.hub.dCollectible.GetFromUID(cardUID);
            gameplayData.CurrentNodeObj.claimedViewUID = col.viewSource;

            //add narrative
            string narrative = col.GetRandomNarrative;
            if (narrative != "")
            {
                gameplayData.CurrentNodeObj.AddNarrative(narrative);
            }
        }
        if (gameplayData.CurrentNodeResolved)
        {
            if (gameplayData.inFinalNode)
            {
                FinishPark();
            }
            else
            {
                EnableRouteSelection();
            }
        }
        //OnNodeChanged.Raise();
    }
    public void SkipCollectible()
    {
        for(int i=0;i< gameplayData.CurrentNodeObj.collectibleClaimed.Count; i++)
        {
            gameplayData.CurrentNodeObj.collectibleClaimed[i] = true;
        }
        for (int i = 0; i < gameplayData.CurrentNodeObj.viewClaimed.Count; i++)
        {
            gameplayData.CurrentNodeObj.viewClaimed[i] = true;
        }
        
        if (gameplayData.CurrentNodeResolved)
        {
            if (gameplayData.inFinalNode)
            {
                FinishPark();
            }
            else
            {
                EnableRouteSelection();
            }
        }
    }
    void UpdateNodeCollectible()
    {
        //update next node collectibles
        GenerateNextNodeCollectibleByNodePfbUID(gameplayData.curNodePfbUID);
    }
    public void CardPlayedOnViewPopop(int index)
    {
        string targetViewUID = gameplayData.popupViews[index];
        if (dDataHub.hub.dView.Contains(targetViewUID))
        {
            gameplayData.popupCollectibles = ShowView(targetViewUID);
        }
        else
        {
            Debug.LogError(string.Format("view ({0}) is not valid at popupPool slot({1})", targetViewUID, index));
        }
    }
    public void CardPlayedOnCollectiblePopop(int index)
    {
        string targetCollectibleUID = gameplayData.popupCollectibles[index];
        if (dDataHub.hub.dCollectible.Contains(targetCollectibleUID))
        {
            GetCollectible(targetCollectibleUID);
        }
        else
        {
            Debug.LogError(string.Format("collectible ({0}) is not valid at popupPool slot({1})", targetCollectibleUID, index));
        }
        gameplayData.popupCollectibles = new List<string>();
        gameplayData.popupViews = new List<string>();
    }
    public void SkipOnCollectiblePopup()
    {
        gameplayData.popupCollectibles = new List<string>();
        gameplayData.popupViews = new List<string>();
    }
    public void RoutePfbTaken(string RoutePfb)
    {
        //ActiveBuffs.controller.DeactivateFrom("in_park_path");
        //check to avoid double click
        if (gameplayData.curStatus == dConstants.ParkConst.ParkStatus.onRoute)
        {
            return;
        }

        uiParkRoute routeObject = gameplayData.currentMap.RetrievePathByPfbUID(RoutePfb);
        ParkPath path = dDataHub.hub.dParkPath.GetFromUID(routeObject.pathUID);
        gameplayData.tierCur = Mathf.Max(gameplayData.tierCur, path.tier);
        gameplayData.curPathDataUID = path.UID;
        gameplayData.progressMax = path.modifiedProgressReq + (path.randomProgress ? Random.Range(-dConstants.ParkConst.RANDOM_PATH_RANGE, dConstants.ParkConst.RANDOM_PATH_RANGE) : 0);
        gameplayData.hideProgress = path.randomProgress;
        //gameplayData.dangerBasePath = path.dangerBase;
        //ActiveBuffs.controller.ActivateBuff("in_park_path", "buff_park_danger_base_add", path.dangerBase, 1);
        for (int i = 0; i < path.buffUID.Count; i++)
        {
            if (path.buffUID[i] != "")
            {
                ApplyBuff(path.UID, path.buffScope[i], path.buffUID[i], path.buffValue[i], path.buffDuration[i]);
            }

        }

        //update map structure
        gameplayData.curPathPfbUID = routeObject.pfbUID;
        gameplayData.nextNodePfbUID = routeObject.outNode.pfbUID;
        gameplayData.nextNodeDataUID = routeObject.outNode.nodeUID;
        gameplayData.visitedNodePfb.Add(gameplayData.curNodePfbUID);
        gameplayData.curNodePfbUID = "";

        //clear narartive then add path narative to next node
        //routeObject.outNode.narrative = "";
        string narrative = path.GetRandomNarrative;
        if (narrative != "")
        {
            routeObject.outNode.AddNarrative(narrative);
        }

        OnNextNodeEnd.Raise();

        //enter route
        EnterRoute();
    }
    public void LaunchParkbyUID(string parkUID)
    {
        gameplayData.Reset();
        gameplayData.nodeUID = parkUID;
        gameplayData.parkUID = parkUID;
        Park park = dDataHub.hub.dPark.GetFromUID(gameplayData.parkUID);
        gameplayData.parkXLSData = park;
        gameplayData.parkName = park.title;
        //gameplayData.seasonKeyword = CalendarManagement.calendar.getCurSeason():

        
        //check initial collectible token
        if (ActiveBuffs.controller.CheckFeatureGate("feature_gate_persona_start_collectible_token"))
        {
            CharacterPersona.controller.Persona_Park_Start_Collectible_Token();
        }
        gameplayData.totalMonths = park.modifiedTimeCost;
        //set max tier
        gameplayData.tierMax = park.maxTier;
        //park buff
        for(int i = 0; i < park.buffUID.Count; i++)
        {
            if(park.buffUID[i] != "")
            {
                ApplyBuff(park.UID, park.buffScope[i], park.buffUID[i], park.buffValue[i], park.buffDuration[i]);
            }
        }
        //solar term
        if (gameplayData.parkUID != "park_session_start")
        {
            SetSolarTerm();
        }
        //set intial stamina
        TotalBuffValueSet staminaBVS = ActiveBuffs.controller.RetrieveValueSetByAction("max_stamina");
        int intialSta = ActiveBuffs.ModifyByBVS(dConstants.ParkConst.INITAL_STAMINA, staminaBVS);
        gameplayData.staminaMax = intialSta;
        gameplayData.staminaCur = intialSta;
        //calling Park Visit UI part to init
        ParkVisitInit.Raise();
        //set initial node data
        gameplayData.curNodeDataUID = "parknode_lv0_start";
        GenerateAllNodeUIDs();
        //refill equipment to 1
        InitEquipment();
        //enter it
        EnterNode();
        //raise ui event
        ParkVisitStart.Raise();
        OnTermStart.Raise();
        //log stats
        dStatistics.stats.PatkVisited(parkUID);
        //play new music
        BackgroundMusicPlayer.mp3.PlayMusic(park.musicUID);
        //add narrative for entering
        string narrative = CalendarManagement.calendar.GetCurDate().DateToString();
        if(dDataHub.hub.dParkSolarTerm.Contains(gameplayData.curSolarTermUID))
        {
            ParkSolarTerm term = dDataHub.hub.dParkSolarTerm.GetFromUID(gameplayData.curSolarTermUID);
            narrative += "，" + term.GetRandomNarrative;
        }
        narrative += string.Format("，游于{0}。", gameplayData.parkName);
        NarrativeEffect.controller.QueueCaption(narrative);
    }
    void InitEquipment()
    {
        for(int i = 0; i < dConstants.Avatar.MAX_ACTIVE_EQUIPMENT_SLOT; i++)
        {
            if(equipmentData.activeEquipment[i] != "")
            {
                gameplayData.equipmentCount.Add(1);
            }
            else
            {
                gameplayData.equipmentCount.Add(-1);
            }
        }
    }
    void PlayAction(string cardUID)
    {
        ParkPathAction action = dDataHub.hub.dParkAction.GetFromUID(cardUID);

        //add collectible tokens
        if (action.collectibleTrigger)
        {
            TotalBuffValueSet bvs = ActiveBuffs.controller.RetrieveValueSetByAction("park_collectible_base");
            float modifiedCollectibleChance = ActiveBuffs.ModifyByBVS(action.collectibleBase, bvs);
            int CollectibleTokenAdd = GenerateCollectibleToken(modifiedCollectibleChance);
            AddCollectibleToken(CollectibleTokenAdd);
            if (modifiedCollectibleChance > 0)
            {
                gameplayData.collectibleRolled = true;
            }
        }
        //int token2add = action.collectibleTrigger ? GenerateCollectibleToken(action.collectibleBase) : 0;
        //AddCollectibleToken(token2add);

        //add danger tokens (new)
        bool minorDangerTriggered = false;
        if (action.dangerTrigger)
        {
            TotalBuffValueSet bvs = ActiveBuffs.controller.RetrieveValueSetByAction("park_danger_base");
            float modifiedDangerChance = ActiveBuffs.ModifyByBVS(action.dangerBase, bvs);
            minorDangerTriggered = GenerateDangerToken(modifiedDangerChance) > 0;
            if(modifiedDangerChance > 0)
            {
                gameplayData.dangerRolled = true;
            }
        }
        //charge all the cost upfront
        //restore stamina if danger triggered
        if (gameplayData.dangerTriggered)
        {
            TotalBuffValueSet restoreBVS = ActiveBuffs.controller.RetrieveValueSetByAction("park_danger_stamina_restore");
            int staminaRestore = ActiveBuffs.ModifyByBVS(0, restoreBVS);
            RestoreStamina(staminaRestore);
        }
        //stamina -
        int staminaCost = action.modifiedStaminaRange.RandomRollByDefaultStep();
        RemoveStamina(staminaCost);
        //Persona_Recover_Action_Stamina 1
        if (staminaCost == 1 && ActiveBuffs.controller.CheckFeatureGate("feature_gate_persona_recover_action_sta1"))
        {
            CharacterPersona.controller.Persona_Recover_Action_Sta1();
        }
        //progress +
        int progressAdd = action.modifiedProgressRange.RandomRollByDefaultStep();
        //Persona_Recover_Progress60
        if (progressAdd >= 60 && ActiveBuffs.controller.CheckFeatureGate("feature_gate_persona_recover_progress60"))
        {
            CharacterPersona.controller.Persona_Recover_Progress60();
        }
        bool enoughProgress = IncreaseProgress(progressAdd);
        //check stamina then reset statmina no less than zero
        bool enoughStamina = gameplayData.staminaCur >= 0;
        
        gameplayData.staminaCur = Mathf.Max(0, gameplayData.staminaCur);

        //deactivate buffs that last one round/action
        if (!action.isDanger)
        {
            ActiveBuffs.controller.TimerCountByProvider("in_park_action", 1);
        }
        //ActiveBuffs.controller.DeactivateFrom("in_park_pre_action");
        //apply buff on the action
        float rng = Random.Range(0, 1f);
        if ((rng < action.buffChance))
        {
            //Debug.Log("post action applied");
            for (int i = 0; i < action.buffUID.Count; i++)
            {
                if(action.buffUID[i] != "")
                {
                    ApplyBuff(action.UID, action.buffScope[i], action.buffUID[i], action.buffValue[i], action.buffDuration[i]);
                }
            }
        }
        //character 2 persona 3
        if (ActiveBuffs.controller.CheckFeatureGate("feature_gate_persona_discover_p20"))
        {
            CharacterPersona.controller.Persona_Discover_P20();
        }
        //character 2 persona 5
        if(gameplayData.actionPlayedInRound == 2 && ActiveBuffs.controller.CheckFeatureGate("feature_gate_persona_action_combo3"))
        {
            CharacterPersona.controller.Persona_Action_Combo3();
        }

        gameplayData.staminaDepleted = !enoughStamina;
        //determine status after playing action
        if (!enoughStamina && !enoughProgress)
        {
            ActionPhase_ForceEndVisit();
        }
        else
        {
            
            if (gameplayData.curStatus == dConstants.ParkConst.ParkStatus.inDanger)
            {
                if (!CheckIfInDanger())
                {
                    ActionPhase_ResolveDanger();
                }
            }
            if (action.forward)
            {
                ActionPhase_Forward();
            }
            if (gameplayData.dangerTriggered)
            {
                ActionPhase_AddMajorDanger();
            }
            else if (minorDangerTriggered)
            {
                ActionPhase_AddMinorDanger();
            }
            else
            {
                if (enoughProgress)
                {
                    ActionPhase_ArriveNextNode();
                }
            }          
        }
    }
    int GenerateDangerToken(float modifiedActionDangerBase)
    {
        float rng = Random.value;
        //Debug.Log(string.Format("rolling a danger chance under ({0}), result is {1}", modifiedDangerChance, rng));
        return rng < modifiedActionDangerBase ? 1 : 0;
    }
    int GenerateCollectibleToken(float actionCollectibleBase)
    {
        float rng = Random.value;
        TotalBuffValueSet bvs = ActiveBuffs.controller.RetrieveValueSetByAction("park_collectible_base");
        float modifiedCollectibleChance = ActiveBuffs.ModifyByBVS(actionCollectibleBase, bvs);
        //Debug.Log(string.Format("rolling a danger chance under ({0}), result is {1}", modifiedDangerChance, rng));
        return rng < modifiedCollectibleChance ? 1 : 0;
    }
    void ActionPhase_ForceEndVisit()
    {
        //called when stamina is negative
        gameplayData.curStatus = dConstants.ParkConst.ParkStatus.onRoute;
        ResetActionPool();
        List<string> card = new List<string>();
        gameplayData.totalScenes += 1;

        if (ActiveBuffs.controller.CheckFeatureGate("feature_gate_persona_unfinished_park"))
        {
            //card.Add("parkaction_persona_unfinished_park");
            CharacterPersona.controller.Persona_End_Park_Not_Final();
            
        }
        card.Add("parkaction_end");
        AddAction2Pool(card);
        if (ActiveBuffs.controller.CheckFeatureGate("feature_gate_help_end_park_no_stamina"))
        {
            CharacterAcquisition.controller.AcquisitionCheck_End_Park_No_Stamina();
        }
    }
    void ActionPhase_ArriveNextNode()
    {
        //called when progress is full
        ResetActionPool();
        List<string> card = new List<string>();
        card.Add("parkaction_arrive");
        AddAction2Pool(card);
        gameplayData.totalScenes += 1;
        gameplayData.curStatus = dConstants.ParkConst.ParkStatus.onArrival;
    }
    void ActionPhase_AddMinorDanger()
    {
        //called when danger check is true
        List<string> dangerCard = PFEntryDeck.DrawCardswConditions(
            dDataHub.hub.dParkDangerAction.GetPFEntrywGateList(),
            1,
            gameplayData.AvailableCardTags,
            new intRange(1, 3),
            gameplayData.removedActions
            );
        AddAction2Pool(dangerCard);
        gameplayData.addedDangerActionIndex = gameplayData.dangerActionPool.Count - 1;
        //gameplayData.totalDangers += 1;
        //gameplayData.curDangerToken = 0;
        if (ActiveBuffs.controller.CheckFeatureGate("feature_gate_persona_recover_minor_danger_trigger"))
        {
            CharacterPersona.controller.Persona_Recover_Minor_Danger();
        }
        if (ActiveBuffs.controller.CheckFeatureGate("feature_gate_help_danger_trigger"))
        {
            CharacterAcquisition.controller.AcquisitionCheck_Trigger_Danger();
        }
    }
    void ActionPhase_AddMajorDanger()
    {
        //called when danger check is true
        List<string> dangerCard = PFEntryDeck.DrawCardswConditions(
            dDataHub.hub.dParkDangerAction.GetPFEntrywGateList(),
            1,
            gameplayData.AvailableCardTags,
            new intRange(0, 0),
            gameplayData.removedActions
            );
        AddAction2Pool(dangerCard);
        gameplayData.totalDangers += 1;
        gameplayData.curDangerToken = 0;
        if (ActiveBuffs.controller.CheckFeatureGate("feature_gate_help_danger_trigger"))
        {
            CharacterAcquisition.controller.AcquisitionCheck_Trigger_Danger();
        }
    }
    void ActionPhase_Forward()
    {
        //one round end
        ActiveBuffs.controller.TimerCountByProvider("in_park_round", 1);
        gameplayData.itemUsedCurRound = 0;
        //called when a forward action is played
        gameplayData.totalScenes += 1;
        ResetActionPool();
        List<string> nextActionSet = PFEntryDeck.DrawCardswConditions(
            dDataHub.hub.dParkRegularAction.GetPFEntrywGateList(),
            gameplayData.ModifiedActionDrawCount,
            gameplayData.AvailableCardTags,
            new intRange(0, gameplayData.tierCur),
            gameplayData.removedActions
            );
        nextActionSet.Sort((a, b) => Random.value.CompareTo(Random.value));
        AddAction2Pool(nextActionSet);
        if(gameplayData.forwardActionPool.Count == 0)
        {
            gameplayData.stayActionPool.RemoveAt(0);
            AddAction2Pool(PFEntryDeck.DrawCardswConditions(
                    dDataHub.hub.dParkForwardAction.GetPFEntrywGateList(),
                    1,
                    gameplayData.AvailableCardTags,
                    new intRange(0, gameplayData.tierCur),
                    gameplayData.removedActions
            ));
        }
        ActionFallBackCheck();
        if(gameplayData.parkUID == dConstants.ParkConst.INTRO_PARK_UID)
        {
            gIntroParkScript.controller.SetActionOnRoute();
        }
            
    }
    void ActionPhase_ResolveDanger()
    {
        gameplayData.curStatus = dConstants.ParkConst.ParkStatus.onRoute;
        gameplayData.dangerIndex = -1;
    }
    void ActionPhase_GenerateCollectiblePopup()
    {
        gameplayData.popupViews.Clear();
        gameplayData.popupCollectibles.Clear();

        List<string> exclusiveList = new List<string>();
        exclusiveList.AddRange(gameplayData.acquiredViews);
        exclusiveList.AddRange(journalData.tokenViews);
        exclusiveList.AddRange(gameplayData.currentMap.RetrieveNodeByPfbUID(gameplayData.nextNodePfbUID).viewUIDs);
        List<string> viewPool = GenerateViewPool(new intRange(Mathf.RoundToInt((float)gameplayData.tierCur/2f), gameplayData.tierCur), 1, exclusiveList);
        gameplayData.popupViews.Add(viewPool[0]);
        //gameplayData.popupViews.Add(DrawViewByTier(Mathf.Max(1, gameplayData.tierCur)));
        gameplayData.curCollectibleToken = 0;
        //only support one collectible atm
        if (ActiveBuffs.controller.CheckFeatureGate("feature_gate_help_collectible_popup"))
        {
            CharacterAcquisition.controller.AcquisitionCheck_Trigger_Collectible_Popup();
        }
    }
    void EnterRoute()
    {
        //called when route selected
        gameplayData.curStatus = dConstants.ParkConst.ParkStatus.onRoute;
        ActionPhase_Forward();
    }
    void EnterNode()
    {
        //set curNode Unvisited 
        gameplayData.currentNodeViewed = false;

        //called when progress is full
        //OnMapResume.Raise();
        gameplayData.progressCur = 0;
        //gameplayData.totalScenes += 1;
        gameplayData.totalNodes += 1;

        //gather structure UID for (curNode, outcoming paths, next nodes)
        if (gameplayData.curPathPfbUID != "")
        {
            uiParkRoute routeObj = gameplayData.CurrentPathObj;
            gameplayData.curNodeDataUID = routeObj.outNode.nodeUID;
            gameplayData.curNodePfbUID = routeObj.outNode.pfbUID;
            gameplayData.visitedPathPfb.Add(gameplayData.curPathPfbUID);
            gameplayData.curPathPfbUID = "";
            gameplayData.inFinalNode = routeObj.outNode.isFinal;
            
        }
        ParkNode node = dDataHub.hub.dParkNode.GetFromUID(gameplayData.curNodeDataUID);
        gameplayData.tierCur = Mathf.Max(gameplayData.tierCur, node.tier);
        //generate route (node + path)
        GenerateRoutesByNodePfbUID(gameplayData.curNodePfbUID);
        if(gameplayData.staminaDepleted && !gameplayData.inFinalNode)
        {
            List<string> parkEndReward = new List<string>();
            if (ActiveBuffs.controller.CheckFeatureGate("feature_gate_persona_unfinished_park"))
            {
                parkEndReward.Add("parkreward_persona_unfinished_park");
            }
            else
            {
                parkEndReward.Add("parkreward_lv1_home");
            }
            if (ActiveBuffs.controller.CheckFeatureGate("feature_gate_help_end_park_no_stamina"))
            {
                CharacterAcquisition.controller.AcquisitionCheck_End_Park_No_Stamina();
            }
            gameplayData.CurrentNodeObj.SetNodeReward(parkEndReward);
        }
        else
        {
            GenerateRewardsByNodePfbUID(gameplayData.curNodePfbUID);
            
        }
        GenerateNextNodeCollectibleByNodePfbUID(gameplayData.curNodePfbUID);
        //check if any rewards are waitingto be selected
        uiParkNode nodePfb = gameplayData.CurrentNodeObj;
        if (nodePfb.rewardUIDs.Count + nodePfb.viewUIDs.Count > 0)
        {
            gameplayData.curStatus = dConstants.ParkConst.ParkStatus.onReward;
            //allow 2nd reward
            if (nodePfb.rewardUIDs.Count >= 2 && node.nodeType == "local" && ActiveBuffs.controller.CheckFeatureGate("feature_gate_persona_extra_reward_village"))
            {
                gameplayData.Allow2ndReward = true;
                CharacterPersona.controller.Persona_Enter_Local();
            }
            //allow redraw collectible reward
            if (nodePfb.viewUIDs.Count > 0 && ActiveBuffs.controller.CheckFeatureGate("feature_gate_persona_extra_collectible_redraw"))
            {
                gameplayData.AllowCollectibleRedraw = true;
                CharacterPersona.controller.Persona_Enter_Node();
            }
            //OnNodeStart.Raise();
        }
        else
        {
            EnableRouteSelection();
        }

        //add narrative to node
        string narrative = node.GetRandomNarrative;
        if (narrative != "")
        {
            nodePfb.AddNarrative(narrative);
        }


        if (gameplayData.parkUID == dConstants.ParkConst.INTRO_PARK_UID && !gameplayData.inFinalNode)
        {
            gIntroParkScript.controller.SetRouteOnMap();
        }
        //deactivate buff
        ActiveBuffs.controller.TimerCountByProvider("in_park_path", 1);
        ActiveBuffs.controller.DeactivateFrom("in_park_round");
        ActiveBuffs.controller.DeactivateFrom("in_park_action");
        

    }
    void GenerateAllNodeUIDs()
    {
        //used to generate all node uids
        List<string> finalNodeFilter = new List<string>();
        finalNodeFilter.Add("final");
        List<string> regularNodeFilter = new List<string>();
        regularNodeFilter.Add("regular");
        for (int i = 0; i < gameplayData.currentMap.nodes.Count; i++)
        {
            List<string> result =
                PFEntryDeck.DrawCardswConditions(
                    dDataHub.hub.dParkNode.GetPFEntrywGateList(),
                    1,
                    gameplayData.currentMap.nodes[i].isFinal ? finalNodeFilter : regularNodeFilter,
                    gameplayData.currentMap.nodes[i].tier
                    );
            if (result.Count == 1)
            {
                //setup node UID
                gameplayData.currentMap.nodes[i].SetNodeUID(result[0]);
            }
            else
            {
                Debug.LogError(string.Format("no valid park node can be found on pfbUID ({0})", gameplayData.currentMap.nodes[i].pfbUID));
            }
        }
    }
    void GenerateNextNodeCollectibleByNodePfbUID(string nodePfbUID, int addTier = 0)
    {
        uiParkNode nodeObj = gameplayData.currentMap.RetrieveNodeByPfbUID(nodePfbUID);
        List<string> tempExclusiveList = new List<string>();
        tempExclusiveList.AddRange(journalData.tokenViews);
        tempExclusiveList.AddRange(nodeObj.viewUIDs);
        List<string> viewPool = new List<string>();
        //collectible_level_buff
        TotalBuffValueSet collectibleBVS = ActiveBuffs.controller.RetrieveValueSetByAction("node_collectible_tier");
        ActiveBuffs.controller.TimerCountByProvider("in_park_collectible", 1);
        int additionalTier = ActiveBuffs.ModifyByBVSNegative(addTier, collectibleBVS);
        //generate details for all following nodes
        for (int i = 0; i < nodeObj.outRoutes.Count; i++)
        {
            if (gameplayData.currentMap.routes.Contains(nodeObj.outRoutes[i]))
            {
                ParkNode nextNodeData = dDataHub.hub.dParkNode.GetFromUID(nodeObj.outRoutes[i].outNode.nodeUID);
                uiParkNode nextNodeObj = gameplayData.currentMap.RetrieveNodeByPfbUID(nodeObj.outRoutes[i].outNode.pfbUID);
                if(nextNodeObj.viewUIDs.Count == 0)
                {
                    viewPool = GenerateViewPool(
                    new intRange(nextNodeObj.collectibleBaseTier + additionalTier, nextNodeObj.collectibleBaseTier + additionalTier),
                    nextNodeData.collectibleCount,
                    tempExclusiveList);
                    nodeObj.outRoutes[i].outNode.SetViewOnNode(viewPool);
                    tempExclusiveList.AddRange(viewPool);
                }
            }   
        }
    }
    void GenerateRoutesByNodePfbUID(string nodePfbUID)
    {
        uiParkNode curNodePfb = gameplayData.currentMap.RetrieveNodeByPfbUID(nodePfbUID);
        //ParkNode curNode = dDataHub.hub.dParkNode.GetFromUID(curNodePfb.nodeUID);
        List<string> usedPathUIDs = new List<string>();
        List<string> applicableFilter = gameplayData.parkXLSData.terrains;
        
        for (int i = 0; i < curNodePfb.outRoutes.Count; i++)
        {
            if (gameplayData.currentMap.routes.Contains(curNodePfb.outRoutes[i]))
            {
                uiParkNode targetNodePfb = gameplayData.currentMap.RetrieveNodeByPfbUID(curNodePfb.outRoutes[i].outNode.pfbUID);
                //int RouteTier = curNodePfb.collectibleBaseTier + Mathf.Max(0, (targetNodePfb.collectibleBaseTier - curNodePfb.collectibleBaseTier - 1));
                int RouteTier = Mathf.Max(1,
                    curNodePfb.layer + 1
                    + (targetNodePfb.layer - curNodePfb.layer - 1) * 2
                    + (targetNodePfb.collectibleBaseTier - targetNodePfb.layer));
                //+ (targetNodePfb.collectibleBaseTier - curNodePfb.collectibleBaseTier - 1);
                /*Debug.Log(string.Format("generate route on node {0} tier of {1} = {2} + {3} + {4}", targetNodePfb.pfbUID,
                    RouteTier,
                    curNodePfb.layer + 1, 
                    (targetNodePfb.layer - curNodePfb.layer - 1),
                    (targetNodePfb.collectibleBaseTier - curNodePfb.collectibleBaseTier - 1)));*/
                List<string> curPathUID = PFEntryDeck.DrawCardswConditions(
                    dDataHub.hub.dParkPath.GetPFEntrywGateList(),
                    1,
                    applicableFilter,
                    new intRange(RouteTier, RouteTier),
                    usedPathUIDs
                    );
                usedPathUIDs.Add(curPathUID[0]);
                curNodePfb.outRoutes[i].SetPath(curPathUID[0]);
            }
        }
    }
    void GenerateRewardsByNodePfbUID(string nodePfbUID)
    {
        uiParkNode nodeObj = gameplayData.currentMap.RetrieveNodeByPfbUID(nodePfbUID);
        ParkNode nodeData = dDataHub.hub.dParkNode.GetFromUID(nodeObj.nodeUID);
        List<string> rewardPool = new List<string>();
        if (nodeData.rewardCount > 0)
        {
            //generate park reward UIDs
            List<string> eligibleTypes = new List<string>();
            eligibleTypes.Add(nodeData.nodeType);
            rewardPool.AddRange(
                PFEntryDeck.DrawCardswConditions(
                    dDataHub.hub.dParkReward.GetPFEntrywGateList(),
                    nodeData.rewardCount,
                    eligibleTypes,
                    new intRange(nodeObj.isFinal ? nodeObj.collectibleBaseTier : 1, nodeObj.collectibleBaseTier))
                    );
        }
        if (nodeData.nodeType == "final") 
        {
            if (ActiveBuffs.controller.CheckFeatureGate("feature_gate_help_finished_park"))
            {
                float rng = Random.Range(0f, 1f);
                if (rng > 0.5f)
                {
                    //rewardPool.Add("parkreward_token2");
                    CharacterAcquisition.controller.AcquisitionCheck_Character2();
                }
            }
            if (ActiveBuffs.controller.CheckFeatureGate("feature_gate_persona_finished_park"))
            {
                //rewardPool.Add("parkreward_extra_star");
                CharacterPersona.controller.Persona_End_Park_Is_Final();
            }
        }
        nodeObj.SetNodeReward(rewardPool);
    }
    List<string> GenerateViewPool(intRange tierRange, int count, List<string> additionalExclusiveList)
    {
        //set tier range between 1 to 5
        tierRange.minInt = Mathf.Max(1, Mathf.Min(5, tierRange.minInt));
        tierRange.maxInt = Mathf.Max(1, Mathf.Min(5, tierRange.maxInt));
        
        List<string> viewPool = new List<string>();
        List<string> exclusiveList = new List<string>(gameplayData.acquiredViews);
        exclusiveList.AddRange(additionalExclusiveList);
        viewPool = PFEntryDeck.DrawCardswConditions(
                    dDataHub.hub.dView.GetModifiedPFEntrywGateList(),
                    count,
                    gameplayData.parkXLSData.terrains,
                    tierRange,
                    exclusiveList
                );
        if (viewPool.Count < count)
        {
            Debug.LogError(string.Format("database do not have enough collectibles for the current park (uid:{0}, tier:{1}-{2})", gameplayData.parkUID, tierRange.minInt, tierRange.maxInt));
            viewPool.Add("view_kong");
        }
        return viewPool;
    }
    /*
    string DrawViewByTier(int nodeTier)
    {
        TotalBuffValueSet bvs = ActiveBuffs.controller.RetrieveValueSetByAction("node_collectible_tier");
        ActiveBuffs.controller.TimerCountByProvider("in_park_collectible", 1);
        List<string> exclusiveList = new List<string>();
        exclusiveList.AddRange(gameplayData.acquiredViews);
        exclusiveList.AddRange(journalData.tokenViews);
        int targetTier = Mathf.Max(1, ActiveBuffs.ModifyByBVS(nodeTier, bvs));
        if (targetTier != nodeTier)
        {
            Debug.Log(string.Format("A collectible is updated by buffs as {0} -> {1}", nodeTier, targetTier));
        }
        List<string> viewResult = new List<string>();
        while (targetTier >= 0)
        {
            viewResult = PFEntryDeck.DrawCardswConditions(
                    dDataHub.hub.dView.GetModifiedPFEntrywGateList(),
                    1,
                    gameplayData.parkXLSData.terrains,
                    new intRange(targetTier, targetTier),
                    exclusiveList
                );
            if (viewResult.Count > 0)
            {
                break;
            }
            else
            {
                targetTier -= 1;
            }
        }
        if (viewResult.Count == 0)
        {
            Debug.LogError(string.Format("data base do not have a eligible view for the current park (uid:{0}, tier:{1}", gameplayData.parkUID, nodeTier));
            return "card_tier4_kong";
        }
        return viewResult[0];
    }
    */
    void EnableRouteSelection()
    {
        if (gameplayData.staminaDepleted)
        {
            FinishPark();
        }
        else
        {
            gameplayData.curStatus = dConstants.ParkConst.ParkStatus.nextNode;
            //character  2 person 2
            //ParkNode node = dDataHub.hub.dParkNode.GetFromUID(gameplayData.curNodeDataUID);
            /*
            if (node.nodeType == "local")
            {
                CharacterPersona.controller.Character2_PersonaLv2();
            }*/
            if(gameplayData.CurrentNodeObj.narrative != "")
            {
                NarrativeEffect.controller.QueueCaption(gameplayData.CurrentNodeObj.narrative+"。");
            }
            OnMapChanged.Raise();
            OnNodeEnd.Raise();
        }
    }
    void RevealNextMap()
    {
        uiParkNode nodePfb = gameplayData.CurrentNodeObj;
        foreach (uiParkRoute route in nodePfb.outRoutes)
        {
            if(route.outNode != null)
            {
                GenerateNextNodeCollectibleByNodePfbUID(route.outNode.pfbUID);
                foreach (uiParkRoute route2 in route.outNode.outRoutes)
                {
                    if (route2.outNode != null)
                    {
                        route2.outNode.SetNodeReveal(true);
                    }
                }
            }
        }
        OnMapChanged.Raise();
    }
    void PreviewAction(string actionUID)
    {
        ParkPathAction action = dDataHub.hub.dParkAction.GetFromUID(actionUID);
        int staminaCost = action.modifiedStaminaRange.GetMedian();
        int progressAdd = action.modifiedProgressRange.GetMedian();
        //bool enoughProgress = (progressAdd + gameplayData.progressCur) >= gameplayData.progressMax;
        //bool enoughStamina = gameplayData.staminaCur >= staminaCost;
        //int potentialCollectibleToken = action.coll;
        //int potentialDangerToken = 0;
        gameplayData.progressPreview = progressAdd + gameplayData.progressCur;
        gameplayData.staminaPreview = gameplayData.staminaCur - staminaCost;
        //gameplayData.previewDangerToken = gameplayData.curDangerToken + potentialDangerToken;
        //gameplayData.previewCollectibleToken = gameplayData.curCollectibleToken + potentialCollectibleToken;
    }
    public void ApplyBuff(string sourceUID, string providerUID, string buffUID, string buffValue, int buffDuration)
    {
        int bfVInt = 0;
        int.TryParse(buffValue, out bfVInt);
        //determine if a buff is a park specific effect
        bool isRegularBuff = false;
        switch (buffUID)
        {
            case "park_end":
                FinishPark();
                break;
            case "stamina_add":
                RestoreStamina(bfVInt);
                break;
            case "progress_add":
                IncreaseProgress(bfVInt);
                break;
            case "draw_stay_action":
                AddAction2Pool(PFEntryDeck.DrawCardswConditions(
                    dDataHub.hub.dParkStayAction.GetPFEntrywGateList(),
                    bfVInt,
                    gameplayData.AvailableCardTags,
                    new intRange(0, gameplayData.tierCur),
                    gameplayData.removedActions
                ));
                break;
            case "reveal_map":
                RevealNextMap();
                break;
            case "draw_forward_action":
                AddAction2Pool(PFEntryDeck.DrawCardswConditions(
                    dDataHub.hub.dParkForwardAction.GetPFEntrywGateList(),
                    bfVInt,
                    gameplayData.AvailableCardTags,
                    new intRange(0, gameplayData.tierCur),
                    gameplayData.removedActions
                ));
                break;
            case "draw_random_action":
                AddAction2Pool(PFEntryDeck.DrawCardswConditions(
                    dDataHub.hub.dParkRegularAction.GetPFEntrywGateList(),
                    bfVInt,
                    gameplayData.AvailableCardTags,
                    new intRange(0, gameplayData.tierCur),
                    gameplayData.removedActions
                ));
                break;
            case "draw_specific_action":
                List<string> cardUID = new List<string>();
                cardUID.Add(buffValue);
                AddAction2Pool(cardUID);
                break;
            case "add_danger_counter":
                AddDangerToken(bfVInt);
                break;
            case "add_collectible_counter":
                AddCollectibleToken(bfVInt);
                break;
            case "redraw_collectible":
                //should only be used in node reward with one collectible
                if (!gameplayData.CollectibleResolved)
                {
                    List<string> exlucdingList = new List<string>(gameplayData.acquiredViews);
                    exlucdingList.AddRange(gameplayData.CurrentNodeObj.viewUIDs);
                    exlucdingList.AddRange(journalData.tokenViews);
                    int collectibleCount = gameplayData.CurrentNodeObj.viewUIDs.Count;
                    int targetTier = dDataHub.hub.dView.GetFromUID(gameplayData.CurrentNodeObj.viewUIDs[0]).tier;
                    gameplayData.CurrentNodeObj.viewUIDs = GenerateViewPool(new intRange(targetTier, targetTier), collectibleCount, exlucdingList);
                    gameplayData.CurrentNodeObj.viewClaimed.Clear();
                    for (int i=0;i< gameplayData.CurrentNodeObj.viewUIDs.Count; i++)
                    {
                        gameplayData.CurrentNodeObj.viewClaimed.Add(false);
                    }
                    OnNodeChanged.Raise();
                }
                Debug.Log("deprecated function redarw_collectible, to do");
                break;
            case "skip_collectible":
                //should only be used in node reward
                if (!gameplayData.CollectibleResolved)
                {
                    SkipCollectible();
                    UpdateNodeCollectible();
                }
                break;
            case "update_node_collectible":
                //should only be used in node reward
                UpdateNodeCollectible();
                break;
            case "redraw_action":
                PlayAction("parkaction_skip");
                break;
            case "restore_equipment":
                RestoreEquipment(bfVInt);
                break;
            default:
                isRegularBuff = true;
                break;
        }
        if (isRegularBuff)
        {
            //this should be a regular buff
            float value = 0;
            float.TryParse(buffValue, out value);
            ActiveBuffs.controller.ActivateBuff(sourceUID, providerUID, buffUID, value, buffDuration);
        }
    }
    public void FinishPark()
    {
        //call on exit park start
        gameplayData.curStatus = dConstants.ParkConst.ParkStatus.parkEnd;
        OnRouteEnd.Raise();
        //OnMapResume.Raise();
        OnSumamryStart.Raise();
        AddViewArts2Chronicle();
        //clear buffs
        ActiveBuffs.controller.DeactivateFrom("in_park_path");
        ActiveBuffs.controller.DeactivateFrom("in_park_round");
        ActiveBuffs.controller.DeactivateFrom("in_park_action");
        if (gameplayData.staminaReserved > 0)
        {
            ActiveBuffs.controller.ActivateBuff("parkaction_endearly", "park_visit", "buff_max_stamina_add", gameplayData.staminaReserved, 2);
        }
    }
    public void ExitPark()
    {
        //call on exit park confimation
        //CurrencyManagement.wallet.AddCurrency("currency_xp", gameplayData.totalProgress);
        gChronicle.controller.ConcludeCurrentAction();
        ParkVisitEnd.Raise();
        gameplayData.Reset();
        //player test music
        BackgroundMusicPlayer.mp3.PlayMusic("village");
    }
    void RestoreEquipment(int amount)
    {
        List<int> eligibleEquipmentIndex = new List<int>();
        for(int i = 0; i < gameplayData.equipmentCount.Count; i++)
        {
            if(gameplayData.equipmentCount[i] == 0)
            {
                eligibleEquipmentIndex.Add(i);
            }
        }
        eligibleEquipmentIndex.Sort((a, b) => Random.value.CompareTo(Random.value));
        for(int i = 0; i < eligibleEquipmentIndex.Count; i++)
        {
            if(i < amount)
            {
                gameplayData.equipmentCount[eligibleEquipmentIndex[i]] += 1;
            }
        }
    }
    void AddViewArts2Chronicle()
    {
        for(int i=0;i< gameplayData.acquiredCollectibles.Count; i++)
        {
            Collectible card = dDataHub.hub.dCollectible.GetFromUID(gameplayData.acquiredCollectibles[i]);
            gChronicle.controller.AddArtForCurrentAction(card.artAsset.objectArt, true);
        }
    }
    public void ResetActionPool()
    {
        gameplayData.stayActionPool.Clear();
        gameplayData.forwardActionPool.Clear();
        gameplayData.dangerActionPool.Clear();
        gameplayData.stayActionPlayed.Clear();
        gameplayData.forwardActionPlayed.Clear();
        gameplayData.dangerActionPlayed.Clear();
    }
    public void AddAction2Pool(List<string> actions)
    {
        foreach(string uid in actions)
        {
            ParkPathAction action = dDataHub.hub.dParkAction.GetFromUID(uid);
            if (action.isDanger)
            {
                gameplayData.dangerActionPool.Add(uid);
                gameplayData.dangerActionPlayed.Add(false);
            }
            else if (action.forward)
            {
                gameplayData.forwardActionPool.Add(uid);
                gameplayData.forwardActionPlayed.Add(false);
            }
            else
            {
                gameplayData.stayActionPool.Add(uid);
                gameplayData.stayActionPlayed.Add(false);
            }
        }
        CheckExceedCards();
        if (CheckIfInDanger())
        {
            gameplayData.curStatus = dConstants.ParkConst.ParkStatus.inDanger;
        }
    }
    bool CheckIfInDanger()
    {
        bool inDanger = false;
        for (int i = 0; i < gameplayData.dangerActionPool.Count; i++)
        {
            if (!gameplayData.dangerActionPlayed[i])
            {
                inDanger = true;
                gameplayData.dangerIndex = i;
            }
        }
        return inDanger;
    }
    
    void RemoveStamina(int staCost)
    {
        gameplayData.staminaCur -= staCost;
        gameplayData.staminaCur = Mathf.Min(gameplayData.staminaCur, gameplayData.staminaMax);
        gameplayData.staminaDelta -= staCost;
        //Debug.Log("cur statmina is" + gameplayData.staminaCur);
    }
    public bool RestoreStamina(int staRes)
    {
        int staminaActualAdd = Mathf.Min(staRes, gameplayData.staminaMax - gameplayData.staminaCur);
        gameplayData.staminaCur += staminaActualAdd;
        gameplayData.staminaDelta += staRes;
        OnMapChanged.Raise();
        return true;
    }
    bool IncreaseProgress(int progressAdd)
    {
        bool arrival = false;
        int progressGain = progressAdd > 0 ? progressAdd : 0;
        int progressActualAdd = Mathf.Min(progressAdd, gameplayData.progressMax - gameplayData.progressCur);
        gameplayData.totalProgress += progressGain;
        CurrencyManagement.wallet.AddCurrency("currency_xp", progressGain);
        gameplayData.progressCur += progressAdd;
        gameplayData.progressDelta += progressActualAdd;
        if (gameplayData.progressCur >= gameplayData.progressMax)
        {
            arrival = true;
        }
        gameplayData.progressCur = Mathf.Max(Mathf.Min(gameplayData.progressCur, gameplayData.progressMax), 0);
        
        return arrival;
    }
    void AddDangerToken(int count)
    {
        gameplayData.curDangerToken += count;
        gameplayData.curDangerToken = Mathf.Max(0, Mathf.Min(gameplayData.maxDangerToken, gameplayData.curDangerToken));
        //gameplayData.latestDangerTokenAcquired = count;
        gameplayData.dangerTokenDelta += count;
        if(ActiveBuffs.controller.CheckFeatureGate("feature_gate_persona_recover_addtion_two_danger_token"))
        {
            if(gameplayData.curDangerToken == 2)
            {
                CharacterPersona.controller.Persona_Recover_Addition_Danger2_Activate();
            }
            else
            {
                CharacterPersona.controller.Persona_Recover_Addition_Danger2_Deactivate();
            }
        }
    }
    void AddCollectibleToken(int count)
    {
        gameplayData.curCollectibleToken += count;
        gameplayData.curCollectibleToken = Mathf.Max(0, Mathf.Min(gameplayData.maxCollectibleToken, gameplayData.curCollectibleToken));
        //gameplayData.latestCollectibleTokenAcquired = count;
        gameplayData.collectibleTokenDelta += count;
        if (gameplayData.collectibleTriggered)
        {
            ActionPhase_GenerateCollectiblePopup();
        }
    }
    public void ResetLatestActionResult()
    {
        gameplayData.staminaDelta = 0;
        gameplayData.progressDelta = 0;
        gameplayData.dangerTokenDelta = 0;
        gameplayData.collectibleTokenDelta = 0;
        gameplayData.addedDangerActionIndex = -1;
        gameplayData.dangerRolled = false;
        gameplayData.collectibleRolled = false;
    }
    List<string> ShowView(string viewUID)
    {
        View view = dDataHub.hub.dView.GetFromUID(viewUID);
        List<string> collectibleResults = PFEntryDeck.DrawCardswConditions(
            dDataHub.hub.dCollectible.GetPFEntrywGateList(view.collectibleOptions),
            dConstants.ParkConst.COLLECTIBLE_OPTION_COUNT,
            gameplayData.parkXLSData.terrains,
            new intRange(view.tier, view.tier)
            );
        return collectibleResults;
    }
    bool GetCollectible(string cardUID)
    { 
        bool added = false;
        added = gJournal.controller.AddToken(cardUID);
        if (added)
        {
            if (ActiveBuffs.controller.CheckFeatureGate("feature_gate_persona_recover_acquireview"))
            {
                CharacterPersona.controller.Persona_Recover_AcquireView();
            }
            if (ActiveBuffs.controller.CheckFeatureGate("feature_gate_help_acquire_view"))
            {
                Collectible col = dDataHub.hub.dCollectible.GetFromUID(cardUID);
                CharacterAcquisition.controller.AcquisitionCheck_Acquire_Collectible(col.tier);
            }
            gameplayData.acquiredCollectibles.Add(cardUID);
            //CheckBonusInParkByCollectible(cardUID);
        }
        //gameplayData.viewAcquired += 1;
        return added;
    }
    void ActionFallBackCheck()
    {
        if(gameplayData.forwardActionPool.Count == 0)
        {
            AddAction2Pool(PFEntryDeck.DrawCardswConditions(
                    dDataHub.hub.dParkForwardAction.GetPFEntrywGateList(),
                    1,
                    gameplayData.AvailableCardTags,
                    new intRange(0, gameplayData.tierCur)
            ));
            Debug.LogError(string.Format("no eligible action forward card in the next set. Fallback card ({0}) added.", gameplayData.forwardActionPool[gameplayData.forwardActionPool.Count-1]));
        }
    }
    string DrawViewByNode(ParkNode nodeData)
    {
        TotalBuffValueSet bvs = ActiveBuffs.controller.RetrieveValueSetByAction("collectible_upgrade");
        ActiveBuffs.controller.TimerCountByProvider("in_park_collectible", 1);
        int targetTier = ActiveBuffs.ModifyByBVS(nodeData.tier, bvs);
        if(targetTier != nodeData.tier)
        {
            Debug.Log(string.Format("A collectible is updated by buffs as {0} -> {1}", nodeData.tier, targetTier));
        }
        List<string> viewResult = new List<string>();
        while (targetTier >= 0)
        {
            viewResult = PFEntryDeck.DrawCardswConditions(
                    dDataHub.hub.dCollectible.GetModifiedPFEntrywGateList(),
                    1,
                    gameplayData.parkXLSData.terrains,
                    new intRange(targetTier, targetTier),
                    gameplayData.acquiredViews
                );
            if (viewResult.Count > 0)
            {
                break;
            }
            else
            {
                targetTier -= 1;
            }
        }
        if(viewResult.Count == 0)
        {
            Debug.LogError(string.Format("data base do not have a eligible view for the current park (uid:{0}, tier:{1}", gameplayData.parkUID, nodeData.tier));
            return "card_0_kong";
        }
        return viewResult[0];
    }

    void CheckExceedCards()
    {
        int exceededStay = gameplayData.stayActionPool.Count - dConstants.ParkConst.MAX_STAY_CARD;
        int exceededForward = gameplayData.forwardActionPool.Count - dConstants.ParkConst.MAX_FORWARD_CARD;
        int exceededDanger = gameplayData.dangerActionPool.Count - dConstants.ParkConst.MAX_DANGER_CARD;

        //discard exceeding cards and redraw cards on empty slots (ignoring stay or forward)
        if (exceededStay > 0 && exceededForward < 0)
        {
            //remove exceed stay cards
            gameplayData.stayActionPool.RemoveRange(dConstants.ParkConst.MAX_STAY_CARD, exceededStay);
            //add forward cards
            AddAction2Pool(PFEntryDeck.DrawCardswConditions(
                    dDataHub.hub.dParkForwardAction.GetPFEntrywGateList(),
                    exceededStay,
                    gameplayData.AvailableCardTags,
                    new intRange(0, gameplayData.tierCur),
                    gameplayData.removedActions
                ));
        }
        if (exceededForward > 0 && exceededStay < 0)
        {
            //remove exceed forward cards
            gameplayData.forwardActionPool.RemoveRange(dConstants.ParkConst.MAX_FORWARD_CARD, exceededForward);
            //add stay cards
            AddAction2Pool(PFEntryDeck.DrawCardswConditions(
                    dDataHub.hub.dParkStayAction.GetPFEntrywGateList(),
                    exceededForward,
                    gameplayData.AvailableCardTags,
                    new intRange(0, gameplayData.tierCur),
                    gameplayData.removedActions
                ));
        }
        /*Debug.Log(string.Format("action pool count ({0}) exceeds total card map supports ({1})", 
                    gameplayData.stayActionPool.Count + gameplayData.forwardActionPool.Count, 
                    dConstants.ParkConst.MAX_STAY_CARD + dConstants.ParkConst.MAX_FORWARD_CARD));*/

        //move the exceeding danger to override played danger
        if (exceededDanger > 0)
        {
            //start from moving the last danger in the pool
            int movingIndex = gameplayData.dangerActionPool.Count - 1;
            for (int i = 0; i < gameplayData.dangerActionPool.Count; i++)
            {
                if (movingIndex >= dConstants.ParkConst.MAX_DANGER_CARD && gameplayData.dangerActionPlayed[i])
                {
                    gameplayData.dangerActionPool[i] = gameplayData.dangerActionPool[movingIndex];
                    gameplayData.dangerActionPlayed[i] = false;

                    gameplayData.dangerActionPool.RemoveAt(movingIndex);
                    gameplayData.dangerActionPlayed.RemoveAt(movingIndex);
                    movingIndex -= 1;
                }
            }
        }
    }
    void LogPlayedAction(string playedUID)
    {
        bool found = false;
        for (int i = 0; i < gameplayData.playedActions.Count; i++)
        {
            if(gameplayData.playedActions[i].actionUID == playedUID)
            {
                found = true;
                gameplayData.playedActions[i].playedTimes += 1;
                if (gameplayData.playedActions[i].depleted && !gameplayData.depletedActions.Contains(playedUID))
                {
                    gameplayData.depletedActions.Add(playedUID);
                }
                break;
            }
        }
        if (!found)
        {
            ActionPlayed actionLog = new ActionPlayed();
            actionLog.actionUID = playedUID;
            actionLog.playedTimes = 1;
            ParkPathAction action = dDataHub.hub.dParkAction.GetFromUID(playedUID);
            actionLog.maxCount = action.maxCount;
            gameplayData.playedActions.Add(actionLog);
            if (actionLog.depleted && !gameplayData.depletedActions.Contains(playedUID))
            {
                gameplayData.depletedActions.Add(playedUID);
            }
        }
        return;
    }
    void SetSolarTerm()
    {
        //generate the UID of the solar term
        List<string> monthFilter = new List<string>();
        monthFilter.Add((CalendarManagement.calendar.GetCurDate().monthIndex+1).ToString());
        gameplayData.curSolarTermUID = PFEntryDeck.DrawCardswConditions(dDataHub.hub.dParkSolarTerm.GetPFEntrywGateList(),
            1,
            monthFilter)[0];
        ParkSolarTerm term = dDataHub.hub.dParkSolarTerm.GetFromUID(gameplayData.curSolarTermUID);
        for(int i = 0; i < term.buffUID.Count; i++)
        {
            if(term.buffUID[i] != "")
            {
                ApplyBuff(term.UID, term.buffScope[i], term.buffUID[i], term.buffValue[i], term.buffDuration[i]);
            }
        }
        //Raise Popup
        //OnTermStart.Raise();

    }
}
