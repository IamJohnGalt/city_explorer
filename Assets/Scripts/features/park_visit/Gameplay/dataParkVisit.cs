﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ParkRoute
{
    public string pathUID;
    public string nodeUID;
}
[System.Serializable]
public class ActionPlayed
{
    public string actionUID;
    public int playedTimes;
    public int maxCount;
    public bool depleted
    {
        get
        {
            return playedTimes >= maxCount;
        }
    }
    public ActionPlayed()
    {
        actionUID = "";
        playedTimes = 0;
        maxCount = int.MaxValue;
    }
}

[CreateAssetMenu(menuName = "Park/Gameplay/In-Session Data")]
public class dataParkVisit : ScriptableObject
{
    [Header("Basic")]
    public string nodeUID;
    public string parkUID;
    public string parkName;
    public Park parkXLSData;
    //public string seasonKeyword;

    [Header("Map")]
    public uiParkOnMap_Structure currentMap;
    public string curPathPfbUID;
    public string curNodePfbUID;
    public string nextNodePfbUID;
    public uiParkNode CurrentNodeObj { get { return currentMap.RetrieveNodeByPfbUID(curNodePfbUID); } }
    public uiParkRoute CurrentPathObj { get { return currentMap.RetrievePathByPfbUID(curPathPfbUID); } }

    [Header("Date")]
    public string curSolarTermUID;

    [Header("Status")]
    public bool inFinalNode;
    public bool currentNodeViewed;
    public string curPathDataUID;
    public string curNodeDataUID;
    public string nextNodeDataUID;
    public int actionCount;
    public int ModifiedActionDrawCount
    {
        get
        {
            TotalBuffValueSet bvs = ActiveBuffs.controller.RetrieveValueSetByAction("action_draw");
            int modifiedActionDraw = ActiveBuffs.ModifyByBVS(actionCount, bvs);
            return Mathf.Min(dConstants.ParkConst.MAX_STAY_CARD + dConstants.ParkConst.MAX_FORWARD_CARD, Mathf.Max(1,modifiedActionDraw));
        }
    }
    public List<string> AvailableCardTags
    {
        get
        {
            List<string> tags = new List<string>();
            tags.AddRange(parkXLSData.terrains);
            tags.Add(CharacterManagement.controller.GetCurrentCharacter().characterUID);
            tags.Add("shared");
            return tags;
        }
    }

    public dConstants.ParkConst.ParkStatus curStatus;
    public bool StatusIsOnRoute
    {
        get
        {
            return (curStatus == dConstants.ParkConst.ParkStatus.onRoute) || (curStatus == dConstants.ParkConst.ParkStatus.inDanger) || (curStatus == dConstants.ParkConst.ParkStatus.onArrival);
        }
    }
    public bool staminaDepleted;
    //public int dangerPool; // 0 = stay, 1 = forward
    public int dangerIndex;

    [Header("Progress")]
    public int staminaCur;
    public int staminaPreview;
    public int staminaMax;
    public float CurrentStaminaPercent
    {
        get { return Mathf.Max(0, Mathf.Min(1, (float)staminaCur / staminaMax)); }
    }
    public string CurrentActionStaminaAdditionString
    {
        get
        {
            int ActionStaminaAddition = Mathf.RoundToInt(ActiveBuffs.controller.RetrieveBuffValue("buff_action_stamina_add"));
            if (ActionStaminaAddition > 0)
            {
                return string.Format("消耗 +{0}", ActionStaminaAddition);
            }
            else if (ActionStaminaAddition < 0)
            {
                return string.Format("消耗 {0}", ActionStaminaAddition);
            }
            else
            {
                return "";
            }
        }
    }
    public int progressCur;
    public int progressPreview;
    public int progressMax;
    public bool hideProgress;
    public float CurrentProgressPercent
    {
        get { return Mathf.Max(0, Mathf.Min(1, (float)progressCur / progressMax)); }
    }
    public string CurrentProgressAdditionFlatString
    {
        get
        {
            int ProgressFlatAddition = Mathf.RoundToInt(ActiveBuffs.controller.RetrieveBuffValue("buff_action_progress_add"));
            if (ProgressFlatAddition > 0)
            {
                return string.Format("+{0}", ProgressFlatAddition);
            }
            else if (ProgressFlatAddition < 0)
            {
                return string.Format("{0}", ProgressFlatAddition);
            }
            else
            {
                return "";
            }
        }
    }
    public string CurrentProgressAdditionPercentString
    {
        get
        {
            float ProgressPercentAddition = ActiveBuffs.controller.RetrieveBuffValue("buff_action_progress_multi");
            if (ProgressPercentAddition > 0)
            {
                return string.Format("+{0}%", Mathf.RoundToInt(ProgressPercentAddition * 100));
            }
            else if (ProgressPercentAddition < 0)
            {
                return string.Format("{0}%", Mathf.RoundToInt(ProgressPercentAddition * 100));
            }
            else
            {
                return "";
            }
        }
    }
    //public float dangerBasePark;
    //public float dangerBasePath;

    public int tierCur;
    public int tierMax;
    public float CurrentTierPercent
    {
        get { return Mathf.Max(0, Mathf.Min(1, (float)tierCur / tierMax)); }
    }
    public string CurrentDangerAdditionString
    {
        get
        {
            TotalBuffValueSet bvs = ActiveBuffs.controller.RetrieveValueSetByAction("park_danger_base");
            float modifiedDangerBase = ActiveBuffs.ModifyByBVSNegative(0f, bvs);
            if (modifiedDangerBase > 0)
            {
                return string.Format("+{0}%", Mathf.RoundToInt(modifiedDangerBase * 100));
            }
            else if (modifiedDangerBase < 0)
            {
                return string.Format("-{0}%", Mathf.RoundToInt(-modifiedDangerBase * 100));
            }
            else
            {
                return "";
            }
            //Debug.Log(string.Format("current danger base is {0}%", Mathf.RoundToInt(modifieddangerBase * 100)));
        }
    }
    public int previewDangerToken;
    public int curDangerToken;
    public int maxDangerToken { get { return dConstants.ParkConst.MAX_DANGER_TOKEN; } }
    public bool dangerTriggered { get { return curDangerToken >= maxDangerToken; } }
    public string CurrentCollectibleAdditionString
    {
        get
        {
            TotalBuffValueSet bvs = ActiveBuffs.controller.RetrieveValueSetByAction("park_collectible_base");
            float modifiedCollectibleBase = ActiveBuffs.ModifyByBVSNegative(0f, bvs);
            if (modifiedCollectibleBase > 0)
            {
                return string.Format("+{0}%", Mathf.RoundToInt(modifiedCollectibleBase * 100));
            }
            else if (modifiedCollectibleBase < 0)
            {
                return string.Format("-{0}%", Mathf.RoundToInt(-modifiedCollectibleBase * 100));
            }
            else
            {
                return "";
            }
        }
    }
    public int previewCollectibleToken;
    public int curCollectibleToken;
    public int maxCollectibleToken { get { return dConstants.ParkConst.MAX_COLLECTIBLE_TOKEN; } }
    public bool collectibleTriggered { get { return curCollectibleToken >= dConstants.ParkConst.MAX_COLLECTIBLE_TOKEN; } }
    public bool collectible2Collect { get { return popupViews.Count > 0; } }
    public int itemUsedCurRound;

    [Header("Last Action")]
    public int staminaDelta;
    public int progressDelta;
    public int dangerTokenDelta;
    public int collectibleTokenDelta;
    public int addedDangerActionIndex;
    public bool dangerRolled;
    public bool collectibleRolled;

    [Header("Card Pool")]
    public List<string> popupViews = new List<string>();
    public List<string> popupCollectibles = new List<string>();
    public List<string> stayActionPool = new List<string>();
    public List<bool> stayActionPlayed = new List<bool>();
    public List<string> forwardActionPool = new List<string>();
    public List<bool> forwardActionPlayed = new List<bool>();
    public List<string> dangerActionPool = new List<string>();
    public List<bool> dangerActionPlayed = new List<bool>();

    public int actionPlayedInRound
    {
        get
        {
            int count = 0;
            for(int i=0;i< stayActionPlayed.Count; i++)
            {
                count += stayActionPlayed[i] ? 1 : 0;
            }
            for (int i = 0; i < forwardActionPlayed.Count; i++)
            {
                count += forwardActionPlayed[i] ? 1 : 0;
            }
            return count;
        }
    }

    public List<int> equipmentCount = new List<int>();
    public int equipmentPlayedCount
    {
        get
        {
            int used = 0;
            for(int i = 0; i < equipmentCount.Count; i++)
            {
                if(equipmentCount[i] == 0)
                {
                    used += 1;
                }
            }
            return used;
        }
    }
    public bool Allow2ndReward;
    public bool In2ndReward;
    public bool AllowCollectibleRedraw;
    public bool CurrentNodeResolved { get { return RewardClaimed && CollectibleResolved; } }
    public bool RewardClaimed
    {
        get
        {
            bool claimed = false;
            if (CurrentNodeObj == null)
            {
                return true;
            }
            if(CurrentNodeObj.rewardClaimed.Count == 0)
            {
                return true;
            }
            //claim one reward considered claimed in regular nodes
            //if (!CurrentNodeObj.isFinal)
            //{
            int claimedCount = 0;
            for (int i = 0; i < CurrentNodeObj.rewardClaimed.Count; i++)
            {
                if (CurrentNodeObj.rewardClaimed[i])
                {
                    claimedCount += 1;
                }
            }
            if (claimedCount >= 2 && In2ndReward)
            {
                claimed = true;
            }
            else if (claimedCount >= 1 && !In2ndReward)
            {
                claimed = true;
            }
            //}
            //claim all rewards considered claimed in final nodes
            /*else
            {
                bool allClaimed = true;
                for (int i = 0; i < CurrentNodeObj.rewardClaimed.Count; i++)
                {
                    if (CurrentNodeObj.rewardClaimed[i] == false)
                    {
                        allClaimed = false;
                    }
                }
                claimed = allClaimed;
            }*/
            //Debug.Log(string.Format("rewards check: {0}, collectible check: {1}", rewardClaimed, collectibleResolved));
            return claimed;
        }
    }
    public bool CollectibleResolved
    {
        get
        {
            if(CurrentNodeObj.viewClaimed.Count == 0)
            {
                return true;
            }
            bool viewRevealed = false;
            for (int i = 0; i < CurrentNodeObj.viewClaimed.Count; i++)
            {
                if (CurrentNodeObj.viewClaimed[i] == true)
                {
                    viewRevealed = true;
                }
            }
            if (!viewRevealed)
            {
                return false;
            }
            bool collectibleResolved = false;
            if (CurrentNodeObj.collectibleClaimed.Count == 0)
            {
                collectibleResolved = true;
            }
            for (int i = 0; i < CurrentNodeObj.collectibleClaimed.Count; i++)
            {
                if (CurrentNodeObj.collectibleClaimed[i] == true)
                {
                    collectibleResolved = true;
                }
            }
            return collectibleResolved;
        }
    }
    //public List<string> curRewardPool = new List<string>();
    //public List<bool> rewardClaimed = new List<bool>();
    //public List<string> nodePool = new List<string>();
    //public List<string> pathPool = new List<string>();
    //public List<ParkRoute> routePool = new List<ParkRoute>();

    [Header("History")]
    //public int latestDangerTokenAcquired;
    //public int latestCollectibleTokenAcquired;
    public List<ActionPlayed> playedActions = new List<ActionPlayed>();
    public List<string> playedEquipments = new List<string>();
    public List<string> removedActions
    {
        get
        {
            List<string> removedList = new List<string>();
            removedList.AddRange(stayActionPool);
            removedList.AddRange(forwardActionPool);
            removedList.AddRange(depletedActions);
            return removedList;
        }
    }
    public List<string> depletedActions = new List<string>();
    public List<string> acquiredCollectibles = new List<string>();
    public List<string> acquiredViews
    {
        get
        {
            List<string> sources = new List<string>();
            for(int i = 0; i < acquiredCollectibles.Count; i++)
            {
                sources.Add(dDataHub.hub.dCollectible.GetFromUID(acquiredCollectibles[i]).viewSource);
            }
            return sources;
        }
    }
    public List<string> visitedNodePfb = new List<string>();
    public List<string> visitedPathPfb = new List<string>();
    public int staminaReserved
    {
        get
        {
            if(tierMax > 1)
            {
                return Mathf.Min(dConstants.ParkConst.MAX_RESERVED_STAMINA, Mathf.FloorToInt(staminaCur * dConstants.ParkConst.RATIO_RESERVED_STAMINA));
            }
            else
            {
                return -1;
            }
        }
    }
    public int totalMonths;
    public int totalScenes;
    public int totalCards;
    public int totalNodes;
    public int totalProgress;
    public int totalDangers;
    public int totalModifiedProgress
    {
        get { return Mathf.CeilToInt(totalProgress * Mathf.PI); }
    }
    //deprecated
    /*public int totalXP
    {
        get
        {
            TotalBuffValueSet bvs = ActiveBuffs.controller.RetrieveValueSetByAction("xp_gain");
            //older version
            //int total = totalScenes * 2 + totalCards + totalNodes * 20 + totalProgress + totalDangers * 5;
            int total = totalProgress;
            return ActiveBuffs.ModifyByBVS(total, bvs);
        }
    }
    */
    public void Reset()
    {
        nodeUID = "";
        parkUID = "";
        parkName = "";
        parkXLSData = null;
        actionCount = dConstants.ParkConst.ACTION_DRAW_BASE;
        //seasonKeyword = "";

        curSolarTermUID = "";

        inFinalNode = false;
        currentNodeViewed = false;
        curPathDataUID = "";
        curNodeDataUID = "";
        nextNodeDataUID = "";

        curPathPfbUID = "";
        curNodePfbUID = "";
        nextNodePfbUID = "";

        curStatus = dConstants.ParkConst.ParkStatus.nextNode;
        staminaDepleted = false;
        //dangerPool = -1;
        dangerIndex = -1;

        staminaCur = 0;
        staminaPreview = 0;
        staminaMax = 1;
        progressCur = 0;
        progressPreview = 0;
        progressMax = 1;
        hideProgress = false;
        //dangerBasePark = 0f;
        //dangerBasePath = 0f;
        tierCur = 1;
        tierMax = 1;

        previewDangerToken = 0;
        curDangerToken = 0;
        previewCollectibleToken = 0;
        curCollectibleToken = 0;

        itemUsedCurRound = 0;
        
        staminaDelta = 0;
        progressDelta = 0;
        dangerTokenDelta = 0;
        collectibleTokenDelta = 0;
        addedDangerActionIndex = -1;
        dangerRolled = false;
        collectibleRolled = false;

        popupViews = new List<string>();
        popupCollectibles = new List<string>();
        stayActionPool = new List<string>();
        stayActionPlayed = new List<bool>();
        forwardActionPool = new List<string>();
        forwardActionPlayed = new List<bool>();
        dangerActionPool = new List<string>();
        dangerActionPlayed = new List<bool>();
        //curRewardPool = new List<string>();
        //rewardClaimed = new List<bool>();
        //nodePool = new List<string>();
        //pathPool = new List<string>();
        //routePool = new List<ParkRoute>();
        equipmentCount = new List<int>();

        Allow2ndReward = false;
        In2ndReward = false;
        AllowCollectibleRedraw = false;

        //latestDangerTokenAcquired = 0;
        //latestCollectibleTokenAcquired = 0;
        playedActions = new List<ActionPlayed>();
        playedEquipments = new List<string>();
        depletedActions = new List<string>();
        acquiredCollectibles = new List<string>();
        visitedNodePfb = new List<string>();
        visitedPathPfb = new List<string>();
        totalMonths = 0;
        totalScenes = 0;
        totalCards = 0;
        totalNodes = 0;
        totalProgress = 0;
        totalDangers = 0;
    }
}
