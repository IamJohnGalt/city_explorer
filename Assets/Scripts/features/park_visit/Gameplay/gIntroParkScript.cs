﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gIntroParkScript : MonoBehaviour
{
    public static gIntroParkScript controller;

    //public gJournal journalGameplay;
    public dataParkVisit gameplayData;
    public uiParkOnRoute RouteUI;
    public uiParkHeader HeaderUI;
    public uiParkOnSummary SunmmaryUI;
    public uiJournalPage JournalUI;

    public bool firstDanger;
    public bool firstCollectible;
    public bool firstOutOfStamina;
    public bool firstFinalNode;
    public bool firstEquipment;
    public bool skipIntro;

    [SerializeField] int parkIntroIndex = 0;
    [SerializeField] int journalIntroIndex = 0;

    private void Awake()
    {
        if (controller == null)
        {
            controller = this;
        }
        else if (controller != null)
        {
            Destroy(gameObject);
        }
        firstDanger = false;
        firstCollectible = false;
        firstOutOfStamina = false;
        firstFinalNode = false;
        firstEquipment = false;
        skipIntro = true;

        parkIntroIndex = 0;
        journalIntroIndex = 0;
    }

    public void SetRouteOnMap()
    {
        if (gameplayData.totalNodes == 1)
        {
            uiParkNode curNodePfb = gameplayData.currentMap.RetrieveNodeByPfbUID(gameplayData.curNodePfbUID);
            List<string> nodeView = new List<string>();
            nodeView.Add("view_qingkong");
            for (int i = 0; i < curNodePfb.outRoutes.Count; i++)
            {
                if (gameplayData.currentMap.routes.Contains(curNodePfb.outRoutes[i]))
                {
                    curNodePfb.outRoutes[i].SetPath("parkpath_kanke_t1");
                    curNodePfb.outRoutes[i].outNode.SetViewOnNode(nodeView);
                }
            }
        }
        else if (gameplayData.totalNodes == 2)
        {
            uiParkNode curNodePfb = gameplayData.currentMap.RetrieveNodeByPfbUID(gameplayData.curNodePfbUID);
            List<string> nodeView = new List<string>();
            nodeView.Add("view_niaoqun");
            for (int i = 0; i < curNodePfb.outRoutes.Count; i++)
            {
                if (gameplayData.currentMap.routes.Contains(curNodePfb.outRoutes[i]))
                {
                    curNodePfb.outRoutes[i].outNode.SetViewOnNode(nodeView);
                }
            }
        }
        //need to cover 2nd node in 
    }
    public void SetActionOnRoute()
    {
        if(gameplayData.totalScenes == 1)
        {
            gParkVisit.controller.ResetActionPool();
            List<string> nextActionSet = new List<string>();
            nextActionSet.Add("parkaction_shanlu");
            gParkVisit.controller.AddAction2Pool(nextActionSet);

            
        }
        else if(gameplayData.totalScenes == 2)
        {
            gParkVisit.controller.ResetActionPool();
            List<string> nextActionSet = new List<string>();
            nextActionSet.Add("parkaction_xiaolu");
            nextActionSet.Add("parkaction_lubiao");
            nextActionSet.Add("parkaction_gouhuo");
            gParkVisit.controller.AddAction2Pool(nextActionSet);

            
        }
        else if (gameplayData.totalScenes == 3)
        {
            
        }
        else
        {
            //Debug.LogError(string.Format("unscripted scene # as {0}", gameplayData.totalScenes));
        }
    }
    public void SetDialogAndMask()
    {
        if(parkIntroIndex > 13)
        {
            return;
        }
        if (gameplayData.curStatus == dConstants.ParkConst.ParkStatus.nextNode && gameplayData.curNodeDataUID == "parknode_lv0_start" && parkIntroIndex == 0)
        {
            //dialog info
            dataDialogPopup dialogData = uiDialogPopup.controller.AccessPopupData();
            dialogData.Reset();
            dialogData.avatar = "icon_avatar_0";
            dialogData.desc = "欢迎来到邻家小山，你是来这里旅行的人吧。<br>下面就让我这个过来人给你说说旅行的基础技巧吧。";
            dialogData.btn_text = "感谢";
            dialogData.requireConfirmation = true;
            dialogData.type = dataDialogPopup.DialogScenarios.park;
            uiDialogPopup.controller.SetUpDialog();

            //additional widgets
            //FocusMaskEffect.controller.MaskFocusOn(gameplayData.CurrentNodeObj.outRoutes[0].label_sign.transform, 1f);
            FocusMaskEffect.controller.TransparentMaskOn();

            parkIntroIndex = 1;
        }
        else if (gameplayData.curStatus == dConstants.ParkConst.ParkStatus.nextNode && gameplayData.curNodeDataUID == "parknode_lv0_start" && parkIntroIndex == 1)
        {
            //dialog info
            dataDialogPopup dialogData = uiDialogPopup.controller.AccessPopupData();
            dialogData.Reset();
            dialogData.avatar = "icon_avatar_0";
            dialogData.desc = "这是你来到此地所携带的<b>补给</b><sprite name=\"dynamic_current_stamina\">。" +
                "<br>在接下来的游览中你会持续消耗补给，直至耗尽。" +
                "<br>耗尽补给后你会返回附近的城镇，进行休整。";
            dialogData.btn_text = "好，我节约些";
            dialogData.requireConfirmation = true;
            dialogData.type = dataDialogPopup.DialogScenarios.park;
            uiDialogPopup.controller.SetUpDialog();

            //additional widgets
            FocusMaskEffect.controller.MaskFocusOn(HeaderUI.stamina_bar.number.transform, 0.6f);
            FocusMaskEffect.controller.TransparentMaskOn();

            parkIntroIndex = 2;
        }
        else if (gameplayData.curStatus == dConstants.ParkConst.ParkStatus.nextNode && gameplayData.curNodeDataUID == "parknode_lv0_start" && parkIntroIndex == 2)
        {
            //dialog info
            dataDialogPopup dialogData = uiDialogPopup.controller.AccessPopupData();
            dialogData.Reset();
            dialogData.avatar = "icon_avatar_0";
            dialogData.desc = "游览过程中最重要的就是欣赏<b>美景</b>啦，我们前面不远就是一个露营地（风景点）。<br>每个风景点都会有着不同的风景，风景也会随着游览的深入逐渐变得更美，更罕见。";
            dialogData.btn_text = "我该如何前往这个营地呢?";
            dialogData.requireConfirmation = true;
            dialogData.type = dataDialogPopup.DialogScenarios.park;
            uiDialogPopup.controller.SetUpDialog();

            //additional widgets
            FocusMaskEffect.controller.MaskFocusOn(gameplayData.CurrentNodeObj.outRoutes[0].outNode.transform, 1f);
            FocusMaskEffect.controller.TransparentMaskOn();

            parkIntroIndex = 3;
        }
        else if (gameplayData.curStatus == dConstants.ParkConst.ParkStatus.nextNode && gameplayData.curNodeDataUID == "parknode_lv0_start" && parkIntroIndex == 3)
        {
            //dialog info
            dataDialogPopup dialogData = uiDialogPopup.controller.AccessPopupData();
            dialogData.Reset();
            dialogData.avatar = "icon_avatar_0";
            dialogData.desc = "要前往露营地，沿着这条路往前走，不远就是了。" +
                "<br>累积路径要求的<b>距离</b>(现在这条是60)，就能到达目标地点，当然这会消耗补给。" +
                "<br>接下来点击这个路标，并“出发”吧。" +
                "<br><color=#4D4D4D>（场景内交互）</color>";
            //dialogData.btn_text = "我该如何前往这个营地呢?";
            dialogData.requireConfirmation = false;
            dialogData.type = dataDialogPopup.DialogScenarios.park;
            uiDialogPopup.controller.SetUpDialog();

            //additional widgets
            MouseClickHint.controller.ShowHint(gameplayData.CurrentNodeObj.outRoutes[0].label_sign.transform);
            FocusMaskEffect.controller.MaskFocusOn(gameplayData.CurrentNodeObj.outRoutes[0].label_sign.transform, 1f);

            parkIntroIndex = 4;
        }
        else if(gameplayData.curStatus == dConstants.ParkConst.ParkStatus.onRoute && gameplayData.totalScenes == 1 && parkIntroIndex == 4)
        {
            //dialog info
            dataDialogPopup dialogData = uiDialogPopup.controller.AccessPopupData();
            dialogData.Reset();
            dialogData.avatar = "icon_avatar_0";
            //dialogData.desc = "我们所在的路径距离是60，接下来我们要通过执行行动以累积足够的里程<sprite name=\"icon_progress\">，来到达目标地点。";
            dialogData.desc = "接下来我们要通过执行行动获得<sprite name=\"icon_progress\">里程来到达目标地点。";
            dialogData.btn_text = "我们该往哪里走呢?";
            dialogData.requireConfirmation = true;
            dialogData.type = dataDialogPopup.DialogScenarios.park;
            uiDialogPopup.controller.SetUpDialog();

            //additional widgets
            FocusMaskEffect.controller.MaskFocusOn(HeaderUI.progress_bar.max.transform, 0.6f);
            FocusMaskEffect.controller.TransparentMaskOn();

            parkIntroIndex = 5;
        }
        else if (gameplayData.curStatus == dConstants.ParkConst.ParkStatus.onRoute && gameplayData.totalScenes == 1 && parkIntroIndex == 5)
        {
            //dialog info
            dataDialogPopup dialogData = uiDialogPopup.controller.AccessPopupData();
            dialogData.Reset();
            dialogData.avatar = "icon_avatar_0";
            dialogData.desc = "我们可以先走这边的山路。山路是一个<b>前进行动</b>（通过<sprite name=\"icon_forward\">辨识）。" +
                "<br>只有前进类行动会累积里程，同时也会带你前往一个新的场景。" +
                "<br>“山路”会消耗6点补给<sprite name=\"dynamic_current_stamina\">，并获得50里程<sprite name=\"icon_progress\">，同时将你带入一个新场景<sprite name=\"icon_forward\">。" +
                "<br><color=#4D4D4D>（场景内交互）</color>";
            dialogData.requireConfirmation = false;
            dialogData.type = dataDialogPopup.DialogScenarios.park;
            uiDialogPopup.controller.SetUpDialog();

            //additional widgets
            MouseClickHint.controller.ShowHint(RouteUI.curStructure.forward_tokens[0].transform);
            FocusMaskEffect.controller.MaskFocusOn(RouteUI.curStructure.forward_tokens[0].transform, 1.5f);

            parkIntroIndex = 6;
        }
        else if (gameplayData.curStatus == dConstants.ParkConst.ParkStatus.onRoute && gameplayData.totalScenes == 2 && gameplayData.totalCards == 1 && parkIntroIndex == 6)
        {
            //dialog info
            dataDialogPopup dialogData = uiDialogPopup.controller.AccessPopupData();
            dialogData.Reset();
            dialogData.avatar = "icon_avatar_0";
            dialogData.desc = "经过山路我们来到了一个新的场景。现在的场景中有多个行动供我们选择，是时候仔细观察并思考下一步的计划了。";
            dialogData.btn_text = "让我仔细看看";
            dialogData.requireConfirmation = true;
            dialogData.type = dataDialogPopup.DialogScenarios.park;
            uiDialogPopup.controller.SetUpDialog();

            //additional widgets
            //FocusMaskEffect.controller.MaskFocusOn(RouteUI.curStructure.stay_tokens[0].transform, 1.5f);
            FocusMaskEffect.controller.TransparentMaskOn();

            parkIntroIndex = 7;
        }
        else if (gameplayData.curStatus == dConstants.ParkConst.ParkStatus.onRoute && gameplayData.totalScenes == 2 && gameplayData.totalCards == 1 && parkIntroIndex == 7)
        {
            //dialog info
            dataDialogPopup dialogData = uiDialogPopup.controller.AccessPopupData();
            dialogData.Reset();
            dialogData.avatar = "icon_avatar_0";
            dialogData.desc = "让我们先前往篝火稍事休息，恢复1点补给<sprite name=\"dynamic_current_stamina\">。篝火是<b>原地行动</b>。" +
                "<br>原地行动一般不直接累积里程，但是会提供一些有益的效果，同时也不会前往新场景。" +
                "<br><color=#4D4D4D>（场景内交互）</color>";
            dialogData.requireConfirmation = false;
            dialogData.type = dataDialogPopup.DialogScenarios.park;
            uiDialogPopup.controller.SetUpDialog();

            //additional widgets
            MouseClickHint.controller.ShowHint(RouteUI.curStructure.stay_tokens[1].transform);
            FocusMaskEffect.controller.MaskFocusOn(RouteUI.curStructure.stay_tokens[1].transform, 1.5f);

            parkIntroIndex = 8;
        }
        else if (gameplayData.curStatus == dConstants.ParkConst.ParkStatus.onRoute && gameplayData.totalScenes == 2 && gameplayData.totalCards == 2 && parkIntroIndex == 8)
        {
            //dialog info
            dataDialogPopup dialogData = uiDialogPopup.controller.AccessPopupData();
            dialogData.Reset();
            dialogData.avatar = "icon_avatar_0";
            dialogData.desc = "我们还差10点里程<sprite name=\"icon_progress\">就到达营地了。但是小路只提供<b>5点里程</b><sprite name=\"icon_progress\">，还不够。" +
                "<br>直接前往小路的话会让我们再次前往一个新的场景<sprite name=\"icon_forward\">，可能会消耗更多的补给。";
            dialogData.btn_text = "去看看路标吧";
            dialogData.requireConfirmation = true;
            dialogData.type = dataDialogPopup.DialogScenarios.park;
            uiDialogPopup.controller.SetUpDialog();

            //additional widgets
            //FocusMaskEffect.controller.MaskFocusOn(RouteUI.curStructure.stay_tokens[1].transform, 1.5f);
            FocusMaskEffect.controller.TransparentMaskOn();

            parkIntroIndex = 9;
        }
        else if (gameplayData.curStatus == dConstants.ParkConst.ParkStatus.onRoute && gameplayData.totalScenes == 2 && gameplayData.totalCards == 2 && parkIntroIndex == 9)
        {
            //dialog info
            dataDialogPopup dialogData = uiDialogPopup.controller.AccessPopupData();
            dialogData.Reset();
            dialogData.avatar = "icon_avatar_0";
            dialogData.desc = "好主意！路标也是<b>原地行动</b>。它会让你的下一次前进行动额外获得5点里程<sprite name=\"icon_progress\">。这样我们就能到达目标营地了。" +
                "<br><color=#4D4D4D>（场景内交互）</color>";
            dialogData.requireConfirmation = false;
            dialogData.type = dataDialogPopup.DialogScenarios.park;
            uiDialogPopup.controller.SetUpDialog();

            //additional widgets
            MouseClickHint.controller.ShowHint(RouteUI.curStructure.stay_tokens[0].transform);
            FocusMaskEffect.controller.MaskFocusOn(RouteUI.curStructure.stay_tokens[0].transform, 1.5f);

            parkIntroIndex = 10;
        }
        else if (gameplayData.curStatus == dConstants.ParkConst.ParkStatus.onRoute && gameplayData.totalScenes == 2 && gameplayData.totalCards == 3 && parkIntroIndex == 10)
        {
            //dialog info
            dataDialogPopup dialogData = uiDialogPopup.controller.AccessPopupData();
            dialogData.Reset();
            dialogData.avatar = "icon_avatar_0";
            dialogData.desc = "现在再走小路。我们就可以直接<b>到达</b>露营地了。事不宜迟！" +
                "<br><color=#4D4D4D>（场景内交互）</color>";
            dialogData.requireConfirmation = false;
            dialogData.type = dataDialogPopup.DialogScenarios.park;
            uiDialogPopup.controller.SetUpDialog();

            //additional widgets
            MouseClickHint.controller.ShowHint(RouteUI.curStructure.forward_tokens[0].transform);
            FocusMaskEffect.controller.MaskFocusOn(RouteUI.curStructure.forward_tokens[0].transform, 1.5f);

            parkIntroIndex = 11;
        }
        else if (gameplayData.curStatus == dConstants.ParkConst.ParkStatus.onArrival && gameplayData.progressCur == gameplayData.progressMax && parkIntroIndex == 11)
        {
            //dialog info
            dataDialogPopup dialogData = uiDialogPopup.controller.AccessPopupData();
            dialogData.Reset();
            dialogData.avatar = "icon_avatar_0";
            dialogData.desc = "总算到了营地，快进去看看吧。" +
                "<br><color=#4D4D4D>（场景内交互）</color>";
            dialogData.requireConfirmation = false;
            dialogData.type = dataDialogPopup.DialogScenarios.park;
            uiDialogPopup.controller.SetUpDialog();

            //additional widgets
            MouseClickHint.controller.ShowHint(RouteUI.curStructure.stay_tokens[0].transform);
            FocusMaskEffect.controller.MaskFocusOn(RouteUI.curStructure.stay_tokens[0].transform, 1.5f);

            parkIntroIndex = 12;
        }
        else if (gameplayData.curStatus == dConstants.ParkConst.ParkStatus.onReward && parkIntroIndex == 12)
        {
            //dialog info
            dataDialogPopup dialogData = uiDialogPopup.controller.AccessPopupData();
            dialogData.Reset();
            dialogData.avatar = "icon_avatar_0";
            dialogData.desc = "露营地是个节点。每个风景点都有可以欣赏并记录一个美丽的<b>风景</b>并选择一个<b>奖励</b>" +
                "<br>在每段路径上用尽量少的补给，便可以访问尽可能多的风景点。<br>我们休息一下，再启程前往下一个风景点。";
            dialogData.btn_text = "谢谢你带我来这里";
            dialogData.requireConfirmation = true;
            dialogData.type = dataDialogPopup.DialogScenarios.park;
            uiDialogPopup.controller.SetUpDialog();

            //additional widgets
            //MouseClickHint.controller.ShowHint(RouteUI.curStructure.stay_tokens[0].transform);
            //FocusMaskEffect.controller.MaskFocusOn(RouteUI.curStructure.stay_tokens[0].transform, 1.5f);
            FocusMaskEffect.controller.TransparentMaskOn();

            parkIntroIndex = 13;
        }
        else if (gameplayData.curStatus == dConstants.ParkConst.ParkStatus.nextNode && parkIntroIndex == 13)
        {
            //dialog info
            dataDialogPopup dialogData = uiDialogPopup.controller.AccessPopupData();
            dialogData.Reset();
            dialogData.avatar = "icon_avatar_0";
            dialogData.desc = "距离终点还有一段路径，我老了，剩下的路就不陪你了。" +
                "<br>邻家小山也只是个开始，未来的路还很远，更多有趣的经历，就留给你自己去探索了。";
            dialogData.btn_text = "保重，后会有期";
            dialogData.requireConfirmation = true;
            dialogData.type = dataDialogPopup.DialogScenarios.park;
            uiDialogPopup.controller.SetUpDialog();

            //additional widgets
            //MouseClickHint.controller.ShowHint(RouteUI.curStructure.stay_tokens[0].transform);
            //FocusMaskEffect.controller.MaskFocusOn(RouteUI.curStructure.stay_tokens[0].transform, 1.5f);
            FocusMaskEffect.controller.TransparentMaskOn();

            parkIntroIndex = 14;
        }
    }
    public void SetDangerDialogAndMask()
    {
        //Debug.Log("SetDangerDialogAndMask() called");
        if (uiDialogPopup.controller.OngoingDialog())
        {
            return;
        }
        if (skipIntro)
        {
            return;
        }
        //Debug.Log("SetDangerDialogAndMask() continue");
        if (!firstDanger)
        {
            firstDanger = true;
            //dialog info
            dataDialogPopup dialogData = uiDialogPopup.controller.AccessPopupData();
            dialogData.Reset();
            dialogData.avatar = "icon_avatar_0";
            dialogData.desc = "你刚刚在行动中遇到了危险。冒风险的行动往往能带来更好的收益，但是一旦触发危险会迫使你执行相应的减益行动并累计1个标记<sprite name=\"icon_danger_sign\">。" +
                "<br><br>另外<sprite name=\"icon_danger_sign\">危险标记累计到3个会触发严重的灾难。请小心行事！";
            dialogData.requireConfirmation = true;
            dialogData.btn_text = "啊，我要多加小心";
            dialogData.type = dataDialogPopup.DialogScenarios.trigger;
            uiDialogPopup.controller.SetUpDialog();

            FocusMaskEffect.controller.MaskFocusOn(RouteUI.curStructure.danger_tokens[0].transform, 1.5f);
            FocusMaskEffect.controller.TransparentMaskOn();
        }
    }
    public void SetCollectibleDialogAndMask()
    {
        //Debug.Log("SetCollectibleDialogAndMask() called");
        if (uiDialogPopup.controller.OngoingDialog())
        {
            return;
        }
        if (skipIntro)
        {
            return;
        }
        //Debug.Log("SetCollectibleDialogAndMask() continue");
        if (!firstCollectible)
        {
            firstCollectible = true;
            //dialog info
            dataDialogPopup dialogData = uiDialogPopup.controller.AccessPopupData();
            dialogData.Reset();
            dialogData.avatar = "icon_avatar_0";
            dialogData.desc = "你刚刚在行动中收集了1个<sprite name=\"icon_view_sign\">偶遇标记。" +
                "<br><br>可能收集到<sprite name=\"icon_view_sign\">偶遇标记的行动往往会有额外的消耗，但是偶遇标记累计到3个会偶遇额外的美景。请合理收集！";
            dialogData.requireConfirmation = true;
            dialogData.btn_text = "太好了，我努力";
            dialogData.type = dataDialogPopup.DialogScenarios.trigger;
            uiDialogPopup.controller.SetUpDialog();

            FocusMaskEffect.controller.MaskFocusOn(HeaderUI.collectible_counter.transform, 1.5f);
            FocusMaskEffect.controller.TransparentMaskOn();
        }
    }
    public void SetJournalBasicIntro()
    {
        if (uiDialogPopup.controller.OngoingDialog())
        {
            return;
        }
        if (skipIntro || journalIntroIndex > 6)
        {
            return;
        }
        //triggering on first opening journal
        if (journalIntroIndex == 0)
        {
            //dialog info
            dataDialogPopup dialogData = uiDialogPopup.controller.AccessPopupData();
            dialogData.Reset();
            dialogData.avatar = "icon_avatar_0";
            dialogData.desc = "我们又见面了。将旅程中的美景记录并以<b>游记</b>的形式分享就可以让更多的人感受到你当时的心情。" +
                "<br>让我给你说道一下游记的书写吧。";
            dialogData.btn_text = "您请讲";
            dialogData.requireConfirmation = true;
            dialogData.type = dataDialogPopup.DialogScenarios.journal;
            uiDialogPopup.controller.SetUpDialog();

            //additional widgets
            //FocusMaskEffect.controller.MaskFocusOn(gameplayData.CurrentNodeObj.outRoutes[0].label_sign.transform, 1f);
            FocusMaskEffect.controller.TransparentMaskOn();

            journalIntroIndex = 1;
        }
        else if (journalIntroIndex == 1)
        {
            //dialog info
            dataDialogPopup dialogData = uiDialogPopup.controller.AccessPopupData();
            dialogData.Reset();
            dialogData.avatar = "icon_avatar_0";
            dialogData.desc = "每篇游记根据文体不同会需要3-6个的风景才能完成。<br>如果一次旅程收集不够的话还可以再去下一个游览地点继续收集。";
            dialogData.btn_text = "好的";
            dialogData.requireConfirmation = true;
            dialogData.type = dataDialogPopup.DialogScenarios.journal;
            uiDialogPopup.controller.SetUpDialog();

            //additional widgets
            //FocusMaskEffect.controller.MaskFocusOn(gameplayData.CurrentNodeObj.outRoutes[0].label_sign.transform, 1f);
            FocusMaskEffect.controller.MaskFocusOn(JournalUI.journalStructure.subG_collectible.transform, 2f);
            FocusMaskEffect.controller.TransparentMaskOn();

            journalIntroIndex = 2;
        }
        else if (journalIntroIndex == 2)
        {
            //dialog info
            dataDialogPopup dialogData = uiDialogPopup.controller.AccessPopupData();
            dialogData.Reset();
            dialogData.avatar = "icon_avatar_0";
            dialogData.desc = "当收集了足够的风景后，在回到城镇时会自动触发游记的发表。";
            dialogData.btn_text = "明白";
            dialogData.requireConfirmation = true;
            dialogData.type = dataDialogPopup.DialogScenarios.journal;
            uiDialogPopup.controller.SetUpDialog();

            //additional widgets
            //FocusMaskEffect.controller.MaskFocusOn(gameplayData.CurrentNodeObj.outRoutes[0].label_sign.transform, 1f);
            FocusMaskEffect.controller.MaskFocusOn(JournalUI.journalStructure.submit_btn.transform, 1f);
            FocusMaskEffect.controller.TransparentMaskOn();

            journalIntroIndex = 3;
        }
        else if (journalIntroIndex == 3)
        {
            //dialog info
            dataDialogPopup dialogData = uiDialogPopup.controller.AccessPopupData();
            dialogData.Reset();
            dialogData.avatar = "icon_avatar_0";
            dialogData.desc = "游记的星级也就是游记的质量。" +
                "<br>由收集的风景的星级与特定的标签组合共同构成。" +
                "<br>总的来说风景越好，星级与标签都会变好变多。";
            dialogData.btn_text = "有点意思";
            dialogData.requireConfirmation = true;
            dialogData.type = dataDialogPopup.DialogScenarios.journal;
            uiDialogPopup.controller.SetUpDialog();

            //additional widgets
            FocusMaskEffect.controller.MaskFocusOn(JournalUI.journalStructure.totalStar.transform, 1f);
            FocusMaskEffect.controller.TransparentMaskOn();

            journalIntroIndex = 4;
        }
        else if (journalIntroIndex == 4)
        {
            //dialog info
            dataDialogPopup dialogData = uiDialogPopup.controller.AccessPopupData();
            dialogData.Reset();
            dialogData.avatar = "icon_avatar_0";
            dialogData.desc = "这里是游记发表后大致会获得的<b>奖励</b>。" +
                "<br>大部分都随星级提高而变好。" +
                "<br>一般来说发表游记一定会获得星级、铜钱、额外特质，并延后旅行结束的时间。";
            dialogData.btn_text = "努力写好每一篇";
            dialogData.requireConfirmation = true;
            dialogData.type = dataDialogPopup.DialogScenarios.journal;
            uiDialogPopup.controller.SetUpDialog();

            //additional widgets
            FocusMaskEffect.controller.MaskFocusOn(JournalUI.journalStructure.subG_reward.transform, 1.5f);
            FocusMaskEffect.controller.TransparentMaskOn();

            journalIntroIndex = 5;
        }
        else if (journalIntroIndex == 5)
        {
            //dialog info
            dataDialogPopup dialogData = uiDialogPopup.controller.AccessPopupData();
            dialogData.Reset();
            dialogData.avatar = "icon_avatar_0";
            dialogData.desc = "这是<b>额外风景</b>，当收集的风景多于游记需要的风景数量时额外的会留给下篇游记。<br>你可以对各个风景的使用与否进行交换。";
            dialogData.btn_text = "记住了";
            dialogData.requireConfirmation = true;
            dialogData.type = dataDialogPopup.DialogScenarios.journal;
            uiDialogPopup.controller.SetUpDialog();

            //additional widgets
            FocusMaskEffect.controller.MaskFocusOn(JournalUI.journalStructure.subG_extra.transform, 1.5f);
            FocusMaskEffect.controller.TransparentMaskOn();

            journalIntroIndex = 6;
        }
        else if (journalIntroIndex == 6)
        {
            //dialog info
            dataDialogPopup dialogData = uiDialogPopup.controller.AccessPopupData();
            dialogData.Reset();
            dialogData.avatar = "icon_avatar_0";
            dialogData.desc = "最后祝你写作成功，期待你的作品。" +
                "<br><br><color=#4D4D4D>“书生此时还不知道自己的游记会被《四海游集》所收录，最终流传于世。但是他隐隐感觉到了游记于他有一种神奇的使命感，不禁有些热血沸腾”</color>";
            dialogData.btn_text = "我不会令人失望的";
            dialogData.requireConfirmation = true;
            dialogData.type = dataDialogPopup.DialogScenarios.journal;
            uiDialogPopup.controller.SetUpDialog();

            //additional widgets
            //FocusMaskEffect.controller.MaskFocusOn(gameplayData.CurrentNodeObj.outRoutes[0].label_sign.transform, 1f);
            FocusMaskEffect.controller.TransparentMaskOn();

            journalIntroIndex = 7;
        }
    }
    public void SetJournalSubmissionIntro()
    {
        
    }
    public void SetEquipmentIntro()
    {
        //Debug.Log("SetCollectibleDialogAndMask() called");
        if (uiDialogPopup.controller.OngoingDialog())
        {
            return;
        }
        if (skipIntro)
        {
            return;
        }
        //Debug.Log("SetCollectibleDialogAndMask() continue");
        if (!firstEquipment)
        {
            firstEquipment = true;
            //dialog info
            dataDialogPopup dialogData = uiDialogPopup.controller.AccessPopupData();
            dialogData.Reset();
            dialogData.avatar = "icon_avatar_0";
            dialogData.desc = "你刚刚得到了1件装备。每件装备都可以某个场景中使用1次，每次回到城镇时装备使用次数也会恢复，因此不要忘记在返回城镇前使用哦。";
            dialogData.requireConfirmation = true;
            dialogData.btn_text = "那我得记住了";
            dialogData.type = dataDialogPopup.DialogScenarios.trigger;
            uiDialogPopup.controller.SetUpDialog();

            FocusMaskEffect.controller.MaskFocusOn(HeaderUI.equipments[0].transform, 1f);
            FocusMaskEffect.controller.TransparentMaskOn();
        }
    }
    public void SetOutOfStaminaIntro()
    {
        //Debug.Log("SetCollectibleDialogAndMask() called");
        if (uiDialogPopup.controller.OngoingDialog())
        {
            return;
        }
        if (skipIntro)
        {
            return;
        }
        //Debug.Log("SetCollectibleDialogAndMask() continue");
        if (!firstOutOfStamina)
        {
            firstOutOfStamina = true;
            //dialog info
            dataDialogPopup dialogData = uiDialogPopup.controller.AccessPopupData();
            dialogData.Reset();
            dialogData.avatar = "icon_avatar_0";
            dialogData.desc = "哎呀，你刚刚因<b>补给耗尽</b>结束了此次游览。<br>不过没关系，回到城镇后补给会回满，而且你已经饱览了许多美景。旅游的意义在于过程而非结果嘛。";
            dialogData.requireConfirmation = true;
            dialogData.btn_text = "有道理";
            dialogData.type = dataDialogPopup.DialogScenarios.trigger;
            uiDialogPopup.controller.SetUpDialog();

            FocusMaskEffect.controller.MaskFocusOn(SunmmaryUI.endReason.transform, 1f);
            FocusMaskEffect.controller.TransparentMaskOn();
        }
    }
    public void SetFinalNodeIntro()
    {
        //Debug.Log("SetCollectibleDialogAndMask() called");
        if (uiDialogPopup.controller.OngoingDialog())
        {
            return;
        }
        if (skipIntro)
        {
            return;
        }
        //Debug.Log("SetCollectibleDialogAndMask() continue");
        if (!firstFinalNode)
        {
            firstFinalNode = true;
            //dialog info
            dataDialogPopup dialogData = uiDialogPopup.controller.AccessPopupData();
            dialogData.Reset();
            dialogData.avatar = "icon_avatar_0";
            dialogData.desc = "恭喜你到达了这个景点的一个<b>终点</b>。<br>终点会结束此次游览，返回城镇。越大的地区，终点越远。根据自己的补给吗，下次旅行可以考虑去一个区域更大的地方哦。";
            dialogData.requireConfirmation = true;
            dialogData.btn_text = "我考虑下";
            dialogData.type = dataDialogPopup.DialogScenarios.trigger;
            uiDialogPopup.controller.SetUpDialog();

            FocusMaskEffect.controller.MaskFocusOn(SunmmaryUI.endReason.transform, 1f);
            FocusMaskEffect.controller.TransparentMaskOn();
        }
    }
    public void ClearDialogAndMask()
    {
        uiDialogPopup.controller.CloseDialog();
        FocusMaskEffect.controller.ClearMask();
        MouseClickHint.controller.ClearHint();
    }
}
