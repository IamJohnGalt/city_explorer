﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class uiParkOnRoute_Character : MonoBehaviour
{
    //public List<Vector3> waypoints_main;
    [Header("Portrait")]
    [SerializeField] character_entry portrait;

    [Header("Movement")]
    public float MoveSpeedBase = 100f;
    public uiParkActionToken currentToken;
    public List<Vector2> stops_main;
    [SerializeField] Vector2 MoveInPos;

    public void SetPortrait(Sprite sprt)
    {
        portrait.UpdateToCurrentAvatar();
    }
    public void Anim_Test()
    {
        Sequence seq = DOTween.Sequence();
        RectTransform rect = GetComponent<RectTransform>();
        for(int i = 0;i < stops_main.Count;i++)
        {
            seq.Append(rect.DOAnchorPos(stops_main[i], 1f));
        }
    }

    public void Anim_MoveIn()
    {
        RectTransform rect = GetComponent<RectTransform>();
        //Debug.Log("Anim_MoveIn called");
        rect.anchoredPosition = stops_main[0];
        rect.DOAnchorPos(MoveInPos, 1f).SetEase(Ease.OutSine).From();
        currentToken = null;
    }
    public void Anim_MoveTo(uiParkActionToken targetToken, System.Action callback)
    {
        int starting_stop_main = 0;
        
        Sequence seq = DOTween.Sequence();
        RectTransform rect = GetComponent<RectTransform>();
        Vector2 previousPos = rect.anchoredPosition;
        if (currentToken == null)
        {
            starting_stop_main = 0;
        }
        else
        {
            for(int i= currentToken.stop_branch.Count - 1; i>= 0; i--)
            {
                seq.Append(rect.DOAnchorPos(currentToken.stop_branch[i], CalculateMoveTime(previousPos, currentToken.stop_branch[i])).SetEase(Ease.Linear));
                previousPos = currentToken.stop_branch[i];
            }
            starting_stop_main = currentToken.stop_main_index;
        }
        for(int i = starting_stop_main; i != targetToken.stop_main_index + (starting_stop_main >= targetToken.stop_main_index ? -1 : 1) ; i += starting_stop_main >= targetToken.stop_main_index ? -1 : 1)
        {
            seq.Append(rect.DOAnchorPos(stops_main[i], CalculateMoveTime(previousPos, stops_main[i])).SetEase(Ease.Linear));
            previousPos = stops_main[i];
        }
        for (int i = 0; i < targetToken.stop_branch.Count; i++)
        {
            seq.Append(rect.DOAnchorPos(targetToken.stop_branch[i], CalculateMoveTime(previousPos, targetToken.stop_branch[i])).SetEase(Ease.Linear));
            previousPos = targetToken.stop_branch[i];
        }
        seq.AppendCallback(() => callback());
        currentToken = targetToken;
    }
    float CalculateMoveTime(Vector2 startPos, Vector2 endPos)
    {
        return Vector2.Distance(startPos, endPos) / MoveSpeedBase;
    }
}
