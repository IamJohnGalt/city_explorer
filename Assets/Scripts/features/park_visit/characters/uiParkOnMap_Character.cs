﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using DG.Tweening;

public class uiParkOnMap_Character : MonoBehaviour
{
    //public List<Vector3> waypoints_main;
    [Header("Portrait")]
    [SerializeField] character_entry portrait;

    [Header("Movement")]
    public float MoveSpeedBase = 100f;
    public uiParkActionToken currentToken;
    public List<Vector2> stops_main;

    [SerializeField] Vector2 MoveInPos;
    [SerializeField] Vector2 offset;
    [SerializeField] GameEvent EventOnNodeReward;

    public void SetPortrait(Sprite sprt)
    {
        portrait.UpdateToCurrentAvatar();
    }
    public void Anim_Test()
    {
        Sequence seq = DOTween.Sequence();
        RectTransform rect = GetComponent<RectTransform>();
        for (int i = 0; i < stops_main.Count; i++)
        {
            seq.Append(rect.DOAnchorPos(stops_main[i], 1f));
        }
    }

    public void Anim_MoveIn()
    {
        RectTransform rect = GetComponent<RectTransform>();
        rect.DOAnchorPos(MoveInPos, 1f).SetEase(Ease.OutSine).From();
        currentToken = null;
    }
    public void Anim_MoveTo(Vector2 target, System.Action callback)
    {
        RectTransform rect = GetComponent<RectTransform>();
        rect.DOAnchorPos(target + offset, Mathf.Max(0.5f, Mathf.Max(1f, Mathf.Min(2.5f, CalculateMoveTime(rect.anchoredPosition, target))))).SetEase(Ease.Linear).OnComplete(()=>callback());
    }
    float CalculateMoveTime(Vector2 startPos, Vector2 endPos)
    {
        return Vector2.Distance(startPos, endPos) / MoveSpeedBase;
    }
}
