﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Gameplay/Session End")]
public class dataEndSession : ScriptableObject
{
    [Header("Current Ending")]
    public bool ended;
    //public dConstants.EndSession.EndReason reason;
    public List<int> endingIndex;
    [Header("Endings")]
    public List<Sprite> endIcon;
    public List<string> endTitle;
    public List<string> endDesc;

    [Header("Score")]
    public int finalCoinScore;
    public int finalXPScore;
    public int finalTraitScore;
    public int finalEquipmentScore;
    public int finalFameScore;
    public int finalJournalScore;
    public int finalSum { get { return finalCoinScore + finalXPScore + finalJournalScore + finalTraitScore + finalEquipmentScore + finalFameScore; } }

    public void Reset()
    {
        ended = false;
        //reason = dConstants.EndSession.EndReason.not;
        endingIndex = new List<int>();
    }
}
