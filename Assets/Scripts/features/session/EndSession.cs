﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndSession : MonoBehaviour
{
    public static EndSession controller;
    private void Awake()
    {
        if (controller == null)
        {
            controller = this;
        }
        else if (controller != null)
        {
            Destroy(gameObject);
        }
        Init();
    }

    public dataEndSession data;
    public dataContentUnlocked ContentUnlockData;
    public GameEvent SessionEnded;
    public GameEvent SaveTrigger;
    public GameEvent EngineRestartNeeded;
    public GameEvent ContentUnlockUp;
    //public GameEvent SessionEndSave;
    //public dataPopupBox popupData;
    public void Init()
    {
        data.Reset();
    }
    public void SessionEndConfirmedInChronicle()
    {
        SessionEnded.Raise();
    }
    public void SessionEndByTime()
    {
        CalculateFinalScore();
        data.ended = true;
        data.endingIndex.Add(0);
    }
    public void SessionEndByNoMoney()
    {
        CalculateFinalScore();
        data.ended = true;
        data.endingIndex.Add(1);
    }
    public void ClaimPermReward()
    {
        ContentUnlockData.Reset();
        //character progression
        CharacterManagement.controller.EarnFame(CurrencyManagement.wallet.CheckCurrency("currency_star"));
        List<int> tokenCountList = new List<int>();
        for(int i = 0; i < dConstants.Character.TOTAL_CHARACTERS; i++)
        {
            tokenCountList.Add(CurrencyManagement.wallet.CheckCurrency(string.Format("currency_token_charac{0}", i + 1)));
        }
        CharacterManagement.controller.EarnToken(tokenCountList);
        //add fame and otehr stats
        //dStatistics.stats.SavePlayStats();
        //save total xp
        int totalXP = CurrencyManagement.wallet.CheckCurrency("currency_star") * CurrencyManagement.wallet.CheckCurrency("currency_xp");
        TotalBuffValueSet bvs = ActiveBuffs.controller.RetrieveValueSetByAction("xp_gain");
        totalXP = ActiveBuffs.ModifyByBVS(totalXP, bvs);
        metaContentStore.controller.EarnXP(totalXP);
        //raise event to save to file
        SaveTrigger.Raise();
        //only clear this at end seesion
        CurrencyManagement.wallet.ClearUnclaimedCurrency();
        SaveManager.controller.SaveToFile();
        ContentUnlockData.isEndGame = true;
    }
    public void ClaimPreviousPermReward()
    {
        ContentUnlockData.Reset();
        string prevCharacUID = SaveManager.controller.Inquire("cur_character_uid");
        if(prevCharacUID == "")
        {
            return;
        }
        //character progression
        int prevStar = 0;
        int.TryParse(SaveManager.controller.Inquire(string.Format("unclaimed_{0}", "currency_star")), out prevStar);
        CharacterManagement.controller.GrantFame(prevCharacUID, prevStar);
        List<int> tokenCountList = new List<int>();
        int prevToken = 0;
        for (int i = 0; i < dConstants.Character.TOTAL_CHARACTERS; i++)
        {
            int.TryParse(SaveManager.controller.Inquire(string.Format("unclaimed_currency_token_charac{0}", i + 1)), out prevToken);
            tokenCountList.Add(prevToken);
        }
        CharacterManagement.controller.EarnToken(tokenCountList);
        //raise event to save to file
        SaveTrigger.Raise();
        //only clear this at end seesion
        CurrencyManagement.wallet.ClearUnclaimedCurrency();
        SaveManager.controller.SaveToFile();
        ContentUnlockData.isEndGame = false;
    }
    public void ShowNewContent()
    {
        ContentUnlockUp.Raise();
    }
    public void RestartGame()
    {
        EngineRestartNeeded.Raise();
    }
    void CalculateFinalScore()
    {
        data.finalCoinScore = CurrencyManagement.wallet.CheckCurrency("currency_coin") * 1;
        data.finalXPScore = Mathf.RoundToInt(CurrencyManagement.wallet.CheckCurrency("currency_xp") * 0.2f);
        data.finalTraitScore = CurrencyManagement.wallet.CheckCurrency("currency_trait") * 20;
        data.finalEquipmentScore = EquipmentManagement.equipment.data.activeEquipment.Count * 200;
        data.finalFameScore = CurrencyManagement.wallet.CheckCurrency("currency_star") * 30;
        data.finalJournalScore = dStatistics.stats.data.inSessionJournalLogs.Count * 600;
    }
    /*
    public void EndSessionPopup()
    {
        int finalCoinScore = CurrencyManagement.wallet.CheckCurrency("currency_coin") * 1;
        int finalXPScore = Mathf.RoundToInt(CurrencyManagement.wallet.CheckCurrency("currency_xp") * 0.2f);
        int finalTraitScore = CurrencyManagement.wallet.CheckCurrency("currency_trait") * 20;
        int finalEquipmentScore = EquipmentManagement.equipment.data.activeEquipment.Count * 200;
        int finalFameScore = CurrencyManagement.wallet.CheckCurrency("currency_fame") * 200;

        int finalJournalScore = dStatistics.stats.data.inSessionJournalLogs.Count * 600;

        int finalSum = finalCoinScore + finalXPScore + finalJournalScore + finalTraitScore + finalEquipmentScore + finalFameScore;

        popupData.Reset();
        popupData.title = "旅程结束";
        popupData.desc = string.Format("旅程结束。最终得分: {0}\r\n来自铜钱的得分: {1}\r\n来自经验的得分: {2}\r\n来自感想的得分: {3}\r\n来自收藏的得分: {4}\r\n来自声望的得分: {5}\r\n来自游记的得分: {6}",
            finalSum,
            finalCoinScore,
            finalXPScore,
            finalTraitScore,
            finalEquipmentScore,
            finalFameScore,
            finalJournalScore);
        popupData.btnLabel = string.Format("结束旅程\r\n得分:{0}", finalSum);
        popupData.btn_popup_confirm = RestartGame;
        SessionEnded.Raise();
    }*/
}
