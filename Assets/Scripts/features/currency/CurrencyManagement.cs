﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurrencyManagement : MonoBehaviour
{
    public static CurrencyManagement wallet = null;

    public CurrencyList currencyList;
    public GameEvent currenyChanged;
    public GameEvent endByNoMoney;

    void Awake()
    {
        if (wallet == null)
        {
            wallet = this;
        }
        else if (wallet != null)
        {
            Destroy(gameObject);
        }
    }
    public void CurrencyInit()
    {
        currencyList.ResetCurrency();
        AddCurrency_Setup("currency_coin", dConstants.Avatar.INITIAL_COIN);
        AddCurrency_Setup("currency_fame", dConstants.Avatar.INITIAL_FAME);
    }
    public void SaveUnclaimedCurrency()
    {
        SaveManager.controller.Insert(string.Format("unclaimed_{0}", "currency_star"), CheckCurrency("currency_star").ToString());
        for(int i=0;i < dConstants.Character.TOTAL_CHARACTERS; i++)
        {
            SaveManager.controller.Insert(string.Format("unclaimed_currency_token_charac{0}", i+1), CheckCurrency(string.Format("currency_token_charac{0}", i+1)).ToString());
        }
        SaveManager.controller.Insert("cur_character_uid", CharacterManagement.controller.GetCurrentCharacter().characterUID);
    }
    public void ClearUnclaimedCurrency()
    {
        SaveManager.controller.Insert(string.Format("unclaimed_{0}", "currency_star"), "");
        for (int i = 0; i < dConstants.Character.TOTAL_CHARACTERS; i++)
        {
            SaveManager.controller.Insert(string.Format("unclaimed_currency_token_charac{0}", i + 1), "");
        }
        SaveManager.controller.Insert("cur_character_uid", "");
    }
    public void ResetCurrency()
    {
        currencyList.ResetCurrency();
    }
    public void AddCurrency_Setup(string curUID, int value)
    {
        //this doesn't trigger any special effect from acquiring currnecy
        for (int i = 0; i < currencyList.list.Count; i++)
        {
            if (currencyList.list[i].UID == curUID)
            {
                currencyList.list[i].curValue += Mathf.Min(value, currencyList.list[i].gapValue);
            }
        }
        currenyChanged.Raise();
    }
    public void AddCurrency(string curUID, int value)
    {
        //xp special case
        //if (curUID == "currency_xp")
        //{
        //    AddCurrency("currency_perm_xp", value);
        //}
        //special - if feature gate fame -> addtional coin is activated
        /*if (curUID == "currency_fame" && ActiveBuffs.controller.CheckFeatureGate("feature_gate_gain_coin_from_fame"))
        {
            AddCurrency_Setup("currency_coin", value*5);
        }*/
        //regular add
        int amoutnChanged = 0;
        for (int i = 0; i < currencyList.list.Count; i++)
        {
            if (currencyList.list[i].UID == curUID)
            {
                amoutnChanged = Mathf.Min(value, currencyList.list[i].gapValue);
                currencyList.list[i].curValue += amoutnChanged;
                currencyList.list[i].LatestAmountChanged = amoutnChanged;
            }
        }
        if (ActiveBuffs.controller.CheckFeatureGate("feature_gate_help_gain_coin"))
        {
            CharacterAcquisition.controller.AcquisitionCheck_Coin_Gaining(curUID, amoutnChanged);
        }
        //UI event 
        currenyChanged.Raise();
        
    }
    public void RemoveCurrency(string curUID, int value)
    {
        int actualAmountRemoved = 0;
        for (int i = 0; i < currencyList.list.Count; i++)
        {
            if (currencyList.list[i].UID == curUID)
            {
                actualAmountRemoved = Mathf.Min(value, currencyList.list[i].curValue);
                //allow currency set to negative
                currencyList.list[i].curValue -= value;
                currencyList.list[i].LatestAmountChanged = -actualAmountRemoved;
            }
            //trigger help check
            //CharacterHelp.controller.HelpCheck_Character2_HelpLv5();
        }
        if (ActiveBuffs.controller.CheckFeatureGate("feature_gate_help_spend_coin"))
        {
            CharacterAcquisition.controller.AcquisitionCheck_Coin_Spending(curUID, actualAmountRemoved);
        }
        currenyChanged.Raise();
    }
    public int CheckCurrency(string curUID)
    {
        for(int i = 0; i < currencyList.list.Count; i++)
        {
            if(currencyList.list[i].UID == curUID)
            {
                return currencyList.list[i].curValue;
            }
        }
        return -1;
    }
    public int CheckCurrencyLatestChange(string curUID)
    {
        for (int i = 0; i < currencyList.list.Count; i++)
        {
            if (currencyList.list[i].UID == curUID)
            {
                int amount = currencyList.list[i].LatestAmountChanged;
                currencyList.list[i].LatestAmountChanged = 0;
                return amount;
            }
        }
        return 0;
    }
    public bool NotEnoughCoin()
    {
        return CheckCurrency("currency_coin") < 0;
    }
    public void NoMoneyEndCheck()
    {
        if(CheckCurrency("currency_coin") < 0)
        {
            endByNoMoney.Raise();
        }
    }
    public void ConvertCoin2Star(int coin_ratio)
    {
        //Debug.Log("ConvertCoin2Star, earning: " + Mathf.Max(0, Mathf.FloorToInt((float)CheckCurrency("currency_coin") / coin_ratio)));
        AddCurrency("currency_star", Mathf.Max(0, Mathf.FloorToInt((float)CheckCurrency("currency_coin") / coin_ratio)));
    }
    public int GetInterests(float rate)
    {
        int interestsAmount = Mathf.Max(0, Mathf.FloorToInt(CheckCurrency("currency_coin") * rate));
        AddCurrency("currency_coin", interestsAmount);
        return interestsAmount;
    }
}
