﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Dictionary/Wallet")]
public class CurrencyList : ScriptableObject
{
    public List<PlayerCurrency> list;

    public void ResetCurrency()
    {
        foreach(PlayerCurrency pc in list)
        {
            pc.curValue = 0;
        }
    }
    public PlayerCurrency RetrieveCurrencyDefinition(string currencyUID)
    {
        for(int i=0;i< list.Count; i++)
        {
            if(list[i].UID == currencyUID)
            {
                return list[i];
            }
        }
        return null;
    }
}

