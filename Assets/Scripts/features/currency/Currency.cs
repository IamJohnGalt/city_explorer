﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Currency
{
    public string UID;
    public string CurrencyName;
    public CardArt artAsset;
    public Currency()
    {
        UID = "";
        CurrencyName = "";
        artAsset = new CardArt();
    }
}
public enum CurrencyType { constant, optional, hideen};
[System.Serializable]
public class PlayerCurrency : Currency
{
    public int curValue;
    public int maxCap;
    public int displayCap;
    public CurrencyType status;
    public int LatestAmountChanged;
    public int gapValue { get { return maxCap > 0 ? maxCap - curValue : int.MaxValue; } }
    public PlayerCurrency()
    {
        UID = "";
        maxCap = int.MaxValue;
        displayCap = int.MaxValue;
        status = CurrencyType.optional;
        curValue = 0;
        LatestAmountChanged = 0;
    }
}
