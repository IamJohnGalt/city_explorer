﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Dictionary/Currency Definition")]
public class CurrencyDefinitionDictionary : GenericDataDict<Currency>
{
    public override void DebugCount()
    {
        Debug.Log("Total of " + UIDList.Count + " nodes are added to current Currency Definition Dictionary");
    }
}

