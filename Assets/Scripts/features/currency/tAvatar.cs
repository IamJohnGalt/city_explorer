﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class tAvatar : MonoBehaviour {

	public static tAvatar Avatar = null;

    [Header("Data")]
    public string CharacterUID;
    //public PlayerWallet myWallet;
    public PlayerAge myAge;
    //public PlayerEquipment myEquipment;
    //public PlayerTrait myTrait;
    public dataCharacterStats myStats;

    [Header("In-game Objects")]
    //public tNode curNode;
    //public tNode clickNode;

    [Header("Settings")]
    public GameEvent GameEndByMoney;
    public GameEvent playerMoveEvent;

    //public int coins;
    

    private Vector2 AVATAR_POS_OFFSET = new Vector2(0.2f, 0f);
    // Use this for initialization
	void Awake() {
		if(Avatar == null)
        {
            Avatar = this;
        }
        else if(Avatar != null)
        {
            Destroy(gameObject);
        }
	}
    void Start()
    {
        
    }
	// Update is called once per frame
	void Update () {
		
	}
    public void TestCharacterInit()
    {
        //myWallet.currencyList.ResetCurrency();
        AvatarInit("default_character", 500);
    }
    public void AvatarInit(string uid, int startCoin)
    {
        gameObject.SetActive(true);
        //set uid
        CharacterUID = uid;
        //set currency
        //myWallet.owningDict.Reset();
        //myWallet.AddCurrency("currency_coin", startCoin);
        //set age
        myAge.SetPlayerAge(dConstants.Avatar.INITIAL_AGE, dConstants.Avatar.END_AGE, dConstants.Avatar.BORN_YEAR, dConstants.Avatar.BORN_SEASON, dConstants.Avatar.BORN_MILESTONE);
        //clear equipment
        //myEquipment.ResetEquipment();
        //clear trait
        //myTrait.ResetTrait();
        //set calendar
        //gCalendar.controller.SetCalendar(dConstants.Avatar.BORN_YEAR, dConstants.Avatar.BORN_SEASON, dConstants.Avatar.BORN_MILESTONE);
        //set born location
        //MoveTo(WorldObjectFinder.controller.GetNodeByUID(locUID));
        //curNode.OnVisit();
        //dStatistics.stats.VisitLocation(curNode.UID);
    }
    
    //deprecate function
    /*public void AddCoin(int value)
    {
        myWallet.AddCurrency("currency_coin", value);
        if (value > 0)
        {
            //dStatistics.stats.AddTotalMoney(value);
        }
        else
        {
            
        }

    }*/
    public int CheckCoin()
    {
        return CurrencyManagement.wallet.CheckCurrency("currency_coin");
    }
    public void CheckCoinGameEndCondition()
    {
        if(CheckCoin() <= 0)
        {
            GameEndByMoney.Raise();
        }
    }
}
