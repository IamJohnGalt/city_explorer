﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Chronicle/Gameplay/In-Session Data")]
public class dataChronicle : ScriptableObject
{
    //[Header("Basic")]
    //public string journalUID;

    [Header("Gameplay")]
    public string parkUID;
    public string activityUID;
    public string currentRegion;
    public dConstants.ChronicleConst.ChronicleStatus status;
    public bool skipIntro;
    public bool activityQueued;
    public bool secondActivity;
    public bool endOptionSelected;
    public string QueuedSpecialEvent;
    public bool allowSkip;
    public List<ChronicleActionLog> pastActions;
    public ChronicleActionLog currentAction;
    public List<string> currentOptions;
    public int actionLogCount;
    public int inSessionStartSequenceIndex;


    [Header("History")]
    public List<string> visitedParks;
    public List<string> artFilepath;

    public void Reset()
    {
        parkUID = "";
        activityUID = "";
        currentRegion = "";
        status = dConstants.ChronicleConst.ChronicleStatus.journalSelection;
        skipIntro = true;
        activityQueued = false;
        secondActivity = false;
        endOptionSelected = false;
        QueuedSpecialEvent = "";
        allowSkip = false;
        pastActions = new List<ChronicleActionLog>();
        currentAction = new ChronicleActionLog();
        currentOptions = new List<string>();
        actionLogCount = 0;
        inSessionStartSequenceIndex = 1;

        visitedParks = new List<string>();
        artFilepath = new List<string>();

        
    }

}
