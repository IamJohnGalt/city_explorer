﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ChronicleActionLog
{
    public int actionIndex;
    public Date date;
    public string actionType;
    public string actionUID;
    public string desc;
    public List<string> tokenPaths;
    public List<string> viewPaths;

    public ChronicleActionLog()
    {
        actionIndex = 0;
        date = new Date();
        actionType = "";
        actionUID = "";
        desc = "";
        tokenPaths = new List<string>();
        viewPaths = new List<string>();
    }
}

[System.Serializable]
public class ChronicleActivity : PFEntrywGate
{
    public string log_desc;
    public int coinCost;
    public int modifiedCoinCost
    {
        get
        {
            TotalBuffValueSet coinBVS = ActiveBuffs.controller.RetrieveValueSetByAction("coin_cost_on_chronicle");
            return ActiveBuffs.ModifyByBVS(coinCost, coinBVS);
        }
    }
    public int timeCost;
    public int modifiedTimeCost
    {
        get
        {
            TotalBuffValueSet timeBVS = ActiveBuffs.controller.RetrieveValueSetByAction("time_cost_on_chronicle");
            return ActiveBuffs.ModifyByBVS(timeCost, timeBVS);
        }
    }
    public CardArt artAsset;
    public float buffChance;
    public List<string> buffScope;
    public List<string> buffUID;
    public List<string> buffValue;
    public List<int> buffDuration;

    public ChronicleActivity() : base()
    {
        log_desc = "";
        coinCost = 0;
        timeCost = 0;
        artAsset = new CardArt();
        buffChance = 1;
        buffScope = new List<string>();
        buffUID = new List<string>();
        buffValue = new List<string>();
        buffDuration = new List<int>();
    }
}
[System.Serializable]
public class JournalLog
{
    public string UID;
    public int totalStars;
    public Date submittedDate;

    public JournalLog()
    {
        UID = "";
        totalStars = 0;
        submittedDate = new Date();
    }
}
