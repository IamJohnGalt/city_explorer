﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Chronicle/Dict/Activity")]
public class Dict_ChronicleActivity : GenericDataDict<ChronicleActivity>
{
    public override void DebugCount()
    {
        Debug.Log("Total of " + UIDList.Count + " activities are added to Chronicle Activity Dictionary");
    }
    public List<PFEntrywGate> GetPFEntrywGateList()
    {
        List<PFEntrywGate> result = new List<PFEntrywGate>();
        List<ChronicleActivity> fullList = GetFullList();
        foreach (ChronicleActivity item in fullList)
        {
            result.Add(item);
        }
        return result;
    }
    public List<PFEntrywGate> GetPFEntrywGateListWithinCoin(int coin)
    {
        List<PFEntrywGate> result = new List<PFEntrywGate>();
        List<ChronicleActivity> fullList = GetFullList();
        foreach (ChronicleActivity item in fullList)
        {
            if(item.coinCost <= coin)
            {
                result.Add(item);
            }
        }
        return result;
    }
}
