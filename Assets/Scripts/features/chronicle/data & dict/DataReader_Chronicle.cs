﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataReader_Chronicle : GenericDataReader
{
    public Dict_ChronicleActivity dChronicleActivity;

    public override void LoadFromXLS()
    {
        //dChronicleDestination.Reset();
        dChronicleActivity.Reset();
        LoadActivityData("Chronicle - activity_exporter");
        FinishLoading();
    }
    void LoadActivityData(string filename)
    {
        string[,] string_data = CSVReader.SplitCsvGrid(Resources.Load<TextAsset>("XLSData/" + filename).text);
        if (string_data[0, 0] != "UID")
        {
            Debug.LogError("Invalid File (No UID found): " + filename);
            return;
        }
        if (dChronicleActivity == null)
        {
            Debug.Log("Null Ref on Data Dictionary");
        }
        for (int iRow = 1; iRow < string_data.GetUpperBound(1); iRow++)
        {
            ChronicleActivity act = new ChronicleActivity();
            act.UID = string_data[0, iRow];
            act.title = string_data[1, iRow];
            act.desc = string_data[2, iRow];
            act.log_desc = string_data[3, iRow];

            act.release = string_data[4, iRow];
            act.tags = SplitString(string_data[5, iRow], ',');
            int.TryParse(string_data[6, iRow], out act.tier);
            float.TryParse(string_data[7, iRow], out act.weight);

            act.artAsset.objectArt = string_data[8, iRow];
            int.TryParse(string_data[9, iRow], out act.coinCost);
            int.TryParse(string_data[10, iRow], out act.timeCost);
            

            int temp_int = 0;
            float.TryParse(string_data[11, iRow], out act.buffChance);
            //buff 1
            act.buffScope.Add(string_data[12, iRow]);
            act.buffUID.Add(string_data[13, iRow]);
            act.buffValue.Add(string_data[14, iRow]);
            int.TryParse(string_data[15, iRow], out temp_int);
            act.buffDuration.Add(temp_int);

            //buff 2
            act.buffScope.Add(string_data[16, iRow]);
            act.buffUID.Add(string_data[17, iRow]);
            act.buffValue.Add(string_data[18, iRow]);
            int.TryParse(string_data[19, iRow], out temp_int);
            act.buffDuration.Add(temp_int);

            dChronicleActivity.Add(act.UID, act);
        }
        dChronicleActivity.DebugCount();
    }
}
