﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class gChronicle : MonoBehaviour
{
    public static gChronicle controller;

    [Header("Data")]
    [SerializeField] dataChronicle gameplayData;
    [SerializeField] dataJournal journalData;
    [SerializeField] dataEndSession endingData;
    [SerializeField] dataPlayerTrait traitData;
    [SerializeField] gJournal journalScript;
    [SerializeField] uiJournalPage journalPage;

    [Header("Game Event")]
    [SerializeField] GameEvent EventChronicleUpdate;
    [SerializeField] GameEvent EventMenuTimeReveal;
    [SerializeField] GameEvent EventMenuCoinReveal;
    [SerializeField] GameEvent EventMenuProgressReveal;
    [SerializeField] GameEvent EventSaveGame;

    private void Awake()
    {
        if (controller == null)
        {
            controller = this;
        }
        else if (controller != null)
        {
            Destroy(gameObject);
        }
    }
    public void TraitSelected(int index)
    {
        TraitManagement.controller.TraitSelected(index);
        Trait trait = dDataHub.hub.traitDict.GetFromUID(TraitManagement.controller.GetLatestTraitUID());
        gameplayData.currentAction.desc = string.Format("获得特质: {0}", trait.title);
        //gameplayData.currentAction.artPaths.Add(string.Format("Art/token/{0}", trait.art.objectArt));

        ConcludeCurrentAction();
    }
    public void JournalSelected(int index)
    {
        JournalOption curOption = dDataHub.hub.dJournalOption.GetFromUID(gameplayData.currentOptions[index]);
        //Debug.Log(string.Format("filter options: ({0}), slotRangMin ({1})", curOption.eligibleFilters[0], curOption.slotRange.minInt));
        List<string> journalStyleUIDs = PFEntryDeck.DrawCardswConditions(dDataHub.hub.dJournalStyle.GetPFEntrywGateList(),
            1,
            curOption.eligibleFilters,
            curOption.slotRange);
        if(journalStyleUIDs.Count == 0)
        {
            Debug.LogError(string.Format("unable to find a valid journal style as the result of option ({0})", curOption.UID));
            //journalStyleUIDs = PFEntryDeck.DrawCardswConditions(dDataHub.hub.dJournalStyle.GetPFEntrywGateList(),1);
            //Debug.LogError(string.Format("unable to find a valid journal style as the result of option ({0})", journalStyleUIDs.Count));
            return;
        }
        journalScript.JournalSelected(journalStyleUIDs[0]);
        gameplayData.currentAction.desc = string.Format("开始创作{0}", journalData.journalName);
        AddArtForCurrentAction(journalData.journalBgArt);
        ConcludeCurrentAction();
    }

    public void JournalSubmitted()
    {
        gameplayData.currentAction.desc = string.Format("{0}发表", journalData.journalName);
        AddArtForCurrentAction(journalData.journalBgArt);
        //gameplayData.status = dConstants.ChronicleConst.ChronicleStatus.inJournalReward;
        JournalRewardClaimStart();
        ConcludeCurrentAction();
        EventChronicleUpdate.Raise();
    }
    public void JournalConlusionSave()
    {
        EventSaveGame.Raise();
        SaveManager.controller.SaveToFile();
    }
    public void SubmissionSelected(int index)
    {
        if(gameplayData.currentOptions[index] == "journal_submission_submit")
        {
            gJournal.controller.EnableJournalSubmission();
        }
        else
        {
            DetermineNextAction();
            gJournal.controller.ForceJournalUpdate();
        }
    }
    public void JournalRewardClaimStart()
    {
        gameplayData.status = dConstants.ChronicleConst.ChronicleStatus.inJournalReward;
    }

    public void JournalRewardClaim()
    {
        int CurrentClaimedIndex = -1;
        for(int i = 0; i < journalData.constantRewards.Count; i++)
        {
            if (!journalData.constantRewards[i].claimed)
            {
                CurrentClaimedIndex = i;
                break;
            }
        }
        journalScript.RewardClaim(CurrentClaimedIndex);
        if(journalData.RewardAllClaimed)
        {
            //conclude journal reward claim phase
            journalScript.RewardClaimConclude();
        }
        ConcludeCurrentAction();
    }
    public void EndConfirmed()
    {
        EndSession.controller.SessionEndConfirmedInChronicle();
    }
    public void ParkSelected(int index)
    {
        journalData.parkVisited = true;
        gameplayData.parkUID = gameplayData.currentOptions[index];
        gameplayData.status = dConstants.ChronicleConst.ChronicleStatus.inPark;
        //queue an activity if not in session start mode
        gameplayData.activityQueued = true;

        Park park = dDataHub.hub.dPark.GetFromUID(gameplayData.parkUID);
        gameplayData.currentAction.desc = string.Format("游于{0}{1},饱览美景", park.regionText, park.title);

        gParkVisit.controller.LaunchParkbyUID(park.UID);

        //coin & time cost when park visit starts
        CurrencyManagement.wallet.RemoveCurrency("currency_coin", park.modifiedCoinCost);
        CalendarManagement.calendar.QueuedTimeElapse(park.modifiedTimeCost);
        //trigger park gameplay
    }
    public void ActiivitySelected(int index)
    {
        //trigger help check
        //CharacterHelp.controller.HelpCheck_Character1_HelpLv4(gameplayData.currentOptions[index]);

        ChronicleActivity act = dDataHub.hub.dChronicleActivity.GetFromUID(gameplayData.currentOptions[index]);
        if (act.modifiedCoinCost <= CurrencyManagement.wallet.CheckCurrency("currency_coin") || act.modifiedCoinCost <= 0)
        {
            gameplayData.activityUID = gameplayData.activityUID != "" ? gameplayData.activityUID : gameplayData.currentOptions[index];
            gameplayData.activityUID = gameplayData.activityUID == "" ? gameplayData.currentOptions[index] : gameplayData.activityUID;
            gameplayData.status = dConstants.ChronicleConst.ChronicleStatus.inActivity;
            ExecuteActivity(gameplayData.activityUID);
        }
    }
    public void ActiivitySkipped()
    {
        if(gameplayData.status == dConstants.ChronicleConst.ChronicleStatus.activity)
        {
            gameplayData.status = dConstants.ChronicleConst.ChronicleStatus.inActivity;
            gameplayData.currentAction.desc = string.Format("回到镇上什么也没有做");
            //gameplayData.currentAction.artPaths.Add(string.Format("Art/token/{0}", act.artAsset.objectArt));
            CalendarManagement.calendar.TimeElapse(dConstants.ChronicleConst.SKIP_ACTIVITY_TIME_COST);
            ConcludeCurrentAction();
        }
        else if(gameplayData.status == dConstants.ChronicleConst.ChronicleStatus.destination)
        {
            gameplayData.activityQueued = true;
            gameplayData.status = dConstants.ChronicleConst.ChronicleStatus.inPark;
            gameplayData.currentAction.desc = string.Format("在镇上又呆了一段时间");
            CalendarManagement.calendar.TimeElapse(dConstants.ChronicleConst.SKIP_PARK_TIME_COST);
            //gameplayData.currentAction.artPaths.Add(string.Format("Art/token/{0}", act.artAsset.objectArt));
            ConcludeCurrentAction();
        }
        else if (gameplayData.status == dConstants.ChronicleConst.ChronicleStatus.journalSubmission)
        {
            DetermineNextAction();
            gJournal.controller.ForceJournalUpdate();
        }
    }

    public void PlayerLevelUp()
    {
        CreateNewAction();
        PlayerLevelManagement.controller.LevelUpWithRandomBonus();
        PlayerLevel plevel = dDataHub.hub.plevelDict.GetFromUID(PlayerLevelManagement.controller.GetLatestLevelUpUID());
        gameplayData.status = dConstants.ChronicleConst.ChronicleStatus.inLevelup;
        gameplayData.currentAction.desc = string.Format("积累足够的里程，{0}", plevel.desc);
        //gameplayData.currentAction.artPaths.Add(string.Format("Art/token/{0}", plevel.art.objectArt));
        ConcludeCurrentAction();
    }
    void CreateNewAction()
    {
        gameplayData.currentAction = new ChronicleActionLog();
        gameplayData.currentAction.date = CalendarManagement.calendar.GetCurDate();
        gameplayData.currentOptions.Clear();
    }
    public void Add2PastAction(ChronicleActionLog log)
    {
        gameplayData.pastActions.Add(log);
        log.actionIndex = gameplayData.pastActions.Count;
    }
    public void ConcludeCurrentAction()
    {
        Add2PastAction(gameplayData.currentAction);
        if (gameplayData.status == dConstants.ChronicleConst.ChronicleStatus.inActivity)
        {
            if(gameplayData.activityUID != "")
            {
                ChronicleActivity act = dDataHub.hub.dChronicleActivity.GetFromUID(gameplayData.activityUID);
                CurrencyManagement.wallet.RemoveCurrency("currency_coin", act.modifiedCoinCost);

                //trigger help check
                //CharacterHelp.controller.HelpCheck_Character2_HelpLv2(act.modifiedCoinCost);
                CalendarManagement.calendar.TimeElapse(act.modifiedTimeCost);
                ActiveBuffs.controller.TimerCountByProvider("in_chronicle_activity", 1);
            }
            gameplayData.activityUID = "";
        }
        else if (gameplayData.status == dConstants.ChronicleConst.ChronicleStatus.inPark)
        {
            if(gameplayData.parkUID != "")
            {
                Park park = dDataHub.hub.dPark.GetFromUID(gameplayData.parkUID);
                gameplayData.currentRegion = park.regionUID;
                //time elapse when park visit ends
                CalendarManagement.calendar.TimeElapse(0);
                ActiveBuffs.controller.TimerCountByProvider("park_visit", 1);
            }
            gameplayData.parkUID = "";
        }
        else
        {
            CalendarManagement.calendar.TimeElapse(0);
        }
        gameplayData.allowSkip = false;
        DetermineNextAction();
    }
    public void DetermineNextAction()
    {
        if(gameplayData.inSessionStartSequenceIndex >= 0)
        {
            DetermineNextSessionStartAction();
        }
        else if (gameplayData.QueuedSpecialEvent != "")
        {
            StartScheduledSpecialEvent();
        }
        else if(gameplayData.status == dConstants.ChronicleConst.ChronicleStatus.inJournalReward && !journalData.RewardAllClaimed)
        {
            //continue journal claim
            //display handled in uiJOurnalPage
            StartJournalClaim();
        }
        else if (PlayerLevelManagement.controller.NextLevelUp())
        {
            PlayerLevelUp();
        }
        else if (TraitManagement.controller.NextTraitAvaialble())
        {
            GenerateTraitOptions();
        }
        else if(journalData.gameplayStatus == dConstants.JournalConst.JournalStatus.selection && !endingData.ended)
        {
            //CalendarManagement.calendar.CheckEndGameByTime();
            GenerateJournalOptions();
        }
        else if (gameplayData.status != dConstants.ChronicleConst.ChronicleStatus.journalSubmission
            && journalData.journalIsSubmittable
            && journalData.gameplayStatus != dConstants.JournalConst.JournalStatus.selection)
        {
            GenerateSubmissionOptions();
        }
        else
        {
            //CurrencyManagement.wallet.NoMoneyEndCheck();
            if (endingData.ended)
            {
                if (!gameplayData.endOptionSelected)
                {
                    GenerateEndOptions();
                }
                else if (ActiveBuffs.controller.CheckFeatureGate("feature_gate_persona_endgame_coin2star"))
                {
                    AddCoin2StarActivity();
                    CharacterPersona.controller.Persona_Session_End_Coin2Star();
                }
                else
                {
                    GenerateSessionEndOptions();
                }
                //CalendarManagement.calendar.CheckEndGameByTime(); 
            }
            else if (gameplayData.activityQueued)
            {
                GenerateActivityOptions();
            }
            else if (gameplayData.secondActivity)
            {
                GenerateActivityOptions();
            }
            else
            {
                GenerateParkOptions();
            }
        }
    }
    public void CreateDescForCurrentAction(string desc)
    {
        gameplayData.currentAction.desc = desc;
    }
    public void AppendDescForCurrentAction(string desc)
    {
        gameplayData.currentAction.desc += desc;
    }
    public void AddArtForCurrentAction(string filepath, bool isView = false)
    {
        if (!isView)
        {
            gameplayData.currentAction.tokenPaths.Add(filepath);
        }
        else
        {
            gameplayData.currentAction.viewPaths.Add(filepath);
        }
        
    }
    public void InitChronicle()
    {
        Debug.LogError("deprecated function, should not be called");
        gameplayData.Reset();
        ChronicleActionLog firstlog = new ChronicleActionLog();
        firstlog.date = CalendarManagement.calendar.GetCurDate();
        firstlog.desc = string.Format("星夜奇象，五星连珠，{0}将在明天踏上自己为期三年的旅途...",CharacterManagement.controller.GetCurrentCharacter().characterName);
        firstlog.actionIndex = gameplayData.actionLogCount++;
        gameplayData.pastActions.Add(firstlog);
        CalendarManagement.calendar.TimeElapse(0);
        GenerateStartOption();
        //DetermineNextAction();
    }
    void DetermineNextSessionStartAction()
    {
        switch (gameplayData.inSessionStartSequenceIndex)
        {
            case 0:
                SessionStart_0();
                break;
            case 1:
                if (gameplayData.skipIntro)
                {
                    SessionStart_Skip();
                }
                else
                {
                    SessionStart_1();
                }
                break;
            case 2:
                //SessionStart_2();
                //break;
            //case 3:
                SessionStart_3();
                break;
            case 4:
                SessionStart_4();
                break;
            case 5:
                SessionStart_5();
                break;
            case 6:
                SessionStart_6();
                break;
            case 7:
                SessionStart_7();
                break;
            default:
                Debug.LogError("invalid session start index: " + gameplayData.inSessionStartSequenceIndex);
                break;
        }
    }
    public void SessionStart_0()
    {
        gameplayData.Reset();

        CreateNewAction();
        gameplayData.status = dConstants.ChronicleConst.ChronicleStatus.activity;
        gameplayData.allowSkip = false;
        gameplayData.currentAction.desc = "开始旅程";
        gameplayData.currentOptions.Clear();
        gameplayData.currentOptions.Add("activity_session_start_play_intro");
        gameplayData.currentOptions.Add("activity_session_start_skip_intro");

        gameplayData.inSessionStartSequenceIndex = 1;
        EventChronicleUpdate.Raise();

        //DetermineNextAction();
    }
    public void SessionStart_Skip()
    {
        //1
        gJournal.controller.AddToken("collectible_qingkong_v1", true);
        gJournal.controller.AddToken("collectible_niaoqun_v2", true);
        CurrencyManagement.wallet.AddCurrency_Setup("currency_star", 1);
        ChronicleActionLog log = new ChronicleActionLog();
        log.date = CalendarManagement.calendar.GetCurDate();
        log.desc = string.Format("游于邻家小山,饱览美景");
        log.viewPaths.Add("view_qingkong");
        log.viewPaths.Add("view_niaoqun");
        log.actionIndex = gameplayData.actionLogCount++;
        gameplayData.pastActions.Add(log);

        //3
        log = new ChronicleActionLog();
        log.date = CalendarManagement.calendar.GetCurDate();
        log.desc = string.Format("邻家小山的旅行激起了{0}内心的波澜，一发不可收拾，踏上了自己传奇的旅途...", CharacterManagement.controller.GetCurrentCharacter().characterClass);
        log.actionIndex = gameplayData.actionLogCount++;
        gameplayData.pastActions.Add(log);
        //4
        log = new ChronicleActionLog();
        log.date = CalendarManagement.calendar.GetCurDate();
        log.desc = "告知家人出行的计划";
        log.actionIndex = gameplayData.actionLogCount++;
        gameplayData.pastActions.Add(log);
        EventMenuTimeReveal.Raise();
        //5
        log = new ChronicleActionLog();
        log.date = CalendarManagement.calendar.GetCurDate();
        log.desc = "取出自己的一些积蓄";
        log.actionIndex = gameplayData.actionLogCount++;
        gameplayData.pastActions.Add(log);
        EventMenuCoinReveal.Raise();

        gameplayData.inSessionStartSequenceIndex = 6;
        DetermineNextAction();
    }
    public void SessionStart_1()
    {
        gameplayData.Reset();
        //directly enter the first park
        CreateNewAction();
        gameplayData.status = dConstants.ChronicleConst.ChronicleStatus.inPark;
        gameplayData.currentOptions.Clear();
        gameplayData.currentOptions.Add("park_session_start");
        //add buff to make sure player can finish it
        //ActiveBuffs.controller.ActivateBuff("", "park_visit", "buff_path_progress_req_multi", -0.5f, 1);
        ParkSelected(0);
        gameplayData.activityQueued = false;
        gameplayData.inSessionStartSequenceIndex++;
        EventChronicleUpdate.Raise();
    }
    //deprecated
    /*
    public void SessionStart_2()
    {
        GenerateSubmissionOptions();
        
        gameplayData.inSessionStartSequenceIndex++;
        
    }
    */
    public void SessionStart_3()
    {
        //after submit the first journal
        ChronicleActionLog firstlog = new ChronicleActionLog();
        firstlog.date = CalendarManagement.calendar.GetCurDate();
        firstlog.desc = string.Format("邻家小山的旅行激起了{0}内心的波澜，一发不可收拾，踏上了自己传奇的旅途...", CharacterManagement.controller.GetCurrentCharacter().characterClass);
        firstlog.actionIndex = gameplayData.actionLogCount++;
        gameplayData.pastActions.Add(firstlog);

        gameplayData.inSessionStartSequenceIndex += 2;
        DetermineNextAction();
        EventChronicleUpdate.Raise();
    }
    public void SessionStart_4()
    {
        //after submit the first journal
        CreateNewAction();
        gameplayData.status = dConstants.ChronicleConst.ChronicleStatus.activity;
        gameplayData.allowSkip = false;
        gameplayData.currentAction.desc = "告知家人出行的计划";
        gameplayData.currentOptions.Clear();
        gameplayData.currentOptions.Add("activity_session_start_claim_time");

        gameplayData.inSessionStartSequenceIndex++;
        EventChronicleUpdate.Raise();
    }
    public void SessionStart_5()
    {
        //after submit the first journal
        CreateNewAction();
        gameplayData.status = dConstants.ChronicleConst.ChronicleStatus.activity;
        gameplayData.allowSkip = false;
        gameplayData.currentAction.desc = "取出自己的一些积蓄";
        gameplayData.currentOptions.Clear();
        gameplayData.currentOptions.Add("activity_session_start_claim_coin");

        gameplayData.inSessionStartSequenceIndex++;
        EventChronicleUpdate.Raise();
    }
    public void SessionStart_6()
    {
        //get one extra bonus. after claimed all starting fund.
        GenerateStartOption();
        gameplayData.inSessionStartSequenceIndex++;
    }
    public void SessionStart_7()
    {
        //end the session start. offcially enter the game
        int RemoveAmount = CurrencyManagement.wallet.CheckCurrency("currency_xp");
        CurrencyManagement.wallet.RemoveCurrency("currency_xp", RemoveAmount);
        CreateNewAction();
        gameplayData.status = dConstants.ChronicleConst.ChronicleStatus.activity;
        gameplayData.allowSkip = false;
        gameplayData.currentAction.desc = "正式启程...";
        gameplayData.currentOptions.Clear();
        gameplayData.currentOptions.Add("activity_session_start_claim_progress");

        gameplayData.inSessionStartSequenceIndex = -1;
        EventChronicleUpdate.Raise();
    }
    void AddCoin2StarActivity()
    {
        CreateNewAction();
        gameplayData.status = dConstants.ChronicleConst.ChronicleStatus.activity;
        gameplayData.currentOptions.Clear();
        gameplayData.currentOptions.Add("activity_endgame_coin2star");
        ActiveBuffs.controller.ToggleFeatureGate("feature_gate_persona_endgame_coin2star", false);
        EventChronicleUpdate.Raise();
    }
    void StartScheduledSpecialEvent()
    {
        if (gameplayData.QueuedSpecialEvent == "activity_gain_interests")
        {
            CharacterPersona.controller.Persona_Year_End_Coin();
        }
        CreateNewAction();
        gameplayData.status = dConstants.ChronicleConst.ChronicleStatus.activity;
        gameplayData.currentOptions.Clear();
        gameplayData.currentOptions.Add(gameplayData.QueuedSpecialEvent);
        gameplayData.QueuedSpecialEvent = "";
        
        EventChronicleUpdate.Raise();
    }
    public void GenerateSessionEndOptions()
    {
        CreateNewAction();

        gameplayData.currentAction.desc = "旅程结束，由于...";
        gameplayData.status = dConstants.ChronicleConst.ChronicleStatus.sessionEnd;
        //gameplayData.currentOptions.AddRange(EndSession.controller.GetEndReasons());

        EventChronicleUpdate.Raise();
    }
    public void GenerateTraitOptions()
    {
        CreateNewAction();
        TraitManagement.controller.GenerateTraitOptions();
        gameplayData.currentAction.desc = string.Format("{0}获得了特质...", CharacterManagement.controller.GetCurrentCharacterTitle());
        gameplayData.status = dConstants.ChronicleConst.ChronicleStatus.inTrait;
        gameplayData.currentOptions.Clear();
        gameplayData.currentOptions.AddRange(traitData.traitOptions);
        EventChronicleUpdate.Raise();
    }
    public void GenerateJournalOptions()
    {
        CreateNewAction();
        //gJournal.controller.GenerateJournalOptions();
        List<string> availableFilter = new List<string>();
        List<string> exclusiveFilter = new List<string>();
        int maxTier = dStatistics.stats.data.inSessionJournalLogs.Count + 1;
        //Debug.Log("current journal option tier: " + maxTier);
        TotalBuffValueSet bvs = ActiveBuffs.controller.RetrieveValueSetByAction("journal_option_count");
        int drawCount = ActiveBuffs.ModifyByBVS(dConstants.JournalConst.MAX_OPTIONAL_JOURNAL, bvs);
        gameplayData.currentOptions.AddRange(PFEntryDeck.DrawCardswConditions(
            dDataHub.hub.dJournalOption.GetPFEntrywGateList(),
            drawCount,
            availableFilter,
            new intRange(1, maxTier),
            exclusiveFilter));
        gameplayData.currentAction.desc = "动笔下一篇游记...";
        gameplayData.status = dConstants.ChronicleConst.ChronicleStatus.journalSelection;

        EventChronicleUpdate.Raise();
    }
    public void GenerateSubmissionOptions()
    {
        CreateNewAction();
        gameplayData.currentAction.desc = string.Format("{0}已经很完整了...", journalData.journalName);

        gameplayData.status = dConstants.ChronicleConst.ChronicleStatus.journalSubmission;
        gameplayData.currentOptions.Add("journal_submission_submit");
        //gameplayData.currentOptions.Add("journal_submission_polish");

        EventChronicleUpdate.Raise();
    }
    public void StartJournalClaim()
    {
        CreateNewAction();
        gameplayData.currentAction.desc = string.Format("{0}的发表带来了...", journalData.journalName);

        EventChronicleUpdate.Raise();
    }
    public void GenerateStartOption()
    {
        CreateNewAction();
        gameplayData.currentAction.desc = "临行的前一晚...";

        gameplayData.status = dConstants.ChronicleConst.ChronicleStatus.activity;
        gameplayData.allowSkip = false;
        List<string> applicableFilter = new List<string>();
        applicableFilter.Add("start");
        //to do: advanced activity draw process? not sure what is needed here :(
        gameplayData.currentOptions = PFEntryDeck.DrawCardswConditions(
           dDataHub.hub.dChronicleActivity.GetPFEntrywGateList(),
           dConstants.ChronicleConst.MAX_OPTIONAL_ACTIVITY,
           applicableFilter,
           new intRange(0, 0)
           );
        gameplayData.currentOptions.Sort((a, b) => Random.value.CompareTo(Random.value));

        EventChronicleUpdate.Raise();
    }
    public void GenerateEndOptions()
    {
        CreateNewAction();
        gameplayData.currentAction.desc = "踏上归途前的最后一晚...";
        gameplayData.endOptionSelected = true;
        gameplayData.status = dConstants.ChronicleConst.ChronicleStatus.activity;
        gameplayData.allowSkip = false;
        List<string> applicableFilter = new List<string>();
        applicableFilter.Add("end");
        //to do: advanced activity draw process? nort sure what is needed here :(
        gameplayData.currentOptions = PFEntryDeck.DrawCardswConditions(
           dDataHub.hub.dChronicleActivity.GetPFEntrywGateList(),
           dConstants.ChronicleConst.MAX_OPTIONAL_ACTIVITY,
           applicableFilter,
           new intRange(0, 0)
           );
        if(journalData.CurTokenCount > 0 && journalData.gameplayStatus == dConstants.JournalConst.JournalStatus.journal_working)
        {
            gameplayData.currentOptions.Add("activity_end_fengbi");
        }
        gameplayData.currentOptions.Sort((a, b) => Random.value.CompareTo(Random.value));
        EventChronicleUpdate.Raise();
    }
    public void GenerateParkOptions()
    {
        CreateNewAction();
        gameplayData.currentAction.desc = "动身前往下一地区...";

        TotalBuffValueSet bvs = ActiveBuffs.controller.RetrieveValueSetByAction("park_option_count");
        int drawCount = ActiveBuffs.ModifyByBVS(dConstants.ChronicleConst.MAX_OPTIONAL_PARK, bvs);
        gameplayData.allowSkip = false;

        List<string> applicableFilter = new List<string>();
        List<string> options = new List<string>();

        gameplayData.status = dConstants.ChronicleConst.ChronicleStatus.destination;

        //draw small park
        options = PFEntryDeck.DrawCardswConditions(
           dDataHub.hub.dPark.GetPFEntrywGateList(),
           1,
           applicableFilter,
           new intRange(1, 1));
        if (options.Count > 0)
        {
            gameplayData.currentOptions.Add(options[0]);
        }
        //draw mid park
        options = PFEntryDeck.DrawCardswConditions(
           dDataHub.hub.dPark.GetPFEntrywGateList(),
           1,
           applicableFilter,
           new intRange(2, 2));
        if (options.Count > 0)
        {
            gameplayData.currentOptions.Add(options[0]);
        }
        //draw large park
        options = PFEntryDeck.DrawCardswConditions(
           dDataHub.hub.dPark.GetPFEntrywGateList(),
           1,
           applicableFilter,
           new intRange(3, 3));
        if (options.Count > 0)
        {
            gameplayData.currentOptions.Add(options[0]);
        }
        EventChronicleUpdate.Raise();
    }
    public void GenerateActivityOptions()
    {
        if (ActiveBuffs.controller.CheckFeatureGate("feature_gate_character_2nd_activity"))
        {
            if (gameplayData.activityQueued)
            {
                gameplayData.secondActivity = true;
                gameplayData.activityQueued = false;
                CreateNewAction();
                gameplayData.currentAction.desc = "游览结束，回到附近的镇上随便逛逛...";
            }
            else if (gameplayData.secondActivity)
            {
                //CharacterPersona.controller.Character2_PersonaLv3();
                gameplayData.secondActivity = false;
                CreateNewAction();
                gameplayData.currentAction.desc = "在镇上多逛逛...";
            }
        }
        else
        {
            gameplayData.activityQueued = false;
            CreateNewAction();
            gameplayData.currentAction.desc = "游览结束，回到附近的镇上随便逛逛...";
        }

        int curCoin = Mathf.Max(0, CurrencyManagement.wallet.CheckCurrency("currency_coin"));
        TotalBuffValueSet bvs = ActiveBuffs.controller.RetrieveValueSetByAction("activity_option_count");
        int drawCount = ActiveBuffs.ModifyByBVS(dConstants.ChronicleConst.MAX_OPTIONAL_ACTIVITY, bvs);

        gameplayData.status = dConstants.ChronicleConst.ChronicleStatus.activity;
        gameplayData.allowSkip = false;
        List<string> options = new List<string>();
        List<string> applicableFilter = new List<string>();
        applicableFilter.Add(gameplayData.currentRegion);
        applicableFilter.Add("shared");
        //gen tier 2
        options = PFEntryDeck.DrawCardswConditions(
           dDataHub.hub.dChronicleActivity.GetPFEntrywGateListWithinCoin(curCoin),
           1,
           applicableFilter,
           new intRange(2, 2)
           );
        if(options.Count > 0)
        {
            gameplayData.currentOptions.Add(options[0]);
        }
        //gen tier 3
        options = PFEntryDeck.DrawCardswConditions(
           dDataHub.hub.dChronicleActivity.GetPFEntrywGateList(),
           1,
           applicableFilter,
           new intRange(3, 3)
           );
        if (options.Count > 0)
        {
            gameplayData.currentOptions.Add(options[0]);
        }
        //gen tier 4
        options = PFEntryDeck.DrawCardswConditions(
           dDataHub.hub.dChronicleActivity.GetPFEntrywGateList(),
           1,
           applicableFilter,
           new intRange(4, 4)
           );
        if (options.Count > 0)
        {
            gameplayData.currentOptions.Add(options[0]);
        }
        //gameplayData.currentOptions.Reverse();
        if (gameplayData.currentOptions.Count < drawCount)
        {
           gameplayData.currentOptions.InsertRange(0, PFEntryDeck.DrawCardswConditions(
           dDataHub.hub.dChronicleActivity.GetPFEntrywGateListWithinCoin(curCoin),
           drawCount - gameplayData.currentOptions.Count,
           applicableFilter,
           new intRange(1, 1)
           ));
        }
        //gameplayData.currentOptions.Sort((a, b) => Random.value.CompareTo(Random.value));
        EventChronicleUpdate.Raise();
    }
    void RedrawActivity()
    {
        //Park park = dDataHub.hub.dPark.GetFromUID(gameplayData.parkUID);
        //charege the redraw activity
        ChronicleActivity act = dDataHub.hub.dChronicleActivity.GetFromUID(gameplayData.activityUID);
        CurrencyManagement.wallet.RemoveCurrency("currency_coin", act.modifiedCoinCost);

        int curCoin = Mathf.Max(0, CurrencyManagement.wallet.CheckCurrency("currency_coin"));
        TotalBuffValueSet bvs = ActiveBuffs.controller.RetrieveValueSetByAction("activity_option_count");
        int drawCount = ActiveBuffs.ModifyByBVS(dConstants.ChronicleConst.MAX_OPTIONAL_ACTIVITY, bvs);

        List<string> applicableFilter = new List<string>();
        applicableFilter.Add(gameplayData.currentRegion);
        applicableFilter.Add("shared");
        List<string> excludingList = new List<string>();
        excludingList.Add(gameplayData.activityUID);
        excludingList.AddRange(gameplayData.currentOptions);
        gameplayData.currentOptions.Clear();

        List<string> options = new List<string>();
        //gen tier 2
        options = PFEntryDeck.DrawCardswConditions(
           dDataHub.hub.dChronicleActivity.GetPFEntrywGateListWithinCoin(curCoin),
           1,
           applicableFilter,
           new intRange(2, 2),
           excludingList
           );
        if (options.Count > 0)
        {
            gameplayData.currentOptions.Add(options[0]);
        }
        //gen tier 3
        options = PFEntryDeck.DrawCardswConditions(
           dDataHub.hub.dChronicleActivity.GetPFEntrywGateList(),
           1,
           applicableFilter,
           new intRange(3, 3),
           excludingList
           );
        if (options.Count > 0)
        {
            gameplayData.currentOptions.Add(options[0]);
        }
        //gen tier 4
        options = PFEntryDeck.DrawCardswConditions(
           dDataHub.hub.dChronicleActivity.GetPFEntrywGateList(),
           1,
           applicableFilter,
           new intRange(4, 4),
           excludingList
           );
        if (options.Count > 0)
        {
            gameplayData.currentOptions.Add(options[0]);
        }
        //gameplayData.currentOptions.Reverse();

        if (gameplayData.currentOptions.Count < drawCount)
        {
           gameplayData.currentOptions.InsertRange(0, PFEntryDeck.DrawCardswConditions(
               dDataHub.hub.dChronicleActivity.GetPFEntrywGateListWithinCoin(curCoin),
               drawCount - gameplayData.currentOptions.Count,
               applicableFilter,
               new intRange(1, 1),
               excludingList
           ));
        }
        //gameplayData.currentOptions.Sort((a, b) => Random.value.CompareTo(Random.value));
        //reset to choose new activity
        gameplayData.status = dConstants.ChronicleConst.ChronicleStatus.activity;
        gameplayData.activityUID = "";
    }
    void RandomActivity()
    {
        //Park park = dDataHub.hub.dPark.GetFromUID(gameplayData.parkUID);
        List<string> applicableFilter = new List<string>();
        applicableFilter.Add(gameplayData.currentRegion);
        applicableFilter.Add("shared");
        List<string> excludingList = new List<string>();
        excludingList.Add(gameplayData.activityUID);
        //to do: advanced activity draw process? nort sure what is needed here :(
        List<string> options = PFEntryDeck.DrawCardswConditions(
           dDataHub.hub.dChronicleActivity.GetPFEntrywGateList(),
           1,
           applicableFilter,
           new intRange(2, 4),
           excludingList
           );
        if(options.Count > 0)
        {
            ExecuteActivity(options[0]);
        }
        else
        {
            Debug.LogError("can't find an eligbile chronicle activity");
        }
    }
    void ExecuteActivity(string activityUID)
    {
        bool isRedrawn = false;
        int queuedRandomActivity = 0;
        ChronicleActivity act = dDataHub.hub.dChronicleActivity.GetFromUID(activityUID);

        float rng = Random.Range(0, 1f);
        if ((rng < act.buffChance))
        {
            //Debug.Log("post action applied");
            for (int i = 0; i < act.buffUID.Count; i++)
            {
                if (act.buffUID[i] != "")
                {
                    int bfVInt = 0;
                    int.TryParse(act.buffValue[i], out bfVInt);
                    float bfVFloat = 0;
                    float.TryParse(act.buffValue[i], out bfVFloat);
                    //determine if a buff is a park specific effect
                    bool isRegularBuff = false;
                    switch (act.buffUID[i])
                    {
                        case "random_token":
                            //to do: expand it into a randomized set
                            //Debug.LogError(string.Format("deprecated key as random_info is called"));
                            GrantRandomCharacToken();
                            break;
                        case "display_menu_time":
                            EventMenuTimeReveal.Raise();
                            break;
                        case "display_menu_coin":
                            EventMenuCoinReveal.Raise();
                            break;
                        case "display_menu_progress":
                            EventMenuProgressReveal.Raise();
                            break;
                        case "random_park_buff_grant":
                            GrantRandomParkBuff(act.UID);
                            break;
                        case "random_journal_buff_grant":
                            GrantRandomJournalRule();
                            break;
                        case "redraw_activity":
                            isRedrawn = true;
                            break;
                        case "random_activity":
                            queuedRandomActivity = bfVInt;
                            break;
                        case "add_star_by_current_slots":
                            int curTokeninJournal = journalData.CurTokenCount;
                            CurrencyManagement.wallet.AddCurrency("currency_star", curTokeninJournal);
                            break;
                        case "add_collectible":
                            gJournal.controller.AddToken(act.buffValue[i]);
                            AddArtForCurrentAction(dDataHub.hub.dCollectible.GetFromUID(act.buffValue[i]).artAsset.objectArt, true);
                            break;
                        case "add_sea_collectible_by_tier":
                            int targetTier = bfVInt;
                            List<string> terrainFilter = new List<string>();
                            //terrainFilter.Add("sea");
                            List<string> collectibles = PFEntryDeck.DrawCardswConditions(
                                dDataHub.hub.dCollectible.GetPFEntrywGateListWithTag("sea"),
                                1,
                                terrainFilter,
                                new intRange(targetTier, targetTier),
                                journalData.tokens
                                );
                            string collectibleUID = collectibles.Count > 0 ? collectibles[0] : dConstants.JournalConst.FALLBACK_VIEW_UID;
                            //View view = dDataHub.hub.dView.GetFromUID(viewUID);
                            //int rngIndex = Random.Range(0, view.collectibleOptions.Count);
                            gJournal.controller.AddToken(collectibleUID);
                            AddArtForCurrentAction(dDataHub.hub.dCollectible.GetFromUID(collectibleUID).artAsset.objectArt, true);
                            break;
                        case "add_collectible_by_tier":
                            targetTier = bfVInt;
                            terrainFilter = new List<string>();
                            //terrainFilter.Add("sea");
                            collectibles = PFEntryDeck.DrawCardswConditions(
                                dDataHub.hub.dCollectible.GetModifiedPFEntrywGateList(),
                                1,
                                terrainFilter,
                                new intRange(targetTier, targetTier),
                                journalData.tokens
                                );
                            collectibleUID = collectibles.Count > 0 ? collectibles[0] : dConstants.JournalConst.FALLBACK_VIEW_UID;
                            //view = dDataHub.hub.dView.GetFromUID(viewUID);
                            //rngIndex = Random.Range(0, view.collectibleOptions.Count);
                            gJournal.controller.AddToken(collectibleUID);
                            AddArtForCurrentAction(dDataHub.hub.dCollectible.GetFromUID(collectibleUID).artAsset.objectArt, true);
                            break;
                        case "gain_interests":
                            CurrencyManagement.wallet.GetInterests(bfVFloat);
                            break;
                        case "convert_coin2star":
                            CurrencyManagement.wallet.ConvertCoin2Star(bfVInt);
                            break;
                        case "force_submission":
                            gJournal.controller.ForceJournalSubmission();
                            break;
                        case "skip_intro":
                            gameplayData.skipIntro = true;
                            gIntroParkScript.controller.skipIntro = true;
                            break;
                        case "start_intro":
                            gameplayData.skipIntro = false;
                            gIntroParkScript.controller.skipIntro = false;
                            break;
                        default:
                            isRegularBuff = true;
                            break;
                    }
                    if (isRegularBuff)
                    {
                        //this should be a regular buff
                        float value = 0;
                        float.TryParse(act.buffValue[i], out value);
                        if(act.buffUID[i] != "")
                        {
                            ActiveBuffs.controller.ActivateBuff(act.UID, act.buffScope[i], act.buffUID[i], value, act.buffDuration[i]);
                        }
                    }
                }
            }
        }
        if (isRedrawn)
        {
            RedrawActivity();
            EventChronicleUpdate.Raise();
        }
        else if (queuedRandomActivity > 0)
        {
            for(int i=0;i< queuedRandomActivity; i++)
            {
                RandomActivity();
            }
        }
        else
        {
            gameplayData.currentAction.desc = string.Format("{0}{1}", act.log_desc, act.desc);
            //gameplayData.currentAction.artPaths.Add(string.Format("Art/token/{0}", act.artAsset.objectArt));
            ConcludeCurrentAction();
        }
    }
    void GrantRandomParkBuff(string sourceUID)
    {
        int rng = Random.Range(0, 4);
        switch (rng)
        {
            case 0:
                ActiveBuffs.controller.ActivateBuff(sourceUID, "park_visit", "buff_park_danger_base_add", -0.2f, 1);
                break;
            case 1:
                ActiveBuffs.controller.ActivateBuff(sourceUID, "park_visit", "buff_park_collectible_base_add", 0.2f, 1);
                break;
            case 2:
                ActiveBuffs.controller.ActivateBuff(sourceUID, "park_visit", "buff_action_progress_add", 5, 1);
                break;
            case 3:
                ActiveBuffs.controller.ActivateBuff(sourceUID, "park_visit", "buff_action_progress_multi", 0.1f, 1);
                break;
            case 4:
                ActiveBuffs.controller.ActivateBuff(sourceUID, "park_visit", "buff_action_draw_add", 1, 1);
                break;
            default:
                Debug.LogError(string.Format("invalid rng on grantrandom park buff function"));
                break;
        }
        
    }
    void GrantRandomJournalRule()
    {
        int rng = Random.Range(0, 2);
        Debug.LogError(string.Format("invalid function grant random journal buff"));
    }
    void GrantRandomCharacToken()
    {
        List<int> availableCharacterIndex = new List<int>();
        for(int i = 1; i <= dConstants.Character.TOTAL_CHARACTERS; i++)
        {
            if(string.Format("character_{0}", i) != CharacterManagement.controller.GetCurrentCharacter().characterUID)
            {
                int curStoryLv = CharacterManagement.controller.GetCharacterByUID(string.Format("character_{0}", i)).curStoryLevel;
                if (curStoryLv > 0 && curStoryLv < dConstants.Character.MAX_STORY_LEVEL)
                {
                    availableCharacterIndex.Add(i);
                }
            }
        }
        //if no unlocked character use all characters
        if (availableCharacterIndex.Count == 0)
        {
            for (int i = 1; i <= dConstants.Character.TOTAL_CHARACTERS; i++)
            {
                if (string.Format("character_{0}", i) != CharacterManagement.controller.GetCurrentCharacter().characterUID)
                {
                    int curStoryLv = CharacterManagement.controller.GetCharacterByUID(string.Format("character_{0}", i)).curStoryLevel;
                    if (curStoryLv < dConstants.Character.MAX_STORY_LEVEL)
                    {
                        availableCharacterIndex.Add(i);
                    }
                }
            }
        }
        if (availableCharacterIndex.Count == 0)
        {
            Debug.LogError("unable to find another valid character on GrantRandomCharacToken()");
            return;
        }
        int rng = Random.Range(0, availableCharacterIndex.Count);
        CharacterAcquisition.controller.AcquireStory(string.Format("character_{0}", availableCharacterIndex[rng]), string.Format("currency_token_charac{0}", availableCharacterIndex[rng]), 1, "[城中偶遇]");
        
    }
    public void ScheduleGainInterests()
    {
        gameplayData.QueuedSpecialEvent = "activity_gain_interests";
    }
}
