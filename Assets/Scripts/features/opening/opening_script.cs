﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

[System.Serializable]
public class OpeningCaption
{
    public ui_text textObj;
    public float duration;
    public float delay;
}
[System.Serializable]
public class FadingImage
{
    public Image img;
    public float duration;
    public float transition;
    public float delay;
}
[System.Serializable]
public class MovingObject
{
    public RectTransform rect;
    public float transition;
    public Vector2 vector;
    public float delay;
}
[System.Serializable]
public class FillingImage
{
    public Image img;
    public float transition;
    public float end;
    public float delay;
}
[System.Serializable]
public class ZoomingObject
{
    public GameObject obj;
    public float transition;
    public float end;
    public float delay;
}
[System.Serializable]
public class OpeningSFX
{
    public string SFXName;
    public float delay;
}
[System.Serializable]
public class OpeningMusic
{
    public string MusicName;
    public float delay;
}
public class opening_script : MonoBehaviour
{
    [SerializeField] GameObject page_group;
    [SerializeField] List<OpeningCaption> captions;
    [SerializeField] List<FadingImage> pictures_fading;
    [SerializeField] List<MovingObject> object_moving;
    [SerializeField] List<FillingImage> pictures_filling;
    [SerializeField] List<ZoomingObject> objects_zooming;
    [SerializeField] List<OpeningSFX> sfxs;
    [SerializeField] List<OpeningMusic> musics;
    [SerializeField] float totalScriptTime;
    [SerializeField] ui_text SkipText;
    [SerializeField] GameEvent EventOpeningEnd;
    private bool loadingReady;
    private bool scriptEnd;
    private int consectiveClicks;

    private Sequence CapSeq;
    private Sequence FadeSeq;
    private Sequence ObjSeq;
    private Sequence FillSeq;
    private Sequence ZoomSeq;
    private Sequence SfxSeq;
    private Sequence MusicSeq;
    private Sequence endFunc;

    public bool isEnd()
    {
        return !page_group.activeSelf;
    }
    private void Awake()
    {
        loadingReady = false;
        scriptEnd = false;
        consectiveClicks = 0;
        //page_group.SetActive(false);
    }
    private void Start()
    {
        PlayScript();
    }
    /*private void Update()
    {
        if (Input.GetKeyUp(KeyCode.R))
        {
            PlayScript();
        }
    }*/
    public void ScreenClicked()
    {

        consectiveClicks += 1;
        SkipText.gameObject.SetActive(consectiveClicks > 0);
        if (consectiveClicks >= 3)
        {
            ScriptEnd();
            SFXPlayer.mp3.StopAllSFX();
            SFXPlayer.mp3.PlaySFX("guqin_opening");
        }
        
    }
    void PlayScript()
    {
        page_group.SetActive(true);
        CapSeq = DOTween.Sequence();
        FadeSeq = DOTween.Sequence();
        ObjSeq = DOTween.Sequence();
        FillSeq = DOTween.Sequence();
        ZoomSeq = DOTween.Sequence();
        SfxSeq = DOTween.Sequence();
        MusicSeq = DOTween.Sequence();
        endFunc = DOTween.Sequence();

        for (int i = 0; i < captions.Count; i++)
        {
            CapSeq.Insert(0f, captions[i].textObj.str.DOFade(1f, 0.5f).SetDelay(captions[i].delay));
            CapSeq.Insert(0f, captions[i].textObj.str.DOFade(0f, 0.5f).SetDelay(captions[i].delay + captions[i].duration));
        }
        for (int i = 0; i < pictures_fading.Count; i++)
        {
            FadeSeq.Insert(0f, pictures_fading[i].img.DOFade(1f, pictures_fading[i].transition).SetDelay(pictures_fading[i].delay));
            FadeSeq.Insert(0f, pictures_fading[i].img.DOFade(0f, Mathf.Min(pictures_fading[i].transition, 1f)).SetDelay(pictures_fading[i].delay + pictures_fading[i].duration));
        }
        for (int i = 0; i < object_moving.Count; i++)
        {
            ObjSeq.Insert(0f, object_moving[i].rect.DOAnchorPos(object_moving[i].vector, object_moving[i].transition).SetDelay(object_moving[i].delay));
        }
        for (int i = 0; i < pictures_filling.Count; i++)
        {
            FillSeq.Insert(0f, pictures_filling[i].img.DOFillAmount(pictures_filling[i].end, pictures_filling[i].transition).SetDelay(pictures_filling[i].delay));
        }
        for (int i = 0; i < objects_zooming.Count; i++)
        {
            ZoomSeq.Insert(0f, objects_zooming[i].obj.transform.DOScale(Vector3.one * objects_zooming[i].end, objects_zooming[i].transition).SetDelay(objects_zooming[i].delay).SetEase(Ease.InOutSine));
        }
        for (int i = 0; i < sfxs.Count; i++)
        {
            string sfxName = sfxs[i].SFXName;
            float delay = sfxs[i].delay;
            SfxSeq.InsertCallback(delay, () => PlaySoundInOpening(sfxName));
        }
        for (int i = 0; i < musics.Count; i++)
        {
            string musicName = musics[i].MusicName;
            float delay = musics[i].delay;
            MusicSeq.InsertCallback(delay, () => PlayMusicInOpening(musicName));
        }
        endFunc.AppendInterval(totalScriptTime).OnComplete(ScriptEnd);

        //music
        //BackgroundMusicPlayer.mp3.PlayMusic("school");
    }
    public void UIClose()
    {
        KillAllTweens();
        page_group.SetActive(false);
        EventOpeningEnd.Raise();
    }
    void KillAllTweens()
    {
        CapSeq.Kill();
        FadeSeq.Kill();
        ObjSeq.Kill();
        FillSeq.Kill();
        ZoomSeq.Kill();
        SfxSeq.Kill();
        MusicSeq.Kill();
        endFunc.Kill();
    }
    public void ScriptEnd()
    {
        scriptEnd = true;
        if (loadingReady)
        {
            UIClose();
        }
    }
    public void LoadingEnd()
    {
        loadingReady = true;
        if (scriptEnd)
        {
            UIClose();
        }
    }
    void PlaySoundInOpening(string sfxName)
    {
        if (page_group.activeSelf)
        {
            SFXPlayer.mp3.PlaySFX(sfxName);
        }
    }
    void PlayMusicInOpening(string musicName)
    {
        if (page_group.activeSelf)
        {
            BackgroundMusicPlayer.mp3.PlayMusic(musicName);
        }
    }
}
