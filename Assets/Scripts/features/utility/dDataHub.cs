﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class dDataHub : MonoBehaviour {

    public static dDataHub hub;

    public bool DataReady;

    [Header("Character")]
    public Dict_CharacterStats dCharacterStats;

    [Header("Chronicle")]
    public Dict_ChronicleActivity dChronicleActivity;

    [Header("Journal")]
    public Dict_JournalOption dJournalOption;
    public Dict_JournalStyle dJournalStyle;
    public Dict_JournalReward dJournalReward;

    [Header("Park Visit")]
    public Dict_Park dPark;
    public Dict_ParkNode dParkNode;
    public Dict_ParkPath dParkPath;
    public Dict_ParkReward dParkReward;
    public Dict_ParkPathAction dParkAction;
    public Dict_ParkPathAction dParkRegularAction;
    public Dict_ParkPathAction dParkStayAction;
    public Dict_ParkPathAction dParkForwardAction;
    public Dict_ParkPathAction dParkDangerAction;
    public Dict_ParkSolarTerm dParkSolarTerm;

    [Header("Collectible")]
    public Dict_View dView;
    public Dict_Collectible dCollectible;
    public AllKeywords allKeywords;

    [Header("Content Unlock")]
    public Dict_ContentItem dContentItem;

    [Header("Progression")]
    public PlayerLevelDictionary plevelDict;
    public EquipmentDictionary equipmentDict;
    public TraitDictionary traitDict;

    [Header("v2 read-only")]
    public CurrencyDefinitionDictionary currencyDict;
    public BuffDefDictionary buffDict;
    public ScopeDefDictionary scopeDict;

    [Header("children data readers")]
    public List<GenericDataReader> activeChildren;
    public GameEvent AllDataLoaded;

    [Header("Loading Screen")]
    public dataLoadingPage loadingData;
    public GameEvent GameLoading;

    void Awake()
    {
        if (hub == null)
        {
            hub = this;
        }
        else if (hub != null)
        {
            Destroy(gameObject);
        }
        //DontDestroyOnLoad(hub); 
    }
    public void Init()
    {
        List<GenericDataReader> allscripts = GetComponents<GenericDataReader>().ToList();
        foreach(GenericDataReader gdr in allscripts)
        {
            if (gdr.enabled)
            {
                activeChildren.Add(gdr);
            }
        }
    }
    public void LoadDataFromXLS()
    {
        Init();
        foreach (GenericDataReader gdr in activeChildren)
        {
            gdr.LoadFromXLS();
        }
    }
    public float CheckDataLoadingProgress()
    {
        float totalScrtips = activeChildren.Count();
        float finishedScripts = 0;
        foreach (GenericDataReader gdr in activeChildren)
        {
            if (gdr.finished)
            {
                finishedScripts += 1;
            }
        }
        float progress = finishedScripts / totalScrtips;
        loadingData.flavorText = "Loading data";
        loadingData.progression = 0.1f + 0.2f * progress;
        GameLoading.Raise();
        if (progress == 1)
        {
            StartCoroutine("AllDataLoadedWrapper");
        }
        return finishedScripts / totalScrtips;
    }
    IEnumerator AllDataLoadedWrapper()
    {
        yield return new WaitForSeconds(0.1f);
        loadingData.flavorText = "All data imported";
        loadingData.progression = 0.3f;
        GameLoading.Raise();
        yield return new WaitForSeconds(0.2f);
        DataReady = true;

        AllDataLoaded.Raise();
    }

}
