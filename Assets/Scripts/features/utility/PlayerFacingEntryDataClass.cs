﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class PFEntry
{
    //PFEntry aka "player facing entry"
    public string UID;
    public string title;
    public string desc;
    public string flavor;
    public string entryType;
    public PFEntry()
    {
        UID = "";
        title = "未命名";
        desc = "无描述";
        flavor = "";
        entryType = "undefinedType";
    }

    public PFEntry(PFEntry copyItem)
    {
        UID = copyItem.UID;
        title = copyItem.title;
        desc = copyItem.desc;
        flavor = copyItem.flavor;
        entryType = copyItem.entryType;
    }
}
[System.Serializable]
public class PFEntrywGate : PFEntry
{
    public string release;
    public List<string> tags;
    public int tier;
    public float weight;
    public PFEntrywGate() : base()
    {
        release = "";
        tags = new List<string>();
        tier = -1;
        weight = 0;
    }
    public PFEntrywGate(PFEntrywGate copyItem) : base(copyItem)
    {
        release = copyItem.release;
        tags = new List<string>();
        tags.AddRange(copyItem.tags);
        tier = copyItem.tier;
        weight = copyItem.weight;
    }
}

public static class PFEntryDeck
{
    public static List<string> DrawCardswConditions(List<PFEntrywGate> sourceList, int drawCount, List<string> applicableFilters = null, intRange tierRange = null, List<string> exclusiveList = null)
    {
        applicableFilters = applicableFilters ?? new List<string>();
        tierRange = tierRange ?? new intRange(0, int.MaxValue);
        exclusiveList = exclusiveList ?? new List<string>();

        List<string> results = new List<string>();
        List<KeyValuePair<string, float>> applicableEntries = new List<KeyValuePair<string, float>>();

        float totalWeight = 0;
        for (int i = 0; i < drawCount; i++)
        {
            //generate weighted deck
            applicableEntries.Clear();
            totalWeight = 0;
            foreach (PFEntrywGate entry in sourceList)
            {
                if (ActiveBuffs.controller.CheckFeatureGate(entry.release) 
                    && myUtility.CompareTags(applicableFilters, entry.tags) 
                    && (intRange.inRange(tierRange, entry.tier) || entry.tier < 0) 
                    && !exclusiveList.Contains(entry.UID) 
                    && !results.Contains(entry.UID) && entry.weight > 0)
                {
                    totalWeight += entry.weight;
                    applicableEntries.Add(new KeyValuePair<string, float>(entry.UID, totalWeight));
                }
            }
            float rng = Random.Range(0, totalWeight);
            foreach (KeyValuePair<string, float> pair in applicableEntries)
            {
                if(pair.Value >= rng)
                {
                    results.Add(pair.Key);
                    break;
                }
            }
        }
        if(results.Count < drawCount)
        {
            Debug.Log(string.Format("unfulfilled draw as {0}/{1}", results.Count, drawCount));
        }
        //results.Sort((a, b) => Random.value.CompareTo(Random.value));
        return results;
    }
}
[System.Serializable]
public class BuffItem
{
    public string scope;
    public string uid;
    public string value;
    public int duration;

    public BuffItem()
    {
        scope = "";
        uid = "";
        value = "";
        duration = 0;
    }
}





