﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class myUtility
{
    public static bool CompareTags(List<string> eligibleTags, List<string> itemTags)
    {

        bool matched = false;
        if(eligibleTags == null)
        {
            return true;
        }
        else if(eligibleTags.Count == 0 || itemTags.Count == 0)
        {
            return true;
        }

        foreach (string target in eligibleTags)
        {
            foreach (string tag in itemTags)
            {
                if (tag == target)
                {
                    matched = true;
                    return matched;
                }
            }
        }
        return matched;
    }
}
