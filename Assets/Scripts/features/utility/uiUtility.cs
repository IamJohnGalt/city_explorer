﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public static class uiUtility
{
    public static void SetBtnAppearance(GameObject btn, bool active)
    {
        if (active)
        {
            btn.GetComponent<Button>().interactable = true;
            btn.GetComponent<Image>().color = Color.green;
        }
        else
        {
            btn.GetComponent<Button>().interactable = false;
            btn.GetComponent<Image>().color = Color.white;
        }
    }
}

