﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class dLocalHub : MonoBehaviour
{
    public static dLocalHub hub;

    public StringAsset keywords;
    void Awake()
    {
        if (hub == null)
        {
            hub = this;
        }
        else if (hub != null)
        {
            Destroy(gameObject);
        }
        //DontDestroyOnLoad(hub);
    }
}
