﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Gameplay/LastAct")]
public class LastAct : ScriptableObject, ISerializationCallbackReceiver
{
    public string CardUID;
    public int ActivityIndex;
    public string LocationUID;
    public int CardTier;
    public int TypeIndex;

    public void LogLastCard(string card_uid = "", int activity_index = dConstants.UNDEFINED_ACTIVITY_INDEX, 
        string location_uid = "", int card_tier = 0, int card_type_index = dConstants.UNDEFINED_CARD_TYPE_INDEX)
    {
        CardUID = card_uid;
        ActivityIndex = activity_index;
        LocationUID = location_uid;
        CardTier = card_tier;
        TypeIndex = card_type_index;
    }
    public void OnAfterDeserialize()
    {

    }

    public void OnBeforeSerialize()
    {

    }

}
