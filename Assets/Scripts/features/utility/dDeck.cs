﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
[System.Serializable]
public class CardwithAW
{
    public string UID;
    public float AccumWeight;
    public CardwithAW()
    {
        UID = "";
        AccumWeight = 0;
    }
    public CardwithAW(string _UID, float _AccumwWeight)
    {
        UID = _UID;
        AccumWeight = _AccumwWeight;
    }
}
[System.Serializable]
public class dDeck
{
    public List<CardwithAW> list = new List<CardwithAW>();
    public float TotalAccumWeights;
    public dDeck()
    {
        list = new List<CardwithAW>();
    }
    public dDeck(dDeck copyList)
    {
        list = new List<CardwithAW>(copyList.list);
    }
    public dDeck(List<CardDrop> cardDrops, int progressTier)
    {
        TotalAccumWeights = 0;
        foreach(CardDrop cd in cardDrops)
        {
            if (cd.reqProgressTier <= progressTier)
            {
                if(ActiveBuffs.controller.CheckFeatureGate(cd.releaseGate))
                {
                    TotalAccumWeights += cd.weight;
                    CardwithAW card = new CardwithAW(cd.cardUID, TotalAccumWeights);
                    list.Add(card);
                }
                
            }
        }
    }
    public dDeck(List<RewardDrop> rewardDrops, string targetType = null)
    {
        TotalAccumWeights = 0;
        foreach (RewardDrop rd in rewardDrops)
        {
            if(targetType != null)
            {
                if (rd.specialType == targetType)
                {
                    TotalAccumWeights += rd.weight;
                    CardwithAW card = new CardwithAW(rd.rewardUID, TotalAccumWeights);
                    list.Add(card);
                }
            }
        }
    }
    public dDeck(List<RewardDrop> rewardDrops, int group)
    {
        TotalAccumWeights = 0;
        foreach (RewardDrop rd in rewardDrops)
        {
            if (rd.groupIndex == group)
            {
                TotalAccumWeights += rd.weight;
                CardwithAW card = new CardwithAW(rd.rewardUID, TotalAccumWeights);
                list.Add(card);
            }
        }
    }
    
    public string DrawNext()
    {
        float rng = Random.Range(0, TotalAccumWeights);
        for(int cardIndex = 0; cardIndex < list.Count; cardIndex++)
        {
            if(list[cardIndex].AccumWeight >= rng)
            {
                return list[cardIndex].UID;
            }
        }
        Debug.LogError("AccumWeightCardList failes to generate a card UID");
        return null;
    }
    public List<string> DrawX(int count)
    {
        List<string> draws = new List<string>();
        int loopTimes = 0;
        string curDraw = "";
        while(loopTimes < 100)
        {
            if(draws.Count < count && draws.Count < list.Count)
            {
                curDraw = DrawNext();
                if (!draws.Contains(curDraw))
                {
                    draws.Add(curDraw);
                }
            }
            else
            {
                break;
            }
        }
        return draws;
    }
}
