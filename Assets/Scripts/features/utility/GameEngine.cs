﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameEngine : MonoBehaviour
{
    public static GameEngine controller;
    private void Awake()
    {
        if (controller == null)
        {
            controller = this;
        }
        else if (controller != null)
        {
            Destroy(gameObject);
        }
        //DontDestroyOnLoad(controller);
    }
    
    void Start()
    {
        StartCoroutine("StartGameWDelay");
        //StartGame();
    }
    [Header("Engine Events")]
    public GameEvent FakedDataLoaded;
    public GameEvent GameFirstStart;
    public GameEvent GameReset;

    [Header("Loading Screen")]
    public dataLoadingPage loadingMessage;
    public GameEvent GameLoading;

    private bool dataLoadFinished = false;
    public void StartGame()
    {
        if (!dDataHub.hub.DataReady)
        {
            StartCoroutine("FirstTimeLoad");
        }
        else
        {
            StartCoroutine("Reload");
        }
    }
    public void Reload4NewSession()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        StartCoroutine("StartGameWDelay");
    }
    public void Back2MainMenu()
    {
        
    }
    IEnumerator StartGameWDelay()
    {
        yield return new WaitForSeconds(1f);
        StartGame();
    }
    IEnumerator FirstTimeLoad()
    {
        GameReset.Raise();
        loadingMessage.flavorText = "Initializing the Engine";
        loadingMessage.progression = 0.1f;
        GameLoading.Raise();
        yield return new WaitForSeconds(0.2f);
        GameFirstStart.Raise();
    }
    IEnumerator Reload()
    {
        GameReset.Raise();
        yield return new WaitForSeconds(0.1f);
        loadingMessage.flavorText = "Restarting the Engine";
        loadingMessage.progression = 0.3f;
        GameLoading.Raise();
        yield return new WaitForSeconds(0.4f);
        FakedDataLoaded.Raise();
    }
}
