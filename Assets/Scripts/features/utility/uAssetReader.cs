﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class uAssetReader : MonoBehaviour
{
    public static uAssetReader controller;
    private void Awake()
    {
        if (controller == null)
        {
            controller = this;
        }
        else if (controller != null)
        {
            Destroy(gameObject);
        }
        //must be placed under Resource/Readable
        allSprites = Resources.LoadAll<Sprite>(string.Format("Readable")).ToList();
    }
    private List<Sprite> allSprites = new List<Sprite>();
    //public Sprite[] arraySprites;

    [SerializeField] Sprite debugSprite;
    [SerializeField] Sprite defaultSprite;

    public Sprite GetByName(string fileName)
    {
        Sprite result = CheckSpecialKeywords(fileName);
        foreach(Sprite spt in allSprites)
        {
            if(spt.name == fileName)
            {
                result = spt;
                break;
            }
        }
        if(result == null)
        {
            Debug.LogError(string.Format("unable to find sprite name {0} in readable resources", fileName));
        }
        return result ?? debugSprite;
    }
    Sprite CheckSpecialKeywords(string fileName)
    {
        switch (fileName)
        {
            case "dynamic_current_stamina":
                return CharacterManagement.controller.GetCurrentCharacter().food;
            case "dynamic_current_stamina_shape":
                return CharacterManagement.controller.GetCurrentCharacter().food_shape;
            default:
                return null;
        }
    }
}
