﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct TagProgressData
{
    public string keyword;
    public int curCount;
    public int reqCount;
    public int optCount;

    public string rewardSign;
    public string curReward;
    public string minReward;
    public string maxReward;

    public string specialRewardType;
}
[System.Serializable]
public struct SubmissionReward
{
    public string formula;
    public float value;
}
[System.Serializable]
public class CardDrop
{
    public string cardUID;
    public string releaseGate;
    public float weight;
    public int reqProgressTier;
    public CardDrop()
    {
        cardUID = "";
        releaseGate = "";
        weight = 0;
        reqProgressTier = 0;
    }
    public CardDrop(CardDrop copyItem)
    {
        cardUID = copyItem.cardUID;
        releaseGate = copyItem.releaseGate;
        weight = copyItem.weight;
        reqProgressTier = copyItem.reqProgressTier;
    }
    public void DebugPrint()
    {
        Debug.Log("Card UID: " + cardUID + " with weight of " + weight);
    }
}
[System.Serializable]
public struct CardArt
{
    public string objectArt;
    public string backgroundArt;
}
[System.Serializable]
public class RewardDrop
{
    public string rewardUID;
    public string releaseGate;
    public float weight;
    public string specialType;
    public int groupIndex;
}
[System.Serializable] 
public class intwCap
{
    public int curValue;
    public int capValue;
    public int curGap { get { return capValue - curValue; } }
}
[System.Serializable] 
public class intPair
{
    public int keyInt;
    public int valueInt;
}
[System.Serializable]
public class intRange
{
    public int minInt;
    public int maxInt;

    public int RandomRoll()
    {
        return Random.Range(minInt, maxInt+1);
    }
    public int RandomRollByDefaultStep()
    {
        int median = (minInt + maxInt) / 2;
        int step = 1;
        if(median > 10 && median <= 100)
        {
            step = 5;
        }
        else if(median >= 100 && median <= 1000)
        {
            step = 10;
        }
        else if(median > 1000)
        {
            step = 50;
        }
        //always has 1 more step when the max is overflowed
        int options = Mathf.CeilToInt((float)Mathf.Abs(maxInt - minInt) / step);
        int rngIndex = Random.Range(0, options+1);
        return Mathf.Min(maxInt, minInt + rngIndex * step);
    }
    public int GetMedian()
    {
        return (minInt + maxInt) / 2;
    }
    public intRange()
    {
        minInt = 0;
        maxInt = 0;
    }
    public intRange(int min, int max)
    {
        minInt = min;
        maxInt = max;
    }
    public intRange(string str_min, string str_max)
    {
        int min = 0;
        int max = 0;
        int.TryParse(str_min, out min);
        int.TryParse(str_max, out max);
        minInt = min;
        maxInt = max;
    }
    public bool isZero()
    {
        if(minInt == maxInt && minInt == 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public static bool inRange(intRange intR, int target)
    {
        if(target <= intR.maxInt && target >= intR.minInt)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}