﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class dConstants {

    
    public const int MAX_CARD_TIER = 3;
    public const string MASTER_JOURNAL_NAME = "《四海游集》";
    public const string MASTER_JOURNAL_SHORT = "《四》";


    public enum CardClass { view, item, food, charac, undefined };
    public enum ActivityType { sightsee, shop, taste, drink, explore, undefined };
    public const int UNDEFINED_ACTIVITY_INDEX = 4;
    public const int UNDEFINED_CARD_TYPE_INDEX = -1;
    public const int NUM_ACTIVITY = 5;
    public const int MAX_LOOP_TIMES = 100;
    public const int NUM_LOCATIONS = 22;
    public static class Map
    {
        public enum NodeStatus { active, inactive, locked, hidden };
        public enum PathStatus { active, inactive, locked, hidden};
        public const int NODE_COOLDOWN = 1;
        public const int PATH_COOLDOWN = 0;
        public const int ACTIVITY_COOLDOWN = 1;
        public const int EXPLORATION_COST = 5;
    }
    public static class Submission
    {
        public const int MAX_SUBMISSION_CARD = 5;
        public const int MAX_RESERVE_CARD = 2;
        public const int MAX_REWARD_GROUP = 4;
        public const int MAX_REWARD_OPTION = 4;
        public const int MAX_REWARD_RESULT = 1;
    }
    public static class Avatar
    {
        public const int INITIAL_COIN = 100;
        public const int INITIAL_FAME = 0;
        public const int INITIAL_MONTH_LEFT = 24;

        public const int BORN_YEAR = 12;
        public const int BORN_SEASON = 2;
        public const int BORN_MILESTONE = 3;
        public const int INITIAL_AGE = 25;
        public const int END_AGE = 30;

        public const int MAX_ACTIVE_EQUIPMENT_SLOT = 5;
        public const int MAX_TRAIT_SLOT = 10;
        public const int MAX_TRAIT_SELECTION_OPTION = 3;
        public const int RARE_TRAIT_APPEAR_AT = 3;
        public const int MAX_TRAIT_POINT = 10;
        public const int MAX_PLAYER_LEVEL = 9;
    }
    public static class EndSession
    {
        public enum EndReason { not, money, time, special };
    }
    public static class ParkConst
    {
        public enum ParkStatus { onReward, onRoute, inDanger, onArrival, nextNode, parkEnd };
        public enum TokenStatus { hidden, preview, playable, played, active, inactive };
        public const int MAX_STAY_CARD = 4;
        public const int MAX_FORWARD_CARD = 3;
        public const int MAX_DANGER_CARD = 2;
        public const int ACTION_DRAW_BASE = 3;
        public const int REWARD_DRAW_BASE = 3;
        public const int ROUTE_DRAW_BASE = 3;
        public const int NODE_DRAW_BASE = 3;
        public const int PATH_DRAW_BASE = 3;
        public const int INITAL_STAMINA = 20;

        public const int MAX_DANGER_TOKEN = 3;
        public const int MAX_COLLECTIBLE_TOKEN = 3;
        public const int COLLECTIBLE_PREVIEW_COUNT = 1;
        public const int RANDOM_PATH_RANGE = 20;
        public const int COLLECTIBLE_OPTION_COUNT = 2;
        public const int SECOND_REWARD_COIN_COST_VILLAGE = 50;
        public const int REDRAW_COLLECTIBLE_COST = 1;
        public const int MAX_RESERVED_STAMINA = 5;
        public const float RATIO_RESERVED_STAMINA = 0.3f;

        public const string INTRO_PARK_UID = "park_session_start";
    }
    public static class JournalConst
    {
        public enum JournalStatus { selection, journal_working, journal_full, reward };
        public enum SlotStatus { hidden, empty, filled, selected };
        public const float EMPTY_LABEL_ALPHA = 0.5f;
        public const int MAX_OPTIONAL_JOURNAL = 3;
        public const int MAX_OPTIONAL_REWARD = 3;
        public const float TOTAL_STAR_AMPLIFIER = 1f;
        public const string CASE_OPTIONAL_REWARD = "has_optional_reward";
        public const string CASE_RANDOMIZED_REWARD = "has_random_reward";
        public const string FALLBACK_VIEW_UID = "view_0_kong";
        public const string FALLBACK_COLLECTIBLE_UID = "collectible_0_kong_v1";
        public const int MAX_REWARD_TIER = 40;
    }
    public static class ChronicleConst
    {
        public enum ChronicleStatus { destination, activity, inPark, inActivity, journalSelection, journalSubmission, inJournalReward, inTrait, inLevelup, sessionEnd};
        public const int MAX_OPTIONAL_PARK = 3;
        public const int MAX_OPTIONAL_ACTIVITY = 3;
        public const int SKIP_PARK_TIME_COST = 3;
        public const int SKIP_ACTIVITY_TIME_COST = 1;
        public const int SKIP_JOURNAL_TIME_COST = 0;
        public const int RECOMMENDED_STAMINA_S = 20;
        public const int RECOMMENDED_STAMINA_M = 26;
        public const int RECOMMENDED_STAMINA_L = 35;
    }
    public static class Sound
    {
        public const float MASTER_BG_VOLUME = 0.8f;
        public const float MASTER_SFX_VOLUME = 1f;
    }
    public static class Character
    {
        public enum StatusType { unknown, info, final };
        public const int NUM_COL_STATUS = 3;
        public const int NUM_COL_CARD = 9;

        public const int TOTAL_CHARACTERS = 6;
        public const int MAX_CHARACTER_LEVEL = 5;
        public const int MAX_STORY_LEVEL = 5;

        public const int CHAR1_FORWARD = 10;
        public const int CHAR2_COIN = 30;
        public const int CHAR3_FAME = 1;
    }
    public static class Sightseeing
    {
        public enum ViewType { structure, animal, natural };
        public const int MAX_POOL_SIZE = 1;
        public const int MAX_PROGRESS_TIER = 6;
        public const int NUM_CARD_TYPE = 3;
        public const int MAX_REWARD_STEP = 3;
        public const int NUM_CARD_PER_SET = 7;
        public static readonly int[] NUM_CARD_BY_TYPE = { 2, 3, 5 };
        public static readonly string[] TypeText = { "建筑", "灵兽", "自然"};
    }
    public static class Shopping
    {

        public enum ItemType { gem, craft, porcelain };
        public const int MAX_POOL_SIZE = 3;
        public const int MAX_PROGRESS_TIER = 6;
        public const int NUM_CARD_TIER = 3;
        public const int NUM_CARD_TYPE = 3;
        public const int MAX_REWARD_STEP = 3;
        public const int NUM_CARD_PER_SET = 3;
        public static readonly string[] TypeText = { "玉石", "工艺", "瓷器"};
    }
    public static class Tasting
    {
        public enum FlavorType { brothy, spicy, nutritious, sweet, refreshing };
        public const int MAX_CARD_SLOT = 4;
        public const int NUM_PROGRESS_TIER = 1;
        public const int NUM_CARD_TIER = 3;
        public const int NUM_CARD_TYPE = 5;
        public const int MAX_REWARD_STEP = 3;
        public const int NUM_CARD_PER_SET = 3;
        public const int MAX_INDEX_T1_FLAVOR_TYPE = 4;
        public const int MAX_INDEX_T2_FLAVOR_TYPE = 5;
    }
    public static class Drinking
    {
        public enum InfoType { generic, location, character };
        public const int MAX_DRINK_TIER = 3;
        public const int MAX_REROLL_TIMES = 10;
        public const string GENERIC_DRINK_NODE = "node_generic";
    }
    public static class Exploration
    {
        public const int MAX_DISCOVERY = 10;
    }
    public static class Date
    {
        public const int NUM_MILESTONE_PER_SEASON = 6;
        public const int NUM_DAY_PER_MONTH = 28;
        public const int NUM_MONTH_PER_YEAR = 12;
        public const int NUM_SEASON_PER_YEAR = 4;
        public const int MAX_YEAR = 60;
        public static readonly string[] YearText = new string[]
        {
            "甲子","乙丑","丙寅","丁卯","戊辰","己巳","庚午","辛未","壬申","癸酉",
            "甲戌","乙亥","丙子","丁丑","戊寅","己卯","庚辰","辛巳","壬午","癸未",
            "甲申","乙酉","丙戌","丁亥","戊子","己丑","庚寅","辛卯","壬辰","癸巳",
            "甲午","乙未","丙申","丁酉","戊戌","己亥","庚子","辛丑","壬寅","癸卯",
            "甲辰","乙巳","丙午","丁未","戊申","己酉","庚戌","辛亥","壬子","癸丑",
            "甲寅","乙卯","丙辰","丁巳","戊午","己未","庚申","辛酉","壬戌","癸亥"
        };
        public static readonly string[] MonthText = new string[]
        {
            "壹","贰","叁","肆","伍","陆","柒","捌","玖","拾","辜","腊",

        };
        public static readonly string[] SeasonText = new string[]
        {
            "春", "夏", "秋", "冬"
        };
        public static readonly string[,] MilestoneText = new string[NUM_SEASON_PER_YEAR, NUM_MILESTONE_PER_SEASON]{
            { "立春", "雨水", "惊蛰", "春分", "清明", "谷雨" },
            { "立夏", "小满", "芒种", "夏至", "小暑", "大暑" },
            { "立秋", "处暑", "白露", "秋分", "寒露", "霜降" },
            { "立冬", "小雪", "大雪", "冬至", "小寒", "大寒" }
        };
    }

}
