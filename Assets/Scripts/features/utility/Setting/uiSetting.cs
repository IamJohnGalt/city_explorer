﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class uiSetting : uiElement
{
    [SerializeField] dataSetting data;

    [Header("page")]
    [SerializeField] GameObject cover;
    [SerializeField] GameObject setting;
    [SerializeField] GameObject credit;
    [SerializeField] GameObject sound_on;
    [SerializeField] GameObject sound_off;
    [SerializeField] GameObject setting_entry;

    [Header("children_object")]
    [SerializeField] ui_text curMusicVol;
    [SerializeField] ui_text curSFXVol;
    [SerializeField] ui_button endSession;
    [SerializeField] ui_button quitGame;

    private bool openingEnded;

    private void Start()
    {
        UIInit();
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Anim_OpenPage();
        }
    }
    public void music_volume_up()
    {
        data.musicVolume = Mathf.Min(1, data.musicVolume + 0.1f);
        BackgroundMusicPlayer.mp3.UpdateVolume();
        SFXPlayer.mp3.PlaySFX("button_click");
        UIUpdate();
    }
    public void music_volume_down()
    {
        data.musicVolume = Mathf.Max(0, data.musicVolume - 0.1f);
        BackgroundMusicPlayer.mp3.UpdateVolume();
        SFXPlayer.mp3.PlaySFX("button_click");
        UIUpdate();
    }
    public void sfx_volume_up()
    {
        data.sfxVolume = Mathf.Min(1, data.sfxVolume + 0.1f);
        SFXPlayer.mp3.UpdateVolume();
        SFXPlayer.mp3.PlaySFX("button_click");
        UIUpdate();
    }
    public void sfx_volume_down()
    {
        data.sfxVolume = Mathf.Max(0, data.sfxVolume - 0.1f);
        SFXPlayer.mp3.UpdateVolume();
        SFXPlayer.mp3.PlaySFX("button_click");
        UIUpdate();
    }
    public void btn_toggle_volume()
    {
        if(data.musicVolume + data.sfxVolume > 0)
        {
            data.musicVolume = 0f;
            data.sfxVolume = 0f;
            BackgroundMusicPlayer.mp3.UpdateVolume();
            SFXPlayer.mp3.UpdateVolume();
        }
        else
        {
            data.musicVolume = 0.5f;
            data.sfxVolume = 0.5f;
            BackgroundMusicPlayer.mp3.UpdateVolume();
            SFXPlayer.mp3.UpdateVolume();
        }
        UIUpdate();
    }
    public void btn_open_setting()
    {
        Anim_OpenPage();
    }
    public void btn_close_setting()
    {
        Anim_ClosePage();
    }
    public void btn_open_credit()
    {
        Anim_OpenCredit();
        SFXPlayer.mp3.PlaySFX("button_click");
    }
    public void btn_close_credit()
    {
        Anim_CloseCredit();
        SFXPlayer.mp3.PlaySFX("button_click");
    }
    public void btn_quit_game()
    {
        Application.Quit();
    }
    public void btn_end_session()
    {
        EndSession.controller.RestartGame();
    }
    public override void UIInit()
    {
        openingEnded = false;
        setting.gameObject.SetActive(false);
        credit.gameObject.SetActive(false);
        cover.SetActive(false);
        UIUpdate();
    }
    public void OpeningIsEnded()
    {
        openingEnded = true;
        UIUpdate();
    }
    public override void UIUpdate()
    {
        sound_on.SetActive(!openingEnded && (data.musicVolume + data.sfxVolume > 0));
        sound_off.SetActive(!openingEnded && (data.musicVolume + data.sfxVolume == 0));
        setting_entry.SetActive(openingEnded);
        curMusicVol.SetText(Mathf.RoundToInt(data.musicVolume * 100).ToString());
        curSFXVol.SetText(Mathf.RoundToInt(data.sfxVolume * 100).ToString());
    }
    public void Anim_OpenPage()
    {
        RectTransform rect = setting.GetComponent<RectTransform>();
        rect.localScale = Vector3.one;
        rect.DOScale(Vector3.zero, 0.3f).From().SetEase(Ease.OutSine);
        setting.gameObject.SetActive(true);
        credit.gameObject.SetActive(false);
        cover.SetActive(true);
    }
    public void Anim_ClosePage()
    {
        setting.gameObject.SetActive(false);
        credit.gameObject.SetActive(false);
        cover.SetActive(false);
    }
    public void Anim_OpenCredit()
    {
        RectTransform rect = credit.GetComponent<RectTransform>();
        rect.localScale = Vector3.one;
        rect.DOScale(Vector3.zero, 0.3f).From().SetEase(Ease.OutSine);
        credit.gameObject.SetActive(true);
    }
    public void Anim_CloseCredit()
    {
        credit.gameObject.SetActive(false);
    }
}
