﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Utility/Settings")]
public class dataSetting : ScriptableObject
{
    public float musicVolume;
    public float sfxVolume;
}
