﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CardDropwithAW
{
    public CardDrop cardDrop;
    public float AccumWeight;
    public CardDropwithAW()
    {
        cardDrop = null;
        AccumWeight = 0;
    }
    public CardDropwithAW(CardDrop _cardDrop, float _AccumwWeight)
    {
        cardDrop = _cardDrop;
        AccumWeight = _AccumwWeight;
    }
}
[System.Serializable]
public class uDeck
{
    public List<CardDropwithAW> CardDropAWList = new List<CardDropwithAW>();
    public float TotalWeights;

    public uDeck()
    {
        CardDropAWList = new List<CardDropwithAW>();
        TotalWeights = 0;
    }
    public uDeck(List<CardDrop> dropList)
    {
        TotalWeights = 0;
        foreach (CardDrop cd in dropList)
        {
            TotalWeights += cd.weight;
            CardDropwithAW CardDropAW = new CardDropwithAW(cd, TotalWeights);
            CardDropAWList.Add(CardDropAW);
        }
    }
    public string DrawCardBetween(int min_tier = int.MinValue, int max_tier = int.MaxValue)
    {
        int loopTimes = 0;
        while(loopTimes++ < dConstants.MAX_LOOP_TIMES)
        {
            CardDrop cd = DrawCard();
            if(cd.reqProgressTier <= max_tier && ActiveBuffs.controller.CheckFeatureGate(cd.releaseGate))
            {
                return cd.cardUID;
            }
        }
        Debug.LogError(string.Format("no valid card found in a deck"));
        return null;
    }
    CardDrop DrawCard()
    {
        float rng = Random.Range(0, TotalWeights);
        for (int cardIndex = 0; cardIndex < CardDropAWList.Count; cardIndex++)
        {
            if (CardDropAWList[cardIndex].AccumWeight >= rng)
            {
                return CardDropAWList[cardIndex].cardDrop;
            }
        }
        Debug.LogError("AccumWeightCardList failes to generate a card UID");
        return null;
    }
}
