﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class uiJournalStructure : MonoBehaviour
{
    public delegate void btn_hit_func();
    public btn_hit_func ref_submit_func;
    public btn_hit_func ref_max_func;

    [Header("Sub Groups")]
    public GameObject subG_wip;
    public GameObject subG_collectible;
    public GameObject subG_star;
    public GameObject subG_extra;
    public GameObject subG_reward;
    
    [Header("Journal Objects")]
    public Image journalPaper;
    public List<journal_slot> mainSlots;
    public List<ui_collectible> mainCollectibles;
    public ui_label journalName;
    public ui_button submit_btn;
    public ui_button max_btn;

    [Header("Star Objects")]
    public ui_star totalStar;
    public GameObject bonusGroup;
    public GameObject wipDivider;
    public GameObject hintDivider;
    public List<journal_rule> bonusItems;
    public List<journal_rule> wipItems;
    public List<journal_rule> hintItems;

    [Header("Extra Objects")]
    public List<journal_slot> extraSlots;
    public List<ui_collectible> extraCollectibles;

    [Header("Reward Objects")]
    public List<reward_item> previews;

    public void submit_btn_hit()
    {
        ref_submit_func?.Invoke();
    }
    public void max_btn_hit()
    {
        ref_max_func?.Invoke();
    }

}
