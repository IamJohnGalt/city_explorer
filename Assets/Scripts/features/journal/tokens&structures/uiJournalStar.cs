﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class uiJournalStar : uipfb_atom
{
    public delegate void btn_hit_func();
    public btn_hit_func ref_total_clicked_func;

    [Header("Children Objects")]
    public uipfb_label starCount;

    public void SetStar(int star)
    {
        starCount.SetText(star.ToString());
    }
    public void OnStarClicked()
    {
        ref_total_clicked_func?.Invoke();
    }
    
}
