﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "Journal/Journal Style Set")]
public class JournalSet : ScriptableObject
{
    public List<string> styleNames;
    public List<int> slotCount;
    public List<GameObject> journalTokens;
    public List<GameObject> journalStructures;

    public GameObject GetToken(string styleUID)
    { 
        //old function
        JournalStyle style = dDataHub.hub.dJournalStyle.GetFromUID(styleUID);
        //Debug.Log(string.Format("journalStyle ({0}) with group ({1})", styleUID, style.styleGroup));
        string styleGroup = dDataHub.hub.dJournalStyle.GetFromUID(styleUID).styleGroup;
        for (int i=0;i< styleNames.Count; i++)
        {
            if(styleNames[i] == styleGroup)
            {
                return journalTokens[i];
            }
        }
        return null;
    }
    public GameObject GetStructure(string styleUID)
    {
        //old function
        string styleGroup = dDataHub.hub.dJournalStyle.GetFromUID(styleUID).styleGroup;
        for (int i = 0; i < styleNames.Count; i++)
        {
            if (styleNames[i] == styleGroup)
            {
                return journalStructures[i];
            }
        }
        return null;
    }
    public GameObject GetStructureBySlot(int count)
    {
        for (int i = 0; i < slotCount.Count; i++)
        {
            if (slotCount[i] == count)
            {
                return journalStructures[i];
            }
        }
        return null;
    }
}
