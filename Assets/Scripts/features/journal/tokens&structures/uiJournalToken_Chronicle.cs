﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class uiJournalToken_Chronicle : MonoBehaviour
{
    [SerializeField] Transform tokenSlot;
    [SerializeField] uiJournalToken token;
    [SerializeField] JournalSet journalSet;

    public void SetJournalToken(string journalUID)
    {
        if(token != null)
        {
            Destroy(token.gameObject);
        }
        token = Instantiate(journalSet.GetToken(journalUID), tokenSlot).GetComponent<uiJournalToken>();
        token.SetToken(journalUID);
    }
}
