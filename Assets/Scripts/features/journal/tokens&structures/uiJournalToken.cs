﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class uiJournalToken : uipfb_atom
{
    public delegate void btn_hit_func(int index);
    public btn_hit_func ref_joutnal_selected_func;

    [Header("Data")]
    public string journalUID;
    public int tokenIndex = 0;

    [Header("Children Objects")]
    [SerializeField] GameObject content;
    [SerializeField] GameObject group_others;
    [SerializeField] uipfb_art franmeArt;
    [SerializeField] uipfb_label journalName;
    [SerializeField] List<uipfb_icon> fakeTexts;
    [SerializeField] uipfb_art featuredArt;

    public void SetToken(string journalStyleUID)
    {
        journalUID = journalStyleUID;
        JournalStyle style = dDataHub.hub.dJournalStyle.GetFromUID(journalStyleUID);
        //journalName.SetText(style.title);
        if(style.artAsset.objectArt != "")
        {
            featuredArt.SetSprite(string.Format("Art/token/{0}", style.artAsset.objectArt));
        }
        else
        {
            featuredArt.SetVisible(false);
        }
    }
    public void SetTokenBackground(string journalStyleUID)
    {
        journalUID = journalStyleUID;
        JournalStyle style = dDataHub.hub.dJournalStyle.GetFromUID(journalStyleUID);
        //journalName.SetText(style.title);
        if (style.artAsset.objectArt != "")
        {
            featuredArt.SetSprite(string.Format("Art/token/{0}", style.artAsset.objectArt));
        }
        else
        {
            featuredArt.SetVisible(false);
        }
    }
    public void OnTokenClicked()
    {
        ref_joutnal_selected_func?.Invoke(tokenIndex);
    }
}
