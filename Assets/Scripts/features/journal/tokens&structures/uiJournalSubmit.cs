﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class uiJournalSubmit : MonoBehaviour
{
    public delegate void btn_hit_func();
    public btn_hit_func ref_submit_func;

    public List<Sprite> submitBtnSprites;
    public uipfb_label_v2 submitBtn;
    public bool full = false;
    public void SetCurrentStatus(string txt, bool isFull)
    {
        full = isFull;
        submitBtn.SetText(!isFull ? txt : "");
        submitBtn.SetIcon(!isFull ? submitBtnSprites[0] : submitBtnSprites[1]);
    }
    public void OnSubmitEnter()
    {
        if (full)
        {
            submitBtn.SetFade(1f);
        }
        else
        {
            submitBtn.SetFade(0.5f);
        }
    }
    public void OnSubmitExit()
    {
        submitBtn.SetFade(0.5f);
    }
    public void OnSubmitClicked()
    {
        ref_submit_func?.Invoke();
    }
}
