﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class uiJournalRewardPreview : uipfb_atom
{
    [Header("Children Objects")]
    [SerializeField] uipfb_label_v2 rewardLabel;

    public void SetRewardPreview(string journalRewardUID)
    {
        if (journalRewardUID == dConstants.JournalConst.CASE_OPTIONAL_REWARD)
        {
            rewardLabel.SetIcon(Resources.Load<Sprite>(string.Format("Art/token/{0}", "token_question")));
            rewardLabel.SetText("可选奖励");
        }
        else if(journalRewardUID == dConstants.JournalConst.CASE_RANDOMIZED_REWARD)
        {
            rewardLabel.SetIcon(Resources.Load<Sprite>(string.Format("Art/token/{0}", "token_question")));
            rewardLabel.SetText("随机奖励");
        }
        else
        {
            JournalReward reward = dDataHub.hub.dJournalReward.GetFromUID(journalRewardUID);
            switch (reward.rewardType)
            {
                case "random_equipment":
                    rewardLabel.SetIcon(Resources.Load<Sprite>(string.Format("Art/token/{0}", "token_random_equipment")));
                    rewardLabel.SetText(reward.desc);
                    break;
                case "currency":
                    Currency currency = dDataHub.hub.currencyDict.GetFromUID(reward.rewardUID);
                    rewardLabel.SetIcon(Resources.Load<Sprite>(string.Format("Art/token/{0}", currency.artAsset.objectArt)));
                    rewardLabel.SetText(reward.desc);
                    break;
                default:
                    Debug.LogError(string.Format("Invalid reward type {0} found", reward.rewardType));
                    break;
            }
        }
        
    }
}
