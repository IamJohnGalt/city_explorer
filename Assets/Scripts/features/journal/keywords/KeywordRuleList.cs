﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[CreateAssetMenu(menuName = "Data/KeywordRuleList")]
public class KeywordRuleList : ScriptableObject
{
    public List<float> rewardLvByTier;
    public List<KeywordRule> list;
    public bool Contains(string UID)
    {
        foreach (KeywordRule kr in list)
        {
            if (kr.UID == UID)
            {
                return true;
            }
        }
        return false;
    }
    public bool ReadRewardLvByTier()
    {
        rewardLvByTier.Clear();
        for (int i = 0; i < dConstants.MAX_CARD_TIER; i++)
        {
            List<float> reward = (from rule in list where rule.keyword == "tier" + (i+1) select rule.value).ToList();
            if(reward.Count == 1)
            {
                rewardLvByTier.Add(reward[0]);
            }
            else
            {
                Debug.LogError(string.Format("unable to find tier {0} rule in keywoprd ruleset", i + 1));
                return false;
            }
        }
        return true;
    }
}
