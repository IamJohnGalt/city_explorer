﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Data/KeywordOrderList")]
public class KeywordOrderList : ScriptableObject
{
    public List<KeywordOrder> list;

    public bool Contains(string _keyword)
    {
        foreach(KeywordOrder kw in list)
        {
            if(kw.word == _keyword)
            {
                return true;
            }  
        }
        return false;
    }
    public KeywordOrder GetByKeyword(string _word)
    {
        foreach (KeywordOrder kw in list)
        {
            if (kw.word == _word)
            {
                return kw;
            }
        }
        Debug.LogError(string.Format("can not find keyword: {0}", _word));
        return new KeywordOrder();
    }
}
