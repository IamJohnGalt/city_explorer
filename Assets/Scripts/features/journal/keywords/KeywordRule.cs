﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct KeywordOrder
{
    public string word;
    public int orderIndex;
    public int typeIndex;
}
[System.Serializable]
public class KeywordRule
{
    public string UID;
    public string keyword;
    public string formula;
    public float value;
    public int reqCount;
    public string specialType;

    public KeywordRule()
    {
        UID = "";
        keyword = "";
        formula = "";
        value = 0;
        reqCount = -1;
        specialType = "";
    }
}
