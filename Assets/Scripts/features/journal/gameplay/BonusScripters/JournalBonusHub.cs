﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class JournalBonusHub : MonoBehaviour
{
    public GameObject rulesetHolder;
    [SerializeField] dataJournal gameplayData;
    [SerializeField] List<string> staticRuleset;
    public List<JournalBonusRuleBase> ruleset = new List<JournalBonusRuleBase>();

    public void GetAllRules()
    {
        ruleset.Clear();
        ruleset.AddRange(rulesetHolder.GetComponents<JournalBonusRuleBase>().ToList());
    }
    public void ActivateRules(List<string> additionalRuleUIDs = null)
    {
        DeactivateAllRules();
        additionalRuleUIDs = additionalRuleUIDs ?? new List<string>();
        foreach (JournalBonusRuleBase rule in ruleset)
        {
            if (additionalRuleUIDs.Contains(rule.ruleUID) || staticRuleset.Contains(rule.ruleUID) || ActiveBuffs.controller.CheckFeatureGate(rule.ruleUID))
            {
                rule.InitRule();
            }
        }
    }
    void DeactivateAllRules()
    {
        foreach (JournalBonusRuleBase rule in ruleset)
        {
            rule.activated = false;
        }
    }
    public void CheckRuleset()
    {
        List<StarBonusItem> bonusItems = new List<StarBonusItem>();
        List<StarBonusItem> bonusWIPs = new List<StarBonusItem>();
        List<StarBonusItem> bonusQuests = new List<StarBonusItem>();
        foreach (JournalBonusRuleBase rule in ruleset)
        {
            //check each activated to determine item/quest it should add
            if (rule.activated)
            {
                List<StarBonusItem> result = rule.CheckRule();
                foreach(StarBonusItem item in result)
                {
                    if (item.status == StarBonusItem.BonusItemStatus.activated)
                    {
                        bonusItems.Add(item);
                    }
                    else if (item.status == StarBonusItem.BonusItemStatus.wip)
                    {
                        bonusWIPs.Add(item);
                    }
                    else if (item.status == StarBonusItem.BonusItemStatus.preview)
                    {
                        bonusQuests.Add(item);
                    }
                }
                
            }
        }
        gameplayData.starBonusItems = bonusItems;
        gameplayData.starBonusWIPs = bonusWIPs;
        gameplayData.starBonusQuests = bonusQuests;
    }
    public StarBonusItem PreviewRule(string ruleUID)
    {
        StarBonusItem preview = new StarBonusItem();
        //Debug.Log("current rule count: " + ruleset.Count);
        foreach (JournalBonusRuleBase rule in ruleset)
        {
            if (ruleUID == rule.ruleUID)
            {
                //Debug.Log("found rule UID: " + rule.ruleUID);
                preview = rule.PreviewRule();
            }
        }
        return preview;
    }
}
