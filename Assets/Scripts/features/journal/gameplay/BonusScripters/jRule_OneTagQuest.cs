﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class jRule_OneTagQuest : JournalBonusRuleBase
{
    [Header("Activate")]
    [SerializeField] List<string> keywordOptions;
    [SerializeField] string title;
    [SerializeField] List<int> bonusStarByOptions;
    [SerializeField] string prefix;
    [SerializeField] string suffix;

    [Header("Preview")]
    [SerializeField] bool showPreview;
    [SerializeField] string preview_title;
    [SerializeField] string preview_prefix;
    [SerializeField] string preview_suffix;

    private int selectedIndex = -1;
    public override void InitRule()
    {
        selectedIndex = Random.Range(0, keywordOptions.Count);
        base.InitRule();
    }

    public override List<StarBonusItem> CheckRule()
    {
        List<StarBonusItem> result = new List<StarBonusItem>();
        bool completed = false;
        for (int i = 0; i < gameplayData.curComboTokenCount; i++)
        {
            if (gameplayData.tokens[i] != "")
            {
                Collectible card = dDataHub.hub.dCollectible.GetFromUID(gameplayData.tokens[i]);
                for (int j = 0; j < card.keywords.Count; j++)
                {
                    if (card.keywords[j] == keywordOptions[selectedIndex])
                    {
                        completed = true;
                    }
                }
            }
        }
        if (!completed && showPreview)
        {
            StarBonusItem item = new StarBonusItem();
            item.status = StarBonusItem.BonusItemStatus.preview;
            item.title = preview_title;
            item.prefix = preview_prefix;
            item.keywords.Add(keywordOptions[selectedIndex]);
            item.bonusStar = bonusStarByOptions[selectedIndex];
            item.suffix = preview_suffix; 
            result.Add(item);
        }
        else if (completed)
        {
            StarBonusItem item = new StarBonusItem();
            item.status = StarBonusItem.BonusItemStatus.activated;
            item.title = title;
            item.prefix = prefix;
            item.keywords.Add(keywordOptions[selectedIndex]);
            item.bonusStar = bonusStarByOptions[selectedIndex];
            item.suffix = suffix;
            result.Add(item);
        }
        return result;
    }
    public override StarBonusItem PreviewRule()
    {
        //Debug.Log("preview rule one tag called");
        StarBonusItem result = new StarBonusItem();
        result.status = StarBonusItem.BonusItemStatus.preview;
        result.title = preview_title;
        result.prefix = "[特殊任务]至少获得";
        result.bonusStar = bonusStarByOptions[0];
        result.suffix = "";
        return result;
    }
}
