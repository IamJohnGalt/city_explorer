﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class jRule_Feeling : JournalBonusRuleBase
{
    public string targetGroup = "feeling";
    public string title;
    public List<int> reqCount;
    public List<int> bonusStar;
    public List<string> suffix;
    private List<int> feelingCount = new List<int>();
    private List<string> existedFeelings = new List<string>();

    public override List<StarBonusItem> CheckRule()
    {
        List<StarBonusItem> result = new List<StarBonusItem>();
        existedFeelings.Clear();
        feelingCount.Clear();
        for (int i = 0; i < gameplayData.curComboTokenCount; i++)
        {
            if (gameplayData.tokens[i] != "")
            {
                Collectible card = dDataHub.hub.dCollectible.GetFromUID(gameplayData.tokens[i]);
                foreach (string kw in card.keywords)
                {
                    if (dDataHub.hub.allKeywords.KeywordIsGroup(kw, targetGroup))
                    {
                        if (existedFeelings.Contains(kw))
                        {
                            for(int j=0;j< existedFeelings.Count; j++)
                            {
                                if(existedFeelings[j] == kw)
                                {
                                    feelingCount[j] = feelingCount[j] + 1;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            existedFeelings.Add(kw);
                            feelingCount.Add(1);
                        }
                    }
                }
            }
        }
        if (existedFeelings.Count == 0)
        {
            //empty bonus
        }
        else if (ActiveBuffs.controller.CheckFeatureGate("feature_gate_no_more_feeling_conflict"))
        {
            //ignore feeling conflict, find the largest
            int resultIndex = 0;
            int curMax = 0;
            for(int i = 0; i < feelingCount.Count; i++)
            {
                curMax = Mathf.Max(curMax, feelingCount[i]);
            }
            for (int i = 0; i < reqCount.Count; i++)
            {
                if (reqCount[i] == curMax)
                {
                    resultIndex = i;
                    break;
                }
            }
            StarBonusItem item = new StarBonusItem();
            item.status = StarBonusItem.BonusItemStatus.activated;
            item.title = title;
            item.keywords.Add(targetGroup);
            item.prefix = "来自所有";
            TotalBuffValueSet starBVS = ActiveBuffs.controller.RetrieveValueSetByAction("journal_combo_star");
            item.bonusStar = ActiveBuffs.ModifyByBVS(bonusStar[resultIndex], starBVS);
            item.suffix = suffix[resultIndex];
            result.Add(item);
        }
        else if(existedFeelings.Count == 1)
        {
            //one feeling - + bonus
            int resultIndex = 0;
            for (int i = 0; i < reqCount.Count; i++)
            {
                if (reqCount[i] == feelingCount[0])
                {
                    resultIndex = i;
                    break;
                }
            }
            StarBonusItem item = new StarBonusItem();
            item.status = StarBonusItem.BonusItemStatus.activated;
            item.title = title;
            item.keywords.Add(targetGroup);
            item.prefix = "来自同一类型";
            TotalBuffValueSet starBVS = ActiveBuffs.controller.RetrieveValueSetByAction("journal_combo_star");
            item.bonusStar = ActiveBuffs.ModifyByBVS(bonusStar[resultIndex], starBVS);
            item.suffix = suffix[resultIndex];
            result.Add(item);
        }
        else if(existedFeelings.Count > 1)
        {
            //conflict - penalty
            StarBonusItem item = new StarBonusItem();
            int resultIndex = 0;
            item.status = StarBonusItem.BonusItemStatus.activated;
            item.title = title;
            item.keywords.Add(targetGroup);
            item.prefix = "来自";
            item.bonusStar = bonusStar[resultIndex];
            item.suffix = suffix[resultIndex];
            result.Add(item);
        }
        return result;
    }
}
