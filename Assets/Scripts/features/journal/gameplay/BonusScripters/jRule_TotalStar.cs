﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class jRule_TotalStar : JournalBonusRuleBase
{
    public override List<StarBonusItem> CheckRule()
    {
        List<StarBonusItem> result = new List<StarBonusItem>();
        StarBonusItem item = new StarBonusItem();
        int totalStars = 0;
        for(int i = 0; i < gameplayData.journalXLSData.maxTokenCount; i++)
        {
            totalStars += gameplayData.tokenStar[i];
        }

        item.status = StarBonusItem.BonusItemStatus.activated;
        item.bonusStar = totalStars;
        item.prefix = "来自全部风景";
        result.Add(item);
        return result;
    }
}
