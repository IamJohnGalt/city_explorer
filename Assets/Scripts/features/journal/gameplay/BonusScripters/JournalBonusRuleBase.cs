﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JournalBonusRuleBase : MonoBehaviour
{
    public dataJournal gameplayData;
    public string ruleUID;
    public bool activated;
    private void Start()
    {
        
    }
    public virtual void InitRule()
    {
        activated = true;
    }
    public virtual List<StarBonusItem> CheckRule()
    {
        List<StarBonusItem> items = new List<StarBonusItem>();
        return items;
    }
    public virtual StarBonusItem PreviewRule()
    {
        StarBonusItem item = new StarBonusItem();
        Debug.Log("preview rule base called");
        return item;
    }
}
