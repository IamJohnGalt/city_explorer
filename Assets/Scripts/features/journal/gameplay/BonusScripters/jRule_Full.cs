﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class jRule_Full : JournalBonusRuleBase
{
    [Header("Activate")]
    [SerializeField] string title = "完整";
    [SerializeField] int bonusStar;
    //[SerializeField] string suffix;

    public override List<StarBonusItem> CheckRule()
    {
        List<StarBonusItem> result = new List<StarBonusItem>();
        if (gameplayData.gameplayStatus == dConstants.JournalConst.JournalStatus.journal_full)
        {
            StarBonusItem item = new StarBonusItem();
            item.status = StarBonusItem.BonusItemStatus.activated;
            item.title = title;
            item.prefix = "来自 填满游记";
            item.bonusStar = bonusStar;
            //result.suffix = suffix;
            result.Add(item);
        }
        return result;
    }
}