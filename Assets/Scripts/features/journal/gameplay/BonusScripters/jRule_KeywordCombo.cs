﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class jRule_KeywordCombo : JournalBonusRuleBase
{
    [Header("Activate")]
    [SerializeField] List<string> requiredKeywords;
    [SerializeField] List<string> optionalKeywords;
    [SerializeField] string title;
    [SerializeField] List<int> reqCount;
    [SerializeField] List<int> bonusStar;
    [SerializeField] string suffix;

    [Header("Preview")]
    [SerializeField] bool showPreview;
    [SerializeField] bool showWIP;
    [SerializeField] string preview_title;
    [SerializeField] string preview_suffix;

    private List<bool> requiredKeywordHit = new List<bool>();
    private List<bool> optionalKeywordHit = new List<bool>();

    public override List<StarBonusItem> CheckRule()
    {
        List<StarBonusItem> result = new List<StarBonusItem>();

        //reset all hits to false
        requiredKeywordHit.Clear();
        optionalKeywordHit.Clear();
        for(int i=0;i< requiredKeywords.Count; i++)
        {
            requiredKeywordHit.Add(false);
        }
        for (int i = 0; i < optionalKeywords.Count; i++)
        {
            optionalKeywordHit.Add(false);
        }
        int totalHit = 0;
        
        //start counting
        for (int i = 0; i < gameplayData.curComboTokenCount; i++)
        {
            if (gameplayData.tokens[i] != "")
            {
                Collectible card = dDataHub.hub.dCollectible.GetFromUID(gameplayData.tokens[i]);
                for (int j = 0; j < card.keywords.Count; j++)
                {
                    for (int k = 0; k < requiredKeywords.Count; k++)
                    {
                        if (dDataHub.hub.allKeywords.KeywordIsGroup(card.keywords[j], requiredKeywords[k]) || card.keywords[j] == requiredKeywords[k])
                        {
                            //hit a req
                            if (!requiredKeywordHit[k])
                            {
                                requiredKeywordHit[k] = true;
                                totalHit += 1;
                            }
                        }
                    }
                    for (int k = 0; k < optionalKeywords.Count; k++)
                    {
                        if (dDataHub.hub.allKeywords.KeywordIsGroup(card.keywords[j], optionalKeywords[k]) || card.keywords[j] == optionalKeywords[k])
                        {
                            if (!optionalKeywordHit[k])
                            {
                                optionalKeywordHit[k] = true;
                                totalHit += 1;
                            }
                        }
                    }
                }
            }
        }
        //check if all required is hit
        int requireHitCount = 0;
        bool allRequiredHit = false;
        for (int i = 0; i < requiredKeywordHit.Count; i++)
        {
            if (requiredKeywordHit[i])
            {
                requireHitCount += 1;
            }
        }
        if(requireHitCount == requiredKeywordHit.Count)
        {
            allRequiredHit = true;
        }
        //combo formed
        if (allRequiredHit && totalHit >= reqCount[0])
        {
            //find bonus star and return item
            int resultIndex = -1;
            for (int i = 0; i < reqCount.Count; i++)
            {
                if (reqCount[i] == totalHit)
                {
                    resultIndex = i;
                    break;
                }
            }
            if(resultIndex >= 0)
            {
                StarBonusItem item = new StarBonusItem();
                item.status = StarBonusItem.BonusItemStatus.activated;
                item.title = title;
                item.keywords.AddRange(requiredKeywords);
                for (int k = 0; k < optionalKeywords.Count; k++)
                {
                    if (optionalKeywordHit[k])
                    {
                        item.keywords.Add(optionalKeywords[k]);
                    }
                }
                item.prefix = "来自";
                TotalBuffValueSet starBVS = ActiveBuffs.controller.RetrieveValueSetByAction("journal_combo_star");
                item.bonusStar = ActiveBuffs.ModifyByBVS(bonusStar[resultIndex], starBVS);
                item.suffix = suffix;
                result.Add(item);
            }
        }
        else if (totalHit > 0 && totalHit < reqCount[reqCount.Count - 1] && showWIP)
        {
            //can be better but WIP
            StarBonusItem item = new StarBonusItem();
            item.status = StarBonusItem.BonusItemStatus.wip;
            item.title = title;
            item.prefix = "";
            item.keywords.AddRange(requiredKeywords);
            int optionalHit = 0;
            for (int i = 0; i < optionalKeywordHit.Count; i++)
            {
                if (optionalKeywordHit[i])
                {
                    item.keywords.Add(optionalKeywords[i]);
                    optionalHit += 1;
                }
                else
                {
                    item.addition_keywords.Add(optionalKeywords[i]);
                }
            }
            if (allRequiredHit)
            {
                int expectedCount = 1 + requiredKeywords.Count + optionalHit;
                int expectedIndex = -1;
                for (int i = 0; i < reqCount.Count; i++)
                {
                    if (reqCount[i] == expectedCount)
                    {
                        expectedIndex = i;
                        break;
                    }
                }
                TotalBuffValueSet starBVS = ActiveBuffs.controller.RetrieveValueSetByAction("journal_combo_star");
                item.bonusStar = ActiveBuffs.ModifyByBVS(bonusStar[expectedIndex], starBVS);
                item.midfix = "+";
                item.suffix = "之一以获得";
                result.Add(item);
            }
            else
            {
                int expectedCount = optionalHit + requiredKeywords.Count;
                int expectedIndex = -1;
                for (int i = 0; i < reqCount.Count; i++)
                {
                    if (reqCount[i] == expectedCount)
                    {
                        expectedIndex = i;
                        break;
                    }
                }
                TotalBuffValueSet starBVS = ActiveBuffs.controller.RetrieveValueSetByAction("journal_combo_star");
                item.bonusStar = ActiveBuffs.ModifyByBVS(bonusStar[expectedIndex], starBVS);
                item.addition_keywords.Clear();
                item.suffix = "以获得";
                result.Add(item);
            }
        }
        //if combo is not formed and previewable
        else if (showPreview)
        {
            //return preview
            StarBonusItem item = new StarBonusItem();
            item.status = StarBonusItem.BonusItemStatus.preview;
            item.title = title;
            item.keywords.AddRange(requiredKeywords);
            item.prefix = "";
            TotalBuffValueSet starBVS = ActiveBuffs.controller.RetrieveValueSetByAction("journal_combo_star");
            item.bonusStar = ActiveBuffs.ModifyByBVS(bonusStar[0], starBVS);
            item.suffix = "以获得";
            result.Add(item);
        }
        return result;
    }

}
