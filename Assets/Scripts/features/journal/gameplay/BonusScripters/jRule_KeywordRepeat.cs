﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class jRule_KeywordRepeat : JournalBonusRuleBase
{
    [Header("Activate")]
    [SerializeField] string targetGroup;
    [SerializeField] string title;
    [SerializeField] List<int> reqCount;
    [SerializeField] List<int> bonusStar;
    [SerializeField] List<string> suffix;

    [Header("Preview")]
    [SerializeField] bool showPreview;
    //[SerializeField] string preview_title;
    //[SerializeField] string preview_suffix;
    //count on the number of eligible tags
    public override List<StarBonusItem> CheckRule()
    {
        List<StarBonusItem> result = new List<StarBonusItem>();
        int matchingCount = 0;
        for(int i = 0; i < gameplayData.curComboTokenCount; i++)
        {
            if(gameplayData.tokens[i] != "")
            {
                Collectible card = dDataHub.hub.dCollectible.GetFromUID(gameplayData.tokens[i]);
                foreach (string kw in card.keywords)
                {
                    if(dDataHub.hub.allKeywords.KeywordIsGroup(kw, targetGroup) || targetGroup == kw)
                    {
                        matchingCount += 1;
                    }
                }

            }
        }
        //generate bonus item
        int resultIndex = -1;
        for(int i=0;i< reqCount.Count; i++)
        {
            if(reqCount[i] == matchingCount)
            {
                resultIndex = i;
                break;
            }
        }
        if(resultIndex >= 0)
        {
            StarBonusItem item = new StarBonusItem();
            item.status = StarBonusItem.BonusItemStatus.activated;
            item.title = title;
            item.keywords.Add(targetGroup);
            item.prefix = "来自";
            TotalBuffValueSet starBVS = ActiveBuffs.controller.RetrieveValueSetByAction("journal_combo_star");
            item.bonusStar = ActiveBuffs.ModifyByBVS(bonusStar[resultIndex], starBVS);
            item.suffix = suffix[resultIndex];
            result.Add(item);
        }
        else if(matchingCount > 0)
        {
            StarBonusItem item = new StarBonusItem();
            item.status = StarBonusItem.BonusItemStatus.wip;
            item.title = title;
            item.keywords.Add(targetGroup);
            item.prefix = string.Format("至少{0}个", reqCount[0]);
            item.bonusStar = bonusStar[0];
            item.suffix = "以获得";
            result.Add(item);
        }
        //if combo is not formed and previewable
        else if (showPreview)
        {
            //return preview
            StarBonusItem item = new StarBonusItem();
            item.status = StarBonusItem.BonusItemStatus.preview;
            item.title = title;
            item.keywords.Add(targetGroup);
            item.prefix = "每个";
            item.bonusStar = bonusStar[0];
            item.suffix = "额外获得";
            result.Add(item);
        }
        return result;
    }
    public override StarBonusItem PreviewRule()
    {
        //Debug.Log("preview rule keyword repeat called");
        StarBonusItem result = new StarBonusItem();
        result.status = StarBonusItem.BonusItemStatus.preview;
        result.title = title;
        result.keywords.Add(targetGroup);
        result.prefix = "每个";
        result.bonusStar = bonusStar[0];
        result.suffix = "额外获得";
        return result;
    }
}
