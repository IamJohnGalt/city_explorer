﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class jRule_Geo : JournalBonusRuleBase
{
    public string targetGroup = "geo";
    public string title;
    public List<int> reqCount;
    public List<int> bonusStar;
    private List<int> geoCount = new List<int>();
    private List<string> existedGeos = new List<string>();
    //public List<string> suffix;

    public override List<StarBonusItem> CheckRule()
    {
        List<StarBonusItem> result = new List<StarBonusItem>();
        existedGeos.Clear();
        int matchingCount = 0;
        for (int i = 0; i < gameplayData.curComboTokenCount; i++)
        {
            if (gameplayData.tokens[i] != "")
            {
                Collectible card = dDataHub.hub.dCollectible.GetFromUID(gameplayData.tokens[i]);
                foreach (string kw in card.keywords)
                {
                    if (dDataHub.hub.allKeywords.KeywordIsGroup(kw, targetGroup))
                    {
                        if (!existedGeos.Contains(kw))
                        {
                            matchingCount += 1;
                            existedGeos.Add(kw);
                            geoCount.Add(1);
                        }
                        else
                        {
                            for (int j = 0; j < existedGeos.Count; j++)
                            {
                                if (existedGeos[j] == kw)
                                {
                                    geoCount[j] = geoCount[j] + 1;
                                    break;
                                }
                            }
                        }
                        
                    }
                }

            }
        }
        int resultIndex = 0;
        if (matchingCount == 0)
        {
            return result;
        }
        else if (ActiveBuffs.controller.CheckFeatureGate("feature_gate_no_more_geo_conflict"))
        {
            int totalCount = 0;
            for (int i = 0; i < geoCount.Count; i++)
            {
                totalCount += geoCount[i];
            }
            for (int i = 0; i < reqCount.Count; i++)
            {
                if (reqCount[i] == totalCount)
                {
                    resultIndex = i;
                    break;
                }
            }
        }
        else
        {
            for (int i = 0; i < reqCount.Count; i++)
            {
                if (reqCount[i] == matchingCount)
                {
                    resultIndex = i;
                    break;
                }
            }
        }
        //generate bonus item
        StarBonusItem item = new StarBonusItem();
        item.status = StarBonusItem.BonusItemStatus.activated;
        item.title = title;
        item.keywords.Add(targetGroup);
        item.prefix = ActiveBuffs.controller.CheckFeatureGate("feature_gate_no_more_geo_conflict") ? "来自全部" : "来自独特";
        TotalBuffValueSet starBVS = ActiveBuffs.controller.RetrieveValueSetByAction("journal_combo_star");
        item.bonusStar = ActiveBuffs.ModifyByBVS(bonusStar[resultIndex], starBVS);
        item.suffix = "";
        result.Add(item);
        return result;
    }
}
