﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class jRule_CountUniqueKeywords : JournalBonusRuleBase
{
    public string requiredFeatureGate;

    [Header("Activate")]
    [SerializeField] int requiredCount;
    [SerializeField] int bonusStar;
    [SerializeField] string title;
    [SerializeField] string suffix;

    [Header("Preview")]
    [SerializeField] bool showPreview;
    [SerializeField] int previewCount;
    //[SerializeField] string preview_title;
    //[SerializeField] string preview_suffix;

    private List<string> existedKeywords = new List<string>();

    public override List<StarBonusItem> CheckRule()
    {
        List<StarBonusItem> result = new List<StarBonusItem>();
        if (!ActiveBuffs.controller.CheckFeatureGate(requiredFeatureGate))
        {
            return result;
        }
        existedKeywords = new List<string>();
        //start counting
        for (int i = 0; i < gameplayData.curComboTokenCount; i++)
        {
            if (gameplayData.tokens[i] != "")
            {
                Collectible card = dDataHub.hub.dCollectible.GetFromUID(gameplayData.tokens[i]);
                for (int j = 0; j < card.keywords.Count; j++)
                {
                    if (!existedKeywords.Contains(card.keywords[j]))
                    {
                        existedKeywords.Add(card.keywords[j]);
                    }
                }
            }
        }
        //combo formed
        if (existedKeywords.Count >= requiredCount)
        {
            StarBonusItem item = new StarBonusItem();
            item.status = StarBonusItem.BonusItemStatus.activated;
            item.title = title;
            item.keywords = new List<string>();
            item.prefix = "来自";
            TotalBuffValueSet starBVS = ActiveBuffs.controller.RetrieveValueSetByAction("journal_combo_star");
            item.bonusStar = ActiveBuffs.ModifyByBVS(bonusStar, starBVS);
            item.suffix = suffix;
            result.Add(item);
        }
        /*
        else if (existedKeywords.Count >= previewCount)
        {
            result.status = StarBonusItem.BonusItemStatus.wip;
            result.title = title;
            result.keywords = new List<string>();
            result.prefix = suffix;
            TotalBuffValueSet starBVS = ActiveBuffs.controller.RetrieveValueSetByAction("journal_combo_star");
            result.bonusStar = ActiveBuffs.ModifyByBVS(bonusStar, starBVS);
            result.suffix = "以获得";
        }*/
        //if combo is not formed and previewable
        else if (showPreview)
        {
            StarBonusItem item = new StarBonusItem();
            item.status = StarBonusItem.BonusItemStatus.preview;
            item.title = title;
            item.keywords = new List<string>();
            item.prefix = suffix;
            TotalBuffValueSet starBVS = ActiveBuffs.controller.RetrieveValueSetByAction("journal_combo_star");
            item.bonusStar = ActiveBuffs.ModifyByBVS(bonusStar, starBVS);
            item.suffix = "以获得";
            result.Add(item);
        }
        return result;
    }
}
