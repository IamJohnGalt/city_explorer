﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class jRule_UniqueTagCount : JournalBonusRuleBase
{
    public string title;
    public List<int> reqCount;
    public List<int> bonusStar;
    private List<string> existedTags = new List<string>();

    public override List<StarBonusItem> CheckRule()
    {
        List<StarBonusItem> result = new List<StarBonusItem>();
        existedTags.Clear();
        for (int i = 0; i < gameplayData.curComboTokenCount; i++)
        {
            if (gameplayData.tokens[i] != "")
            {
                Collectible card = dDataHub.hub.dCollectible.GetFromUID(gameplayData.tokens[i]);
                foreach (string kw in card.keywords)
                {
                    if (!existedTags.Contains(kw))
                    {
                        existedTags.Add(kw);
                    }
                }
            }
        }
        int resultIndex = -1;
        if (existedTags.Count < reqCount[0])
        {
            StarBonusItem item = new StarBonusItem();
            item.status = StarBonusItem.BonusItemStatus.preview;
            item.title = title;
            item.prefix = string.Format("拥有至少{0}个独特标签以获得", reqCount[0]);
            TotalBuffValueSet starBVS = ActiveBuffs.controller.RetrieveValueSetByAction("journal_combo_star");
            item.bonusStar = ActiveBuffs.ModifyByBVS(bonusStar[0], starBVS);
            item.suffix = "";
            result.Add(item);
            return result;
        }
        else
        {
            for (int i = 0; i < reqCount.Count; i++)
            {
                if (reqCount[i] <= existedTags.Count)
                {
                    resultIndex = i;
                }
            }
            //generate bonus item
            StarBonusItem item = new StarBonusItem();
            item.status = StarBonusItem.BonusItemStatus.activated;
            item.title = title;
            item.prefix = string.Format("来自至少{0}个独特标签", reqCount[resultIndex]);
            TotalBuffValueSet starBVS = ActiveBuffs.controller.RetrieveValueSetByAction("journal_combo_star");
            item.bonusStar = ActiveBuffs.ModifyByBVS(bonusStar[resultIndex], starBVS);
            item.suffix = "";
            result.Add(item);
            return result;
        }
        
    }

}
