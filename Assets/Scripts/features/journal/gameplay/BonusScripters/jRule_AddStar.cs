﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class jRule_AddStar : JournalBonusRuleBase
{
    [Header("Activate")]
    [SerializeField] string title;
    [SerializeField] List<int> bonusStar;
    [SerializeField] string prefix;
    [SerializeField] string suffix;

    public override List<StarBonusItem> CheckRule()
    {
        List<StarBonusItem> result = new List<StarBonusItem>();
        if (bonusStar.Count == 0)
        {
            int totalAddStar = 0;
            TotalBuffValueSet starBVS = ActiveBuffs.controller.RetrieveValueSetByAction("journal_extra_star");
            totalAddStar = ActiveBuffs.ModifyByBVS(0, starBVS);
            if (totalAddStar > 0)
            {
                StarBonusItem item = new StarBonusItem();
                item.status = StarBonusItem.BonusItemStatus.activated;
                item.title = title;
                item.prefix = prefix;
                item.bonusStar = totalAddStar;
                item.suffix = suffix;
                result.Add(item);
                return result;
            }
            else
            {
                return result;
            }
        }
        else
        {
            StarBonusItem item = new StarBonusItem();
            item.status = StarBonusItem.BonusItemStatus.activated;
            item.title = title;
            item.prefix = prefix;
            item.bonusStar = bonusStar[0];
            item.suffix = suffix;
            result.Add(item);
            return result;
        }
        
        
    }
    public override StarBonusItem PreviewRule()
    {
        //Debug.Log("preview rule addstar called");
        StarBonusItem result = new StarBonusItem();
        result.status = StarBonusItem.BonusItemStatus.preview;
        result.title = title;
        result.prefix = "额外获得";
        result.bonusStar = 0;
        result.suffix = "";
        return result;
    }
}
