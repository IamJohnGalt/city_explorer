﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gJournal : MonoBehaviour
{
    public static gJournal controller;

    //[SerializeField] gChronicle chronicleScript;
    [SerializeField] dataJournal gameplayData;
    [SerializeField] dataEndSession endSessionData;
    [SerializeField] AllKeywords allKeywords;
    [SerializeField] JournalBonusHub rulesetHub;
    [SerializeField] StringAsset prefixPool;
    //[SerializeField] JournalQuestHub questHub;
    //[SerializeField] JournalComboHub comboHub;

    [Header("Events")]
    [SerializeField] GameEvent eventJournalUIInit;
    //[SerializeField] GameEvent eventJournalOptionReady;
    [SerializeField] GameEvent eventJournalSelected;
    [SerializeField] GameEvent eventJournalTokenAdded;
    [SerializeField] GameEvent eventJournalUpdated;
    [SerializeField] GameEvent eventJournalRewardReady;
    [SerializeField] GameEvent eventJournalForcePopup;

    private void Awake()
    {
        if (controller == null)
        {
            controller = this;
        }
        else if (controller != null)
        {
            Destroy(gameObject);
        }
    }
    public void JournalGameplayInit()
    {
        rulesetHub.GetAllRules();
        eventJournalUIInit.Raise();

        //start with a random 2 slot journal
        //gameplayData.optionalJournals.Clear();
        //gameplayData.optionalJournals.Add();
        JournalSelected("journal_random_diary");
    }
    public void JournalGameplayReset()
    {
        gameplayData.Reset();
    }
    /*
    public void JournalSelected(int index)
    {
        //old journal selection, new one uses uid instead.
        if (index < gameplayData.optionalJournals.Count)
        {
            JournalStyle style = dDataHub.hub.dJournalStyle.GetFromUID(gameplayData.optionalJournals[index]);
            gameplayData.journalXLSData = style;
            //set status
            gameplayData.gameplayStatus = dConstants.JournalConst.JournalStatus.journal_working;
            gameplayData.journalUID = style.UID;
            gameplayData.journalName = style.title;
            gameplayData.journalBgArt = style.artAsset.backgroundArt;
            gameplayData.journalTagArt = style.artAsset.objectArt;
            gameplayData.curMaxTokenCount = style.modifiedMaxTokenCount;
            gameplayData.curComboTokenCount = style.modifiedComboTokenCount;

            eventJournalSelected.Raise();

            gameplayData.tokens.Clear();
            gameplayData.tokenStar.Clear();

            //setup token slots with all "empty"
            for(int i=0;i< gameplayData.curMaxTokenCount; i++)
            {
                gameplayData.tokens.Add("");
                gameplayData.tokenStar.Add(0);
            }
            ActiveBuffs.controller.ToggleFeatureGate("feature_gate_journal_not_full", !JournalIsFull());

            rulesetHub.ActivateRules(style.additionalRuleset);
            UpdateJournalCalculation();

            //trigger help check
            //CharacterHelp.controller.HelpCheck_Character1_HelpLv2();
        }
        else
        {
            Debug.Log(string.Format("invalid journal selection index ({0})", index));
        }
    }
    */
    public void JournalSelected(string journalStyleUID)
    {
        JournalStyle style = dDataHub.hub.dJournalStyle.GetFromUID(journalStyleUID);
        gameplayData.journalXLSData = style;
        //set status
        gameplayData.gameplayStatus = dConstants.JournalConst.JournalStatus.journal_working;
        gameplayData.journalUID = style.UID;
        gameplayData.journalName = string.Format(style.title,GenerateRandomPrefix());
        gameplayData.journalBgArt = style.artAsset.backgroundArt;
        gameplayData.journalTagArt = style.artAsset.objectArt;
        gameplayData.curMaxTokenCount = style.modifiedMaxTokenCount;
        gameplayData.curComboTokenCount = style.modifiedComboTokenCount;
        CurrencyManagement.wallet.AddCurrency("currency_coin", style.additionalCoin);
        gameplayData.oneParkJournal = (style.styleGroup == "sketch");
        gameplayData.sessionStartJournal = journalStyleUID == "journal_session_start";
        gameplayData.parkVisited = false;


        //setup token slots with all "empty"/previous tokens
        gameplayData.tokens.Clear();
        gameplayData.tokenStar.Clear();
        for (int i = 0; i < gameplayData.curMaxTokenCount; i++)
        {
            if(i < gameplayData.previousTokens.Count)
            {
                gameplayData.tokens.Add(gameplayData.previousTokens[i]);
            }
            else
            {
                gameplayData.tokens.Add("");
            }
            //placehodler as 0 star, will be updated use UpdateJournalCalculation()
            gameplayData.tokenStar.Add(0);
        }
        gameplayData.previousTokens.Clear();
        ActiveBuffs.controller.ToggleFeatureGate("feature_gate_journal_not_full", !JournalIsFull());
        //special case to handle buff star add
        if (style.additionalRuleset.Contains("buff_star_add"))
        {
            ActiveBuffs.controller.ActivateBuff("journal", "journal_submission", "buff_journal_extra_star_add", style.comboTokenCount - 2, 1);
            rulesetHub.ActivateRules();
        }
        else
        {
            rulesetHub.ActivateRules(style.additionalRuleset);
        }
        //persona
        if (ActiveBuffs.controller.CheckFeatureGate("feature_gate_persona_journal_official"))
        {
            AddToken("collectible_official_v1");
            CharacterPersona.controller.Persona_Start_Journal_wOffical();
        }
        UpdateJournalCalculation();
        eventJournalSelected.Raise();

        //trigger help check
        //CharacterHelp.controller.HelpCheck_Character1_HelpLv2();
    }
    public bool AddToken(string tokenUID, bool skipEffect = false)
    {
        bool added = false;
        for (int i=0; i < gameplayData.tokens.Count; i++)
        {
            if(gameplayData.tokens[i] == "")
            {
                gameplayData.tokens[i] = tokenUID;
                added = true;
                CheckBonusRewardByCollectible(tokenUID);
                break;
            } 
        }
        //handle full situation
        if (!added)
        {
            int minTierIndex = -1;
            int minTier = int.MaxValue;
            int curTier = -1;
            int targetTier = dDataHub.hub.dCollectible.GetFromUID(tokenUID).tier;
            for (int i = 0; i < gameplayData.tokens.Count; i++)
            {
                if (gameplayData.tokens[i] != "")
                {
                    curTier = dDataHub.hub.dCollectible.GetFromUID(gameplayData.tokens[i]).tier;
                    if(minTier > curTier)
                    {
                        minTier = curTier;
                        minTierIndex = i;
                    }
                }
            }
            if(minTier < targetTier)
            {
                Debug.Log(string.Format("Replace a lowest tier card with a new card"));
                gameplayData.tokens[minTierIndex] = tokenUID;
                added = true;
            }
        }
        //trigger help check
        //CharacterHelp.controller.HelpCheck_Character1_HelpLv5(tokenUID);
        ActiveBuffs.controller.ToggleFeatureGate("feature_gate_journal_not_full", !JournalIsFull());
        if (JournalIsFull())
        {
            gameplayData.gameplayStatus = dConstants.JournalConst.JournalStatus.journal_full;
        }
        if (added && !skipEffect)
        {
            CollectibleAcquisition.controller.AcquireCollectible(tokenUID);
            //check currency acquisition
        }
        UpdateJournalCalculation();
        ForceJournalUpdate();
        return added;
        
    }
    public void RemoveToken(int index)
    {
        //trigger help check
        //CharacterHelp.controller.HelpCheck_Character2_HelpLv4(gameplayData.tokens[index]);

        gameplayData.tokens[index] = "";
        gameplayData.tokenStar[index] = 0;


        if (!JournalIsFull())
        {
            gameplayData.gameplayStatus = dConstants.JournalConst.JournalStatus.journal_working;
        }
        if(gameplayData.CurTokenCount == gameplayData.curComboTokenCount)
        {
            //move all tokens in extra token slots back
            for (int i = 0; i < gameplayData.curComboTokenCount; i++)
            {
                if(gameplayData.tokens[i] == "")
                {
                    for (int j = gameplayData.curComboTokenCount; j < gameplayData.curMaxTokenCount; j++)
                    {
                        if(gameplayData.tokens[j] != "")
                        {
                            gameplayData.tokens[i] = gameplayData.tokens[j];
                            gameplayData.tokens[j] = "";
                            break;
                        }
                    }
                }
            }
        }
        UpdateJournalCalculation();
        
    }
    public void Rearrange4HighestStar()
    {
        bool foundHigherCombo = false;
        int highestStar = gameplayData.totalStars;
        
        List<bool> bestSelection = new List<bool>();
        int curTokenCount = gameplayData.CurTokenCount;
        int requiredSelectedCount = gameplayData.curComboTokenCount;
        //setup all non-empty tokens each with a false mark
        List<string> allTokens = new List<string>();
        allTokens.AddRange(gameplayData.tokens.GetRange(0, curTokenCount));
        List<bool> tokenSelected = new List<bool>();
        for (int i = 0; i < curTokenCount; i++)
        {
            tokenSelected.Add(false);
        }
        //write down original set
        List<string> originalTokens = new List<string>();
        originalTokens.AddRange(allTokens);

        int CombinationCount = Mathf.RoundToInt(Mathf.Pow(2, curTokenCount));
        for(int i = 0; i < CombinationCount; i++)
        {
            int binaryNumber = i;
            int selectCount = 0;
            for(int tokenIndex = 0; tokenIndex < curTokenCount; tokenIndex++)
            {
                bool CurDigitIs1 = (binaryNumber & 1) == 1;
                tokenSelected[tokenIndex] = CurDigitIs1;
                binaryNumber = binaryNumber >> 1;
                if (CurDigitIs1)
                {
                    selectCount += 1;
                }
            }
            if(selectCount == requiredSelectedCount)
            {
                //test this combo
                int curSelectedIndex = 0;
                int curOutIndex = requiredSelectedCount;
                for (int j=0; j < curTokenCount; j++)
                {
                    if (tokenSelected[j])
                    {
                        gameplayData.tokens[curSelectedIndex++] = allTokens[j];
                    }
                    else
                    {
                        gameplayData.tokens[curOutIndex++] = allTokens[j];
                    }
                }
                UpdateJournalCalculation();
                //if it is the highest start record it
                if(gameplayData.totalStars > highestStar)
                {
                    bestSelection.Clear();
                    bestSelection.AddRange(tokenSelected);
                    highestStar = gameplayData.totalStars;
                    foundHigherCombo = true;
                }
            }
        }

        if (foundHigherCombo)
        {
            int curSelectedIndex = 0;
            int curOutIndex = requiredSelectedCount;
            for (int i = 0; i < curTokenCount; i++)
            {
                if (bestSelection[i])
                {
                    gameplayData.tokens[curSelectedIndex++] = allTokens[i];
                }
                else
                {
                    gameplayData.tokens[curOutIndex++] = allTokens[i];
                }
            }
            UpdateJournalCalculation();
            Debug.Log(string.Format("token rearranged for highest star to {0}", highestStar));
        }
        else
        {
            for (int i = 0; i < curTokenCount; i++)
            {
                gameplayData.tokens[i] = originalTokens[i];
            }
            UpdateJournalCalculation();
            Debug.Log(string.Format("token NOT rearranged since it is already the highest star", highestStar));
        }
    }
    public void ForceJournalSubmission()
    {
        gameplayData.forcedSubmission = true;
        EnableJournalSubmission();
    }
    public void EnableJournalSubmission()
    {
        eventJournalForcePopup.Raise();
    }
    public void ForceJournalUpdate()
    {
        eventJournalUpdated.Raise();
    }
    public bool JournalSubmit()
    {
        if(gameplayData.journalIsSubmittable)
        {
            //trigger help check
            //CharacterHelp.controller.HelpCheck_Character1_HelpLv3();
            GenerateAllReward(Mathf.RoundToInt(gameplayData.totalStars));
            //GenerateRewardOptions(gameplayData.totalStars);
            //set status
            gameplayData.gameplayStatus = dConstants.JournalConst.JournalStatus.reward;
            if (!gameplayData.sessionStartJournal)
            {
                dStatistics.stats.JournalSubmitted(gameplayData.journalUID, gameplayData.totalStars);
            }
            //token acquisition
            if (ActiveBuffs.controller.CheckFeatureGate("feature_gate_help_journal_submission"))
            {
                CharacterAcquisition.controller.AcquisitionCheck_JournalSubmission();
            }
            if (ActiveBuffs.controller.CheckFeatureGate("feature_gate_help_journal_submission_book"))
            {
                JournalStyle style = dDataHub.hub.dJournalStyle.GetFromUID(gameplayData.journalUID);
                if (style.styleGroup == "book")
                {
                    CharacterAcquisition.controller.AcquisitionCheck_JournalSubmission_Book();
                }
            }
            if (ActiveBuffs.controller.CheckFeatureGate("feature_gate_persona_journal_submit_extra_month"))
            {
                CharacterPersona.controller.Persona_Journal_Submit_Extra_Month();
            }
            //count down timer
            ActiveBuffs.controller.TimerCountByProvider("journal_submission", 1);
            gameplayData.forcedSubmission = false;
            gChronicle.controller.JournalSubmitted();
            eventJournalUpdated.Raise();
            return true;
        }
        else
        {
            return false;
        }
    }
    public void RewardClaim(int constatnIndex)
    {
        RewardItem curReward = new RewardItem();
        curReward = gameplayData.constantRewards[constatnIndex];
        switch (curReward.type)
        {
            case RewardItem.RewardType.currency:
                CurrencyManagement.wallet.AddCurrency(curReward.rewardUID, curReward.modifiedAmount);
                if (curReward.rewardUID == "currency_star")
                {
                    HistoryLogItem logItem = new HistoryLogItem();
                    logItem.dateText = CalendarManagement.calendar.GetCurDate().DateToString();
                    logItem.log = string.Format("{0}发表<{1}>，收录为{2}第{3}篇。",
                        CharacterManagement.controller.GetCurrentCharacter().characterClass,
                        gameplayData.journalName,
                        dConstants.MASTER_JOURNAL_SHORT,
                        MasterJournalManagement.controller.TotalJournalCount() + 1);
                    logItem.starValue = curReward.modifiedAmount;
                    MasterJournalManagement.controller.AddHisotyrLog(logItem);
                }
                break;
            case RewardItem.RewardType.equipment:
                EquipmentManagement.equipment.AddEquipment(curReward.rewardUID);
                break;
            case RewardItem.RewardType.time:
                CalendarManagement.calendar.AddTravelMonth(curReward.modifiedAmount);
                break;
            default:
                break;
        }
        curReward.claimed = true;
    }
    public void RewardClaimConclude()
    {
        gameplayData.gameplayStatus = dConstants.JournalConst.JournalStatus.selection;
        //start a new journal
        gameplayData.journalBgArt = "";
        gameplayData.journalTagArt = "";
        //save exceeding token into previous Tokens
        for (int i = gameplayData.curComboTokenCount; i < gameplayData.curMaxTokenCount; i++)
        {
            if (gameplayData.tokens[i] != "")
            {
                gameplayData.previousTokens.Add(gameplayData.tokens[i]);
            }
        }
        eventJournalUpdated.Raise();
        gChronicle.controller.JournalConlusionSave();
    }
    public void RewardSelected(int index = -1)
    {
        
        //reward claim logic
        //Debug.Log("optional reward selected and claimed");
        RewardItem curReward = new RewardItem();
        List<KeyValuePair<string, int>> currencyRewardSets = new List<KeyValuePair<string, int>>();
        //sum all coin before adding
        int starRewardSum = 0;
        int coinRewardSum = 0;
        //claim constant rewards
        for (int i = 0; i < gameplayData.constantRewards.Count; i++)
        {
            curReward = gameplayData.constantRewards[i];
            switch (curReward.type)
            {
                case RewardItem.RewardType.currency:
                    if(curReward.rewardUID == "currency_coin")
                    {
                        coinRewardSum += curReward.modifiedAmount;
                    }
                    else
                    {
                        if(curReward.rewardUID == "currency_star")
                        {
                            starRewardSum += curReward.modifiedAmount;
                        }
                        CurrencyManagement.wallet.AddCurrency(curReward.rewardUID, curReward.modifiedAmount);
                    }
                    break;
                case RewardItem.RewardType.equipment:
                    EquipmentManagement.equipment.AddEquipment(curReward.rewardUID);
                    break;
                case RewardItem.RewardType.time:
                    CalendarManagement.calendar.AddTravelMonth(curReward.modifiedAmount);
                    break;
                default:
                    break;
            }
        }
        CurrencyManagement.wallet.AddCurrency("currency_coin", coinRewardSum);
        //log journal
        HistoryLogItem logItem = new HistoryLogItem();
        logItem.dateText = CalendarManagement.calendar.GetCurDate().DateToString();
        logItem.log = string.Format("{0}发表<{1}>，收录为《九》第{3}篇。", 
            CharacterManagement.controller.GetCurrentCharacter().characterClass, 
            gameplayData.journalName,
            dConstants.MASTER_JOURNAL_NAME,
            MasterJournalManagement.controller.TotalJournalCount()+1);
        logItem.starValue = starRewardSum;
        MasterJournalManagement.controller.AddHisotyrLog(logItem);
        //start a new journal
        gameplayData.gameplayStatus = dConstants.JournalConst.JournalStatus.selection;
        //confirm reward selected
        gChronicle.controller.JournalSubmitted();
        gChronicle.controller.ConcludeCurrentAction();
        //start a new journal
        gameplayData.journalBgArt = "";
        gameplayData.journalTagArt = "";
        eventJournalUpdated.Raise();

        //save exceeding token into previous Tokens
        for (int i = gameplayData.curComboTokenCount; i < gameplayData.curMaxTokenCount; i++)
        {
            if(gameplayData.tokens[i] != "")
            {
                gameplayData.previousTokens.Add(gameplayData.tokens[i]);
            }
        }
    }
    public void SwapToken(int indexA, int indexB)
    {
        if(indexA >= gameplayData.tokens.Count || indexB >= gameplayData.tokens.Count)
        {
            Debug.Log(string.Format("invalid token swap index ({0}, {1})", indexA, indexB));
        }
        string tmp = "";
        tmp = gameplayData.tokens[indexA];

        gameplayData.tokens[indexA] = gameplayData.tokens[indexB];
        gameplayData.tokens[indexB] = tmp;
        UpdateJournalCalculation();
    }
    public List<string> GetCurrentTokenList()
    {
        List<string> tokenList = new List<string>(gameplayData.tokens);
        return tokenList;
    }
    bool JournalIsFull()
    {
        bool isFull = true;
        for(int i = 0; i < gameplayData.tokens.Count; i++)
        {
            if(gameplayData.tokens[i] == "")
            {
                isFull = false;
            }
        }
        return isFull;
    }
    public void GenerateJournalOptions()
    {
        JournalGameplayReset();
        //gameplayData.optionalJournals.Clear();

        //TotalBuffValueSet bvs = ActiveBuffs.controller.RetrieveValueSetByAction("journal_option_count");
        //int drawCount = ActiveBuffs.ModifyByBVS(dConstants.JournalConst.MAX_OPTIONAL_JOURNAL, bvs);

        //gameplayData.optionalJournals = PFEntryDeck.DrawCardswConditions(
        //    dDataHub.hub.dJournalStyle.GetPFEntrywGateList(),
        //    drawCount);
        //set status
        //gameplayData.gameplayStatus = dConstants.JournalConst.JournalStatus.selection;
        //eventJournalOptionReady.Raise();
    }
    void GenerateAllReward(int starLv)
    {
        gameplayData.constantRewards.Clear();
        gameplayData.optionalRewards.Clear();
        //gameplayData.constantRewardClaimed.Clear();
        GenerateConstantReward(starLv);
        GenerateBuffReward();
        GenerateOptionalReward(starLv);
        //Debug.Log(string.Format("generate a reward set ({0} stars): coin ({1}), xp({2}), trait({3}), equipment({4}), fame({5}), optional({6})",
        //starLv, finalCoinLv, finalXPLv, finalTraitLv, finalEquipLv, finalFameLv, finalOptionalLv));
        eventJournalRewardReady.Raise();
    }
    string GenerateRandomPrefix()
    {
        int rng = Random.Range(0, prefixPool.Assets.Count);
        return prefixPool.Assets[rng];
    }
    List<RewardItem> InterprateReward(string jRewardUID, RewardItem addItem = null)
    {
        JournalReward reward = dDataHub.hub.dJournalReward.GetFromUID(jRewardUID);
        List<RewardItem> items = new List<RewardItem>(); 
        switch (reward.rewardType)
        {
            case "currency":
                RewardItem item = new RewardItem();
                item.type = RewardItem.RewardType.currency;
                item.rewardUID = reward.rewardUID;
                item.claimed = false;
                TotalBuffValueSet bvs = ActiveBuffs.controller.RetrieveValueSetByAction(string.Format("journal_{0}", item.rewardUID));
                int modifiedMin = ActiveBuffs.ModifyByBVS(reward.minValue, bvs);
                int modifiedMax = ActiveBuffs.ModifyByBVS(reward.maxValue, bvs);
                item.amount = new intRange(modifiedMin, modifiedMax).RandomRollByDefaultStep();
                if (reward.tags.Contains("slot_coin"))
                {
                    item.source = "来自游记类型";
                }
                else if (reward.tags.Contains("coin"))
                {
                    item.source = "来自游记星级";
                }
                else
                {
                    item.source = "";
                }
                if(addItem != null)
                {
                    if(addItem.rewardUID == item.rewardUID)
                    {
                        addItem.amount += item.amount;
                    }
                }
                else
                {
                    if(item.amount > 0)
                    {
                        items.Add(item);
                    }
                }
                break;
            case "random_equipment":
                if(reward.minValue > 0)
                {
                    List<string> equipmentUIDs = EquipmentManagement.equipment.PreviewEquipmentImprovement(reward.minValue);
                    for(int i = 0; i < equipmentUIDs.Count; i++)
                    {
                        RewardItem equReward = new RewardItem();
                        equReward.type = RewardItem.RewardType.equipment;
                        equReward.rewardUID = equipmentUIDs[i];
                        items.Add(equReward);
                    }
                }
                break;
            case "time":
                RewardItem timeAdd = new RewardItem();
                timeAdd.type = RewardItem.RewardType.time;
                timeAdd.rewardUID = reward.rewardUID;
                bvs = ActiveBuffs.controller.RetrieveValueSetByAction(string.Format("journal_{0}", timeAdd.rewardUID));
                modifiedMin = ActiveBuffs.ModifyByBVS(reward.minValue, bvs);
                modifiedMax = ActiveBuffs.ModifyByBVS(reward.maxValue, bvs);
                timeAdd.amount = new intRange(modifiedMin, modifiedMax).RandomRollByDefaultStep();
                if (timeAdd.amount > 0)
                {
                    items.Add(timeAdd);
                }
                break;
            default:
                break;
        }
        return items;
    }
    void GenerateConstantReward(int starLv)
    {
        List<string> tempTag = new List<string>();
        //
        string tempJournalReward = "";
        //constant fame
        //v2 changes: change fame to star for easier understanding
        int finalFameLv = gameplayData.journalXLSData.fameBase + Mathf.RoundToInt(gameplayData.journalXLSData.fameModifier * starLv);
        TotalBuffValueSet bvs = ActiveBuffs.controller.RetrieveValueSetByAction("journal_currency_fame");
        finalFameLv = Mathf.Min(ActiveBuffs.ModifyByBVS(finalFameLv, bvs),dConstants.JournalConst.MAX_REWARD_TIER);
        if (finalFameLv > 0)
        {
            tempTag = new List<string>();
            tempTag.Add("star");
            //optionalTag.Add("fame");
            tempJournalReward = PFEntryDeck.DrawCardswConditions(dDataHub.hub.dJournalReward.GetPFEntrywGateList(),
                1, tempTag,
                new intRange(finalFameLv, finalFameLv))[0];
            gameplayData.constantRewards.AddRange(InterprateReward(tempJournalReward));
        }
        //constant time (modified from xp)
        int finalTimeLv = gameplayData.journalXLSData.xpBase + Mathf.RoundToInt(gameplayData.journalXLSData.xpModifier * starLv);
        if (finalTimeLv > 0 && !endSessionData.ended)
        {
            tempTag = new List<string>();
            tempTag.Add("time");
            //optionalTag.Add("trait");
            tempJournalReward = PFEntryDeck.DrawCardswConditions(dDataHub.hub.dJournalReward.GetPFEntrywGateList(),
                1, tempTag,
                new intRange(finalTimeLv, finalTimeLv))[0];
            gameplayData.constantRewards.AddRange(InterprateReward(tempJournalReward));
        }
        //constant equip
        int finalEquipLv = gameplayData.journalXLSData.equipmentBase + Mathf.RoundToInt(gameplayData.journalXLSData.equipmentModifier * starLv);
        if (finalEquipLv > 0)
        {
            tempTag = new List<string>();
            tempTag.Add("equipment");
            //optionalTag.Add("equipment");
            tempJournalReward = PFEntryDeck.DrawCardswConditions(dDataHub.hub.dJournalReward.GetPFEntrywGateList(),
                1, tempTag,
                new intRange(finalEquipLv, finalEquipLv))[0];
            gameplayData.constantRewards.AddRange(InterprateReward(tempJournalReward));
        }
        //coin section merging coin from botnh slot and star
        List<RewardItem> coinRewards = new List<RewardItem>();
        int finalSlotCount = Mathf.RoundToInt(Mathf.Min(gameplayData.CurTokenCount, gameplayData.curComboTokenCount) * gameplayData.journalXLSData.coinModifier);
        if (finalSlotCount > 0)
        {
            tempTag = new List<string>();
            tempTag.Add("slot_coin");
            //optionalTag.Add("coin");
            tempJournalReward = PFEntryDeck.DrawCardswConditions(dDataHub.hub.dJournalReward.GetPFEntrywGateList(),
                1, tempTag,
                new intRange(finalSlotCount, finalSlotCount))[0];
            coinRewards = InterprateReward(tempJournalReward);
        }
        int finalCoinLv = gameplayData.journalXLSData.coinBase + Mathf.RoundToInt(gameplayData.journalXLSData.coinModifier * starLv);
        if (finalCoinLv > 0)
        {
            tempTag = new List<string>();
            tempTag.Add("coin");
            //optionalTag.Add("coin");
            tempJournalReward = PFEntryDeck.DrawCardswConditions(dDataHub.hub.dJournalReward.GetPFEntrywGateList(),
                1, tempTag,
                new intRange(finalCoinLv, finalCoinLv))[0];
            if(coinRewards.Count == 1)
            {
                coinRewards.AddRange(InterprateReward(tempJournalReward, coinRewards[0]));
            }
            else
            {
                coinRewards = InterprateReward(tempJournalReward);
            }
        }
        gameplayData.constantRewards.AddRange(coinRewards);

        //constant trait
        int finalTraitLv = gameplayData.journalXLSData.traitBase + Mathf.RoundToInt(gameplayData.journalXLSData.traitModifier * starLv);
        if (finalTraitLv > 0)
        {
            tempTag = new List<string>();
            tempTag.Add("trait");
            //optionalTag.Add("trait");
            tempJournalReward = PFEntryDeck.DrawCardswConditions(dDataHub.hub.dJournalReward.GetPFEntrywGateList(),
                1, tempTag,
                new intRange(finalTraitLv, finalTraitLv))[0];
            gameplayData.constantRewards.AddRange(InterprateReward(tempJournalReward));
        }
        //gameplayData.constantRewards.Sort((a, b) => Random.value.CompareTo(Random.value));

    }
    void GenerateOptionalReward(int starLv)
    {
        List<string> optionalJournalReward = new List<string>();
        int finalOptionalLv = gameplayData.journalXLSData.optionalBase + Mathf.RoundToInt(gameplayData.journalXLSData.optionalModifier * starLv);
        if (finalOptionalLv > 0)
        {
            optionalJournalReward = PFEntryDeck.DrawCardswConditions(dDataHub.hub.dJournalReward.GetPFEntrywGateList(),
                Mathf.Min(dConstants.JournalConst.MAX_OPTIONAL_REWARD, gameplayData.journalXLSData.rewardOptions.Count),
                gameplayData.journalXLSData.rewardOptions,
                new intRange(finalOptionalLv, finalOptionalLv));
            foreach (string jRewardUID in optionalJournalReward)
            {
                gameplayData.optionalRewards.AddRange(InterprateReward(jRewardUID));
            }
        }
        gameplayData.optionalRewards.Sort((a, b) => Random.value.CompareTo(Random.value));
    }
    void GenerateBuffReward()
    {
        //buffed reward
        TotalBuffValueSet bvs = ActiveBuffs.controller.RetrieveValueSetByAction("journal_submission_coin");
        int coinValue = ActiveBuffs.ModifyByBVS(0, bvs);
        if (coinValue > 0)
        {
            RewardItem additionalCoin = new RewardItem();
            additionalCoin.type = RewardItem.RewardType.currency;
            additionalCoin.rewardUID = "currency_coin";
            additionalCoin.amount = coinValue;
            gameplayData.constantRewards.Add(additionalCoin);
        }

        bvs = ActiveBuffs.controller.RetrieveValueSetByAction("journal_submission_fame");
        int fameValue = ActiveBuffs.ModifyByBVS(0, bvs);
        if (fameValue > 0)
        {
            RewardItem additionalFame = new RewardItem();
            additionalFame.type = RewardItem.RewardType.currency;
            additionalFame.rewardUID = "currency_fame";
            additionalFame.amount = fameValue;
            gameplayData.constantRewards.Add(additionalFame);
        }
        
    }
    void UpdateJournalCalculation()
    {
        //CalculateComboBonus();
        //CalculateQuestBonus();
        //CalculateTagBonus();
        UpdateTokenStars();
        rulesetHub.CheckRuleset();
        UpdateRewardPreviews();
        eventJournalUpdated.Raise();
    }

    void UpdateTokenStars()
    {
        for (int i = 0; i < gameplayData.tokens.Count; i++)
        {
            if (gameplayData.tokens[i] != "")
            {
                Collectible collectible = dDataHub.hub.dCollectible.GetFromUID(gameplayData.tokens[i]);
                gameplayData.tokenStar[i] = i < gameplayData.curComboTokenCount ? collectible.starValue : 0;
            }
            else
            {
                gameplayData.tokenStar[i] = 0;
            }
        }
    }
    void UpdateRewardPreviews()
    {
        List<string> tempTag = new List<string>();
        string tempJournalReward = "";

        gameplayData.curRewardPreviews.Clear();
        //order matters below
        //constant star
        int finalFameLv = gameplayData.journalXLSData.fameBase + Mathf.RoundToInt(gameplayData.journalXLSData.fameModifier * gameplayData.totalStars);
        TotalBuffValueSet bvs = ActiveBuffs.controller.RetrieveValueSetByAction("journal_currency_fame");
        finalFameLv = ActiveBuffs.ModifyByBVS(finalFameLv, bvs);
        if (finalFameLv > 0)
        {
            tempTag = new List<string>();
            tempTag.Add("star");
            //optionalTag.Add("fame");
            tempJournalReward = PFEntryDeck.DrawCardswConditions(dDataHub.hub.dJournalReward.GetPFEntrywGateList(),
                1, tempTag,
                new intRange(finalFameLv, finalFameLv))[0];
            gameplayData.curRewardPreviews.Add(tempJournalReward);
        }
        
        //constant time add (modified from xp)
        int finalTimeLv = gameplayData.journalXLSData.xpBase + Mathf.RoundToInt(gameplayData.journalXLSData.xpModifier * gameplayData.totalStars);
        if (finalTimeLv > 0)
        {
            tempTag = new List<string>();
            tempTag.Add("time");
            tempJournalReward = PFEntryDeck.DrawCardswConditions(dDataHub.hub.dJournalReward.GetPFEntrywGateList(),
                1, tempTag,
                new intRange(finalTimeLv, finalTimeLv))[0];
            gameplayData.curRewardPreviews.Add(tempJournalReward);
        }
        //constant trait
        int finalTraitLv = gameplayData.journalXLSData.traitBase + Mathf.RoundToInt(gameplayData.journalXLSData.traitModifier * gameplayData.totalStars);
        if (finalTraitLv > 0)
        {
            tempTag = new List<string>();
            tempTag.Add("trait");
            //optionalTag.Add("trait");
            tempJournalReward = PFEntryDeck.DrawCardswConditions(dDataHub.hub.dJournalReward.GetPFEntrywGateList(),
                1, tempTag,
                new intRange(finalTraitLv, finalTraitLv))[0];
            gameplayData.curRewardPreviews.Add(tempJournalReward);
        }
        //constant equip
        int finalEquipLv = gameplayData.journalXLSData.equipmentBase + Mathf.RoundToInt(gameplayData.journalXLSData.equipmentModifier * gameplayData.totalStars);
        if (finalEquipLv > 0)
        {
            tempTag = new List<string>();
            tempTag.Add("equipment");
            //optionalTag.Add("equipment");
            tempJournalReward = PFEntryDeck.DrawCardswConditions(dDataHub.hub.dJournalReward.GetPFEntrywGateList(),
                1, tempTag,
                new intRange(finalEquipLv, finalEquipLv))[0];
            gameplayData.curRewardPreviews.Add(tempJournalReward);
        }
        //new constant coin from slot
        int finalSlotCount = Mathf.RoundToInt(Mathf.Min(gameplayData.CurTokenCount, gameplayData.curComboTokenCount) * gameplayData.journalXLSData.coinModifier);
        if (finalSlotCount > 0)
        {
            tempTag = new List<string>();
            tempTag.Add("slot_coin");
            //optionalTag.Add("coin");
            tempJournalReward = PFEntryDeck.DrawCardswConditions(dDataHub.hub.dJournalReward.GetPFEntrywGateList(),
                1, tempTag,
                new intRange(finalSlotCount, finalSlotCount))[0];
            gameplayData.curRewardPreviews.Add(tempJournalReward);
        }
        //old constant coin from star
        int finalCoinLv = gameplayData.journalXLSData.coinBase + Mathf.RoundToInt(gameplayData.journalXLSData.coinModifier * gameplayData.totalStars);
        if (finalCoinLv > 0)
        {
            tempTag = new List<string>();
            tempTag.Add("coin");
            tempJournalReward = PFEntryDeck.DrawCardswConditions(dDataHub.hub.dJournalReward.GetPFEntrywGateList(),
                1, tempTag,
                new intRange(finalCoinLv, finalCoinLv))[0];
            gameplayData.curRewardPreviews.Add(tempJournalReward);
        }

        int finalOptionalLv = gameplayData.journalXLSData.optionalBase + Mathf.RoundToInt(gameplayData.journalXLSData.optionalModifier * gameplayData.totalStars);
        if (finalOptionalLv > 0)
        {
            gameplayData.curRewardPreviews.Add(dConstants.JournalConst.CASE_OPTIONAL_REWARD);
        }
    }
    /*
    void ClaimReward(RewardItem item)
    {
        switch (item.type)
        {
            case RewardItem.RewardType.currency:
                CurrencyManagement.wallet.AddCurrency(item.rewardUID, item.modifiedAmount);
                break;
            case RewardItem.RewardType.equipment:
                EquipmentManagement.equipment.AddEquipment(item.rewardUID);
                //Debug.Log(string.Format("old equipment acquisition from journal, needs to be revised"));
                break;
            default:
                break;
        }
    }*/
    void CheckBonusRewardByCollectible(string collectibleUID)
    {
        Collectible view = dDataHub.hub.dCollectible.GetFromUID(collectibleUID);
        int bonusFame = 0;
        if (view.keywords.Contains("natural"))
        {
            TotalBuffValueSet naturalXPBVS = ActiveBuffs.controller.RetrieveValueSetByAction("collectible_get_natural_instant_xp");
            int bonusXP = ActiveBuffs.ModifyByBVS(0, naturalXPBVS);
            if (bonusXP > 0)
            {
                Debug.Log(string.Format("xp earned by {0} from natural card {1}", bonusXP, collectibleUID));
                CurrencyManagement.wallet.AddCurrency("currency_xp", bonusXP);
            }

            TotalBuffValueSet naturalFameBVS = ActiveBuffs.controller.RetrieveValueSetByAction("collectible_get_natural_instant_fame");
            bonusFame += ActiveBuffs.ModifyByBVS(0, naturalFameBVS);
        }
        if (view.keywords.Contains("animal"))
        {
            TotalBuffValueSet animalFameBVS = ActiveBuffs.controller.RetrieveValueSetByAction("collectible_get_animal_instant_fame");
            bonusFame += ActiveBuffs.ModifyByBVS(0, animalFameBVS);
        }
        if (view.keywords.Contains("structure"))
        {
            TotalBuffValueSet structureTraitBVS = ActiveBuffs.controller.RetrieveValueSetByAction("collectible_get_structure_instant_trait");
            int bonusTrait = ActiveBuffs.ModifyByBVS(0, structureTraitBVS);
            if (bonusTrait > 0)
            {
                Debug.Log(string.Format("trait earned by {0} from structure card {1}", bonusTrait, collectibleUID));
                CurrencyManagement.wallet.AddCurrency("currency_trait", bonusTrait);
            }
            TotalBuffValueSet structureFameBVS = ActiveBuffs.controller.RetrieveValueSetByAction("collectible_get_structure_instant_fame");
            bonusFame += ActiveBuffs.ModifyByBVS(0, structureFameBVS);
        }
        if (bonusFame > 0)
        {
            Debug.Log(string.Format("fame earned by {0} from card {1}", bonusFame, collectibleUID));
            CurrencyManagement.wallet.AddCurrency("currency_fame", bonusFame);
        }
    }
    public StarBonusItem PreviewRule(string ruleUID)
    {
        return rulesetHub.PreviewRule(ruleUID);
    }
}
