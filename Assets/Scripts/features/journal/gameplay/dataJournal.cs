﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Journal/Gameplay/In-Session Data")]
public class dataJournal : ScriptableObject
{
    [Header("Basic")]
    public string journalUID;
    public string journalName;
    public string journalBgArt;
    public string journalTagArt;
    public JournalStyle journalXLSData;

    [Header("Gameplay")]
    public dConstants.JournalConst.JournalStatus gameplayStatus;
    public int curMaxTokenCount;
    public int curComboTokenCount;
    public bool sessionStartJournal;
    public bool oneParkJournal;
    public bool parkVisited;
    public bool forcedSubmission;

    [Header("Star Bonus")]
    public List<StarBonusItem> starBonusItems;
    public List<StarBonusItem> starBonusWIPs;
    public List<StarBonusItem> starBonusQuests;

    public int totalStars
    {
        get
        {
            int total = 0;
            for (int i = 0; i < starBonusItems.Count; i++)
            {
                total += starBonusItems[i].bonusStar;
            }
            //TotalBuffValueSet starBVS = ActiveBuffs.controller.RetrieveValueSetByAction("journal_total_star");
            //return ActiveBuffs.ModifyByBVS(total, starBVS);
            return Mathf.Min(total,dConstants.JournalConst.MAX_REWARD_TIER);
        }
    }

    [Header("Tokens")]
    public List<string> previousTokens;
    public List<string> tokens;
    public List<string> tokenViews
    {
        get
        {
            List<string> sources = new List<string>();
            for (int i = 0; i < tokens.Count; i++)
            {
                if(tokens[i] != "")
                {
                    sources.Add(dDataHub.hub.dCollectible.GetFromUID(tokens[i]).viewSource);
                }
            }
            return sources;
        }
    }
    public List<int> tokenStar;
    public int CurTokenCount
    {
        get
        {
            int result = 0;
            for (int i = 0; i < tokens.Count; i++)
            {
                if (tokens[i] != "")
                {
                    result += 1;
                }
            }
            return result;
        }
    }
    public bool journalIsSubmittable
    {
        get
        {
            return (totalStars > 0 && ((CurTokenCount >= curComboTokenCount) || (parkVisited && oneParkJournal))) || sessionStartJournal || forcedSubmission;
        }
    }

    [Header("Options")]
    public List<string> optionalJournals;

    [Header("Rewards")]
    public List<string> curRewardPreviews;
    public List<RewardItem> constantRewards;
    public bool RewardAllClaimed
    {
        get
        {
            bool result = true;
            for(int i = 0; i < constantRewards.Count; i++)
            {
                if (!constantRewards[i].claimed)
                {
                    result = false;
                    return result;
                }
            }
            return result;
        }
    }
    //public List<bool> constantRewardClaimed;
    public List<RewardItem> optionalRewards;

    public void Reset()
    {
        journalUID = "";
        journalName = "";
        journalBgArt = "";
        journalTagArt = "";
        journalXLSData = null;

        gameplayStatus = dConstants.JournalConst.JournalStatus.selection;
        curMaxTokenCount = 0;
        curComboTokenCount = 0;
        sessionStartJournal = false;
        oneParkJournal = false;
        parkVisited = false;
        forcedSubmission = false;

        previousTokens = new List<string>();
        tokens = new List<string>();
        tokenStar = new List<int>();

        optionalJournals = new List<string>();

        curRewardPreviews = new List<string>();
        constantRewards = new List<RewardItem>();
        //constantRewardClaimed = new List<bool>();
        optionalRewards = new List<RewardItem>();
    }
}
