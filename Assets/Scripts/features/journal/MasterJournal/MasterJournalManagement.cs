﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MasterJournalManagement : MonoBehaviour
{
    public static MasterJournalManagement controller;

    private static int MAX_HISTORY_COUNT = 9999;

    private void Awake()
    {
        if (controller == null)
        {
            controller = this;
        }
        else if (controller != null)
        {
            Destroy(gameObject);
        }
    }

    [SerializeField] dataHistoryLog historyData;

    public void LoadHistory()
    {
        historyData.Reset();
        for (int i = 0; i < MAX_HISTORY_COUNT; i++)
        {
            HistoryLogItem item = new HistoryLogItem();
            item.dateText = SaveManager.controller.Inquire(string.Format("history_date_{0}", i));
            item.log = SaveManager.controller.Inquire(string.Format("history_log_{0}", i));
            int.TryParse(SaveManager.controller.Inquire(string.Format("history_star_{0}", i)), out item.starValue);
            //all items loaded
            if(item.dateText == "" && item.log == "")
            {
                break;
            }
            else
            {
                historyData.history.Add(item);
            }
            //reach MAX
            if(i == MAX_HISTORY_COUNT - 1)
            {
                Debug.LogError("Load History have reached max history items the system supports");
            }
        }
    }
    public void SaveHistory()
    {
        for(int i=0;i< historyData.history.Count; i++)
        {
            SaveManager.controller.Insert(string.Format("history_date_{0}", i), historyData.history[i].dateText);
            SaveManager.controller.Insert(string.Format("history_log_{0}", i), historyData.history[i].log);
            SaveManager.controller.Insert(string.Format("history_star_{0}", i), historyData.history[i].starValue.ToString());
        }
    }
    public void AddHisotyrLog(HistoryLogItem item)
    {
        historyData.history.Add(item);

    }
    public int TotalJournalCount()
    {
        return historyData.history.Count;
    }
}
