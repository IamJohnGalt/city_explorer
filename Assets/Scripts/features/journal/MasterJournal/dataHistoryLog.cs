﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class HistoryLogItem
{
    public string dateText;
    public string log;
    public int starValue;

    public HistoryLogItem()
    {
        dateText = "";
        log = "";
        starValue = 0;
    }
}
[CreateAssetMenu(menuName = "meta Game/History")]
public class dataHistoryLog : ScriptableObject
{
    public List<HistoryLogItem> history;
    public void Reset()
    {
        history = new List<HistoryLogItem>();
    }
}
