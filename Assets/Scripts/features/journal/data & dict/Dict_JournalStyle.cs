﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Journal/Dict/Style")]
public class Dict_JournalStyle : GenericDataDict<JournalStyle>
{
    public override void DebugCount()
    {
        Debug.Log("Total of " + UIDList.Count + " jounral styles are added to Journal Style Dictionary");
    }

    public List<PFEntrywGate> GetPFEntrywGateList()
    {
        List<PFEntrywGate> result = new List<PFEntrywGate>();
        List<JournalStyle> fullList = GetFullList();
        foreach (JournalStyle item in fullList)
        {
            result.Add(item);
        }
        return result;
    }
}
