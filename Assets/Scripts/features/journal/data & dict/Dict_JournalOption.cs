﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Journal/Dict/Option")]
public class Dict_JournalOption : GenericDataDict<JournalOption>
{
    public override void DebugCount()
    {
        Debug.Log("Total of " + UIDList.Count + " jounral options are added to Journal Option Dictionary");
    }

    public List<PFEntrywGate> GetPFEntrywGateList()
    {
        List<PFEntrywGate> result = new List<PFEntrywGate>();
        List<JournalOption> fullList = GetFullList();
        foreach (JournalOption item in fullList)
        {
            result.Add(item);
        }
        return result;
    }
}