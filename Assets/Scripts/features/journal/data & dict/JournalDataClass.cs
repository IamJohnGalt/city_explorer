﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class JournalOption : PFEntrywGate
{
    public CardArt artAsset;
    public intRange slotRange;
    public List<string> eligibleFilters;
    public string minCoin;
    public JournalOption() : base()
    {
        artAsset = new CardArt();
        slotRange = new intRange(0, int.MaxValue);
        eligibleFilters = new List<string>();
        minCoin = "0";
    }
}
[System.Serializable]
public class JournalStyle : PFEntrywGate
{
    public string contentGroup;
    public string contentName;
    public string styleGroup;
    public string styleName;
    public int maxTokenCount;
    public int comboTokenCount;
    public int modifiedMaxTokenCount
    {
        get
        {
            TotalBuffValueSet bvs = ActiveBuffs.controller.RetrieveValueSetByAction("journal_collectible_slot");
            return ActiveBuffs.ModifyByBVS(maxTokenCount, bvs);
        }
    }
    public int modifiedComboTokenCount
    {
        get
        {
            TotalBuffValueSet bvs = ActiveBuffs.controller.RetrieveValueSetByAction("journal_collectible_slot");
            return ActiveBuffs.ModifyByBVS(comboTokenCount, bvs);
        }
    }
    public int specialTokenCount
    {
        get
        {
            return maxTokenCount - comboTokenCount;
        }
    }
    //public List<string> bonus_tag_group;
    public List<string> additionalRuleset;
    public int additionalCoin;
    public int coinBase;
    public float coinModifier;
    public int equipmentBase;
    public float equipmentModifier;
    public int traitBase;
    public float traitModifier;
    public int fameBase;
    public float fameModifier;
    public int xpBase;
    public float xpModifier;
    public List<string> rewardOptions;
    public int optionalBase;
    public float optionalModifier;
    public CardArt artAsset;

    public JournalStyle() : base()
    {
        artAsset = new CardArt();
        entryType = "Journal Style";

        contentGroup = "";
        contentName = "";
        styleGroup = "";
        styleName = "";
        maxTokenCount = 0;
        comboTokenCount = 0;
        //bonus_tag_group = new List<string>();
        additionalRuleset = new List<string>();
        additionalCoin = 0;
        coinBase = 0;
        coinModifier = 0f;
        equipmentBase = 0;
        equipmentModifier = 0f;
        traitBase = 0;
        traitModifier = 0f;
        fameBase = 0;
        fameModifier = 0f;
        xpBase = 0;
        xpModifier = 0f;
        rewardOptions = new List<string>();
        optionalBase = 0;
        optionalModifier = 0f;
    }
}

[System.Serializable]
public class JournalReward : PFEntrywGate
{
    public int rewardLv;
    public string rewardType;
    public string rewardUID;
    public int minValue;
    public int maxValue;
    //public CardArt artAsset;

    public JournalReward() : base()
    {
        //artAsset = new CardArt();

        rewardLv = 0;
        rewardType = "";
        rewardUID = "";
        minValue = 0;
        maxValue = 0;
    }
}
[System.Serializable]
public class JournalStarBonus : PFEntrywGate
{
    public string keywordGroup;
    public int reqCount;
    public string formula;
    public float bonusValue;

    public JournalStarBonus() : base()
    {
        keywordGroup = "";
        reqCount = 0;
        formula = "";
        bonusValue = 0;

    }
}
[System.Serializable] 
public class StarBonusItem
{
    public enum BonusItemStatus { hidden, preview, activated, wip};
    public BonusItemStatus status;
    public string title;
    public int bonusStar;
    public string prefix;
    public string midfix;
    public string suffix;
    public List<string> keywords;
    public List<string> addition_keywords;

    public StarBonusItem()
    {
        status = BonusItemStatus.hidden;
        title = "";
        bonusStar = 0;
        prefix = "";
        midfix = "";
        suffix = "";
        keywords = new List<string>();
        addition_keywords = new List<string>();
    }
}
[System.Serializable]
public class QuestBonusItem
{
    public bool activated
    {
        get
        {
            bool result = true;
            foreach(bool s in switches){
                result = result && s;
            }
            return result;
        }
    }
    public List<bool> switches;
    public int bonusStar;

    public QuestBonusItem()
    {
        switches = new List<bool>();
        bonusStar = 0;
    }
}
[System.Serializable]
public class RewardItem
{
    public enum RewardType { currency, equipment, buff, time };
    public RewardType type;
    public string rewardUID;
    public int amount;
    public string source;
    public bool claimed;
    public int modifiedAmount
    {
        get
        {
            int result = amount;
            return result;
        }
    }

    public RewardItem()
    {
        type = RewardType.currency;
        rewardUID = "";
        amount = 0;
        source = "";
        claimed = false;
    }
}
public class RewardPreview
{

}