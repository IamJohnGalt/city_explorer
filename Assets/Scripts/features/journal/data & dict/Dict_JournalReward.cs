﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Journal/Dict/Reward")]
public class Dict_JournalReward : GenericDataDict<JournalReward>
{
    public override void DebugCount()
    {
        Debug.Log("Total of " + UIDList.Count + " jounral reward items are added to Journal Reward Dictionary");
    }
    public List<PFEntrywGate> GetPFEntrywGateList()
    {
        List<PFEntrywGate> result = new List<PFEntrywGate>();
        List<JournalReward> fullList = GetFullList();
        foreach (JournalReward item in fullList)
        {
            result.Add(item);
        }
        return result;
    }
}
