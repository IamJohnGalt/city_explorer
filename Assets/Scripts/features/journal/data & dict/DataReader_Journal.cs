﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataReader_Journal : GenericDataReader
{
    //public Dict_JournalBonus dJournalBonus;
    public Dict_JournalOption dJournalOption;
    public Dict_JournalStyle dJournalStyle;
    public Dict_JournalReward dJournalReward;
    //public Dict_JournalReward dJournalBaseReward;
    //public Dict_JournalReward dJournalOptionalReward;
    //public Dict_JournalReward dJournalAllReward;

    public override void LoadFromXLS()
    {
        dJournalOption.Reset();
        dJournalStyle.Reset();
        dJournalReward.Reset();
        LoadOptionData("Journal - option_exporter");
        LoadStyleData("Journal - style_exporter");
        LoadRewardData("Journal - reward_exporter");
        FinishLoading();
    }
    void LoadOptionData(string filename)
    {
        string[,] string_data = CSVReader.SplitCsvGrid(Resources.Load<TextAsset>("XLSData/" + filename).text);
        if (string_data[0, 0] != "UID")
        {
            Debug.LogError("Invalid File (No UID found): " + filename);
            return;
        }
        if (dJournalStyle == null)
        {
            Debug.Log("Null Ref on Data Dictionary");
        }
        for (int iRow = 1; iRow < string_data.GetUpperBound(1); iRow++)
        {
            JournalOption option = new JournalOption();
            option.UID = string_data[0, iRow];
            option.title = string_data[1, iRow];
            option.desc = string_data[2, iRow];

            option.release = string_data[3, iRow];
            option.tags = SplitString(string_data[4, iRow], ',');
            int.TryParse(string_data[5, iRow], out option.tier);
            float.TryParse(string_data[6, iRow], out option.weight);

            option.artAsset.objectArt = string_data[7, iRow];
            intRange range = new intRange();
            int.TryParse(string_data[8, iRow], out range.minInt);
            int.TryParse(string_data[9, iRow], out range.maxInt);
            option.slotRange = range;
            option.eligibleFilters = SplitString(string_data[10, iRow], ',');
            option.minCoin = string_data[11, iRow];

            dJournalOption.Add(option.UID, option);
        }
        dJournalOption.DebugCount();
    }
    void LoadStyleData(string filename)
    {
        string[,] string_data = CSVReader.SplitCsvGrid(Resources.Load<TextAsset>("XLSData/" + filename).text);
        if (string_data[0, 0] != "UID")
        {
            Debug.LogError("Invalid File (No UID found): " + filename);
            return;
        }
        if (dJournalStyle == null)
        {
            Debug.Log("Null Ref on Data Dictionary");
        }
        for (int iRow = 1; iRow < string_data.GetUpperBound(1); iRow++)
        {
            JournalStyle style = new JournalStyle();
            style.UID = string_data[0, iRow];
            style.title = string_data[1, iRow];
            style.desc = string_data[2, iRow];
            style.contentGroup = string_data[3, iRow];
            style.contentName = string_data[4, iRow];
            style.styleGroup = string_data[5, iRow];
            style.styleName = string_data[6, iRow];

            style.release = string_data[7, iRow];
            style.tags = SplitString(string_data[8, iRow], ',');
            int.TryParse(string_data[9, iRow], out style.tier);
            float.TryParse(string_data[10, iRow], out style.weight);

            //style.artAsset.objectArt = string_data[28, iRow];
            style.artAsset.backgroundArt = string_data[11, iRow];

            int.TryParse(string_data[12, iRow], out style.maxTokenCount);
            int.TryParse(string_data[13, iRow], out style.comboTokenCount);
            //style.bonus_tag_group = SplitString(string_data[13, iRow], ',');
            style.additionalRuleset = SplitString(string_data[14, iRow], ',');
            int.TryParse(string_data[15, iRow], out style.additionalCoin);
            int.TryParse(string_data[16, iRow], out style.coinBase);
            float.TryParse(string_data[17, iRow], out style.coinModifier);
            int.TryParse(string_data[18, iRow], out style.equipmentBase);
            float.TryParse(string_data[19, iRow], out style.equipmentModifier);
            int.TryParse(string_data[20, iRow], out style.traitBase);
            float.TryParse(string_data[21, iRow], out style.traitModifier);
            int.TryParse(string_data[22, iRow], out style.fameBase);
            float.TryParse(string_data[23, iRow], out style.fameModifier);
            int.TryParse(string_data[24, iRow], out style.xpBase);
            float.TryParse(string_data[25, iRow], out style.xpModifier);
            style.rewardOptions = SplitString(string_data[26, iRow], ',');
            int.TryParse(string_data[27, iRow], out style.optionalBase);
            float.TryParse(string_data[28, iRow], out style.optionalModifier);
            
            dJournalStyle.Add(style.UID, style);
        }
        dJournalStyle.DebugCount();
    }
    void LoadRewardData(string filename)
    {
        string[,] string_data = CSVReader.SplitCsvGrid(Resources.Load<TextAsset>("XLSData/" + filename).text);
        if (string_data[0, 0] != "UID")
        {
            Debug.LogError("Invalid File (No UID found): " + filename);
            return;
        }
        if (dJournalReward == null)
        {
            Debug.Log("Null Ref on Data Dictionary");
        }
        for (int iRow = 1; iRow < string_data.GetUpperBound(1); iRow++)
        {
            JournalReward reward = new JournalReward();
            reward.UID = string_data[0, iRow];
            reward.title = string_data[1, iRow];
            reward.desc = string_data[2, iRow];
            reward.release = string_data[3, iRow];
            reward.tags = SplitString(string_data[4, iRow], ',');
            int.TryParse(string_data[5, iRow], out reward.tier);
            float.TryParse(string_data[6, iRow], out reward.weight);

            reward.rewardType = string_data[7, iRow];
            reward.rewardUID = string_data[8, iRow];
            int.TryParse(string_data[9, iRow], out reward.minValue);
            int.TryParse(string_data[10, iRow], out reward.maxValue);

            dJournalReward.Add(reward.UID, reward);
            //dJournalAllReward.Add(reward.UID, reward);
        }
        dJournalReward.DebugCount();


    }
    /*void LoadBaseRewardData(string filename)
    {
        string[,] string_data = CSVReader.SplitCsvGrid(Resources.Load<TextAsset>("XLSData/" + filename).text);
        if (string_data[0, 0] != "UID")
        {
            Debug.LogError("Invalid File (No UID found): " + filename);
            return;
        }
        if (dJournalBaseReward == null)
        {
            Debug.Log("Null Ref on Data Dictionary");
        }
        for (int iRow = 1; iRow < string_data.GetUpperBound(1); iRow++)
        {
            JournalReward reward = new JournalReward();
            reward.UID = string_data[0, iRow];

            reward.release = string_data[1, iRow];
            reward.tags = SplitString(string_data[2, iRow], ',');
            int.TryParse(string_data[3, iRow], out reward.tier);
            float.TryParse(string_data[4, iRow], out reward.weight);

            reward.rewardType = string_data[5, iRow];
            reward.rewardUID = string_data[6, iRow];
            reward.minValue = string_data[7, iRow];
            reward.maxValue = string_data[8, iRow];

            dJournalBaseReward.Add(reward.UID, reward);
            dJournalAllReward.Add(reward.UID, reward);
        }
        dJournalBaseReward.DebugCount();


    }
    void LoadOptionalRewardData(string filename)
    {
        string[,] string_data = CSVReader.SplitCsvGrid(Resources.Load<TextAsset>("XLSData/" + filename).text);
        if (string_data[0, 0] != "UID")
        {
            Debug.LogError("Invalid File (No UID found): " + filename);
            return;
        }
        if (dJournalOptionalReward == null)
        {
            Debug.Log("Null Ref on Data Dictionary");
        }
        for (int iRow = 1; iRow < string_data.GetUpperBound(1); iRow++)
        {
            JournalReward reward = new JournalReward();
            reward.UID = string_data[0, iRow];

            reward.release = string_data[1, iRow];
            reward.tags = SplitString(string_data[2, iRow], ',');
            int.TryParse(string_data[3, iRow], out reward.tier);
            float.TryParse(string_data[4, iRow], out reward.weight);

            reward.rewardType = string_data[5, iRow];
            reward.rewardUID = string_data[6, iRow];
            reward.minValue = string_data[7, iRow];
            reward.maxValue = string_data[8, iRow];

            dJournalOptionalReward.Add(reward.UID, reward);
            dJournalAllReward.Add(reward.UID, reward);
        }
        dJournalOptionalReward.DebugCount();


    }
    void LoadBonusData(string filename)
    {
        string[,] string_data = CSVReader.SplitCsvGrid(Resources.Load<TextAsset>("XLSData/" + filename).text);
        if (string_data[0, 0] != "UID")
        {
            Debug.LogError("Invalid File (No UID found): " + filename);
            return;
        }
        if (dJournalBonus == null)
        {
            Debug.Log("Null Ref on Data Dictionary");
        }
        for (int iRow = 1; iRow < string_data.GetUpperBound(1); iRow++)
        {
            JournalStarBonus bonus = new JournalStarBonus();
            bonus.UID = string_data[0, iRow];

            bonus.release = string_data[1, iRow];
            bonus.tags = SplitString(string_data[2, iRow], ',');
            int.TryParse(string_data[3, iRow], out bonus.tier);
            float.TryParse(string_data[4, iRow], out bonus.weight);
            bonus.desc = string_data[5, iRow];
            bonus.keywordGroup = string_data[6, iRow];
            int.TryParse(string_data[7, iRow], out bonus.reqCount);
            bonus.formula = string_data[8, iRow];
            float.TryParse(string_data[9, iRow], out bonus.bonusValue);

            dJournalBonus.Add(bonus.UID, bonus);
        }
        dJournalBonus.DebugCount();
    }
    */
}
