﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Journal/Dict/Bonus")]
public class Dict_JournalBonus : GenericDataDict<JournalStarBonus>
{
    public override void DebugCount()
    {
        Debug.Log("Total of " + UIDList.Count + " star bonus items are added to Journal Bonus Dictionary");
    }
}
