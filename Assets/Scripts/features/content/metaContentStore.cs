﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class metaContentStore : MonoBehaviour
{
    public static metaContentStore controller;
    private void Awake()
    {
        if (controller == null)
        {
            controller = this;
        }
        else if (controller != null)
        {
            Destroy(gameObject);
        }
        //DontDestroyOnLoad(controller);
    }

    public dataContentStore data;
    public GameEvent ContentStoreReady;
     
    public void ContentStoreInit()
    {
        data.Reset();
        LoadStoreData();
        //ContentStoreReady.Raise();
    }
    public void LoadStoreData()
    {
        string savestring = SaveManager.controller.Inquire(string.Format("contentstore_totalXP"));
        if (savestring != "")
        {
            int.TryParse(savestring, out data.XPEarned);
        }
        //for playtest
        Debug.Log("for playtest, force set all contents to be unlocked.");
        data.XPEarned = 50000000;
        ApplyAllUnlockedItems();
        //debug use only
        //EarnK(20);
        //SaveStoreData();
    }
    public void ApplyAllUnlockedItems()
    {
        List<ContentItem> fullItemList = dDataHub.hub.dContentItem.GetFullList();
        for(int i=0;i< fullItemList.Count; i++)
        {
            if(fullItemList[i].xp_req <= data.XPEarned)
            {
                if(fullItemList[i].unlockGate != "")
                {
                    ActiveBuffs.controller.ToggleFeatureGate(fullItemList[i].unlockGate, true);
                }
                data.unlockedItems.Add(fullItemList[i].UID);
            }
        }
    }
    public void SaveStoreData()
    {
        SaveManager.controller.Insert(string.Format("contentstore_totalXP"), data.XPEarned.ToString());
    }
    public void SetStoreMax()
    {
        SaveManager.controller.Insert(string.Format("contentstore_totalXP"), int.MaxValue.ToString());
    }
    public void EarnXP(int value)
    {
        data.XPEarned += value;
        //SaveStoreData();
    }
    public int CheckK()
    {
        return data.XPEarned;
    }
}
