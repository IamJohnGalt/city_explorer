﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Content/Dict/Content")]
public class Dict_ContentItem : GenericDataDict<ContentItem>
{
    public override void DebugCount()
    {
        Debug.Log("Total of " + UIDList.Count + " content unlock item are added to Content Item Dictionary");
    }
}
