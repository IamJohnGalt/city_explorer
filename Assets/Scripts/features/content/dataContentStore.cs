﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "meta Game/Content Store")]
public class dataContentStore : ScriptableObject
{
    public int XPEarned;
    public List<string> unlockedItems;

    public void Reset()
    {
        XPEarned = 0;
        unlockedItems = new List<string>();
    }
    public int GetNextItemXPReq()
    {
        List<ContentItem> AllItems = dDataHub.hub.dContentItem.GetFullList();
        for (int i = 0; i < AllItems.Count; i++)
        {
            if (AllItems[i].xp_req > XPEarned)
            {
                return AllItems[i].xp_req;
            }
        }
        return 0;
    }
    public int GetLastItemXPReq()
    {
        List<ContentItem> AllItems = dDataHub.hub.dContentItem.GetFullList();
        for (int i = 0; i < AllItems.Count; i++)
        {
            if (AllItems[i].xp_req > XPEarned)
            {
                if(i > 0)
                {
                    return AllItems[i-1].xp_req;
                }
                else
                {
                    return 0;
                }
            }
        }
        return 0;
    }
}
