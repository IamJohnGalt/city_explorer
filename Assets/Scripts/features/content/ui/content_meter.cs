﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class content_meter : MonoBehaviour
{
    [Header("Children Objects")]
    [SerializeField] ui_icon art;
    [SerializeField] ui_text title;
    [SerializeField] ui_icon_text xp_req;

    public void SetMeterByUID(string UID)
    {
        ContentItem item = dDataHub.hub.dContentItem.GetFromUID(UID);
        art.SetIcon(item.artAsset.objectArt);
        title.SetText(item.title);
        xp_req.majorStr.SetText(item.xp_req.ToString());
    }
}
