﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContentItem : PFEntry
{
    public int itemIndex;
    public int xp_req;
    public string unlockGate;
    public CardArt artAsset;

    public ContentItem()
    {
        itemIndex = -1;
        xp_req = -1;
        unlockGate = "";
        artAsset = new CardArt();
    }
}
