﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataReader_Content : GenericDataReader
{
    public Dict_ContentItem dContentItem;
    public override void LoadFromXLS()
    {
        dContentItem.Reset();

        LoadContentItemData("Content Unlock - content_exporter");

        FinishLoading();
    }
    public void LoadContentItemData(string filename)
    {
        string[,] string_data = CSVReader.SplitCsvGrid(Resources.Load<TextAsset>("XLSData/" + filename).text);
        if (string_data[0, 0] != "UID")
        {
            Debug.LogError("Invalid File (No UID found): " + filename);
            return;
        }
        if (dContentItem == null)
        {
            Debug.Log("Null Ref on Data Dictionary");
        }
        for (int iRow = 1; iRow < string_data.GetUpperBound(1); iRow++)
        {
            ContentItem item = new ContentItem();

            item.UID = string_data[0, iRow];
            item.title = string_data[1, iRow];
            item.desc = string_data[2, iRow];
            int.TryParse(string_data[3, iRow], out item.itemIndex);
            int.TryParse(string_data[4, iRow], out item.xp_req);
            item.unlockGate = string_data[5, iRow];
            item.artAsset.objectArt = string_data[6, iRow];

            dContentItem.Add(item.UID, item);
        }
        dContentItem.DebugCount();
    }
}
