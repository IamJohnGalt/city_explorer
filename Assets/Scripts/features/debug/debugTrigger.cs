﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class debugTrigger : MonoBehaviour
{
    public bool activate;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (activate)
        {
            executeFunction();
            activate = false;
        }
    }
    public virtual void executeFunction()
    {

    }
}
