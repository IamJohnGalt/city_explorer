﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class debugBuff : debugTrigger
{
    public string buffUID;
    public float buffValue;


    public override void executeFunction()
    {
        ActiveBuffs.controller.ActivateBuff("debug_tool", "debug_tool", buffUID, buffValue);
        buffUID = "";
        buffValue = 0;
    }
}
