﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InChronicleDebugSet : MonoBehaviour
{
    public void DEBUG_CoinAdd100()
    {
        CurrencyManagement.wallet.AddCurrency_Setup("currency_coin", 100);
    }
    public void DEBUG_RandomTrait()
    {
        TraitManagement.controller.AddRandomTraitByTier();
    }
    public void DEBUG_RandomEquipment()
    {
        EquipmentManagement.equipment.ImproveRandomEquipment();
    }
    public void DEBUG_RandomCollectible()
    {
        int rng = Random.Range(0, dDataHub.hub.dCollectible.UIDList.Count);
        string uid = dDataHub.hub.dCollectible.UIDList[rng];
        gJournal.controller.AddToken(uid);
        //colSubmission.collection.AddCard(uid);
        //Debug.Log(string.Format("A randomized card (UID: {0}) attempts to be added to Submission", uid));
    }
    public void DEBUG_TimeElapse48()
    {
        CalendarManagement.calendar.TimeElapse(48);
    }
}
