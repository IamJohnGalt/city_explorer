﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class debugAddTrait : debugTrigger
{
    public string traitUID;
    public override void executeFunction()
    {
        if (traitUID != "")
        {
            TraitManagement.controller.AddTrait(traitUID);
            traitUID = "";
        }
        else
        {
            TraitManagement.controller.AddRandomTraitByTier(1);
        }
    }
}
