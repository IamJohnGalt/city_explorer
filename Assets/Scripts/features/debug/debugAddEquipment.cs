﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class debugAddEquipment : debugTrigger
{
    public string equipmentUID;
    public override void executeFunction()
    {
        if(equipmentUID != "")
        {
            EquipmentManagement.equipment.AddEquipment(equipmentUID);
            equipmentUID = "";
        }
        else
        {
            EquipmentManagement.equipment.ImproveRandomEquipment();
        }
    }
}
