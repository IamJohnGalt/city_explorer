﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class debugGERaiser : debugTrigger
{
    public List<GameEvent> event2raise;

    public override void executeFunction()
    {
        foreach (GameEvent ge in event2raise)
        {
            ge.Raise();
        }
    }
}
