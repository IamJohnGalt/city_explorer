﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddRandomCard : debugTrigger
{
    public gJournal journalGameplay;
    public override void executeFunction()
    {
        int rng = Random.Range(0, dDataHub.hub.dCollectible.UIDList.Count);
        string uid = dDataHub.hub.dCollectible.UIDList[rng];
        journalGameplay.AddToken(uid);
        //colSubmission.collection.AddCard(uid);
        Debug.Log(string.Format("A randomized card (UID: {0}) attempts to be added to Submission", uid));
    }
}
