﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class debugForceSaveCharacters : debugTrigger
{
    [SerializeField] GameEvent SaveTrigger;
    public override void executeFunction()
    {
        SaveTrigger.Raise();
        SaveManager.controller.SaveToFile();
    }
}
