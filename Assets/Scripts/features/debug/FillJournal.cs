﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FillJournal : debugTrigger
{
    public gJournal journalGameplay;
    public override void executeFunction()
    {
        int rng;
        string uid;
        for(int i=0; i<9; i++)
        {
            rng = Random.Range(0, dDataHub.hub.dCollectible.UIDList.Count);
            uid = dDataHub.hub.dCollectible.UIDList[rng];
            journalGameplay.AddToken(uid);
        }
        //colSubmission.collection.AddCard(uid);
        Debug.Log(string.Format("Journal filled"));
    }
}
