﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class debugCurrencyAdd : debugTrigger
{
    public string currentcyUID;
    public int value;
    public override void executeFunction()
    {
        CurrencyManagement.wallet.AddCurrency(currentcyUID, value);
        currentcyUID = "";
        value = 0;
    }
}
