﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class debugLevelUp : debugTrigger
{
    public string levelUpUID;
    public override void executeFunction()
    {
        if (levelUpUID != "")
        {
            PlayerLevelManagement.controller.LevelUp(levelUpUID);
        }
        else
        {
            PlayerLevelManagement.controller.LevelUpWithRandomBonus();
        }
    }
}
