﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InParkDebugSet : MonoBehaviour
{
    [SerializeField] dataParkVisit parkVisitData;
    [SerializeField] gParkVisit parkVisitScript;

    public void DEBUG_ArriveNode()
    {
        parkVisitData.progressCur = parkVisitData.progressMax;
    }
    public void DEBUG_FinishPark()
    {
        parkVisitScript.FinishPark();
    }
    public void DEBUG_RefillStamina()
    {
        parkVisitData.staminaCur = parkVisitData.staminaMax;
    }
    public void DEBUG_DangerTokenFull()
    {
        parkVisitData.curDangerToken = dConstants.ParkConst.MAX_DANGER_TOKEN;
    }
    public void DEBUG_CollectibleTokenFull()
    {
        parkVisitData.curCollectibleToken = dConstants.ParkConst.MAX_COLLECTIBLE_TOKEN;
    }
}
