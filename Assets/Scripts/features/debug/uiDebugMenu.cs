﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class uiDebugMenu : MonoBehaviour
{
    public bool isPlayTest;
    public bool ready;

    [Header("Groups")]
    [SerializeField] GameObject page;
    [SerializeField] GameObject menu_group;
    [SerializeField] GameObject dialog_group;

    [Header("Children Objects")]
    [SerializeField] ui_text dialog_text;

    [Header("Event")]
    [SerializeField] GameEvent SaveTrigger;

    private void Awake()
    {
        page.SetActive(false);
        ready = false;
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.BackQuote))
        {
            if (!page.activeSelf)
            {
                ShowDebugMenu();
            }
            else
            {
                PageClose();
            }
        }
    }
    public void SetReady()
    {
        ready = true;
        //ShowPlaytestStartDialog();
    }
    public void PageClose()
    {
        page.SetActive(false);
    }
    public void ShowDebugMenu()
    {
        dialog_text.SetText(string.Format("欢迎来到测试菜单，所有的DEBUG功能都在这里"));
        page.SetActive(true);
        menu_group.SetActive(true);
        dialog_group.SetActive(true);
    }
    public void ShowPlaytestStartDialog()
    {
        if (isPlayTest)
        {
            dialog_text.SetText(string.Format("欢迎你参加《旅者》的游戏测试，<br>如果你卡在了游戏的任何一个阶段，请拍照/截屏发送至作者的微信/邮箱，这将对游戏的开发有着巨大的帮助，非常感谢！<br><br>接下来请尽情享受你的旅程..."));
            page.SetActive(true);
            menu_group.SetActive(false);
            dialog_group.SetActive(true);
        }
    }
    public void ShowPlaytestEndDialog()
    {
        if (isPlayTest)
        {
            dialog_text.SetText(string.Format("恭喜你完整地完成了一段旅程！（不是那么容易的哦~）<br>请将下面的总结页面拍照/截屏发送至作者的微信/邮箱，这将对游戏的开发有着巨大的帮助，非常感谢！"));
            page.SetActive(true);
            menu_group.SetActive(false);
            dialog_group.SetActive(true);
        }
    }

    public void DEBUG_ForceRestartGame()
    {
        EndSession.controller.RestartGame();
    }
    public void DEBUG_ForceSave()
    {
        SaveTrigger.Raise();
        SaveManager.controller.SaveToFile();
    }
    public void DEBUG_ResetSave()
    {
        SaveManager.controller.ResetCurSave();
        SaveManager.controller.SaveToFile();
        DEBUG_ForceRestartGame();
    }
    public void DEBUG_MaxCharacter()
    {
        CharacterManagement.controller.SetCharacterMAX();
        SaveManager.controller.SaveToFile();
        DEBUG_ForceRestartGame();
    }
    public void DEBUG_UnlockContent()
    {
        metaContentStore.controller.SetStoreMax();
        SaveManager.controller.SaveToFile();
        DEBUG_ForceRestartGame();
    }
}
