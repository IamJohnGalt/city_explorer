﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[CreateAssetMenu(menuName = "Dictionary/PlayerLevelDefinition")]
public class PlayerLevelDictionary : GenericDataDict<PlayerLevel>
{
    public override void DebugCount()
    {
        Debug.Log("Total of " + UIDList.Count + " player levels are added to current Player Level Item Dictionary");
    }
    public List<PFEntrywGate> GetPFEntrywGateList()
    {
        List<PFEntrywGate> result = new List<PFEntrywGate>();
        List<PlayerLevel> fullList = GetFullList();
        foreach (PlayerLevel item in fullList)
        {
            result.Add(item);
        }
        return result;
    }
}
