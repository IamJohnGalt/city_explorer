﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataReader_PlayerLevel : GenericDataReader
{
    public PlayerLevelDictionary plevelDict;
    public override void LoadFromXLS()
    {
        plevelDict.Reset();
        LoadPlayerLevelData("Progression - xp_exporter");
        FinishLoading();
    }
    void LoadPlayerLevelData(string filename)
    {
        string[,] string_data = CSVReader.SplitCsvGrid(Resources.Load<TextAsset>("XLSData/" + filename).text);
        if (string_data[0, 0] != "UID")
        {
            Debug.LogError("Invalid File (No UID found): " + filename);
            return;
        }
        if (plevelDict == null)
        {
            Debug.Log("Null Ref on Dictionary");
        }
        for (int iRow = 1; iRow < string_data.GetUpperBound(1); iRow++)
        {
            PlayerLevel plevel = new PlayerLevel();
            plevel.UID = string_data[0, iRow];
            plevel.title = string_data[1, iRow];
            plevel.desc = string_data[2, iRow];


            plevel.release = string_data[3, iRow];
            plevel.tags = SplitString(string_data[4, iRow], ',');
            int.TryParse(string_data[5, iRow], out plevel.tier);
            float.TryParse(string_data[6, iRow], out plevel.weight);

            plevel.art.objectArt = string_data[7, iRow];
            int.TryParse(string_data[8, iRow], out plevel.nextLvCost);
            if (string_data[9, iRow] != "")
            {
                plevel.buffUIDs.Add(string_data[9, iRow]);
                plevel.buffValues.Add(string_data[10, iRow]);
            }
            if(string_data[11, iRow] != "")
            {
                plevel.buffUIDs.Add(string_data[11, iRow]);
                plevel.buffValues.Add(string_data[12, iRow]);
            }
            if (string_data[13, iRow] != "")
            {
                plevel.buffUIDs.Add(string_data[13, iRow]);
                plevel.buffValues.Add(string_data[14, iRow]);
            }
            if (string_data[15, iRow] != "")
            {
                plevel.buffUIDs.Add(string_data[15, iRow]);
                plevel.buffValues.Add(string_data[16, iRow]);
            }
            //card.DebugPrint();
            plevelDict.Add(plevel.UID, plevel);
        }
        plevelDict.DebugCount();
    }
}
