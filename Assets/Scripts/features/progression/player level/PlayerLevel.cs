﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLevel : PFEntrywGate
{
    public CardArt art;
    public int nextLvCost;
    public List<string> buffUIDs;
    public List<string> buffValues;

    public PlayerLevel() : base()
    {
        entryType = "playerLevel";
        art.objectArt = "";
        art.backgroundArt = "";
        nextLvCost = 0;
        buffUIDs = new List<string>();
        buffValues = new List<string>();
    }
}
