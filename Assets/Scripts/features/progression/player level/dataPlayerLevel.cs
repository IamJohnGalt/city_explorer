﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Gameplay/Player Level")]
public class dataPlayerLevel : ScriptableObject
{
    [Header("Current Status")]
    public string acquiredLevelUp;
    public int currentLevel = 0;
    public int maxLevel = 0;
    public int nextLvCost;

    //[Header("Cost")]
    //[SerializeField] List<int> xpCostByLevels = new List<int>();
    public int GetNextLvXPCost { get { return nextLvCost; } }

    public void Reset()
    {
        acquiredLevelUp = "";
        currentLevel = 1;
        maxLevel = dConstants.Avatar.MAX_PLAYER_LEVEL;
        nextLvCost = int.MaxValue;
    }
}
