﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLevelManagement : MonoBehaviour
{
    public static PlayerLevelManagement controller;

    private void Awake()
    {
        if (controller == null)
        {
            controller = this;
        }
        else if (controller != null)
        {
            Destroy(gameObject);
        }
    }
    [Header("Data")]
    public dataPlayerLevel data;

    [Header("Game Event")]
    public GameEvent PlayerLevelChanged;

    public string GetCurrentStaminaBonus()
    {
        if(data.acquiredLevelUp != "")
        {
            PlayerLevel levelup = dDataHub.hub.plevelDict.GetFromUID(data.acquiredLevelUp);
            return string.Format("+{0}", levelup.buffValues[0]);
        }
        else
        {
            return string.Format("+0");
        }
        
        
    }
    public int GetCurLevel()
    {
        return data.currentLevel;
    }
    public void TestPlayerLevelInit()
    {
        ResetPlayerLevel();
        //AddRandomEquipmentByTier(1);
        //Debug.Log(string.Format("current displayed date: {0}{1}{2}", calendarData.YearToString, calendarData.MonthToString, calendarData.DateToString));
    }
    public bool NextLevelUp()
    {
        int value = CurrencyManagement.wallet.CheckCurrency("currency_xp");
        return value >= data.GetNextLvXPCost && !PlayerLevelIsMax();
    }
    public int NextLevelXPCost()
    {
        return data.GetNextLvXPCost;
    }
    public bool LevelUp(string levelUpUID)
    {
        CurrencyManagement.wallet.RemoveCurrency("currency_xp", data.GetNextLvXPCost);
        if (!PlayerLevelIsMax())
        {
            data.acquiredLevelUp = levelUpUID;
            data.currentLevel += 1;
            //add buff from trait
            PlayerLevel levelup = dDataHub.hub.plevelDict.GetFromUID(levelUpUID);
            ActiveBuffs.controller.DeactivateFrom("player_level");
            for (int i = 0; i < levelup.buffUIDs.Count; i++)
            {
                float value = 0;
                float.TryParse(levelup.buffValues[i], out value);
                if (levelup.buffUIDs[i] != "")
                {
                    ActiveBuffs.controller.ActivateBuff(levelup.UID, "player_level", levelup.buffUIDs[i], value);
                }
            }
            data.nextLvCost = levelup.nextLvCost;
            PlayerLevelChanged.Raise();
            return true;
        }
        else
        {
            return false;
        }
    }
    public bool LevelUpWithRandomBonus()
    {
        List<string> avaialbleTags = new List<string>();
        string levelUpUID = PFEntryDeck.DrawCardswConditions(
            dDataHub.hub.plevelDict.GetPFEntrywGateList(),
            1,
            avaialbleTags,
            new intRange(data.currentLevel + 1, data.currentLevel + 1)
            )[0];
        return LevelUp(levelUpUID);
    }
    public void ResetPlayerLevel()
    {
        data.Reset();
        List<string> avaialbleTags = new List<string>();
        string levelUpUID = PFEntryDeck.DrawCardswConditions(
            dDataHub.hub.plevelDict.GetPFEntrywGateList(),
            1,
            avaialbleTags,
            new intRange(data.currentLevel, data.currentLevel)
            )[0];
        PlayerLevel curLv = dDataHub.hub.plevelDict.GetFromUID(levelUpUID);
        data.nextLvCost = curLv.nextLvCost;
        PlayerLevelChanged.Raise();
    }
    public bool PlayerLevelIsMax()
    {
        return !(data.currentLevel < data.maxLevel);
    }
    public string GetLatestLevelUpUID()
    {
        return data.acquiredLevelUp;
    }
}
