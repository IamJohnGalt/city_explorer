﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[CreateAssetMenu(menuName = "Dictionary/TraitDefinition")]
public class TraitDictionary : GenericDataDict<Trait>
{
    public override void DebugCount()
    {
        Debug.Log("Total of " + UIDList.Count + " trait definitions are added to current Trait Dictionary");
    }
    public string GetRandomTraitByTier(int _tier)
    {
        List<string> availableTraits = new List<string>();
        availableTraits = (from trait in DataDict where trait.Value.tier == _tier select trait.Key).ToList();
        int rng = Random.Range(0, availableTraits.Count);
        return availableTraits[rng];
    }
    public List<PFEntrywGate> GetPFEntrywGateList()
    {
        List<PFEntrywGate> result = new List<PFEntrywGate>();
        List<Trait> fullList = GetFullList();
        foreach (Trait item in fullList)
        {
            result.Add(item);
        }
        return result;
    }
}
