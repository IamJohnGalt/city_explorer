﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataReader_Trait : GenericDataReader
{
    public TraitDictionary traitDict;
    public override void LoadFromXLS()
    {
        traitDict.Reset();
        LoadTraitData("Progression - trait_exporter");
        FinishLoading();
    }
    void LoadTraitData(string filename)
    {
        string[,] string_data = CSVReader.SplitCsvGrid(Resources.Load<TextAsset>("XLSData/" + filename).text);
        if (string_data[0, 0] != "UID")
        {
            Debug.LogError("Invalid File (No UID found): " + filename);
            return;
        }
        if (traitDict == null)
        {
            Debug.Log("Null Ref on Dictionary");
        }
        for (int iRow = 1; iRow < string_data.GetUpperBound(1); iRow++)
        {
            Trait trait = new Trait();
            trait.UID = string_data[0, iRow];
            trait.title = string_data[1, iRow];
            trait.desc = string_data[2, iRow];


            trait.release = string_data[3, iRow];
            trait.tags = SplitString(string_data[4, iRow], ',');
            int.TryParse(string_data[5, iRow], out trait.tier);
            float.TryParse(string_data[6, iRow], out trait.weight);

            trait.art.objectArt = string_data[7, iRow];
            trait.art.backgroundArt = string_data[8, iRow];

            trait.buffUIDs.Add(string_data[9, iRow]);
            trait.buffValues.Add(string_data[10, iRow]);
            if (string_data[11, iRow] != "")
            {
                trait.buffUIDs.Add(string_data[11, iRow]);
                trait.buffValues.Add(string_data[12, iRow]);
            }
            if (string_data[13, iRow] != "")
            {
                trait.buffUIDs.Add(string_data[13, iRow]);
                trait.buffValues.Add(string_data[14, iRow]);
            }
            if (string_data[15, iRow] != "")
            {
                trait.buffUIDs.Add(string_data[15, iRow]);
                trait.buffValues.Add(string_data[16, iRow]);
            }

            //card.DebugPrint();
            traitDict.Add(trait.UID, trait);
        }
        traitDict.DebugCount();
    }
}
