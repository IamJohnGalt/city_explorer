﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTrait : MonoBehaviour
{
    public dataPlayerTrait data;
    public GameEvent TraitChanged;

    public bool AddTrait(string uid)
    {
        if (!TraitIsFull())
        {
            data.activeTrait.Add(uid);
            //add buff from equipment
            Trait trait = dDataHub.hub.traitDict.GetFromUID(uid);
            for (int i = 0; i < trait.buffUIDs.Count; i++)
            {
                float value = 0;
                float.TryParse(trait.buffValues[i], out value);
                ActiveBuffs.controller.ActivateBuff(trait.UID, trait.UID, trait.buffUIDs[i], value);
            }
            TraitChanged.Raise();
            return true;
        }
        else
        {
            return false;
        }
    }
    public bool CheckTrait(string uid)
    {
        return data.activeTrait.Contains(uid);
    }
    public void ResetTrait()
    {
        data.activeTrait.Clear();
        data.traitCapacity = dConstants.Avatar.MAX_TRAIT_SLOT;
        TraitChanged.Raise();
    }
    public bool TraitIsFull()
    {
        return !(data.activeTrait.Count < data.traitCapacity);
    }
}
