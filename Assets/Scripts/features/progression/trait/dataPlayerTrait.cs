﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Gameplay/Player Trait")]
public class dataPlayerTrait : ScriptableObject
{
    [Header("Player")]
    public List<string> activeTrait;
    //public List<string> activeTraitRare;
    public string latestTraitUID;
    public int traitCapacity = 0;
    public List<string> bannedTraits;
    //public int acquiredTraitCount;

    [Header("Cost")]
    [SerializeField] List<int> thoughtCostByTraitCount;
    public int GetNextTraitCost
    {
        get
        {
            return thoughtCostByTraitCount[activeTrait.Count];
        }
    }

    [Header("Selection")]
    public List<string> traitOptions;
    public int traitOptionCount;
    public int selectedIndex;

    public void Reset()
    {
        activeTrait = new List<string>();
        //activeTraitRare = new List<string>();
        latestTraitUID = "";
        traitCapacity = dConstants.Avatar.MAX_TRAIT_SLOT;
        bannedTraits = new List<string>();

        //acquiredTraitCount = 0;

        traitOptions = new List<string>();
        traitOptionCount = dConstants.Avatar.MAX_TRAIT_SELECTION_OPTION;
        selectedIndex = -1;
    }
}
