﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trait : PFEntrywGate
{
    public CardArt art;
    public List<string> buffUIDs;
    public List<string> buffValues;

    public Trait() : base()
    {
        entryType = "trait";
        art.objectArt = "";
        art.backgroundArt = "";
        buffUIDs = new List<string>();
        buffValues = new List<string>();
    }
}
