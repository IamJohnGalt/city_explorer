﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TraitManagement : MonoBehaviour
{
    public static TraitManagement controller;
    void Awake()
    {
        if (controller == null)
        {
            controller = this;
        }
        else if (controller != null)
        {
            Destroy(gameObject);
        }
    }
    [Header("Data")]
    //public dataTraitSelection data;
    public dataPlayerTrait data;
    
    [Header("Game Event")]
    //public GameEvent TraitSelectionStart;
    public GameEvent TraitChanged;
    public void ResetTrait()
    {
        data.Reset();
        TraitChanged.Raise();
    }
    public void TraitInit()
    {
        ResetTrait();
        //AddRandomEquipmentByTier(1);
        //Debug.Log(string.Format("current displayed date: {0}{1}{2}", calendarData.YearToString, calendarData.MonthToString, calendarData.DateToString));
    }

    public void GenerateTraitOptions()
    {
        List<string> avaialbleTags = new List<string>();
        List<string> excludingTraits = new List<string>();
        excludingTraits.AddRange(data.bannedTraits);
        excludingTraits.AddRange(data.activeTrait);
        //excludingTraits.AddRange(data.activeTraitRare);
        //only draw t3 trait if you dont have any t3.
        int curMaxRange = data.activeTrait.Count == dConstants.Avatar.RARE_TRAIT_APPEAR_AT - 1 ? 3 : 2;
        int curMinRange = data.activeTrait.Count == dConstants.Avatar.RARE_TRAIT_APPEAR_AT - 1 ? 3 : 1;

        List<string> options = PFEntryDeck.DrawCardswConditions(
            dDataHub.hub.traitDict.GetPFEntrywGateList(),
            data.traitOptionCount,
            avaialbleTags,
            new intRange(curMinRange, curMaxRange),
            excludingTraits
            );
        if(options.Count < data.traitOptionCount)
        {
            Debug.LogError(string.Format("not enough trait options in trait selection generation"));
        }
        data.traitOptions = options;
        //TraitSelectionStart.Raise();
    }
    public void TraitSelected(int index)
    {
        CurrencyManagement.wallet.RemoveCurrency("currency_trait", data.GetNextTraitCost);
        AddTrait(data.traitOptions[index]);
        data.traitOptions.Clear();
    }
    public bool AddTrait(string uid)
    {
        if (!TraitIsFull())
        {
            
            //data.acquiredTraitCount += 1;
            //add buff from trait
            Trait trait = dDataHub.hub.traitDict.GetFromUID(uid);
            data.activeTrait.Add(uid);
            for (int i = 0; i < trait.buffUIDs.Count; i++)
            {
                float value = 0;
                float.TryParse(trait.buffValues[i], out value);
                ActiveBuffs.controller.ActivateBuff(trait.UID, trait.UID, trait.buffUIDs[i], value);
            }
            data.latestTraitUID = uid;
            TraitChanged.Raise();
            return true;
        }
        else
        {
            return false;
        }
    }
    public bool AddRandomTraitByTier(int tier = -1)
    {
        if(tier == -1)
        {
            tier = data.activeTrait.Count == dConstants.Avatar.RARE_TRAIT_APPEAR_AT - 1 ? 3 : Random.Range(1,3);
        }
        if (!TraitIsFull())
        {
            int loopTimes = 0;
            string traitUID = "";
            while (loopTimes++ < dConstants.MAX_LOOP_TIMES)
            {
                //traitUID = dDataHub.hub.traitDict.GetRandomTraitByTier(tier);
                List<string> filter = new List<string>();
                List<string> traitUIDs = PFEntryDeck.DrawCardswConditions(dDataHub.hub.traitDict.GetPFEntrywGateList(),
                    1,
                    filter,
                    new intRange(tier, tier)
                    );
                if(traitUIDs.Count > 0)
                {
                    traitUID = traitUIDs[0];
                    if (!data.activeTrait.Contains(traitUIDs[0]))
                    {
                        AddTrait(traitUID);
                        TraitChanged.Raise();
                        return true;
                    }
                }
            }
            //if it reaches the max loop times
            AddTrait(traitUID);
            TraitChanged.Raise();
            return true;
        }
        else
        {
            return false;
        }
    }
    public bool NextTraitAvaialble()
    {
        int value = CurrencyManagement.wallet.CheckCurrency("currency_trait");
        return value >= data.GetNextTraitCost;
    }
    public int NextTraitCost()
    {
        return data.GetNextTraitCost;
    }
    public bool CheckTrait(string uid)
    {
        return data.activeTrait.Contains(uid);
    }
    public bool TraitIsFull()
    {
        return !(data.activeTrait.Count < data.traitCapacity);
    }
    public string GetLatestTraitUID()
    {
        return data.latestTraitUID;
    }
}
