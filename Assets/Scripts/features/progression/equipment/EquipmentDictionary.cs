﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[CreateAssetMenu(menuName = "Dictionary/EquipmentDefinition")]
public class EquipmentDictionary : GenericDataDict<Equipment>
{
    public override void DebugCount()
    {
        Debug.Log("Total of " + UIDList.Count + " equipment definitions are added to current Equipment Dictionary");
    }
    public List<PFEntrywGate> GetPFEntrywGateList()
    {
        List<PFEntrywGate> result = new List<PFEntrywGate>();
        List<Equipment> fullList = GetFullList();
        foreach (Equipment item in fullList)
        {
            result.Add(item);
        }
        return result;
    }
    public string GetRandomEquipmentByTier(int _tier)
    {
        
        List<string> availableEquipments = new List<string>();
        availableEquipments = (from equ in DataDict where equ.Value.tier == _tier select equ.Key).ToList();
        int rng = Random.Range(0, availableEquipments.Count);
        //Debug.Log(string.Format("try get a random equipment of tier({0})", _tier));
        return availableEquipments[rng];
    }
    public string GetRandomEquipmentWithinTier(int _tier)
    {

        List<string> availableEquipments = new List<string>();
        availableEquipments = (from equ in DataDict where equ.Value.tier <= _tier select equ.Key).ToList();
        int rng = Random.Range(0, availableEquipments.Count);
        //Debug.Log(string.Format("try get a random equipment of tier({0})", _tier));
        return availableEquipments[rng];
    }
}
