﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Equipment : PFEntrywGate
{
    public CardArt art;
    //public string slot;
    public List<string> upgradeOptions;
    public List<BuffItem> buffs;

    public Equipment() : base()
    {
        entryType = "equipment";
        art = new CardArt();
        //slot = "special";
        upgradeOptions = new List<string>();
        buffs = new List<BuffItem>();
    }
}
