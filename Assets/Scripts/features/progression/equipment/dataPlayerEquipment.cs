﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class EquipmentTypeBySlot
{
    public string type;
    public int slotIndex;
}

[CreateAssetMenu(menuName = "Gameplay/Player Equipment")]
public class dataPlayerEquipment : ScriptableObject
{
    public List<EquipmentTypeBySlot> equipmentTypes;
    public List<string> activeEquipment = new List<string>();
    //public int equipmentCapacity = 0;
    public void Reset()
    {
        activeEquipment = new List<string>();
        //equipmentCapacity = 10;
    }
}
