﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipmentManagement : MonoBehaviour
{
    public static EquipmentManagement equipment;
     
    private void Awake()
    {
        if (equipment == null)
        {
            equipment = this;
        }
        else if (equipment != null)
        {
            Destroy(gameObject);
        }
    }
    [Header("Data")]
    public dataPlayerEquipment data;

    [Header("Game Event")]
    public GameEvent EquipmentChanged;

    public void EquipmentInit()
    {
        ResetEquipment();
        //AddRandomEquipmentByTier(1);
        //Debug.Log(string.Format("current displayed date: {0}{1}{2}", calendarData.YearToString, calendarData.MonthToString, calendarData.DateToString));
    }

    public void AddEquipment(string uid, int index = -1)
    {
        Equipment newEqu = dDataHub.hub.equipmentDict.GetFromUID(uid);
        //find the correct slot if index is not given
        if(index == -1)
        {
            for(int i = 0; i < data.equipmentTypes.Count; i++)
            {
                if (data.equipmentTypes[i].type == newEqu.tags[0])
                {
                    index = i;
                }
            }
        }
        //error check
        if(index == -1)
        {
            Debug.LogError(string.Format("unable to find the correct slot for equipment UID ({0})", uid));
            return;
        }
        //remvoe old equipment buffs
        if (data.activeEquipment[index] != "")
        {
            Equipment oldEqu = dDataHub.hub.equipmentDict.GetFromUID(data.activeEquipment[index]);
            ActiveBuffs.controller.DeactivateFrom(oldEqu.UID);
        }
        //add new equipment & its buffs
        data.activeEquipment[index] = newEqu.UID;
        for(int i = 0; i < newEqu.buffs.Count; i++)
        {
            if(newEqu.buffs[i].scope == "perm_equipment")
            {
                float buffValue = 0;
                float.TryParse(newEqu.buffs[i].value, out buffValue);
                ActiveBuffs.controller.ActivateBuff(newEqu.UID, newEqu.UID, newEqu.buffs[i].uid, buffValue);
            }
        }
        gChronicle.controller.AddArtForCurrentAction(newEqu.art.objectArt);
        if (ActiveBuffs.controller.CheckFeatureGate("feature_gate_help_gain_equipment_t2"))
        {
            CharacterAcquisition.controller.AcquisitionCheck_Gain_Equipment(newEqu.tier);
        }
        EquipmentChanged.Raise();
    }
    int ChooseRandomSlotIndex(bool includeExisted, bool includeEmpty)
    {
        List<int> availableIndexes = new List<int>();
        for(int i = 0; i < data.equipmentTypes.Count; i++)
        {
            if(data.activeEquipment[i] != "" && includeExisted)
            {
                Equipment equ = dDataHub.hub.equipmentDict.GetFromUID(data.activeEquipment[i]);
                int availableOptions = 0;
                for(int j = 0; j < equ.upgradeOptions.Count; j++)
                {
                    Equipment equ_up = dDataHub.hub.equipmentDict.GetFromUID(equ.upgradeOptions[j]);
                    if (ActiveBuffs.controller.CheckFeatureGate(equ_up.release))
                    {
                        availableOptions += 1;
                    }
                }
                if(availableOptions > 0)
                {
                    availableIndexes.Add(i);
                }
            }
            else if(data.activeEquipment[i] == "" && includeEmpty)
            {
                //special check for slot 5 : collections (passive equipmnent)
                if (i == 5)
                {
                    if (ActiveBuffs.controller.CheckFeatureGate("feature_gate_equipment_collection"))
                    {
                        availableIndexes.Add(i);
                    }
                }
                else
                {
                    availableIndexes.Add(i);
                }
            }
        }
        if(availableIndexes.Count == 0)
        {
            return -1;
        }
        int rng = Random.Range(0, availableIndexes.Count);
        return availableIndexes[rng];
    }
    public void UpgradeRandomEquiupment(int index = -1)
    {
        int targetIndex = (index == -1 ? ChooseRandomSlotIndex(true, false) : index);
        if (targetIndex == -1)
        {
            Debug.Log("equipment full");
            return;
        }
        string curEquipmentUID = data.activeEquipment[targetIndex];
        Equipment equ = dDataHub.hub.equipmentDict.GetFromUID(curEquipmentUID);
        if(equ.upgradeOptions.Count == 0)
        {
            AcquireNewRandomEquiupment();
            return;
        }
        int rng = Random.Range(0, equ.upgradeOptions.Count);
        AddEquipment(equ.upgradeOptions[rng], targetIndex);
        //data.activeEquipment[targetIndex] = equ.upgradeOptions[rng];
    }
    public void AcquireNewRandomEquiupment(int index = -1)
    {
        int targetIndex = (index == -1 ? ChooseRandomSlotIndex(false, true) : index);
        if (targetIndex == -1)
        {
            Debug.Log("equipment full");
            return;
        }
        List<string> equipmentTypeTarget = new List<string>();
        equipmentTypeTarget.Add(data.equipmentTypes[targetIndex].type);
        List<string> avaialbleEquipments = PFEntryDeck.DrawCardswConditions(dDataHub.hub.equipmentDict.GetPFEntrywGateList(),
            1,
            equipmentTypeTarget,
            new intRange(1, 1));
        /*if(avaialbleEquipments.Count < 1)
        {
            Debug.LogError(string.Format("can't find a valid equipment for {0}", equipmentTypeTarget[0]));
            return;
        }*/
        AddEquipment(avaialbleEquipments[0], targetIndex);
        //CharacterAcquisition.controller.AcquisitionCheck_Character3_lv0();
    }
    public void AcquireUpgradedRandomEquiupment(int index = -1)
    {
        int targetIndex = (index == -1 ? ChooseRandomSlotIndex(false, true) : index);
        if (targetIndex == -1)
        {
            Debug.Log("equipment full");
            return;
        }
        List<string> equipmentTypeTarget = new List<string>();
        equipmentTypeTarget.Add(data.equipmentTypes[targetIndex].type);
        List<string> avaialbleEquipments = PFEntryDeck.DrawCardswConditions(dDataHub.hub.equipmentDict.GetPFEntrywGateList(),
            1,
            equipmentTypeTarget,
            new intRange(1, 1));
        AddEquipment(avaialbleEquipments[0], targetIndex);
        UpgradeRandomEquiupment(targetIndex);
    }
    public List<string> PreviewEquipmentImprovement(int count)
    {
        List<string> results = new List<string>();
        List<int> targetIndexes = new List<int>();
        int rngIndex = -1;
        int loopTimes = 0;
        while (loopTimes < dConstants.MAX_LOOP_TIMES)
        {
            if(targetIndexes.Count >= count)
            {
                break;
            }
            else
            {
                loopTimes += 1;
            }
            rngIndex = ChooseRandomSlotIndex(true, true);
            if (!targetIndexes.Contains(rngIndex))
            {
                targetIndexes.Add(rngIndex);
            }
        }
        if(targetIndexes.Count == 0)
        {
            Debug.Log("equipment full");
        }
        else
        {
            List<string> equipmentTypeTarget = new List<string>();
            for (int i = 0; i < targetIndexes.Count; i++)
            {
                if(data.activeEquipment[targetIndexes[i]] == "")
                {
                    equipmentTypeTarget.Clear();
                    equipmentTypeTarget.Add(data.equipmentTypes[targetIndexes[i]].type);
                    List<string> avaialbleEquipments = PFEntryDeck.DrawCardswConditions(dDataHub.hub.equipmentDict.GetPFEntrywGateList(),
                        1,
                        equipmentTypeTarget,
                        new intRange(1, 1));
                    if (avaialbleEquipments.Count > 0)
                    {
                        results.Add(avaialbleEquipments[0]);
                    }
                }
                else
                {
                    Equipment equ = dDataHub.hub.equipmentDict.GetFromUID(data.activeEquipment[targetIndexes[i]]);
                    int rng = Random.Range(0, equ.upgradeOptions.Count);
                    results.Add(equ.upgradeOptions[rng]);
                }
                
            }
        }
        return results;
    }
    public void ImproveRandomEquipment()
    {
        int targetIndex = ChooseRandomSlotIndex(true, true);
        if(data.activeEquipment[targetIndex] != "")
        {
            UpgradeRandomEquiupment(targetIndex);
        }
        else
        {
            AcquireNewRandomEquiupment(targetIndex);
        }
    }
    public bool RemoveFirstEquipment()
    {
        if(data.activeEquipment.Count == 0)
        {
            return false;
        }
        else
        {
            data.activeEquipment.RemoveAt(0);
            ActiveBuffs.controller.ToggleFeatureGate("feature_gate_owning_equipment", data.activeEquipment.Count > 0);
            EquipmentChanged.Raise();
            return true;
        }
    }
    public int ActiveEquipmentCount()
    {
        int count = 0;
        for(int i=0;i< data.activeEquipment.Count; i++)
        {
            if(data.activeEquipment[i] != "")
            {
                count += 1;
            }
        }
        return count;
    }
    /*
    public bool AddRandomEquipmentByTier(int _tier)
    {
        if (!EquipmenetIsFull())
        {
            int loopTimes = 0;
            string equipmentUID = "";
            while (loopTimes++ < dConstants.MAX_LOOP_TIMES)
            {
                equipmentUID = dDataHub.hub.equipmentDict.GetRandomEquipmentByTier(_tier);
                if (!data.activeEquipment.Contains(equipmentUID))
                {
                    AddEquiupment(equipmentUID);
                    EquipmentChanged.Raise();
                    return true;
                }
            }
            //if it reaches the max loop times
            AddEquiupment(equipmentUID);
            EquipmentChanged.Raise();
            return true;
        }
        else
        {
            return false;
        }
    }
    public bool AddRandomEquipmentWithinTier(int _tier)
    {
        if (!EquipmenetIsFull())
        {
            int loopTimes = 0;
            string equipmentUID = "";
            while (loopTimes++ < dConstants.MAX_LOOP_TIMES)
            {
                equipmentUID = dDataHub.hub.equipmentDict.GetRandomEquipmentWithinTier(_tier);
                if (!data.activeEquipment.Contains(equipmentUID))
                {
                    AddEquiupment(equipmentUID);
                    EquipmentChanged.Raise();
                    return true;
                }
            }
            //if it reaches the max loop times
            AddEquiupment(equipmentUID);
            EquipmentChanged.Raise();
            return true;
        }
        else
        {
            return false;
        }
    }
    */
    public void ResetEquipment()
    {
        data.activeEquipment.Clear();
        for (int i = 0; i < data.equipmentTypes.Count; i++)
        {
            data.activeEquipment.Add("");
        }
        EquipmentChanged.Raise();
    }

    public string GetLatestEquipmentUID()
    {
        return data.activeEquipment[data.activeEquipment.Count - 1];
    }
}
