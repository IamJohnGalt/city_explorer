﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataReader_Equipment : GenericDataReader
{
    public EquipmentDictionary equipmentDict;
    public override void LoadFromXLS()
    {
        equipmentDict.Reset();
        LoadEquipmentData("Progression - equipment_exporter");
        FinishLoading();
    }
    void LoadEquipmentData(string filename)
    {
        string[,] string_data = CSVReader.SplitCsvGrid(Resources.Load<TextAsset>("XLSData/" + filename).text);
        if (string_data[0, 0] != "UID")
        {
            Debug.LogError("Invalid File (No UID found): " + filename);
            return;
        }
        if (equipmentDict == null)
        {
            Debug.Log("Null Ref on Dictionary");
        }
        for (int iRow = 1; iRow < string_data.GetUpperBound(1); iRow++)
        {
            Equipment equip = new Equipment();
            equip.UID = string_data[0, iRow];
            equip.title = string_data[1, iRow];
            equip.desc = string_data[2, iRow];
            

            equip.release = string_data[3, iRow];
            equip.tags = SplitString(string_data[4, iRow], ',');
            int.TryParse(string_data[5, iRow], out equip.tier);
            float.TryParse(string_data[6, iRow], out equip.weight);

            equip.art.objectArt = string_data[7, iRow];
            //equip.slot = string_data[8, iRow];
            equip.upgradeOptions = SplitString(string_data[8, iRow], ',');

            BuffItem buff = new BuffItem();
            if(string_data[10, iRow] != "")
            {
                buff = new BuffItem();
                buff.scope = string_data[9, iRow];
                buff.uid = string_data[10, iRow];
                buff.value = string_data[11, iRow];
                int.TryParse(string_data[12, iRow], out buff.duration);
                equip.buffs.Add(buff);
            }
            if (string_data[14, iRow] != "")
            {
                buff = new BuffItem();
                buff.scope = string_data[13, iRow];
                buff.uid = string_data[14, iRow];
                buff.value = string_data[15, iRow];
                int.TryParse(string_data[16, iRow], out buff.duration);
                equip.buffs.Add(buff);
            }
            equip.art.backgroundArt = string_data[17, iRow];
            //card.DebugPrint();
            equipmentDict.Add(equip.UID, equip);
        }
        equipmentDict.DebugCount();
    }
}
