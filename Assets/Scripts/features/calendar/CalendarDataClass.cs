﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Date
{
    public int yearIndex = 0;
    public int milestoneIndex = 0;
    public int monthIndex = 0;
    public int dayIndex = 0;
    public int seasonIndex
    {
        get
        {
            return Mathf.Max(0, Mathf.FloorToInt((float)monthIndex / dConstants.Date.NUM_MONTH_PER_YEAR * dConstants.Date.NUM_SEASON_PER_YEAR));
        }
    }
    public int ToInt
    {
        get
        {
            return dayIndex + monthIndex * dConstants.Date.NUM_DAY_PER_MONTH + yearIndex * dConstants.Date.NUM_MONTH_PER_YEAR * dConstants.Date.NUM_DAY_PER_MONTH;
        }
    }
    public Date()
    {
        yearIndex = 0;
        milestoneIndex = 0;
        monthIndex = 0;
        dayIndex = 0;
    }
    public Date(Date _date)
    {
        yearIndex = _date.yearIndex;
        milestoneIndex = _date.milestoneIndex;
        monthIndex = _date.monthIndex;
        dayIndex = _date.dayIndex;
    }
    public string DateToString()
    {
        return string.Format("{0}{1}", YearToString(), MonthToString());
    }
    public string YearToString()
    {
        return string.Format("{0}<size=75%><color=#555555>年</color></size>", dConstants.Date.YearText[yearIndex % dConstants.Date.YearText.Length]);
    }
    public string DayToString()
    {
        return string.Format("{0}<size=75%><color=#555555>日</color></size>", dayIndex + 1);
    }
    public string MonthToString()
    {
        return string.Format("{0}<size=75%><color=#555555>月</color></size>", dConstants.Date.MonthText[monthIndex % dConstants.Date.MonthText.Length]);
    }
    public bool isSameMonth(Date compareDate)
    {
        return (compareDate.yearIndex == yearIndex) && (compareDate.monthIndex == monthIndex);
    }
}
