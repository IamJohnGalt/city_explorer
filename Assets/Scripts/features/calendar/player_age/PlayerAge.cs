﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAge : MonoBehaviour
{
    public dataPlayerAge myAge;
    public void SetPlayerAge(int age, int endAge, int year, int season, int milestone)
    {
        myAge.curAge = age;
        myAge.yearBorn = year;
        myAge.seasonBorn = season;
        myAge.milestoneBorn = milestone;
        myAge.endAge = endAge;
    }
   
}
