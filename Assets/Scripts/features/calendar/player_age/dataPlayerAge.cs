﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Gameplay/Player Age")]
public class dataPlayerAge : ScriptableObject
{
    public int curAge;
    public int yearBorn;
    public int seasonBorn;
    public int milestoneBorn;

    public int endAge;
    public int yearLeft;
    public int turnLeft;
}
