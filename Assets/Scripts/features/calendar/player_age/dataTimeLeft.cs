﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Gameplay/Game Time Left")]
public class dataTimeLeft : ScriptableObject
{
    public int yearLeft;
    public int turnLeft;
}
