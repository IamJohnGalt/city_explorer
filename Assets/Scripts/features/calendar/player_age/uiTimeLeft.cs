﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class uiTimeLeft : uiElement
{
    public dataTimeLeft timeLeft;

    public TextMeshProUGUI text_time_left;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public override void UIUpdate()
    {
        string year_left = "";
        string turn_left = "";
        if (timeLeft.yearLeft <= 0)
        {
            year_left = "<1";
        }
        else
        {
            year_left = timeLeft.yearLeft.ToString();
        }
        turn_left = timeLeft.turnLeft.ToString();
        text_time_left.text = string.Format("旅程还剩：{0}年 ({1}回合)", year_left, turn_left);
    }
}
