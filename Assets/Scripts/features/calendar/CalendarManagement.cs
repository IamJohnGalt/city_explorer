﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityScript.Scripting.Pipeline;

public class CalendarManagement : MonoBehaviour
{
    // Start is called before the first frame update
    public static CalendarManagement calendar;

    private void Awake()
    {
        if (calendar == null)
        {
            calendar = this;
        }
        else if (calendar != null)
        {
            Destroy(gameObject);
        }
    }

    public dataCalendar calendarData;
    //public dataPlayerAge myAge;
    //public dataTimeLeft timeLeft;

    //public GameEvent NextDay;
    //public GameEvent playerBirthday;
    public GameEvent dateChanged;
    public GameEvent gameEndByTime;

    [SerializeField] List<string> seasonalBuffUIDs;
    [SerializeField] SpriteAsset SeasonSprites;
    public void LoadDate()
    {
        int.TryParse(SaveManager.controller.Inquire(string.Format("yearIndex")), out calendarData.curDate.yearIndex);
        int.TryParse(SaveManager.controller.Inquire(string.Format("monthIndex")), out calendarData.curDate.monthIndex);
        int.TryParse(SaveManager.controller.Inquire(string.Format("dayIndex")), out calendarData.curDate.dayIndex);
    }
    public void SaveDate()
    {
        SaveManager.controller.Insert(string.Format("yearIndex"), calendarData.curDate.yearIndex.ToString());
        SaveManager.controller.Insert(string.Format("monthIndex"), calendarData.curDate.monthIndex.ToString());
        SaveManager.controller.Insert(string.Format("dayIndex"), calendarData.curDate.dayIndex.ToString());
    }
    public string SeasonToString(Date date = null)
    {
        date = date ?? calendar.GetCurDate();
        string season = "";
        if (date.monthIndex >= 1 && date.monthIndex <= 3)
        {
            season = dConstants.Date.SeasonText[0];
        }
        else if (date.monthIndex >= 4 && date.monthIndex <= 6)
        {
            season = dConstants.Date.SeasonText[1];
        }
        else if (date.monthIndex >= 7 && date.monthIndex <= 9)
        {
            season = dConstants.Date.SeasonText[2];
        }
        else
        {
            season = dConstants.Date.SeasonText[3];
        }
        return season;
    }
    public Sprite GetSeasonSprite(Date date = null)
    {
        date = date ?? calendar.GetCurDate();
        if (date.monthIndex >= 1 && date.monthIndex <= 3)
        {
            return SeasonSprites.Assets[0];
        }
        else if (date.monthIndex >= 4 && date.monthIndex <= 6)
        {
            return SeasonSprites.Assets[1];
        }
        else if (date.monthIndex >= 7 && date.monthIndex <= 9)
        {
            return SeasonSprites.Assets[2];
        }
        else
        {
            return SeasonSprites.Assets[3];
        }
    }
    public void SetCalendar(int startYear, int startMonth)
    {
        calendarData.Reset();
        calendarData.curDate.yearIndex = startYear;
        calendarData.curDate.monthIndex = startMonth;
        GenearateMiscs();
        SeasonSwitch();       
    }
    public void Next()
    {
        calendarData.curDate.milestoneIndex += 1;
        if(calendarData.curDate.milestoneIndex >= dConstants.Date.NUM_MILESTONE_PER_SEASON)
        {
            calendarData.curDate.milestoneIndex = 0;
            //calendarData.curDate.seasonIndex += 1;
            
            if (calendarData.curDate.seasonIndex >= dConstants.Date.NUM_SEASON_PER_YEAR)
            {
                //calendarData.curDate.seasonIndex = 0;
                calendarData.curDate.yearIndex += 1;
                if (calendarData.curDate.yearIndex >= dConstants.Date.MAX_YEAR)
                {
                    calendarData.curDate.yearIndex = 0;
                    Debug.LogError("Max Year (60) arrives");
                }
            }
            SeasonSwitch();
        }
        //CheckBirthday();
        //UpdateTimeLeft();
        //NextDay.Raise();
    }
    public void AddTravelMonth(int month)
    {
        calendarData.latestMonthChanged = month;
        calendarData.totalTravelMonthAccumulated += month;
        dateChanged.Raise();
    }
    public int GetMonthLeft()
    {
        return calendarData.monthLeft;
    }
    public Date GetCurDate()
    {
        return new Date(calendarData.curDate);
    }
    public void ResetCalendar()
    {
        SetCalendar(Random.Range(0, 0), Random.Range(0, dConstants.Date.NUM_MONTH_PER_YEAR));
        //Debug.Log(string.Format("current displayed date: {0}{1}{2}", calendarData.YearToString, calendarData.MonthToString, calendarData.DayToString));
    }
    public void CalendarInit()
    {
        LoadDate();
        SetAsInitialDate();
        TimeElapse(0, false);
        //Debug.Log(string.Format("current displayed date: {0}{1}{2}", calendarData.YearToString, calendarData.MonthToString, calendarData.DayToString));
    }
    public void TimeElapse(int month, bool checkEnd = true)
    {
        calendarData.latestMonthChanged = -month;
        calendarData.curDate.monthIndex += month + calendarData.queuedMonthElapsed;
        calendarData.queuedMonthElapsed = 0;
        if (calendarData.curDate.monthIndex >= dConstants.Date.NUM_MONTH_PER_YEAR)
        {
            int yearStep = Mathf.FloorToInt((float)calendarData.curDate.monthIndex / dConstants.Date.NUM_MONTH_PER_YEAR);
            calendarData.curDate.yearIndex += yearStep;
            calendarData.curDate.monthIndex -= yearStep * dConstants.Date.NUM_MONTH_PER_YEAR;
        }
        if (month == 0)
        {
            calendarData.curDate.dayIndex += Random.Range(1, 1);
            calendarData.curDate.dayIndex = Mathf.Min(31, calendarData.curDate.dayIndex);
        }
        else
        {
            calendarData.curDate.dayIndex = 0;
            GenearateMiscs();
        }
        CheckBirthday();
        SeasonSwitch();
        if (checkEnd)
        {
            CheckEndGameByTime();
        }
        dateChanged.Raise();
    }
    public void QueuedTimeElapse(int month)
    {
        calendarData.queuedMonthElapsed += month;
        calendarData.latestMonthChanged = -month;
        dateChanged.Raise();
    }
    public void CheckEndGameByTime()
    {
        if(ReachReturningDate())
        {
            gameEndByTime.Raise();
        }
    }
    public void GenearateMiscs()
    {
        //calendarData.curDate.seasonIndex = Mathf.CeilToInt((float)calendarData.curDate.monthIndex / dConstants.Date.NUM_MONTH_PER_YEAR * dConstants.Date.NUM_SEASON_PER_YEAR);
        //calendarData.curDate.milestoneIndex = Random.Range(0, dConstants.Date.NUM_MILESTONE_PER_SEASON - 1);
        calendarData.curDate.dayIndex = Random.Range(0, 0);
    }
    /*public void CheckBirthday()
    {
        if (calendarData.seasonIndex == myAge.seasonBorn && calendarData.milestoneIndex == myAge.milestoneBorn)
        {
            myAge.curAge += 1;
            Debug.Log("Today is my birhtday!");
            playerBirthday.Raise();
        }
    }*/
    /*void UpdateTimeLeft()
    {
        if (myAge.endAge < myAge.curAge)
        {
            gameEndByAge.Raise();
            return;
        }
        else
        {
            timeLeft.yearLeft = myAge.endAge - myAge.curAge;
            int year2Turn = (myAge.endAge - myAge.curAge) * dConstants.Date.NUM_SEASON_PER_YEAR * dConstants.Date.NUM_MILESTONE_PER_SEASON;
            int curYTD = calendarData.seasonIndex * dConstants.Date.NUM_MILESTONE_PER_SEASON + calendarData.milestoneIndex;
            int bdYTD = myAge.seasonBorn * dConstants.Date.NUM_MILESTONE_PER_SEASON + myAge.milestoneBorn;
            if(bdYTD > curYTD)
            {
                timeLeft.turnLeft = year2Turn + bdYTD - curYTD;
            }
            else
            {
                timeLeft.turnLeft = year2Turn + dConstants.Date.NUM_SEASON_PER_YEAR * dConstants.Date.NUM_MILESTONE_PER_SEASON - curYTD + bdYTD;
            }
            //Debug.Log(string.Format("curYTD: {0}, bdYTD: {1}, year2Turn: {2}", curYTD, bdYTD, year2Turn));
            
        }
    }*/
    void SeasonSwitch()
    {
        //ActiveBuffs.controller.DeactivateFrom("calendar");
        for(int i = 0; i < seasonalBuffUIDs.Count; i++)
        {
            ActiveBuffs.controller.ToggleFeatureGate(seasonalBuffUIDs[i], false);
        }
        int monthIndex = calendarData.curDate.monthIndex;
        if (monthIndex >= 1 && monthIndex <= 3)
        {
            ActiveBuffs.controller.ToggleFeatureGate(seasonalBuffUIDs[0], true);
        }
        else if (monthIndex >= 4 && monthIndex <= 6)
        {
            ActiveBuffs.controller.ToggleFeatureGate(seasonalBuffUIDs[1], true);
        }
        else if (monthIndex >= 7 && monthIndex <= 9)
        {
            ActiveBuffs.controller.ToggleFeatureGate(seasonalBuffUIDs[2], true);
        }
        else
        {
            ActiveBuffs.controller.ToggleFeatureGate(seasonalBuffUIDs[3], true);
        }
    }
    public void DisableSeasons()
    {
        for(int i = 0; i < seasonalBuffUIDs.Count; i++)
        {
            ActiveBuffs.controller.ToggleFeatureGate(seasonalBuffUIDs[i], false);
        }
    }
    void SetAsInitialDate()
    {
        calendarData.startingDate = new Date(calendarData.curDate);
        calendarData.nextBirthday = new Date(calendarData.startingDate);
        calendarData.nextBirthday.yearIndex += 1;
    }
    bool ReachReturningDate()
    {
        //Debug.Log(string.Format("data comparision cur {0} end {1}", calendarData.curDate.ToInt, calendarData.returningDate.ToInt));
        return calendarData.curDate.ToInt >= calendarData.returningDate.ToInt;
    }
    void CheckBirthday()
    {
        //Date targetDate;
        int loopTimes = 0;
        while(loopTimes < dConstants.MAX_LOOP_TIMES)
        {
            if (calendarData.nextBirthday.ToInt < calendarData.curDate.ToInt)
            {
                ChronicleActionLog birthdayLog = new ChronicleActionLog();
                birthdayLog.actionType = "notification";
                birthdayLog.actionUID = "";
                birthdayLog.date = new Date(calendarData.nextBirthday);
                birthdayLog.desc = string.Format("外出旅行已经{0}个年头，思乡之情渐浓（还剩{1}年）。", calendarData.pastYears, calendarData.yearLeft > 0 ? calendarData.yearLeft.ToString() : "不到1");
                gChronicle.controller.Add2PastAction(birthdayLog);

                if (ActiveBuffs.controller.CheckFeatureGate("feature_gate_gain_interests"))
                {
                    /*int interestsAmount = CurrencyManagement.wallet.GetInterests();
                    ChronicleActionLog interestsLog = new ChronicleActionLog();
                    interestsLog.actionType = "notification";
                    interestsLog.actionUID = "";
                    interestsLog.date = new Date(calendarData.nextBirthday);
                    interestsLog.desc = string.Format("家中产业生意兴隆，根据当前铜钱，获得了盈利{0}。", interestsAmount);
                    gChronicle.controller.Add2PastAction(interestsLog);
                    */
                    gChronicle.controller.ScheduleGainInterests();
                }
                calendarData.nextBirthday.yearIndex += 1;
            }
            else
            {
                break;
            }
        }
        if(loopTimes >= dConstants.MAX_LOOP_TIMES)
        {
            Debug.LogError("detecting infinite loop in Calendar Management");
        }

        
    }
}
