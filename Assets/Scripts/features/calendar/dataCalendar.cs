﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Gameplay/Calendar")]
public class dataCalendar : ScriptableObject
{
    public Date curDate;
    public Date startingDate;
    public int latestMonthChanged;
    public int queuedMonthElapsed;
    public int pastYears
    {
        get
        {
            return curDate.yearIndex - startingDate.yearIndex;
        }
    }
    public string TotalTravelTimeString
    {
        get
        {
            int totalMonths = (curDate.yearIndex - startingDate.yearIndex) * dConstants.Date.NUM_MONTH_PER_YEAR + curDate.monthIndex - startingDate.monthIndex;
            int year = Mathf.FloorToInt((float)totalMonths / dConstants.Date.NUM_MONTH_PER_YEAR);
            int month = totalMonths - year * dConstants.Date.NUM_MONTH_PER_YEAR;
            return string.Format("{0}年{1}月", year, month);
        }
    }
    public int monthLeft
    {
        get
        {
            return returningDate.yearIndex * dConstants.Date.NUM_MONTH_PER_YEAR + returningDate.monthIndex - curDate.yearIndex * dConstants.Date.NUM_MONTH_PER_YEAR - curDate.monthIndex - queuedMonthElapsed;
        }
    }
    public int yearLeft
    {
        get
        {
            return Mathf.FloorToInt((monthLeft) / dConstants.Date.NUM_MONTH_PER_YEAR);
        }
    }
    public int totalTravelMonthAccumulated;
    public Date returningDate
    {
        get
        {
            Date returningDate = new Date(startingDate);
            int totalTravelYear = Mathf.FloorToInt((float)totalTravelMonthAccumulated / dConstants.Date.NUM_MONTH_PER_YEAR);
            returningDate.yearIndex += totalTravelYear;
            returningDate.monthIndex += totalTravelMonthAccumulated - totalTravelYear * dConstants.Date.NUM_MONTH_PER_YEAR;
            return returningDate;
        }
    }
    public Date nextBirthday;
    public void Reset()
    {
        curDate = new Date();
        startingDate = new Date();
        totalTravelMonthAccumulated = dConstants.Avatar.INITIAL_MONTH_LEFT;
        nextBirthday = new Date();
        latestMonthChanged = 0;
        queuedMonthElapsed = 0;

}
    public string YearToString
    {
        get { return string.Format("{0}<size=75%><color=#555555>年</color></size>", dConstants.Date.YearText[curDate.yearIndex % dConstants.Date.YearText.Length]); }
    }
    public string MonthToString
    {
        get { return string.Format("{0}<size=75%><color=#555555>月</color></size>", dConstants.Date.MonthText[curDate.monthIndex % dConstants.Date.MonthText.Length]); }
    }
    public string DayToString
    {
        get { return string.Format("{0}<size=75%><color=#555555>日</color></size>", curDate.dayIndex +1); }
    }
    public string SeasonToString
    {
        get { return dConstants.Date.SeasonText[curDate.seasonIndex]; }
    }
    public string MilestoneToString
    {
        get { return dConstants.Date.MilestoneText[curDate.seasonIndex, curDate.milestoneIndex]; }
    }
}

