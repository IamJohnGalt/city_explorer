﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System;

[System.Serializable]
public class SaveData
{
    //public int GetCount { get { return dict.Count; } }

    public string save_str;

    public string ConvertToString(Dictionary<string, string> _dict)
    {
        string save = "";
        List<string> _list = new List<string>();
        foreach(KeyValuePair<string, string> item in _dict)
        {
            _list.Add(item.Key + "@" + item.Value);
        }
        save = string.Join("$", _list);
        //Debug.Log("save_str: " + save);
        return save;
    }
    public Dictionary<string, string> ConvertToDictionary(string _str)
    {
        Dictionary<string, string> _dict = new Dictionary<string, string>();
        List<string> _list = new List<string>();
        _list = _str.Split('$').ToList();
        //Debug.Log("list item count: " + _list.Count);
        foreach (string item in _list)
        {
            //Debug.Log("item 2 split: " + item);
            if(item.Length > 0)
            {
                string[] KeyValue = item.Split('@');
                _dict.Add(KeyValue[0], KeyValue[1]);
            }
            
        }
        //Debug.Log("dict item count: " + _dict.Count);
        return _dict;
    }

}

public class SaveManager : MonoBehaviour
{
    public static SaveManager controller;
    void Awake()
    {
        if (controller == null)
        {
            controller = this;
        }
        else if (controller != null)
        {
            Destroy(gameObject);
        }
        //DontDestroyOnLoad(controller);
        //ResetCurSave();
    }

    [Header("Savings")]
    public GameEvent SaveLoaded;
    public GameEvent SaveLoadedFinished;
    public GameEvent SaveSaved;
    public SaveData curSave;

    [Header("Loading Screen")]
    public dataLoadingPage loadingData;
    public GameEvent GameLoading;

    private Dictionary<string, string> dict2save = new Dictionary<string, string>();
    public List<string> dictVisualized = new List<string>();

    public void ResetCurSave()
    {
        curSave.save_str = "";
        dict2save.Clear();
        VisualizeSaves();
    }
    public string Inquire(string uid)
    {
        if (dict2save.ContainsKey(uid))
        {
            return dict2save[uid];
        }
        else
        {
            //Debug.Log("an invalid save string inquired");
            return "";
        }
    }
    public void Insert(string uid, string data)
    {
        if (dict2save.ContainsKey(uid))
        {
            dict2save[uid] = data;
        }
        else
        {
            dict2save.Add(uid, data);
            //VisualizeSaves();
        }
    }
    void VisualizeSaves()
    {
        dictVisualized.Clear();
        foreach (KeyValuePair<string, string> item in dict2save)
        {
            dictVisualized.Add(item.Key + ": " + item.Value);
        }
        //Debug.Log(string.Format("{0} save items visualized", dictVisualized.Count));
    }
    public void LoadFromFile()
    {
        try
        {
            BinaryFormatter bf = new BinaryFormatter();
            if (!File.Exists(Path.Combine(Application.persistentDataPath, "gamesave.save")))
            {
                ClearSaveFile();
            }
            FileStream file = File.Open(Path.Combine(Application.persistentDataPath, "gamesave.save"), FileMode.Open);
            //Debug.Log(Application.persistentDataPath);
            curSave.save_str = (string)bf.Deserialize(file);
            dict2save = curSave.ConvertToDictionary(curSave.save_str);
            file.Close();
        }
        catch (IOException ex)
        {
            Console.WriteLine(ex.Message);
            Debug.LogError("game load fails, using fresh start");
        }
        VisualizeSaves();
        StartCoroutine("SaveLoadedWrapper");
    }
    public void SaveToFile()
    {
        try
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Create(Path.Combine(Application.persistentDataPath, "gamesave.save"));
            curSave.save_str = curSave.ConvertToString(dict2save);
            bf.Serialize(file, curSave.save_str);
            file.Close();
        }
        catch (IOException ex)
        {
            Console.WriteLine(ex.Message);
            Debug.LogError("game save fails, no progression saved");
        }
        //Debug.Log(string.Format("game saved to file with {0} items in it", dict2save.Count));
        VisualizeSaves();
        SaveSaved.Raise();
    }
    public void ClearSaveFile()
    {
        ResetCurSave();
        SaveToFile();
    }
    IEnumerator SaveLoadedWrapper()
    {
        yield return new WaitForSeconds(0.1f);
        loadingData.flavorText = "Save Loaded";
        loadingData.progression = 0.9f;
        GameLoading.Raise();
        yield return new WaitForSeconds(0.5f);
        loadingData.flavorText = "Entering Selection";
        loadingData.progression = 1f;
        GameLoading.Raise();

        SaveLoaded.Raise();
        SaveLoadedFinished.Raise();
    }
}
