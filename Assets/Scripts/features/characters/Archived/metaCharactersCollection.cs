﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class metaCharactersCollection : MonoBehaviour
{
    public static metaCharactersCollection controller;
    private void Awake()
    {
        if (controller == null)
        {
            controller = this;
        }
        else if (controller != null)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(controller);
    }

    public dataCharacterCollection data;
    public GameEvent CharacterCollectionPageUp;
    public GameEvent CharacterSelectionFinished;

    public string playingCharacUID { get { return data.characterStats[data.PlayingCharacterIndex].UID; } }
    public string friendCharacUID { get { return data.characterStats[data.FriendCharacterIndex].UID; } }

    public void CharacterCollectionInit()
    {
        LoadSetting();
        LoadRelationship();
        CharacterCollectionPageUp.Raise();
    }
    void LoadSetting()
    {
        //load from data
    }
    void LoadRelationship()
    {
        //load from save
        for (int i = 0; i < data.characterSettings.Count; i++)
        {
            for (int j = 0; j < data.characterSettings[i].otherUID.Count; j++)
            {
                int relationshipValue = 0;
                string savestring = SaveManager.controller.Inquire(string.Format("relationship_{0}_{1}", data.characterSettings[i].UID, data.characterSettings[i].otherUID[j]));
                if(savestring != "")
                {
                    int.TryParse(savestring, out relationshipValue);
                    data.characterSettings[i].relationship[j].curValue = relationshipValue;
                    //Debug.Log(string.Format("relationship_{0}_{1} is read from saved file as {2}", data.characterSettings[i].UID, data.characterSettings[i].otherUID[j], savestring));
                } 
            }
        }
        UpdateAllCharactersStatus();

        //debug use only
        //RelationshipImprove("character_1", "character_2", 20);
        //SaveRelationship();
    }
    public void CharacterCollectionFinish()
    {
        tAvatar.Avatar.myStats = data.characterStats[data.PlayingCharacterIndex];
        CharacterSelectionFinished.Raise();
    }
    public void SaveRelationship()
    {
        for (int i = 0; i < data.characterSettings.Count; i++)
        {
            for (int j = 0; j < data.characterSettings[i].otherUID.Count; j++)
            {
                SaveManager.controller.Insert(
                    string.Format("relationship_{0}_{1}", data.characterSettings[i].UID, data.characterSettings[i].otherUID[j]),
                    data.characterSettings[i].relationship[j].curValue.ToString());
            }
        }
    }

    public void RelationshipImprove(string charac_uid1, string charac_uid2, int value)
    {
        for(int i = 0; i < data.characterSettings.Count; i++)
        {
            for(int j = 0;j < data.characterSettings[i].otherUID.Count; j++)
            {
                if(data.characterSettings[i].UID == charac_uid1 && data.characterSettings[i].otherUID[j] == charac_uid2)
                {
                    data.characterSettings[i].relationship[j].curValue += Mathf.Min(value, data.characterSettings[i].relationship[j].curGap);
                }
                else if (data.characterSettings[i].UID == charac_uid2 && data.characterSettings[i].otherUID[j] == charac_uid1)
                {
                    data.characterSettings[i].relationship[j].curValue += Mathf.Min(value, data.characterSettings[i].relationship[j].curGap);
                }
            }
        }
        UpdateAllCharactersStatus();
        SaveRelationship();
    }
    public void RelationshipImproveInSession(int value)
    {
        RelationshipImprove(playingCharacUID, friendCharacUID, value);
    }
    public void PersonaImproveInSession(int value)
    {
        //to do: increase persona value for playing character
        Debug.Log(string.Format("Persona value increased for {0} with {1}", playingCharacUID, value));
    }
    public void UpdateAllCharactersStatus()
    {
        for (int i = 0; i < data.characterSettings.Count; i++)
        {
            if(data.characterSettings[i].status == CharacStatus.hidden || data.characterSettings[i].status == CharacStatus.locked)
            {
                if (CheckCharacterUnlocked(GetCharacSettingByUID(data.characterSettings[i].UID)))
                {
                    data.characterSettings[i].status = CharacStatus.unlocked;
                }
            } 
        }
        for (int i = 0; i < data.characterSettings.Count; i++)
        {
            if (data.characterSettings[i].status == CharacStatus.unlocked)
            {
                if (CheckCharacterAcquired(GetCharacSettingByUID(data.characterSettings[i].UID)))
                {
                    data.characterSettings[i].status = CharacStatus.acquired;
                    // new character acquire event 
                }
            }
        }
    }
    dataCharacterSetting GetCharacSettingByUID(string characUID)
    {
        for(int i = 0; i < data.characterSettings.Count; i++)
        {
            if(data.characterSettings[i].UID == characUID)
            {
                return data.characterSettings[i];
            }
        }
        return null;
    }
    bool CheckCharacterUnlocked(dataCharacterSetting characSetting)
    {
        for(int i=0;i< characSetting.otherUID.Count; i++)
        {
            if (characSetting.availability[i] && GetCharacSettingByUID(characSetting.otherUID[i]).status == CharacStatus.acquired)
            {
                return true;
            }
        }
        return false;
    }
    bool CheckCharacterAcquired(dataCharacterSetting characSetting)
    {
        for (int i = 0; i < characSetting.otherUID.Count; i++)
        {
            if (characSetting.availability[i] && characSetting.relationship[i].curGap == 0)
            {
                return true;
            }
        }
        return false;
    }
}
