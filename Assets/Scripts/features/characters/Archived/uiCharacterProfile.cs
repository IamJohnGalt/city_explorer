﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class uiCharacterProfile : MonoBehaviour
{
    public dataCharacterCollection data;

    [Header("Children Objects")]
    public GameObject playing_group;
    public GameObject friend_group;

    public Image playing_icon;
    public TextMeshProUGUI playing_name;
    public TextMeshProUGUI playing_trait;
    public Image playing_trait_icon;
    public TextMeshProUGUI playing_dialog;

    public Image friend_icon;
    public TextMeshProUGUI friend_name;
    public TextMeshProUGUI friend_dialog;

    public void SetPlayingProfile(string uid = "")
    {
        if(uid == "")
        {
            playing_group.SetActive(false);
        }
        else
        {
            dataCharacterStats charac = RetreiveCharacterByUID(uid);
            if (charac != null)
            {
                playing_icon.sprite = charac.characterIcon;
                playing_name.text = charac.characterName;
                playing_trait.text = charac.traitName;
                playing_trait_icon.sprite = charac.traitIcon;
                playing_dialog.text = charac.GetRandomDialog();
            }
            playing_group.SetActive(true);
        }
    }
    public void SetFriendProfile(string uid = "")
    {
        if (uid == "")
        {
            friend_group.SetActive(false);
        }
        else
        {
            dataCharacterStats charac = RetreiveCharacterByUID(uid);
            if (charac != null)
            {
                friend_icon.sprite = charac.characterIcon;
                friend_name.text = charac.characterName;
                friend_dialog.text = charac.GetRandomDialog();
            }
            friend_group.SetActive(true);
        }
    }
    dataCharacterStats RetreiveCharacterByUID(string uid)
    {
        foreach(dataCharacterStats CStats in data.characterStats)
        {
            if(CStats.UID == uid)
            {
                return CStats;
            }
        }
        return null;
    }
}
