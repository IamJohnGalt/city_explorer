﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "meta Game/Character Collection")]
public class dataCharacterCollection : ScriptableObject
{
    public List<dataCharacterSetting> characterSettings;
    public List<dataCharacterStats> characterStats;
    public bool playingConfirmed;
    public int PlayingCharacterIndex;
    public int FriendCharacterIndex;
}
