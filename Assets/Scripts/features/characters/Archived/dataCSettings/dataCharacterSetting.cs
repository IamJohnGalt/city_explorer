﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CharacStatus { hidden, locked, unlocked, acquired };

[CreateAssetMenu(menuName = "meta Game/Character Setting")]
[System.Serializable]
public class dataCharacterSetting : ScriptableObject
{
    public string UID;
    public CharacStatus status;
    public List<string> otherUID;
    public List<bool> availability;
    public List<intwCap> relationship;
}
