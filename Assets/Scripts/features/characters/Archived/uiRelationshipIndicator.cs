﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class uiRelationshipIndicator : uiElement
{
    public dataCharacterCollection data;
    public List<string> characterUIDs;

    public TextMeshProUGUI text_indicator;

    override public void UIUpdate()
    {
        for (int i = 0; i < data.characterSettings.Count; i++)
        {
            for (int j = 0; j < data.characterSettings[i].otherUID.Count; j++)
            {
                if (data.characterSettings[i].UID == characterUIDs[0] && data.characterSettings[i].otherUID[j] == characterUIDs[1])
                {
                    text_indicator.text = string.Format("{0}/{1}", data.characterSettings[i].relationship[j].curValue, data.characterSettings[i].relationship[j].capValue);
                }
            }
        }
    }
}
