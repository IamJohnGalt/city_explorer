﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "meta Game/Character Stats")]
[System.Serializable]
public class dataCharacterStats : ScriptableObject
{
    public string UID;

    [Header("Profile")]
    public string characterName;
    public Sprite characterIcon;
    public string traitUID;
    public string traitName;
    public Sprite traitIcon;
    public List<string> dialogs;

    [Header("World")]
    public string bornLocUID;
    public int startCoins;

    [Header("Stats")]
    public List<int> supplyCost;
    public List<int> supplyStamina;

    public string GetRandomDialog()
    {
        int rng = Random.Range(0, dialogs.Count);
        return dialogs[rng];
    }
}
