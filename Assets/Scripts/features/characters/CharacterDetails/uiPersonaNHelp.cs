﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class uiPersonaNHelp : uipfb_atom
{
    [SerializeField] uipfb_label_v2 levelTxt;
    [SerializeField] uipfb_label_v2 personaTxt;
    [SerializeField] uipfb_label_v2 helpTxt;

    public void SetPersonaNHelp(int level, string personaUID, string helpUID)
    {
        levelTxt.SetText(string.Format("Lv {0}", level));
        CharacterStat persona = dDataHub.hub.dCharacterStats.GetFromUID(personaUID);
        CharacterStat help = dDataHub.hub.dCharacterStats.GetFromUID(helpUID);
        personaTxt.SetText(persona.desc);
        helpTxt.SetText(help.desc);
    }
}
