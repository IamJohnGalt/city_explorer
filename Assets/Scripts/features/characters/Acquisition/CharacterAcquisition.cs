﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAcquisition : MonoBehaviour
{
    public static CharacterAcquisition controller;
    void Awake()
    {
        if (controller == null)
        {
            controller = this;
        }
        else if (controller != null)
        {
            Destroy(gameObject);
        }
    }
    [Header("Data")]
    public dataAcquisitionPopup data;

    [Header("Game Event")]
    [SerializeField] GameEvent StoryAcquired;

    public void AcquireStory(string characterUID, string currencyUID, int amount, string acquireMethod)
    {
        CurrencyManagement.wallet.AddCurrency(currencyUID, amount);
        data.character = CharacterManagement.controller.GetCharacterByUID(characterUID);
        data.currencyUID = currencyUID;
        data.amount = amount;
        data.acquireMethod = acquireMethod;
        StoryAcquired.Raise();
    }
    public void AcquisitionCheck_JournalSubmission()
    {
        //Debug.Log("AcquisitionCheck_Character1_lv0 called");
        //发表游记 (50%)
        if (CharacterManagement.controller.GetCurrentCharacter().characterUID != "character_1")
        {
            float rng = Random.Range(0f, 1f);
            if(rng > 0.5f)
            {
                AcquireStory("character_1", "currency_token_charac1", 1, "[发表游记]");
            }
        }
    }
    public void AcquisitionCheck_JournalSubmission_Book()
    {
        //Debug.Log("AcquisitionCheck_Character1_lv2 called");
        //发表游记类型为"书"(100%)
        if (CharacterManagement.controller.GetCurrentCharacter().characterUID != "character_1")
        {
            AcquireStory("character_1", "currency_token_charac1", 1, "[发表\"书\"类型游记]");
        }
    }
    public void AcquisitionCheck_Character2()
    {
        //Debug.Log("AcquisitionCheck_Character2 called");
        //游览中发现
        //到达景点的终点（50%）
        //call by buff_instant_currency_token_character_1_add
        if (CharacterManagement.controller.GetCurrentCharacter().characterUID != "character_2")
        {
            AcquireStory("character_2", "currency_token_charac2", 1, "[游览中发现]");
        }
    }
    public void AcquisitionCheck_Coin_Spending(string curUID, int amount)
    {
        //花费铜钱
        
        if (curUID == "currency_coin")
        {
            //Debug.Log("coin spending check w/ amount of " + amount);
            float rng = Random.Range(0, 1f);
            if (amount > 0 && rng < 0.05f)
            {
                AcquireStory("character_3", "currency_token_charac3", 1, "[花费铜钱]");
                //Character2_Acquisition_Reward();
            }
            else if (amount >= 40 && rng < 0.1f)
            {
                AcquireStory("character_3", "currency_token_charac3", 1, "[花费铜钱]");
                //Character2_Acquisition_Reward();
            }
            else if (amount >= 80 && rng < 0.2f)
            {
                AcquireStory("character_3", "currency_token_charac3", 1, "[花费铜钱]");
                //Character2_Acquisition_Reward();
            }
            else if (amount >= 160 && rng < 0.4f)
            {
                AcquireStory("character_3", "currency_token_charac3", 1, "[花费铜钱]");
                //Character2_Acquisition_Reward();
            }
        }
        
    }
    public void AcquisitionCheck_Coin_Gaining(string curUID, int amount)
    {
        //一次性获得200+铜钱
        if (curUID == "currency_coin" && amount >= 200)
        {
            AcquireStory("character_3", "currency_token_charac3", 1, "[获得大笔铜钱]");
            //Character2_Acquisition_Reward();
        }

    }
    public void AcquisitionCheck_Gain_Equipment(int tier)
    {
        //获得或升级装备(达到t2）
        if(tier >= 2)
        {
            AcquireStory("character_4", "currency_token_charac4", 1, "[获得高级装备]");
        }
        //Character3_Acquisition_Reward();
    }
    public void AcquisitionCheck_Use_Equipment()
    {
        //使用装备（15%）
        float rng = Random.Range(0, 1f);
        if (rng < 0.15f)
        {
            AcquireStory("character_4", "currency_token_charac4", 1, "[使用装备]");
            //Character3_Acquisition_Reward();
        }
    }
    public void AcquisitionCheck_Acquire_Collectible(int tier)
    {
        //获得风景（~10%）
        float rng = Random.Range(0, 1f);
        if (tier == 5 && rng < 0.4f)
        {
            AcquireStory("character_5", "currency_token_charac5", 1, "[获得风景]");
            //Character3_Acquisition_Reward();
        }
        else if (tier == 4 && rng < 0.2f)
        {
            AcquireStory("character_5", "currency_token_charac5", 1, "[获得风景]");
            //Character3_Acquisition_Reward();
        }
        else if (tier == 3 && rng < 0.1f)
        {
            AcquireStory("character_5", "currency_token_charac5", 1, "[获得风景]");
            //Character3_Acquisition_Reward();
        }
        else if (tier == 2 && rng < 0.05f)
        {
            AcquireStory("character_5", "currency_token_charac5", 1, "[获得风景]");
            //Character3_Acquisition_Reward();
        }
        else if (rng < 0.02f)
        {
            AcquireStory("character_5", "currency_token_charac5", 1, "[获得风景]");
            //Character3_Acquisition_Reward();
        }
    }
    public void AcquisitionCheck_Trigger_Collectible_Popup()
    {
        //出发偶遇风景（50%）
        float rng = Random.Range(0, 1f);
        if (rng < 0.5f)
        {
            AcquireStory("character_5", "currency_token_charac5", 1, "[触发风景偶遇]");
            //Character3_Acquisition_Reward();
        }
    }
    public void AcquisitionCheck_Trigger_Danger()
    {
        //触发危险（5%）
        float rng = Random.Range(0, 1f);
        if (rng < 0.05f)
        {
            AcquireStory("character_6", "currency_token_charac6", 1, "[触发危险]");
            //Character3_Acquisition_Reward();
        }
    }
    public void AcquisitionCheck_End_Park_No_Stamina()
    {
        //耗尽补给结束游览（50%）
        float rng = Random.Range(0, 1f);
        if (rng < 0.5f)
        {
            AcquireStory("character_6", "currency_token_charac6", 1, "[因补给耗尽而结束游览]");
            //Character3_Acquisition_Reward();
        }
    }
}
