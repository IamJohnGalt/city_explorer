﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Character/Acquisition Popup")]
public class dataAcquisitionPopup : ScriptableObject
{
    public dataCharacter character;
    public string currencyUID;
    public int amount;
    public string acquireMethod;
}
