﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;


public class uiAcquisitionPopup : MonoBehaviour
{
    [Header("Data")]
    [SerializeField] dataAcquisitionPopup data;

    [Header("Template")]
    [SerializeField] uiAcquisitionPopupItem template;

    [Header("DisplayGroup")]
    [SerializeField] GameObject popupContainer;

    [SerializeField] int onGoingPopup = 0;
    private float stayDuration = 2f;
    private float animDuration = 1f;

    private void Awake()
    {
        onGoingPopup = 0;
    }
    public void PopupInit()
    {
        GameObject item = Instantiate(template.gameObject, popupContainer.transform);
        item.GetComponent<uiAcquisitionPopupItem>().SetPopupItem(data);
        Anim_PopupAdd2Sequence(item);
        onGoingPopup += 1;
        //UIUpdate();
        //Anim_PopupSequence();
        //page.SetActive(true);
    }
    public void DestoryItem(GameObject popupItem)
    {
        Destroy(popupItem);
        onGoingPopup -= 1;
    }
    void Anim_PopupAdd2Sequence(GameObject popupItem)
    {
        Sequence anim_seq = DOTween.Sequence();
        anim_seq.AppendInterval((stayDuration + animDuration) * onGoingPopup);
        anim_seq.Append(popupItem.GetComponent<RectTransform>().DOAnchorPosX(600f, animDuration).SetEase(Ease.OutSine).From());
        anim_seq.AppendInterval(stayDuration);
        anim_seq.AppendCallback(()=>DestoryItem(popupItem));
    }
}
