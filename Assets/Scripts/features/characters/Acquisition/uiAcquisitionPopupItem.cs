﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class uiAcquisitionPopupItem : MonoBehaviour
{
    [Header("Children Object")]
    [SerializeField] ui_icon token;
    [SerializeField] ui_text desc;

    public void SetPopupItem(dataAcquisitionPopup data)
    {
        token.SetIcon(data.character.token);
        int tokenAcquired = CurrencyManagement.wallet.CheckCurrency(data.character.tokenUID);
        desc.SetText(string.Format("通过{0}<br>获得了{1}的{2}<br>距下一信物等级{3}/{4}", data.acquireMethod, data.character.characterClass, data.character.tokenName, data.character.curToken + tokenAcquired, data.character.GetCurTokenThreshold));
        gameObject.SetActive(true);
    }

}
