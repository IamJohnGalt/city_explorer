﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ui_mastershelf : MonoBehaviour
{
    [SerializeField] List<ui_characterbook> charatcers;

    [SerializeField] int currentSelection = -1;
    private void Awake()
    {
        currentSelection = -1;
    }
    public void CharatcerSelected(int index)
    {
        if (currentSelection == index)
        {
            return;
        }
        else
        {
            if(currentSelection >= 0)
            {
                charatcers[currentSelection].Anim_BookIn();
            }
            charatcers[index].Anim_BookOut();
            currentSelection = index;
        }
    }
}
