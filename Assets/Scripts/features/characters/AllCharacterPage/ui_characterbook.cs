﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ui_characterbook : MonoBehaviour
{
    [SerializeField] GameObject interactiveZone;
    [SerializeField] RectTransform book;
    [SerializeField] Image character;

    public void Anim_BookOut(bool isHover = false)
    {
        DOTween.Kill(book);
        if (!isHover)
        {
            book.DOAnchorPos(new Vector2(-30f, -73f), 1f);
            SFXPlayer.mp3.PlaySFX("wood_shelf");
        }
        else
        {
            book.DOAnchorPos(new Vector2(-10f, -24f), 0.3f);
        }
        
    }
    public void Anim_BookIn()
    {
        DOTween.Kill(book);
        book.DOAnchorPos(new Vector2(0, 0), 0.5f);
    }
    public void SetCharacter(Sprite characterSprite)
    {
        character.sprite = characterSprite;
    }
}
