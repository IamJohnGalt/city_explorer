﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterManagement : MonoBehaviour
{
    public static CharacterManagement controller;
    void Awake()
    {
        if (controller == null)
        {
            controller = this;
        }
        else if (controller != null)
        {
            Destroy(gameObject);
        }
    }

    [Header("Data")]
    public dataCharacterManagement data;
    [SerializeField] dataContentUnlocked unlockedData;

    [Header("Game Event")]
    [SerializeField] GameEvent CharacterSelectionStart;
    [SerializeField] GameEvent CharacterSelectionFinished;
    [SerializeField] GameEvent SessionStart;
    [SerializeField] GameEvent CharacterDetailUp;

    public void LoadCharacters()
    {
        for (int i = 0; i < data.allCharacters.Count; i++)
        {
            int.TryParse(SaveManager.controller.Inquire(string.Format("{0}_curFameLevel", data.allCharacters[i].characterUID)), out data.allCharacters[i].curFameLevel);
            int.TryParse(SaveManager.controller.Inquire(string.Format("{0}_curStoryLevel", data.allCharacters[i].characterUID)), out data.allCharacters[i].curStoryLevel);
            int.TryParse(SaveManager.controller.Inquire(string.Format("{0}_curFame", data.allCharacters[i].characterUID)), out data.allCharacters[i].curFame);
            int.TryParse(SaveManager.controller.Inquire(string.Format("{0}_curToken", data.allCharacters[i].characterUID)), out data.allCharacters[i].curToken);
            int.TryParse(SaveManager.controller.Inquire(string.Format("{0}_highestFame", data.allCharacters[i].characterUID)), out data.allCharacters[i].highestFame);
        }
        ForceUnlockFirstCharacter();
    }
    public void SaveCharacters()
    {
        for(int i = 0; i < data.allCharacters.Count; i++)
        {
            SaveManager.controller.Insert(string.Format("{0}_curFameLevel", data.allCharacters[i].characterUID), data.allCharacters[i].curFameLevel.ToString());
            SaveManager.controller.Insert(string.Format("{0}_curStoryLevel", data.allCharacters[i].characterUID), data.allCharacters[i].curStoryLevel.ToString());
            SaveManager.controller.Insert(string.Format("{0}_curFame", data.allCharacters[i].characterUID), data.allCharacters[i].curFame.ToString());
            SaveManager.controller.Insert(string.Format("{0}_curToken", data.allCharacters[i].characterUID), data.allCharacters[i].curToken.ToString());
            SaveManager.controller.Insert(string.Format("{0}_highestFame", data.allCharacters[i].characterUID), data.allCharacters[i].highestFame.ToString());
        }
        //SaveManager.controller.SaveToFile();
    }
    public void SetCharacterMAX()
    {
        for (int i = 0; i < data.allCharacters.Count; i++)
        {
            SaveManager.controller.Insert(string.Format("{0}_curFameLevel", data.allCharacters[i].characterUID), dConstants.Character.MAX_CHARACTER_LEVEL.ToString());
            SaveManager.controller.Insert(string.Format("{0}_curStoryLevel", data.allCharacters[i].characterUID), dConstants.Character.MAX_STORY_LEVEL.ToString());
            SaveManager.controller.Insert(string.Format("{0}_curFame", data.allCharacters[i].characterUID), 0.ToString());
            SaveManager.controller.Insert(string.Format("{0}_curToken", data.allCharacters[i].characterUID), 0.ToString());
            //SaveManager.controller.Insert(string.Format("{0}_highestFame", data.allCharacters[i].characterUID), data.allCharacters[i].highestFame.ToString());
        }
    }
    public void CharacterInit()
    {
        //check for prevsession currency
        LoadCharacters();
        EndSession.controller.ClaimPreviousPermReward();
        EndSession.controller.ShowNewContent();
        CharacterSelectionStart.Raise();
    }
    public void ViewTraveller(int index)
    {
        data.selectedIndex = index;
        data.selectedCharacterUID = data.allCharacters[index].characterUID;
        CharacterDetailUp.Raise();
    }
    public void ViewCurrentTraveller()
    {
        ViewTraveller(data.curIndex);
    }
    public void SelectTraveller(int index)
    {
        data.curIndex = index;
        data.currentCharacterUID = data.allCharacters[index].characterUID;
        ApplyAllCharacterBonus();
        //check feature gate "feature_gate_other_unlocked_characters"
        bool hasOtherUnlockedCharacters = false;
        for (int i = 1; i <= dConstants.Character.TOTAL_CHARACTERS; i++)
        {
            if (string.Format("character_{0}", i) != data.currentCharacterUID)
            {
                int curStoryLv = GetCharacterByUID(string.Format("character_{0}", i)).curStoryLevel;
                if (curStoryLv > 0 && curStoryLv < dConstants.Character.MAX_STORY_LEVEL)
                {
                    hasOtherUnlockedCharacters = true;
                    break;
                }
            }
        }
        ActiveBuffs.controller.ToggleFeatureGate("feature_gate_other_unlocked_characters", hasOtherUnlockedCharacters);
        //
        CharacterSelectionFinished.Raise();
        SessionStart.Raise();
    }
    public Sprite GetCurrentCharacterPortrait()
    {
        return data.curCharacter.GetPortrait;
    }
    public string GetCurrentCharacterTitle()
    {
        //CharacterStat level = dDataHub.hub.dCharacterStats.GetStat(data.currentCharacterUID, data.curCharacter.curFameLevel, CharacterStat.StatType.level);
        return data.curCharacter.characterClass;
    }
    public dataCharacter GetCurrentCharacter()
    {
        return data.curCharacter;
    }
    public dataCharacter GetCharacterByUID(string characterUID)
    {
        for(int i = 0; i < data.allCharacters.Count; i++)
        {
            if(data.allCharacters[i].characterUID == characterUID)
            {
                return data.allCharacters[i];
            }
        }
        return null;
    }
    void ApplyAllCharacterBonus()
    {
        //help
        for (int i = 0; i < data.allCharacters.Count; i++)
        {
            if(data.allCharacters[i].characterUID != data.currentCharacterUID)
            {
                for (int j = 0; j <= data.allCharacters[i].curStoryLevel; j++)
                {
                    CharacterStat help = dDataHub.hub.dCharacterStats.GetStat(data.allCharacters[i].characterUID, j, CharacterStat.StatType.help);
                    for (int k = 0; k < help.buffUIDs.Count; k++)
                    {
                        float value = 0;
                        float.TryParse(help.buffValues[k], out value);
                        if(help.buffUIDs[k] != "")
                        {
                            ActiveBuffs.controller.ActivateBuff(help.UID, help.characterUID, help.buffUIDs[k], value);
                        }
                    }
                }
            }
        }
        //persona
        for (int i = 0; i <= data.curCharacter.curStoryLevel; i++)
        {
            CharacterStat persona = dDataHub.hub.dCharacterStats.GetStat(data.curCharacter.characterUID, i, CharacterStat.StatType.persona);
            for (int j = 0; j < persona.buffUIDs.Count; j++)
            {
                float value = 0;
                float.TryParse(persona.buffValues[j], out value);
                if(persona.buffUIDs[j] != "")
                {
                    ActiveBuffs.controller.ActivateBuff(persona.UID, persona.characterUID, persona.buffUIDs[j], value);
                }
                
            }
        }
        //level
        for (int i = 0; i <= data.curCharacter.curFameLevel; i++)
        {
            CharacterStat level = dDataHub.hub.dCharacterStats.GetStat(data.curCharacter.characterUID, i, CharacterStat.StatType.level);
            for (int j = 0; j < level.buffUIDs.Count; j++)
            {
                float value = 0;
                float.TryParse(level.buffValues[j], out value);
                if(level.buffUIDs[j] != "")
                {
                    ActiveBuffs.controller.ActivateBuff(level.UID, level.characterUID, level.buffUIDs[j], value);
                }
                
            }
        }
        //other character taht have reached level max
        for (int i = 0; i < data.allCharacters.Count; i++)
        {
            if(data.allCharacters[i].curFameLevel == dConstants.Character.MAX_CHARACTER_LEVEL && data.allCharacters[i].characterUID != data.currentCharacterUID)
            {
                CharacterStat level = dDataHub.hub.dCharacterStats.GetStat(data.allCharacters[i].characterUID, dConstants.Character.MAX_CHARACTER_LEVEL, CharacterStat.StatType.level);
                for (int j = 0; j < level.buffUIDs.Count; j++)
                {
                    float value = 0;
                    float.TryParse(level.buffValues[j], out value);
                    if (level.buffUIDs[j] != "")
                    {
                        ActiveBuffs.controller.ActivateBuff(level.UID, level.characterUID, level.buffUIDs[j], value);
                    }
                }
            }
        }
    }
    public void EarnFame(int value)
    {
        data.curCharacter.curFame += value;
        if(value > data.curCharacter.highestFame)
        {
            data.curCharacter.highestFame = value;
        }
        int loopTimes = 0;
        while(loopTimes < dConstants.MAX_LOOP_TIMES)
        {
            if (!FameLevelUp(data.curCharacter))
            {
                break;
            }
        }
        //SaveCharacters();
    }
    public void GrantFame(string characUID, int value)
    {
        int selectedIndex = -1;
        for (int i = 0; i < dConstants.Character.TOTAL_CHARACTERS; i++)
        {
            if (characUID == data.allCharacters[i].characterUID)
            {
                selectedIndex = i;
            }
        }

        data.allCharacters[selectedIndex].curFame += value;
        if (value > data.allCharacters[selectedIndex].highestFame)
        {
            data.allCharacters[selectedIndex].highestFame = value;
        }
        int loopTimes = 0;
        while (loopTimes < dConstants.MAX_LOOP_TIMES)
        {
            if (!FameLevelUp(data.allCharacters[selectedIndex]))
            {
                break;
            }
        }
        //SaveCharacters();
    }
    public void EarnToken(List<int> tokenCount)
    {
        for(int i = 0; i < data.allCharacters.Count; i++)
        {
            data.allCharacters[i].curToken += tokenCount[i];
            int loopTimes = 0;
            while (loopTimes < dConstants.MAX_LOOP_TIMES)
            {
                if (!StoryLevelUp(data.allCharacters[i]))
                {
                    break;
                }
            }
        }
        //SaveCharacters();
    }
    bool FameLevelUp(dataCharacter character)
    {
        if(character.curFame >= character.GetCurFameThreshold && character.curFameLevel < dConstants.Character.MAX_CHARACTER_LEVEL)
        {
            CharacterStat level = dDataHub.hub.dCharacterStats.GetStat(character.characterUID, character.curFameLevel + 1, CharacterStat.StatType.level);
            if(level != null)
            {
                //only clear fame if it is not max
                if (character.curFameLevel != dConstants.Character.MAX_CHARACTER_LEVEL)
                {
                    character.curFame -= character.GetCurFameThreshold;
                }
                character.curFameLevel += 1;
                //log fame up
                if(level.title != "")
                {
                    UnlockedItem unlock = new UnlockedItem();
                    unlock.title = string.Format("{0}游记完成度提升至等级{1}，获得：", character.characterClass, level.level);
                    unlock.desc = string.Format("{0}：{1}", level.title, level.desc);
                    unlock.icon = character.avatar;
                    unlockedData.unlocks.Add(unlock);
                }
                //Debug.Log(string.Format("character({0}) levelel up to {1}", character.characterName, character.curFameLevel));
                return true;
            }
        }
        return false;
    }
    bool StoryLevelUp(dataCharacter character)
    {
        if (character.curToken >= character.GetCurTokenThreshold && character.curStoryLevel < dConstants.Character.MAX_STORY_LEVEL)
        {
            CharacterStat persona = dDataHub.hub.dCharacterStats.GetStat(character.characterUID, character.curStoryLevel + 1, CharacterStat.StatType.persona);
            if (persona != null)
            {
                //only clear token if it is not max
                if (character.curStoryLevel != dConstants.Character.MAX_STORY_LEVEL)
                {
                    character.curToken -= character.GetCurTokenThreshold;
                }
                if (character.curStoryLevel == 1 && character.curFameLevel == 0)
                {
                    character.curFameLevel = 1;
                }
                character.curStoryLevel += 1;
                //log token up
                UnlockedItem unlock = new UnlockedItem();
                unlock.title = string.Format("{0}信物等级提升至{1}，获得：", character.characterClass, data.curCharacter.GetStoryLevelText(persona.level));
                unlock.desc = string.Format("{0}：{1}", persona.title, persona.desc);
                unlock.icon = character.token;
                unlockedData.unlocks.Add(unlock);

                //log new help method
                CharacterStat help = dDataHub.hub.dCharacterStats.GetStat(character.characterUID, character.curStoryLevel, CharacterStat.StatType.help);
                if(help.desc != "")
                {
                    unlock = new UnlockedItem();
                    unlock.title = string.Format("{0}解锁了新的信物获取方式：", character.characterClass, data.curCharacter.GetStoryLevelText(persona.level));
                    unlock.desc = string.Format("{0}", help.desc);
                    unlock.icon = character.token;
                    unlockedData.unlocks.Add(unlock);
                }
                //Debug.Log(string.Format("story of ({0}) levelel up to {1}", character.characterName, character.curStoryLevel));
                return true;
            }
        }
        //Debug.Log(string.Format("check character {0} with {1}/{2}", character.characterUID, character.curToken, character.GetCurTokenThreshold));
        return false;
    }
    void ForceUnlockFirstCharacter()
    {
        if(data.allCharacters[0].curStoryLevel < 1)
        {
            data.allCharacters[0].curStoryLevel = 1;
            data.allCharacters[0].curFameLevel = 1;
        }
    }
}
