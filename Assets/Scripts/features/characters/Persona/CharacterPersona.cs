﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterPersona : MonoBehaviour
{
    public static CharacterPersona controller;

    [Header("Event")]
    [SerializeField] GameEvent personaLv2;
    [SerializeField] GameEvent personaLv3;
    [SerializeField] GameEvent personaLv4;
    [SerializeField] GameEvent personaLv5;

    void Awake()
    {
        if (controller == null)
        {
            controller = this;
        }
        else if (controller != null)
        {
            Destroy(gameObject);
        }
    }
    public void Persona_End_Park_Not_Final()
    {
        ActiveBuffs.controller.ActivateBuff("character_1_persona_2", "park_visit", "buff_max_stamina_add", 3f, 2);
        personaLv2.Raise();
    }
    public void Persona_End_Park_Is_Final()
    {
        ActiveBuffs.controller.ActivateBuff("character_1_persona_3", "", "buff_instant_currency_star_add", 2f);
        personaLv3.Raise();
    }
    public void Persona_Journal_Submit_Extra_Month()
    {
        CalendarManagement.calendar.AddTravelMonth(1);
        personaLv4.Raise();
    }
    public void Persona_End_Session_More_XP()
    {
        personaLv4.Raise();
    }
    public void Persona_Recover_Progress60()
    {
        gParkVisit.controller.ApplyBuff("", "", "stamina_add", "1", 0);
        personaLv2.Raise();
    }
    public void Persona_Discover_P20()
    {
        //每次行动有额外20%机会：探索 1
        float rng = Random.Range(0, 1f);
        if (rng < 0.2f)
        {
            gParkVisit.controller.ApplyBuff("", "", "draw_random_action", "1", 0);
            personaLv3.Raise();
        }
    }
    public void Persona_Action_Combo3()
    {
        //每个场景使用行动卡超过3张后，之后行动体力消耗 -3
        gParkVisit.controller.ApplyBuff("character_2_persona_4", "in_park_round", "buff_action_stamina_add", "-1", 1);
        personaLv4.Raise();
    }
    public void Persona_Enter_Local()
    {
        personaLv2.Raise();
    }
    public void Persona_Session_End_Coin2Star()
    {
        personaLv3.Raise();
    }
    public void Persona_Year_End_Coin()
    {
        personaLv4.Raise();
    }
    public void Persona_Recover_Action_Sta1()
    {
        gParkVisit.controller.ApplyBuff("", "", "stamina_add", "1", 0);
        personaLv2.Raise();
    }
    public void Persona_Refill_Equipment_FirstUse()
    {
        //gParkVisit.controller.ApplyBuff("", "", "restore_equipment", "1", 0);
        personaLv3.Raise();
    }
    public void Persona_Start_Journal_wOffical()
    {
        personaLv4.Raise();
    }
    public void Persona_Recover_AcquireView()
    {
        gParkVisit.controller.ApplyBuff("", "", "stamina_add", "1", 0);
        personaLv2.Raise();
    }
    public void Persona_Enter_Node()
    {
        personaLv3.Raise();
    }
    public void Persona_Park_Start_Collectible_Token()
    {
        gParkVisit.controller.ApplyBuff("", "", "add_collectible_counter", "1", 0);
        personaLv4.Raise();
    }
    public void Persona_Recover_Minor_Danger()
    {
        gParkVisit.controller.ApplyBuff("", "", "stamina_add", "1", 0);
        personaLv2.Raise();
    }
    public void Persona_Add_Star_Major_Danger()
    {
        gParkVisit.controller.ApplyBuff("character_6_persona_3", "in_park_round", "buff_action_progress_add", "50", 1);
        personaLv3.Raise();
    }
    public void Persona_Recover_Addition_Danger2_Activate()
    {
        ActiveBuffs.controller.DeactivateFrom("persona_recover_addition_danger2");
        ActiveBuffs.controller.ActivateBuff("character_6_persona_4", "persona_recover_addition_danger2", "buff_action_restore_stamina_add", 2, -1);
        personaLv4.Raise();
    }
    public void Persona_Recover_Addition_Danger2_Deactivate()
    {
        ActiveBuffs.controller.DeactivateFrom("persona_recover_addition_danger2");
    }
    //Deprectated
    /*
    public void Character2_PersonaLv2()
    {
        //到达村落节点时，恢复体力 2
        if (CharacterManagement.controller.GetCurrentCharacter().characterUID == "character_2"
             && ActiveBuffs.controller.CheckFeatureGate("feature_gate_character2_persona_lv2"))
        {
            gParkVisit.controller.ApplyBuff("character_2_persona_2", "", "stamina_add", "2", 0);
        }
    }
    public void Character2_PersonaLv3()
    {
        //可执行两次城镇行动，第二次铜钱消耗 +100%
        if (CharacterManagement.controller.GetCurrentCharacter().characterUID == "character_2"
             && ActiveBuffs.controller.CheckFeatureGate("feature_gate_character2_persona_lv3"))
        {
            ActiveBuffs.controller.ActivateBuff("character_2_persona_3", "in_chronicle_activity", "buff_coin_cost_on_chronicle_multi", 1f, 1);
        }
    }
    
    public void Character2_PersonaLv4()
    {
        //可在城镇中"设宴"，以延后旅程结束时间
        //feature gate unlock new activity
    }
    public void Character2_PersonaLv5()
    {
        //每年结束的时候，根据拥有铜钱，额外获得10%的铜钱
        //feature gate unlock new calendar check
    }
    
    public void Character3_PersonaLv2()
    {
        //使用装备，恢复体力 1
        if (CharacterManagement.controller.GetCurrentCharacter().characterUID == "character_3"
             && ActiveBuffs.controller.CheckFeatureGate("feature_gate_character3_persona_lv2"))
        {
            gParkVisit.controller.ApplyBuff("character_3_persona_2", "", "stamina_add", "1", 0);
        }
    }
    
    public void Character3_PersonaLv3()
    {
        //每个景点一次，立刻恢复第一个消耗的装备
        //handled in gParkvivsit equipment usage
    }
    public void Character3_PersonaLv4()
    {
        //前往东海的景点后，铜钱 +50
        if (CharacterManagement.controller.GetCurrentCharacter().characterUID == "character_3"
             && ActiveBuffs.controller.CheckFeatureGate("feature_gate_character3_persona_lv4"))
        {
            CurrencyManagement.wallet.AddCurrency_Setup("currency_coin", 50);
        }
    }
    */
}
