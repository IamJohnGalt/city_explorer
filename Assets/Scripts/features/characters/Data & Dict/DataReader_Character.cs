﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataReader_Character : GenericDataReader
{
    public Dict_CharacterStats dCharacterStat;
    public override void LoadFromXLS()
    {
        dCharacterStat.Reset();
        LoadStatData("Character - stats_exporter");
        FinishLoading();
    }

    void LoadStatData(string filename)
    {
        string[,] string_data = CSVReader.SplitCsvGrid(Resources.Load<TextAsset>("XLSData/" + filename).text);
        if (string_data[0, 0] != "UID")
        {
            Debug.LogError("Invalid File (No UID found): " + filename);
            return;
        }
        if (dCharacterStat == null)
        {
            Debug.Log("Null Ref on Data Dictionary");
        }
        for (int iRow = 1; iRow < string_data.GetUpperBound(1); iRow++)
        {
            CharacterStat stat = new CharacterStat();
            stat.UID = string_data[0, iRow];
            stat.title = string_data[1, iRow];
            stat.desc = string_data[2, iRow];
            stat.icon.objectArt = string_data[3, iRow];
            stat.characterUID = string_data[4, iRow];
            switch(string_data[5, iRow])
            {
                case "level":
                    stat.type = CharacterStat.StatType.level;
                    break;
                case "persona":
                    stat.type = CharacterStat.StatType.persona;
                    break;
                case "help":
                    stat.type = CharacterStat.StatType.help;
                    break;
                default:
                    break;
            }
            int.TryParse(string_data[6, iRow], out stat.level);
            stat.currency = string_data[7, iRow];
            int.TryParse(string_data[8, iRow], out stat.currencyThreshold);
            if (string_data[9, iRow] != "")
            {
                stat.buffUIDs.Add(string_data[9, iRow]);
                stat.buffValues.Add(string_data[10, iRow]);
            }
            if (string_data[11, iRow] != "")
            {
                stat.buffUIDs.Add(string_data[11, iRow]);
                stat.buffValues.Add(string_data[12, iRow]);
            }
            if (string_data[13, iRow] != "")
            {
                stat.buffUIDs.Add(string_data[13, iRow]);
                stat.buffValues.Add(string_data[14, iRow]);
            }
            if (string_data[15, iRow] != "")
            {
                stat.buffUIDs.Add(string_data[15, iRow]);
                stat.buffValues.Add(string_data[16, iRow]);
            }

            dCharacterStat.Add(stat.UID, stat);
        }
        dCharacterStat.DebugCount();
    }
}
