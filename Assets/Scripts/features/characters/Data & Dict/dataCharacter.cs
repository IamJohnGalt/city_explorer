﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[CreateAssetMenu(menuName = "Character/Character")]
public class dataCharacter : ScriptableObject
{
    [Header("Basic")]
    public string characterUID;
    public string characterName;
    public string characterClass;
    public string desc;
    public Sprite portrait;
    public Sprite portrait_locked;
    public string foodName;
    public Sprite food;
    public Sprite food_shape;
    public string tokenName;
    public string tokenUID;
    public Sprite token;
    public string token_reward;
    public Sprite avatar;
    public Color mainColor;

    [Header("Progression")]
    public int curFameLevel;
    public string GetFameLevelText(int fameLevel)
    {
        return string.Format("等级{0}", curFameLevel);
    }
    public int GetFameNeededByLevel(int fameLevel)
    {
        {
            int totalFameNeeded = 0;
            for (int i = 0; i < fameLevel; i++)
            {
                CharacterStat stat = dDataHub.hub.dCharacterStats.GetStat(characterUID, i + 1, CharacterStat.StatType.level);
                totalFameNeeded += stat.currencyThreshold;
            }
            return totalFameNeeded;
        }
    }
    public int curStoryLevel;
    public string GetStoryLevelText(int storyLevel)
    {
        switch (storyLevel)
        {
            case 1:
                return "壹";
            case 2:
                return "贰";
            case 3:
                return "叁";
            case 4:
                return "肆";
            case 5:
                return "伍";
            default:
                return "未解锁";
        }
    }
    public Sprite GetPortrait
    {
        get
        {
            if(curStoryLevel >= 1)
            {
                return portrait;
            }
            else
            {
                return portrait_locked;
            }
        }
    }

    public bool selectable { get { return curStoryLevel > 0; } }
    public int curFame;
    public int GetCurFameThreshold {
        get
        {
            return dDataHub.hub.dCharacterStats.GetStat(characterUID, curFameLevel, CharacterStat.StatType.level).currencyThreshold;
        }
    }
    public int GetFameAcquired
    {
        get
        {
            int totalFameAcquired = 0;
            for (int i = 0; i < dConstants.Character.MAX_CHARACTER_LEVEL; i++)
            {
                CharacterStat stat = dDataHub.hub.dCharacterStats.GetStat(characterUID, i + 1, CharacterStat.StatType.level);
                if (i < curFameLevel - 1)
                {
                    totalFameAcquired += stat.currencyThreshold;
                }
            }
            totalFameAcquired += curFame;
            return totalFameAcquired;
        }
    }
    public int GetFameNeeded
    {
        get
        {
            int totalFameNeeded = 0;
            for (int i = 0; i < dConstants.Character.MAX_CHARACTER_LEVEL; i++)
            {
                CharacterStat stat = dDataHub.hub.dCharacterStats.GetStat(characterUID, i + 1, CharacterStat.StatType.level);
                totalFameNeeded += stat.currencyThreshold;
            }
            return totalFameNeeded;
        }
    }
    public float GetTotalFameCompletion
    {
        get
        {
            int totalFameAcquired = 0;
            int totalFameNeeded = 0;
            for(int i = 0; i < dConstants.Character.MAX_CHARACTER_LEVEL; i++)
            {
                CharacterStat stat = dDataHub.hub.dCharacterStats.GetStat(characterUID, i+1, CharacterStat.StatType.level);
                if (i < curFameLevel - 1)
                {
                    totalFameAcquired += stat.currencyThreshold;
                }
                totalFameNeeded += stat.currencyThreshold;
            }
            totalFameAcquired += curFame;
            return (float)totalFameAcquired / (float)totalFameNeeded;
        }
    }
    //public int curXPThreshold;
    public int curToken;
    public int GetCurTokenThreshold {
        get
        {
            return dDataHub.hub.dCharacterStats.GetStat(characterUID, curStoryLevel, CharacterStat.StatType.persona).currencyThreshold;
        }
    }
    //public int curTokenThreshold;
    public int highestFame;

}
