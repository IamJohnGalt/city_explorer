﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CharacterStat
{
    public enum StatType { level, persona, help };
    public string UID;
    public string title;
    public string desc;
    public CardArt icon;
    public string characterUID;
    public StatType type;
    public int level;
    public string currency;
    public int currencyThreshold;
    public List<string> buffUIDs;
    public List<string> buffValues;

    public CharacterStat()
    {
        UID = "";
        title = "";
        desc = "";
        icon = new CardArt();
        characterUID = "";
        type = StatType.level;
        level = 0;
        currency = "";
        currencyThreshold = 0;
        buffUIDs = new List<string>();
        buffValues = new List<string>();
    }

}
