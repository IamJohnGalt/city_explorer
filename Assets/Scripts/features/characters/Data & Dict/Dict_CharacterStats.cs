﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[CreateAssetMenu(menuName = "Character/Dict/Stat")]
public class Dict_CharacterStats : GenericDataDict<CharacterStat>
{
    public override void DebugCount()
    {
        Debug.Log("Total of " + UIDList.Count + " stat items are added to Character Stat Dictionary");
    }
    public CharacterStat GetStat(string characterUID, int targetLevel, CharacterStat.StatType targetType)
    {
        List<CharacterStat> eligibleStat = (from stat in DataDict
                                            where
                     stat.Value.characterUID == characterUID
                     && stat.Value.type == targetType
                     && stat.Value.level == targetLevel
                                            select stat.Value).ToList();
        if(eligibleStat.Count == 0)
        {
            return null;
        }
        else if(eligibleStat.Count > 1)
        {
            Debug.LogError(string.Format("multiple stat found on characterUID({0}) with Level({1}) Type({2}", characterUID, targetLevel, targetType));
            return eligibleStat[0];
        }
        else
        {
            return eligibleStat[0];
        }
    }
}

