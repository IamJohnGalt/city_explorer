﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Character/All Characters")]
public class dataCharacterManagement : ScriptableObject
{
    [Header("All Characters")]
    public List<dataCharacter> allCharacters;
    public dataCharacter selectedCharacter { get { return selectedIndex >= 0 ? allCharacters[selectedIndex] : allCharacters[0]; } }
    public dataCharacter curCharacter { get { return curIndex >= 0 ? allCharacters[curIndex] : allCharacters[0]; } }

    [Header("Character Details")]
    public bool pageOn;
    public string selectedCharacterUID;
    public int selectedIndex;

    [Header("In-Session")]
    public string currentCharacterUID;
    public int curIndex;
}
