﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class FocusPointEffect : MonoBehaviour
{
    public static FocusPointEffect controller;

    [Header("Display Groups")]
    [SerializeField] GameObject content;
    [Header("Children Objects")]
    [SerializeField] Transform mask;

    [Header("Anim Constants")]
    [SerializeField] float MIN_SCALE = 1f;
    //[SerializeField] float MAX_SCALE = 10f;

    private bool inPlay;

    private void Awake()
    {
        if (controller == null)
        {
            controller = this;
        }
        else if (controller != null)
        {
            Destroy(gameObject);
        }
        content.SetActive(false);
    }
    public void StartFocusOn(Vector2 ScreenCoordinate, float size_start, float size_mid, float size_end, float transition_time_1, float transition_time_2, float pause_time_1, float pause_time_2, System.Action callback = null)
    {
        if (inPlay)
        {
            Debug.LogError("unable to start a focus point effect since it is inPlay");
            callback();
            return;
        }
        else
        {
            inPlay = true;
        }
        mask.localScale = Vector3.one * MIN_SCALE * size_start;
        mask.GetComponent<RectTransform>().anchoredPosition = ScreenCoordinate;
        content.SetActive(true);
        Sequence seq = DOTween.Sequence();
        seq.Append(mask.DOScale(Vector3.one * MIN_SCALE * size_mid, transition_time_1));
        seq.AppendInterval(pause_time_1);
        seq.Append(mask.DOScale(Vector3.one * MIN_SCALE * size_end, transition_time_2));
        seq.AppendInterval(pause_time_2);
        seq.AppendCallback(() => Anim_ConClude());
        if (callback != null)
        {
            seq.AppendCallback(() => callback());
        }
    }
    public void StartFocusOn(Transform ref_obj, float size_start, float size_mid, float size_end, float transition_time_1, float transition_time_2, float pause_time_1, float pause_time_2, System.Action callback = null)
    {
        if (inPlay)
        {
            Debug.LogError("unable to start a focus point effect since it is inPlay");
            callback();
            return;
        }
        else
        {
            inPlay = true;
        }
        mask.localScale = Vector3.one * MIN_SCALE * size_start;
        mask.transform.position = ref_obj.position;
        content.SetActive(true);
        Sequence seq = DOTween.Sequence();
        seq.Append(mask.DOScale(Vector3.one * MIN_SCALE * size_mid, transition_time_1));
        seq.AppendInterval(pause_time_1);
        seq.Append(mask.DOScale(Vector3.one * MIN_SCALE * size_end, transition_time_2));
        seq.AppendInterval(pause_time_2);
        seq.AppendCallback(() => Anim_ConClude());
        if (callback != null)
        {
            seq.AppendCallback(() => callback());
        }
    }
    void Anim_ConClude()
    {
        content.SetActive(false);
        inPlay = false;
    }
    
}
