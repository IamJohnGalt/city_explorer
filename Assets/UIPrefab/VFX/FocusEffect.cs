﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class FocusEffect : MonoBehaviour
{
    [SerializeField] float ScaleEnd;
    [SerializeField] Transform trans;
    private Sequence seq;
    private void OnEnable()
    {
        StartFocusEffect();
    }
    void StartFocusEffect()
    {
        seq.Rewind(); 
        seq = DOTween.Sequence();
        seq.Append(trans.DOScale(ScaleEnd, 1f).SetLoops(2, LoopType.Restart));
        seq.AppendCallback(() => StartFocusEffect());
    }
    private void OnDisable()
    {
        seq.Rewind();
    }
}
