﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class CollectibleAcquisition : MonoBehaviour
{
    public static CollectibleAcquisition controller;

    [Header("Data")]
    public string collectibleUID;
    public Color ArtMaskColor;
    public bool inPlay;

    [Header("Display Groups")]
    [SerializeField] GameObject content;
    //[SerializeField] GameObject cloud_group;
    [SerializeField] GameObject collectible_group;

    [Header("Children Objects")]
    [SerializeField] GameObject collectible;
    [SerializeField] Image art;
    [SerializeField] Image art_frame;
    [SerializeField] Image art_mask;
    [SerializeField] ui_text collectibleName;
    [SerializeField] Image text_mask;
    [SerializeField] ui_star star;
    [SerializeField] List<ui_tag> tags;

    [SerializeField] Image effect_frame;
    
    [SerializeField] List<Image> clouds;
    [SerializeField] List<Transform> cloud_pos;
    [SerializeField] List<float> cloud_move_x;

    [Header("Game Event")]
    [SerializeField] GameEvent EventReleaseCaptions;

    [Header("Anim Constants")]
    [SerializeField] float FRAME_EXPAND_TIME;
    [SerializeField] float CLOUD_MOVE_TIME;
    [SerializeField] float INFO_REVEAL_TIME;
    [SerializeField] float COLLECT_FLY_TIME;

    [SerializeField] journal_entry journalEntry;



    private void Awake()
    {
        if (controller == null)
        {
            controller = this;
        }
        else if (controller != null)
        {
            Destroy(gameObject);
        }
        content.SetActive(false);
    }
    public void EffectSkip()
    {
        content.SetActive(false);
        Anim_Conclude();
        //RestState();
    }
    public void AcquireCollectible(string _collectibleUID, System.Action callback = null)
    {
        if (_collectibleUID == "collectible_official_v1")
        {
            return;
        }
        if (inPlay)
        {
            Debug.LogError("unable to start a acquire Collectible effect since it is inPlay");
            return;
        }
        else
        {
            inPlay = true;
        }
        content.SetActive(true);
        SetCollectibleByUID(_collectibleUID);
        RestState();
        Anim_CollectibleAcquisition();
        //sfx
        SFXPlayer.mp3.PlaySFX("dizi_view_acquire");
        
    }
    void SetCollectibleByUID(string _collectibleUID)
    {
        if (_collectibleUID == "")
        {
            return;
        }
        else if (!dDataHub.hub.dCollectible.Contains(_collectibleUID))
        {
            Debug.LogError(string.Format("token({0}) is set to invisible due to missing token data by its UID({1})", gameObject.name, _collectibleUID));
            return;
        }
        collectibleUID = _collectibleUID;
        Collectible collectible = dDataHub.hub.dCollectible.GetFromUID(collectibleUID);
        art.sprite = uAssetReader.controller.GetByName(collectible.artAsset.objectArt);
        if (collectible.starValue > 0)
        {
            star.SetText(collectible.starValue);
            star.gameObject.SetActive(true);
        }
        else
        {
            star.gameObject.SetActive(false);
        }
        collectibleName.SetText(collectible.title);
        for (int i = 0; i < tags.Count; i++)
        {

            if (i < collectible.keywords.Count)
            {
                tags[i].SetTag(collectible.keywords[i]);
                tags[i].gameObject.SetActive(true);
            }
            else
            {
                tags[i].gameObject.SetActive(false);
            }
        }
    }
    public void Anim_CollectibleAcquisition()
    {
        //phase 1: expand frame
        effect_frame.transform.DOScaleX(1f, FRAME_EXPAND_TIME).OnComplete(()=> collectible_group.SetActive(true));
        //phase 2: cloud move
        for(int i = 0; i < clouds.Count; i++)
        {
            clouds[i].GetComponent<RectTransform>().DOAnchorPosX(cloud_move_x[i], CLOUD_MOVE_TIME).SetDelay(FRAME_EXPAND_TIME).SetRelative(true);
            clouds[i].DOFade(0f, CLOUD_MOVE_TIME).SetDelay(FRAME_EXPAND_TIME);
        }
        art_mask.DOFade(0f, CLOUD_MOVE_TIME).SetDelay(FRAME_EXPAND_TIME);
        //phase 3: reveal tags, star, text
        text_mask.DOFillAmount(1f, INFO_REVEAL_TIME).SetDelay(CLOUD_MOVE_TIME).OnStart(() => RevealInfo());
        //phase 4: frame_shrink
        effect_frame.transform.DOScale(0f, FRAME_EXPAND_TIME).SetDelay(FRAME_EXPAND_TIME + CLOUD_MOVE_TIME + INFO_REVEAL_TIME)
            .OnStart(() => Anim_CollectibleFly());
        Sequence seq = DOTween.Sequence();
        seq.AppendInterval(FRAME_EXPAND_TIME + CLOUD_MOVE_TIME + INFO_REVEAL_TIME + COLLECT_FLY_TIME);
        seq.AppendCallback(() => Anim_Conclude());
    }
    void RestState()
    {
        
        art_mask.color = ArtMaskColor;
        star.SetFade(0f);
        for (int i = 0; i < tags.Count; i++)
        {
            tags[i].SetFade(0f);
        }
        text_mask.fillAmount = 0f;
        effect_frame.transform.localScale = new Vector3(0f, 1f, 1f);
        for(int i = 0; i < clouds.Count; i++)
        {
            clouds[i].transform.position = cloud_pos[i].position;
            clouds[i].color = Color.white;
        }
        collectible_group.SetActive(false);
        //cloud_group.SetActive(false);
        
    }
    void RevealInfo()
    {
        for (int i = 0; i < tags.Count; i++)
        {
            tags[i].SetFade(1f, INFO_REVEAL_TIME);
        }
        star.SetFade(1f, INFO_REVEAL_TIME);
    }
    void Anim_CollectibleFly()
    {
        content.SetActive(false);
        if (inPlay)
        {
            CollectEffect.Collect(collectible, 1, new Vector2(1f, 0f), UICanvas.canvas.EffectLayer, collectible.transform, UIKeyElements.locator.Journal);
        }
    }
    void Anim_Conclude()
    {
        if (inPlay)
        {
            journalEntry.Anim_AcquireCollectible();
            Sequence seq = DOTween.Sequence();
            seq.AppendInterval(1f);
            seq.AppendCallback(() => EventReleaseCaptions.Raise());
        }
        inPlay = false;
    }
}
