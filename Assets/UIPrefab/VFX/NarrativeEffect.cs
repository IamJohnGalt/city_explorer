﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class NarrativeEffect : MonoBehaviour
{
    public static NarrativeEffect controller;

    [SerializeField] Image mask;
    [SerializeField] ui_text caption;
    private List<string> queuedCaptions;
    private bool inPlay;

    private void Awake()
    {
        if (controller == null)
        {
            controller = this;
        }
        else if (controller != null)
        {
            Destroy(gameObject);
        }
    }
    private void Start()
    {
        inPlay = false;
        caption.gameObject.SetActive(false);
        queuedCaptions = new List<string>();
    }
    public void QueueCaption(string txt)
    {
        queuedCaptions.Add(txt);
    }
    public void ReleaseCaption()
    {
        if (inPlay)
        {
            return;
        }
        if (queuedCaptions.Count > 0)
        {
            inPlay = true;
            string nextTxt = queuedCaptions[0];
            queuedCaptions.RemoveAt(0);
            PlayCaption(nextTxt);
        }
    }
    public void PlayCaption(string txt)
    {
        caption.SetText(txt);
        caption.SetFade(0f);
        mask.fillAmount = 0f;
        Sequence seq = DOTween.Sequence();
        seq.AppendCallback(() => caption.Anim_TextFadeIn());
        seq.AppendCallback(() => mask.DOFillAmount(1f, 3f));
        seq.AppendInterval(7f);
        seq.AppendCallback(() => caption.Anim_TextFadeOut());
        seq.AppendInterval(1f);
        seq.AppendCallback(() => CheckQueuedCaption());
        caption.gameObject.SetActive(true);
    }
    public void CheckQueuedCaption()
    {
        if (queuedCaptions.Count > 0)
        {
            string nextTxt = queuedCaptions[0];
            queuedCaptions.RemoveAt(0);
            PlayCaption(nextTxt);
        }
        else
        {
            inPlay = false;
        }
    }
    public void ClearAll()
    {
        inPlay = false;
        caption.gameObject.SetActive(false);
        queuedCaptions = new List<string>();
    }
}
