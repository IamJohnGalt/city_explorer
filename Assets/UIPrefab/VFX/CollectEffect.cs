﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CollectEffect : MonoBehaviour
{

    //public Transform target;
    public static float popupRange = 100f;
    public static float popupDelay = 0.1f;
    public static float popupDuration = 0.2f;
    public static float hoverDuration = 0f;
    public static float flyDuration = 1.2f;

    private static List<GameObject> particles;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    public static void Collect(GameObject refObj, int numOfItems, Vector2 sizeRange, Transform parent, Transform startPos, Transform endPos, float initialDelay = 0f, float endDelay = 0f)
    {
        particles = new List<GameObject>();
        GameObject tempObj;
        for (int i = 0; i < numOfItems; i++)
        {
            tempObj = Instantiate(refObj, parent) as GameObject;
            tempObj.transform.position = startPos.position;
            tempObj.transform.localScale = Vector3.one * sizeRange.x;
            particles.Add(tempObj);
        }
        float delayTime = initialDelay;
        foreach (GameObject particle in particles)
        {
            float angle = Random.Range(0f, Mathf.PI * 2);
            float distance = Random.Range(0.5f * popupRange, popupRange);
            Sequence tempSequence = DOTween.Sequence()
                .AppendInterval(delayTime)
                //.Append(particle.transform.DOScale(Vector3.zero, popupDuration).SetEase(Ease.Flash).From())
                //.Append(particle.transform.DOMove(startPos.position + distance * new Vector3(Mathf.Cos(angle), Mathf.Sin(angle), 0f), popupDuration)).SetEase(Ease.Flash)
                .AppendInterval(hoverDuration)
                .Append(particle.transform.DOMove(endPos.position, flyDuration)).SetEase(Ease.InCubic)
                .Join(particle.transform.DOScale(Vector3.one * sizeRange.y, flyDuration)).SetEase(Ease.InCubic)
                .AppendInterval(endDelay)
                .OnComplete(() => Destroy(particle))
                .SetAutoKill(true);
            delayTime += popupDelay;
        }
    }
}