﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class FocusMaskEffect : MonoBehaviour
{
    public static FocusMaskEffect controller;

    [Header("Display Groups")]
    [SerializeField] GameObject content;
    [SerializeField] GameObject transparent_mask;
    [SerializeField] GameObject focus_mask;
    [Header("Children Objects")]
    [SerializeField] Transform mask;

    [Header("Anim Constants")]
    [SerializeField] float MIN_SCALE = 1f;
    [SerializeField] float TRANSITION_TIME = 0.5f;
    [SerializeField] int accumClicks = 0;

    private void Awake()
    {
        if (controller == null)
        {
            controller = this;
        }
        else if (controller != null)
        {
            Destroy(gameObject);
        }
        ClearMask();
    }
    public void trans_mask_hit()
    {
        accumClicks++;
        if(accumClicks > 6)
        {
            ClearMask();
        }
    }
    public void MaskFocusOn(Transform ref_obj, float size)
    {
        mask.localScale = Vector3.one * MIN_SCALE * size;
        focus_mask.transform.position = ref_obj.position;
        Sequence seq = DOTween.Sequence();
        seq.Append(mask.DOScale(Vector3.one * MIN_SCALE * size * 2, TRANSITION_TIME).From().SetEase(Ease.InSine));
        
        focus_mask.SetActive(true);
        //transparent_mask.SetActive(false);
        content.SetActive(true);
    }
    public void TransparentMaskOn()
    {
        //focus_mask.SetActive(false);
        transparent_mask.SetActive(true);
        content.SetActive(true);
    }

    public void ClearMask()
    {
        content.SetActive(false);
        focus_mask.SetActive(false);
        transparent_mask.SetActive(false);
        accumClicks = 0;
    }
}
