﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class FloatTextEffect : MonoBehaviour
{
    public static float FLOAT_DISTANCE_Y = 50f;
    public static float FLOAT_DUTRATION = 1f;


    public static FloatTextEffect controller;

    private void Awake()
    {
        if (controller == null)
        {
            controller = this;
        }
        else if (controller != null)
        {
            Destroy(gameObject);
        }
    }

    [SerializeField] ui_icon_text floatSetTemplate;

    public void Float(Transform startPos, string iconName, string value, float relativeSize)
    {
        ui_icon_text tempObj = Instantiate(floatSetTemplate, UICanvas.canvas.EffectLayer);
        tempObj.transform.position = startPos.position;
        tempObj.transform.localScale = Vector3.one * relativeSize;

        tempObj.majorStr.SetText(value);
        tempObj.icon.SetIcon(iconName);

        tempObj.GetComponent<RectTransform>().DOAnchorPosY(FLOAT_DISTANCE_Y * relativeSize, FLOAT_DUTRATION).SetRelative(true).OnComplete(()=>Destroy(tempObj.gameObject));
        tempObj.majorStr.SetFade(0f, FLOAT_DUTRATION);
        tempObj.icon.Anim_FadeTo(0f, FLOAT_DUTRATION);
        tempObj.gameObject.SetActive(true);

    }
}
