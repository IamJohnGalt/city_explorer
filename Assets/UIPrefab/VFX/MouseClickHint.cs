﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MouseClickHint : MonoBehaviour
{
    public static MouseClickHint controller;

    [SerializeField] ui_icon mouseIcon;

    private Sequence seq_click;

    private void Awake()
    {
        if (controller == null)
        {
            controller = this;
        }
        else if (controller != null)
        {
            Destroy(gameObject);
        }
        mouseIcon.gameObject.SetActive(false);
    }
    public void ShowHint(Transform ref_obj)
    {
        transform.position = ref_obj.position;
        mouseIcon.gameObject.SetActive(true);
        AnimLoop_Click();
    }
    public void ClearHint()
    {
        mouseIcon.gameObject.SetActive(false);
    }
    void AnimLoop_Click()
    {
        if (mouseIcon.gameObject.activeSelf)
        {
            mouseIcon.GetComponent<RectTransform>().anchoredPosition = new Vector2(60, -80f);
            mouseIcon.SetFade(0f);
            seq_click.Kill();
            seq_click = DOTween.Sequence();
            seq_click.Append(mouseIcon.GetComponent<RectTransform>().DOAnchorPos(new Vector2(20f,-30f), 1.2f));
            seq_click.InsertCallback(0f, () => mouseIcon.Anim_FadeTo(1f, 1.5f));
            seq_click.AppendInterval(0.8f);
            seq_click.AppendCallback(() => AnimLoop_Click());
        }
    }
}
