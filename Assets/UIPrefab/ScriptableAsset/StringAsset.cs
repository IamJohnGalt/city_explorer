﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "Asset/String Asset")]
public class StringAsset : GenericAsset<string>
{

}
