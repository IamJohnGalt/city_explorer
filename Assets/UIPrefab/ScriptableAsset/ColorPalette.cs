﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Asset/Color Palette")]
public class ColorPalette : GenericAsset<Color>
{

}
