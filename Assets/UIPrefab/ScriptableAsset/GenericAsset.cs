﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenericAsset<T> : ScriptableObject
{
    public List<T> Assets = new List<T>();
    public List<string> AssetNames = new List<string>();

    public T GetAsset(int index)
    {
        return Assets[index];
    }
    public T GetAsset(string asset_name)
    {
        int icon_index = 0;
        List<T> eligibles = new List<T>(); 
        foreach (string name in AssetNames)
        {
            if (name == asset_name)
            {
                eligibles.Add(Assets[icon_index]);
                
            }
            icon_index++;
        }
        if(eligibles.Count == 0)
        {
            Debug.LogError("Error when find asset by its name: " + asset_name);
            //should return default error icon
            return default(T);
        }
        else
        {
            int rngIndex = Random.Range(0, eligibles.Count);
            return eligibles[rngIndex];
        }
        
    }
}
