﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "Asset/Sprite Asset")]
public class SpriteAsset : GenericAsset<Sprite>
{
    
}
