﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Asset/Sound Asset")]
public class SoundAsset : GenericAsset<AudioClip>
{
    
}
