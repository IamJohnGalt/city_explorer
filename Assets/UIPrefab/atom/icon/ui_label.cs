﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class ui_label : MonoBehaviour
{
    [SerializeField] ui_text str;
    public Image frame;

    public void SetText(string txt)
    {
        str.SetText(txt);
        gameObject.SetActive(txt != "");
    }
    public void SetTextColor(Color clr)
    {
        str.SetTextColor(clr);
    }
    public void SetFade(float alpha, float transitionTime = 0f)
    {
        if(transitionTime == 0f)
        {
            Color sptColor = frame.color;
            Color txtColor = str.GetComponent<TextMeshProUGUI>().color;
            frame.color = new Color(sptColor.r, sptColor.g, sptColor.b, alpha);
            str.SetFade(alpha);
        }
        else
        {
            frame.DOFade(alpha, transitionTime);
            str.SetFade(alpha, transitionTime);
        }
        
    }
    public void SetFrame(Sprite sprt)
    {
        frame.sprite = sprt;
    }
    public void Anim_TextFadeIn()
    {
        str.Anim_TextFadeIn();
    }
    public void Anim_TextFadeOut()
    {
        str.Anim_TextFadeOut();
    }
    public void Anim_Explode()
    {
        transform.DOScale(Vector3.one * 2f, 0.5f).SetEase(Ease.OutQuad);
        frame.DOFade(0f, 0.5f).SetEase(Ease.OutQuad);
        str.Anim_Explode();
    }
}
