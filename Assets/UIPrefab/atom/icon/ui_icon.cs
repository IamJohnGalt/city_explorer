﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ui_icon : MonoBehaviour
{
    public GameObject InteractiveArea;
    public Image frame;
    public Image icon;
    public Image mask;

    [Header("Animation Constants")]
    static private float popSizeAtMax = 1.5f;
    static private float earnSizeAtMax = 1.2f;
    static private float loseSizeAtMin = 0.8f;
    static private float floatingIconDistanceRatioByY = 0.6f;
    static private float animationTimeTickOffset = 0.1f;
    static private float animationTimePerTick = 0.3f;

    public void SetInteractive(bool interactable)
    {
        InteractiveArea.SetActive(interactable);
    }
    public void SetIcon(Sprite spt = null)
    {
        icon.sprite = spt;
        icon.gameObject.SetActive(spt != null);
    }
    public void SetIcon(string filename)
    {
        Sprite sprt = uAssetReader.controller.GetByName(filename);
        SetIcon(sprt);
    }
    public void SetIconColor(Color clr)
    {
        icon.color = clr;
    }
    public void SetFrame(Sprite spt = null)
    {
        frame.sprite = spt;
        frame.gameObject.SetActive(spt != null);
    }
    public void SetFrame(string filename)
    {
        Sprite sprt = uAssetReader.controller.GetByName(filename);
        SetFrame(sprt);
    }
    public void SetFrameColor(Color clr)
    {
        frame.color = clr;
    }
    public void SetIconFill(float pct)
    {
        icon.fillAmount = pct;
    }
    public void SetFrameFill(float pct)
    {
        frame.fillAmount = pct;
    }
    public void HoverOn()
    {
        mask.gameObject.SetActive(true);
    }
    public void HoverOff()
    {
        mask.gameObject.SetActive(false);
    }
    public void SetFade(float alpha)
    {
        //Debug.Log(string.Format("icon set fade called"));
        Color curColor = icon.color;
        Color clr = new Color(curColor.r, curColor.g, curColor.b, alpha);
        icon.color = clr;

        curColor = frame.color;
        clr = new Color(curColor.r, curColor.g, curColor.b, alpha);
        frame.color = clr;
    }
    public void Anim_FadeTo(float alpha, float duration)
    {
        icon.DOFade(alpha, duration);
        frame.DOFade(alpha, duration);
    }
    public void Anim_Explode()
    {
        transform.DOScale(Vector3.one * 2f, 0.5f).SetEase(Ease.OutQuad);
        icon.DOFade(0f, 0.5f).SetEase(Ease.OutQuad);
        frame.DOFade(0f, 0.5f).SetEase(Ease.OutQuad);
    }
    public void Anim_Earn()
    {
        //DOTween.Kill(gameObject);
        GameObject particle = Instantiate(icon.gameObject, transform);
        particle.GetComponent<Image>().DOFade(0f, animationTimePerTick).From();
        //Debug.Log("size Delta on Icon is " + icon.GetComponent<RectTransform>().rect.height);
        particle.GetComponent<RectTransform>().DOAnchorPosY(
            icon.GetComponent<RectTransform>().rect.height * floatingIconDistanceRatioByY, 
            animationTimePerTick)
            .From().SetRelative(true).OnComplete(()=>Destroy(particle));
        GetComponent<RectTransform>().DOScale(earnSizeAtMax, animationTimePerTick / 2f).SetDelay(animationTimeTickOffset);
        GetComponent<RectTransform>().DOScale(1f, animationTimePerTick / 2f).SetDelay(animationTimeTickOffset + animationTimePerTick / 2f);
    }
    public void Anim_Lose()
    {
        GameObject particle = Instantiate(icon.gameObject, transform);
        particle.GetComponent<Image>().DOFade(0f, animationTimePerTick).SetDelay(animationTimeTickOffset);
        particle.GetComponent<RectTransform>().DOAnchorPosY(
            icon.GetComponent<RectTransform>().rect.height * floatingIconDistanceRatioByY,
            animationTimePerTick)
            .SetRelative(true).OnComplete(() => Destroy(particle)).SetDelay(animationTimeTickOffset);
        GetComponent<RectTransform>().DOScale(loseSizeAtMin, animationTimePerTick / 2f);
        GetComponent<RectTransform>().DOScale(1f, animationTimePerTick / 2f).SetDelay(animationTimePerTick / 2f);
    }
    public void Anim_Check()
    {
        GetComponent<RectTransform>().DOScale(popSizeAtMax, animationTimePerTick);
        GetComponent<RectTransform>().DOScale(1f, animationTimePerTick).SetDelay(animationTimePerTick);
    }
    public void Anim_Pop()
    {
        GetComponent<RectTransform>().DOScale(popSizeAtMax * 1.5f, animationTimePerTick);
        GetComponent<RectTransform>().DOScale(1f, animationTimePerTick).SetDelay(animationTimePerTick);
    }
    public void Anim_IconFlashing(float flashingTime)
    {
        Color curColor = icon.color;
        Color clr = new Color(curColor.r, curColor.g, curColor.b, 1f);
        icon.color = clr;
        icon.DOFade(0f, flashingTime).SetLoops(-1, LoopType.Yoyo);
    }
    public void Anim_ScaleUpByValue(float endScale)
    {
        transform.DOScale(Vector3.one * endScale, 0.3f).SetEase(Ease.OutBack);
    }
    public void Anim_Highlighted()
    {
        transform.DOScale(Vector3.one * 1.1f, 0.8f).SetEase(Ease.InOutSine).SetLoops(-1, LoopType.Yoyo);
    }
}
