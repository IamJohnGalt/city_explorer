﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class ui_text : MonoBehaviour
{
    public TextMeshProUGUI str;
    public string curStr;
    //public Image frame;
    public void SetText(string txt)
    {
        curStr = txt;
        //additional check to replace "dynamnic_stamina_icon"
        string finalized = ReplaceSpecialKeywords(txt);
        str.text = finalized;
        gameObject.SetActive(txt != "");
    }
    public void SetTextColor(Color clr)
    {
        float curAlpha = str.color.a;
        str.color = new Color(clr.r, clr.g, clr.b, curAlpha);
    }
    public void SetFade(float alpha, float transitionTime = 0f)
    {
        if(transitionTime == 0f)
        {
            Color txtColor = str.GetComponent<TextMeshProUGUI>().color;
            str.color = new Color(txtColor.r, txtColor.g, txtColor.b, alpha);
        }
        else
        {
            str.DOFade(alpha, transitionTime);
        }
    }
    public void ResetText()
    {
        str.SetText(curStr);
    }
    public void Anim_SetToRandomNumber()
    {
        if(curStr == "")
        {
            curStr = str.text;
        }
        int digit = curStr.Length;
        string rngNum = "";
        for(int i = 0; i < digit; i++)
        {
            rngNum += Random.Range(0, 9).ToString();
        }
        //Debug.Log("set text to " + rngNum);
        str.SetText(rngNum);
    }
    public void Anim_TextFadeIn()
    {
        str.DOFade(1f, 0.5f);
    }
    public void Anim_TextFadeOut()
    {
        str.DOFade(0f, 0.5f);
    }
    public void Anim_Explode()
    {
        transform.DOScale(Vector3.one * 2f, 0.5f).SetEase(Ease.OutQuad);
        //frame.DOFade(0f, 0.5f).SetEase(Ease.OutQuad);
        str.DOFade(0f, 0.5f).SetEase(Ease.OutQuad);
    }
    string ReplaceSpecialKeywords(string input)
    {
        //Debug.Log("CharacterManagement.controller.GetCurrentCharacter().food.name is" + CharacterManagement.controller.GetCurrentCharacter().food.name);
        return input.Replace("dynamic_current_stamina", CharacterManagement.controller.GetCurrentCharacter().food.name);
    }
}
