﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ui_buff_tooltip : MonoBehaviour
{
    [SerializeField] List<ui_label> tooltips;
    private List<string> DisplayedUIDs;

    public void SetBuffTooptips(List<string> buffUIDs)
    {
        DisplayedUIDs = new List<string>();
        for (int i = 0; i < tooltips.Count; i++)
        {
            tooltips[i].gameObject.SetActive(false);
            if (i < buffUIDs.Count)
            {
                string buffUID = buffUIDs[i];
                if (dDataHub.hub.buffDict.Contains(buffUID) && !DisplayedUIDs.Contains(buffUID))
                {
                    BuffDefinition bDef = dDataHub.hub.buffDict.GetFromUID(buffUID);
                    if (bDef.buffDesc != "")
                    {
                        tooltips[i].SetText(string.Format("<b>{0}</b><sprite name=\"{1}\"><br>{2}",
                            bDef.buffName,
                            bDef.buffIcon,
                            bDef.buffDesc));
                        DisplayedUIDs.Add(buffUID);
                        tooltips[i].gameObject.SetActive(true);
                    }
                }
            }
        }
    }
    public void ClearBuffTooltips()
    {
        Destroy(gameObject);
    }
    public void Anim_FadeIn()
    {
        for (int i = 0; i < tooltips.Count; i++)
        {
            tooltips[i].SetFade(0f);
            tooltips[i].SetFade(1f, 0.6f);
        }
    }
}
