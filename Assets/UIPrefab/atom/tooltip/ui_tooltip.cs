﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ui_tooltip : MonoBehaviour
{
    public delegate void btn_hit_func();
    public btn_hit_func ref_tooltip_use_func;

    [SerializeField] GameObject expand_group;

    [SerializeField] ui_text title;
    [SerializeField] ui_text desc;
    [SerializeField] ui_button btn;

    public void btn_hit()
    {
        ref_tooltip_use_func?.Invoke();
    }
    public void SetToolTip(string _title, string _desc)
    {
        title.SetText(_title);
        if(_title == "")
        {
            desc.str.alignment = TMPro.TextAlignmentOptions.Left;
        }
        else
        {
            desc.str.alignment = TMPro.TextAlignmentOptions.Center;
        }
        desc.SetText(_desc);
        btn.gameObject.SetActive(false);
    }
    public void AddButton(bool isActive, string btn_text, System.Action use_func)
    {
        btn.majorStr.SetText(btn_text);
        if (isActive)
        {
            btn.EnableButton();
        }
        else
        {
            btn.DisableButton();
        }
        ref_tooltip_use_func = () => use_func();
        btn.gameObject.SetActive(true);
    }
    public void Anim_Popup()
    {
        expand_group.transform.localScale = Vector3.one;
        expand_group.transform.DOScale(Vector3.zero, 0.2f).From().SetEase(Ease.OutSine);
        gameObject.SetActive(true);
    }
    public void HideToolTip()
    {
        gameObject.SetActive(false);
    }
}
