﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ui_button : MonoBehaviour
{
    [SerializeField] GameObject interactiveZone;
    [SerializeField] Image frame;
    [SerializeField] Image mask;
    public ui_text minorStr;
    public ui_text majorStr;
    private void OnEnable()
    {
        HoverOff();
    }
    public void DisableButton()
    {
        interactiveZone.SetActive(false);
        frame.GetComponent<Shadow>().enabled = false;
        Color clr = frame.color;
        frame.color = new Color(clr.r, clr.g, clr.b, 0.5f);
        HoverOff();
    }
    public void EnableButton()
    {
        interactiveZone.SetActive(true);
        frame.GetComponent<Shadow>().enabled = true;
        Color clr = frame.color;
        frame.color = new Color(clr.r, clr.g, clr.b, 1f);
        HoverOff();
    }
    public void HoverOn()
    {
        mask.gameObject.SetActive(true);
        SFXPlayer.mp3.PlaySFX("button_hover");
    }
    public void HoverOff()
    {
        mask.gameObject.SetActive(false);
    }
}
