﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ui_reward : MonoBehaviour
{
    public delegate void btn_hit_func(int index);
    public btn_hit_func ref_action_hit_func;

    [Header("Data")]
    public string tokenUID;
    public dConstants.ParkConst.TokenStatus status;

    [Header("Display Group")]
    [SerializeField] GameObject content;

    [Header("Children Objects")]
    [SerializeField] GameObject interactiveArea;
    [SerializeField] Image frame;
    [SerializeField] Image art;
    [SerializeField] Image mask;
    [SerializeField] ui_text rewardName;
    [SerializeField] ui_text desc;
    //[SerializeField] uipfb_pin pin;

    public void OnTokenClicked(int index)
    {
        switch (status)
        {
            case dConstants.ParkConst.TokenStatus.playable:
                ref_action_hit_func?.Invoke(index);
                status = dConstants.ParkConst.TokenStatus.played;
                Anim_MoveDown();
                break;
            case dConstants.ParkConst.TokenStatus.played:
                //Anim_Shake();
                break;
            default:
                break;
        }
    }

    public void SetTokenByUID(string rewardUID, dConstants.ParkConst.TokenStatus _status = dConstants.ParkConst.TokenStatus.hidden)
    {
        tokenUID = rewardUID;
        status = _status;
        if (!dDataHub.hub.dParkReward.Contains(rewardUID))
        {
            gameObject.SetActive(false);
            Debug.LogError(string.Format("token({0}) is set to invisible due to missing token data by its UID({1})", gameObject.name, rewardUID));
            return;
        }

        if (_status == dConstants.ParkConst.TokenStatus.preview)
        {
            art.sprite = uAssetReader.controller.GetByName("token_question");
            rewardName.SetText("未知奖励");
            rewardName.SetFade(0f);
            //number.SetText("");
            desc.SetText("");
            gameObject.SetActive(true);
        }
        else if(_status == dConstants.ParkConst.TokenStatus.inactive)
        {
            ParkReward reward = dDataHub.hub.dParkReward.GetFromUID(rewardUID);
            art.sprite = uAssetReader.controller.GetByName(reward.artAsset.objectArt);
            rewardName.SetText(reward.title);
            desc.SetText(string.Format("{0}", reward.desc));
            interactiveArea.SetActive(false);
            SetFade(0.3f);
            frame.GetComponent<Shadow>().enabled = false;
            gameObject.SetActive(true);
        }
        else if (_status == dConstants.ParkConst.TokenStatus.played)
        {
            ParkReward reward = dDataHub.hub.dParkReward.GetFromUID(rewardUID);
            art.sprite = uAssetReader.controller.GetByName(reward.artAsset.objectArt);
            rewardName.SetText(reward.title);
            desc.SetText(string.Format("{0}", reward.desc));
            interactiveArea.SetActive(false);
            SetFade(1f);
            frame.GetComponent<Shadow>().enabled = true;
            gameObject.SetActive(true);
        }
        else if(_status == dConstants.ParkConst.TokenStatus.playable)
        {
            ParkReward reward = dDataHub.hub.dParkReward.GetFromUID(rewardUID);
            art.sprite = uAssetReader.controller.GetByName(reward.artAsset.objectArt);
            rewardName.SetText(reward.title);
            desc.SetText(string.Format("{0}", reward.desc));
            interactiveArea.SetActive(true);
            SetFade(1f);
            frame.GetComponent<Shadow>().enabled = true;
            gameObject.SetActive(true);
        }
        else if(_status == dConstants.ParkConst.TokenStatus.hidden)
        {
            gameObject.SetActive(false);
        }
        else
        {
            Debug.LogError("unsupported status appears on ui_reward");
            gameObject.SetActive(false);
        }
        HoverOff();
    }
    public void HoverOn()
    {
        mask.gameObject.SetActive(true);
        SFXPlayer.mp3.PlaySFX("button_hover");
    }
    public void HoverOff()
    {
        mask.gameObject.SetActive(false);
    }
    public void Anim_MoveDown()
    {
        //pin.Anim_Pin(0.3f);
        DOTween.To(() => frame.GetComponent<Shadow>().effectDistance, x => frame.GetComponent<Shadow>().effectDistance = x, new Vector2(3f, -3f), 0.3f);
        content.GetComponent<RectTransform>().DOAnchorPos(new Vector2(7f, -7f), 0.3f).SetRelative();
        //group_tags.GetComponent<RectTransform>().DOAnchorPos(new Vector2(-7f, 7f), 0.3f);
    }
    public void ResetUsage()
    {
        frame.GetComponent<Shadow>().effectDistance = new Vector2(5f, -5f);
        content.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
    }
    void SetFade(float alpha)
    {
        Color clr = art.color;
        art.color = new Color(clr.r, clr.g, clr.b, alpha);

        clr = frame.color;
        frame.color = new Color(clr.r, clr.g, clr.b, alpha);
    }
}
