﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEditor;

public class uiParkNode : uipfb_atom
{
    public enum NodeStatus { visited, passed, current, next, hidden};
    public delegate void btn_hit_func(int index);
    public btn_hit_func ref_reward_hit_func;
    [SerializeField] dataParkVisit gameplayData;
    public string narrative;

    [Header("Data")]
    public string nodeUID;
    public int collectibleBaseTier;
    public List<string> rewardUIDs;
    public List<bool> rewardClaimed;
    public List<string> viewUIDs;
    public List<bool> viewClaimed;
    public List<string> collectibleUIDs;
    public List<bool> collectibleClaimed;
    public string claimedViewUID;

    public NodeStatus status;
    public bool onClick = false;
    [SerializeField] string statusStr;

    [Header("Popup")]
    //[SerializeField] uiParkRewardPopup popupTemplate;
    //[SerializeField] Transform popupLayer;

    [Header("Structure Info")]
    public string pfbUID;
    public intRange tier;

    public int layer;
    public bool isFinal = false;
    public bool isRevealed = false;
    public List<uiParkRoute> inRoutes;
    public List<uiParkRoute> outRoutes;


    [Header("Assets")]
    //public ColorPalette palette;
    public SpriteAsset nodeFrames;

    [Header("Group Objects")]
    [SerializeField] GameObject content_group;
    //[SerializeField] GameObject expand_group;

    [Header("Children Objects")]
    [SerializeField] ui_icon node_icon;
    [SerializeField] ui_icon_tier node_tier;
    [SerializeField] GameObject view_group;
    [SerializeField] Image view;
    [SerializeField] ui_icon_tier view_tier;
    public Transform popup_pos;

    [Header("UI Event")]
    [SerializeField] GameEvent OnNodeStart;
    [SerializeField] GameEvent OnNextNodeStart;
    [SerializeField] GameEvent ClearPathHighlight;

    private Tween animTween;
    void Awake()
    {
        /*if (popupObject == null)
        {
            popupObject = Instantiate<uiParkRewardPopup>(popupTemplate, popupLayer);
            popupObject.SetVisible(false);
        }*/
        //popupObject.transform.parent = popupLayer;
        //popupObject.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
    }
    public void SetNodeUID(string nodeDataUID)
    {
        if (!dDataHub.hub.dParkNode.Contains(nodeDataUID))
        {
            SetVisible(false);
            Debug.LogError(string.Format("node({0}) is set to invisible due to missing node data by its UID({1})", gameObject.name, nodeDataUID));
            return;
        }
        ParkNode node = dDataHub.hub.dParkNode.GetFromUID(nodeDataUID);
        nodeUID = nodeDataUID;
        narrative = "";
        collectibleBaseTier = Random.Range(node.collectibleLv.minInt, node.collectibleLv.maxInt+1);
        if (node.artAsset.objectArt != "")
        {
            node_icon.SetIcon(node.artAsset.objectArt);
            //popupObject.node_type.SetIcon(Resources.Load<Sprite>(string.Format("Art/icon/{0}", node.artAsset.objectArt)));
        }
        else
        {
            node_icon.SetIcon();
            //popupObject.node_type.SetIcon();
        }
        if(collectibleBaseTier > 0)
        {
            node_tier.SetTier(Mathf.Min(5, collectibleBaseTier));
            node_tier.gameObject.SetActive(true);
        }
        else
        {
            node_tier.gameObject.SetActive(false);
        }
        
        //nodePoolIndex = poolIndex;
        //popupObject.node_type.SetText(node.title);
        //popupObject.preview_desc.SetText(node.desc);

    }
    public void AddNarrative(string bit)
    {
        if(narrative == "")
        {
            narrative += bit;
        }
        else
        {
            narrative += "，" + bit;
        }
    }
    public void SetNodeReward(List<string> _rewardUIDs = null, List<bool> _rewardClaimed = null)
    {
        //setup rewards
        if (_rewardUIDs != null)
        {
            rewardUIDs = new List<string>(_rewardUIDs);
        }
        else
        {
            rewardUIDs = new List<string>();
        }
        if(_rewardClaimed != null)
        {
            rewardClaimed = new List<bool>(_rewardClaimed);
        }
        else
        {
            rewardClaimed = new List<bool>();
            for (int i = 0; i < rewardUIDs.Count; i++)
            {
                rewardClaimed.Add(false);
            }
        }
        //UpdateDisplay();
    }
    public void SetViewOnNode(List<string> _viewUIDs = null, List<bool> _viewClaimed = null)
    {
        //setup rewards
        if (_viewUIDs != null)
        {
            viewUIDs = new List<string>(_viewUIDs);
        }
        else
        {
            viewUIDs = new List<string>();
        }
        if (_viewClaimed != null)
        {
            viewClaimed = new List<bool>(_viewClaimed);
        }
        else
        {
            viewClaimed = new List<bool>();
            for(int i=0;i < viewUIDs.Count; i++)
            {
                viewClaimed.Add(false);
            }
        }
        //UpdateDisplay();
    }
    public void SetCollectibleOnNode(List<string> _collectibleUIDs = null, List<bool> _collectibleClaimed = null)
    {
        //setup rewards
        if (_collectibleUIDs != null)
        {
            collectibleUIDs = new List<string>(_collectibleUIDs);
        }
        else
        {
            collectibleUIDs = new List<string>();
        }
        if (_collectibleClaimed != null)
        {
            collectibleClaimed = new List<bool>(_collectibleClaimed);
        }
        else
        {
            collectibleClaimed = new List<bool>();
            for (int i = 0; i < collectibleUIDs.Count; i++)
            {
                collectibleClaimed.Add(false);
            }
        }
    }
    
    public void SetNodeReveal(bool revealed)
    {
        isRevealed = revealed;
    }
    public void SetStatus(string curNodePfb, List<string> visitedNodePfbs, dConstants.ParkConst.ParkStatus gameplayStatus)
    {
        bool passed = visitedNodePfbs.Contains(pfbUID);
        //bool next = IsSourcedFrom(curNodePfb) || isRevealed || isFinal;
        bool next = IsSourcedFrom(curNodePfb) || isRevealed;
        if (curNodePfb == pfbUID)
        {
            if (gameplayStatus == dConstants.ParkConst.ParkStatus.onReward)
            {
                status = NodeStatus.current;
                statusStr = "current";
            }
            else
            {
                status = NodeStatus.visited;
                statusStr = "visited";
            }
        }
        else if (visitedNodePfbs.Contains(pfbUID))
        {
            status = NodeStatus.visited;
            statusStr = "visited";
        }
        else if (passed)
        {
            status = NodeStatus.passed;
            statusStr = "passed";
        }
        else if (next)
        {
            status = NodeStatus.next;
            statusStr = "next";
        }
        else
        {
            status = NodeStatus.hidden;
            statusStr = "hidden";
        }
    }
    public void StatusUpdate()
    {
        //add more reward display logic here
        node_icon.SetInteractive((status == NodeStatus.current || status == NodeStatus.next) || nodeUID == "parknode_lv0_start");
        node_icon.SetFrame(nodeFrames.GetAsset(statusStr));
        //old
        //content_group.SetActive(status != NodeStatus.hidden);
        //new : all nodes that should be hidden will be displayed as a smaller icon with less info
        content_group.transform.localScale = Vector3.one * (status == NodeStatus.hidden ? 0.4f : 1f);
        node_icon.icon.gameObject.SetActive(status != NodeStatus.hidden);
        node_tier.gameObject.SetActive(status != NodeStatus.hidden && nodeUID != "parknode_lv0_start");
        if (claimedViewUID != "")
        {
            view_group.SetActive(true);
            View viewData = dDataHub.hub.dView.GetFromUID(claimedViewUID);
            view.sprite = uAssetReader.controller.GetByName(viewData.artAsset.objectArt);
            view_tier.SetTier(viewData.tier);
        }
        else
        {
            view_group.SetActive(false);
        }
        //popupObject.preview_desc.SetVisible(status == NodeStatus.next);
    }
    public void OnNodeClicked()
    {
        if(status == NodeStatus.current && gameplayData.curStatus == dConstants.ParkConst.ParkStatus.onReward)
        {
            onClick = true;
            OnNodeStart.Raise();
            //popupObject.gameObject.SetActive(!popupObject.gameObject.activeSelf);
            SFXPlayer.mp3.PlaySFX("button_click");
            onClick = false;
        }
        if (status == NodeStatus.next && gameplayData.curStatus == dConstants.ParkConst.ParkStatus.nextNode)
        {
            onClick = true;
            OnNextNodeStart.Raise();
            //popupObject.gameObject.SetActive(!popupObject.gameObject.activeSelf);
            ClearPathHighlight.Raise();
            for(int i = 0; i < inRoutes.Count; i++)
            {
                if(inRoutes[i].inNode == gameplayData.CurrentNodeObj)
                {
                    inRoutes[i].Anim_ShowPathHighlight();
                }  
            }
            Anim_ScaleUp();
            SFXPlayer.mp3.PlaySFX("button_click");
            onClick = false;
        }
    }
    public void OnRewardClicked(int index)
    {
        //Debug.Log(string.Format("onRewardClickedCalled"));
        /*
        if (status == NodeStatus.current && popupObject.rewardTokens[index].status == uiParkRewardToken.RewardTokenStatus.playable)
        {
            selectedRewardIndex = index;
            ref_reward_hit_func?.Invoke(index);
            if (!isFinal)
            {
                status = NodeStatus.visited;
                ExpandGroupCollapse.Raise();
            }
            
        }*/
    }
    public void CollapseNode()
    {
        if (!onClick)
        {
            //popupObject.gameObject.SetActive(false);
        }
    }
    public void Anim_Spawn()
    {
        transform.DOScale(0f, 0.5f).SetEase(Ease.OutBack).From();
    }
    public void Anim_ScaleUp()
    {
        animTween = node_icon.transform.DOScale(Vector3.one * 1.3f, 0.3f).SetEase(Ease.OutBack);
    }
    public void Anim_ScaleReset()
    {
        DOTween.Kill(animTween);
        node_icon.transform.localScale = Vector3.one;
    }
    bool IsSourcedFrom(string nodePfb)
    {
        foreach(uiParkRoute path in inRoutes)
        {
            if(path.inNode.pfbUID == nodePfb)
            {
                return true;
            }
        }
        return false;
    }
    //------------Debug--------------------
#if UNITY_EDITOR
    void OnDrawGizmos()
    {
        if (pfbUID != "" && pfbUID != null)
        {
            GUIStyle style = new GUIStyle();
            style.normal.textColor = Color.green;
            string displayTxt = pfbUID.Replace("pfb_parkNode_", "");
            Handles.Label(transform.position, displayTxt, style);
        }
    }
#endif
}
