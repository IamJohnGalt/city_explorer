﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Generic/Dialog Popup")]
public class dataDialogPopup : ScriptableObject
{
    public enum DialogScenarios { park, journal, trigger};
    public string avatar;
    public string desc;
    public string btn_text;
    public bool requireConfirmation;
    public DialogScenarios type;

    public void Reset()
    {
        avatar = "EMPTY";
        desc = "";
        btn_text = "好的";
        requireConfirmation = false;
        type = DialogScenarios.trigger;
    }
}
