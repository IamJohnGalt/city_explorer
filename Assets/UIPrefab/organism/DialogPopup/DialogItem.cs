﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogItem : MonoBehaviour
{
    [Header("Display Group")]
    public GameObject DialogBoxGroup;
    public GameObject AvatarGroup;

    [Header("Children Object")]
    [SerializeField] uipfb_icon_v2 avatar;
    [SerializeField] uipfb_text desc;

    public void SetPopupItem(dataDialogPopup data)
    {
        
    }
}
