﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class uiDialogPopup : uiElement
{
    public static uiDialogPopup controller;
    void Awake()
    {
        if (controller == null)
        {
            controller = this;
        }
        else if (controller != null)
        {
            Destroy(gameObject);
        }
        content.SetActive(false);
    }

    [Header("Data")]
    [SerializeField] dataDialogPopup data;

    [Header("DisplayGroup")]
    [SerializeField] GameObject content;
    [SerializeField] GameObject dialog;

    [Header("Children Objects")]
    [SerializeField] ui_icon avatar;
    [SerializeField] ui_text desc;
    [SerializeField] ui_button confirm;

    private int onGoingPopup;

    public void btn_confirm_hit()
    {
        dialog.transform.DOScale(0f, 0.5f).OnComplete(() => UIClose());
        
        gIntroParkScript.controller.ClearDialogAndMask();
        if(data.type == dataDialogPopup.DialogScenarios.park)
        {
            Sequence seq = DOTween.Sequence();
            seq.AppendInterval(1f).AppendCallback(() => gIntroParkScript.controller.SetDialogAndMask());
        }
        else if(data.type == dataDialogPopup.DialogScenarios.journal)
        {
            Sequence seq = DOTween.Sequence();
            seq.AppendInterval(1f).AppendCallback(() => gIntroParkScript.controller.SetJournalBasicIntro());
        }
        else
        {
            //do nothing
        }
        SFXPlayer.mp3.PlaySFX("button_click");
    }
    public void SetUpDialog()
    {
        UIUpdate();
        UIInit();
    }
    public void CloseDialog()
    {
        dialog.transform.DOScale(0f, 0.5f).OnComplete(() => UIClose());
        //gIntroParkScript.controller.ClearDialogAndMask();
    }
    public override void UIInit()
    {
        content.SetActive(true);
        Anim_Popup();
    }
    public override void UIUpdate()
    {
        avatar.SetIcon(data.avatar);
        desc.SetText(data.desc);
        confirm.majorStr.SetText(data.btn_text);
        confirm.gameObject.SetActive(data.requireConfirmation);
    }
    public override void UIClose()
    {
        content.SetActive(false);
    }
    public dataDialogPopup AccessPopupData()
    {
        return data;
    }
    public bool OngoingDialog()
    {
        return content.gameObject.activeSelf;
    }
    void Anim_Popup()
    {
        dialog.transform.localScale = Vector3.zero;
        dialog.transform.DOScale(1f, 0.5f);
    }
    
    /*
    public void DestoryItem(GameObject popupItem)
    {
        Destroy(popupItem);
        onGoingPopup -= 1;
    }
    
    void Anim_PopupAdd2Sequence(GameObject popupItem)
    {
        Sequence anim_seq = DOTween.Sequence();
        anim_seq.AppendInterval(3f * onGoingPopup);
        anim_seq.Append(popupItem.GetComponent<DialogItem>().DialogBoxGroup.GetComponent<RectTransform>().DOScale(0f, 0.6f).SetEase(Ease.OutBack).From());
        anim_seq.AppendInterval(5f);
        anim_seq.AppendCallback(() => DestoryItem(popupItem));
    }
    */
}
