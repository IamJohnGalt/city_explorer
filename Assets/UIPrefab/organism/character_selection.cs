﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class character_selection : MonoBehaviour
{
    [Header("Data")]
    [SerializeField] dataCharacter data;

    [Header("Children Objects")]
    [SerializeField] ui_text characterName;
    [SerializeField] Image portrait;
    [SerializeField] ui_icon_text memo;
    [SerializeField] ui_icon_text fame;
    [SerializeField] ui_text highScore;
    [SerializeField] ui_button selection_btn;
    [SerializeField] ui_button more_btn;
    //[SerializeField] uipfb_icon_v2 selectBtn;

    public void SetCharacter(dataCharacter target_data)
    {
        data = target_data;
        UpdateCharacter();
    }
    public void UpdateCharacter()
    {
        characterName.SetText(data.characterClass);
        portrait.sprite = data.GetPortrait;
        memo.icon.SetIcon(data.token);
        if (data.curFameLevel > 0)
        {
            memo.minorStr.SetText("信物");
            memo.majorStr.SetText(data.GetStoryLevelText(data.curStoryLevel));
            fame.majorStr.SetText(data.GetFameLevelText(data.curFameLevel));

            memo.gameObject.SetActive(true);
            fame.gameObject.SetActive(true);
        }
        else
        {
            memo.minorStr.SetText("收集更多信物解锁");
            memo.majorStr.SetText(string.Format("{0}/{1}", data.curToken, data.GetCurTokenThreshold));

            memo.gameObject.SetActive(true);
            fame.gameObject.SetActive(false);
        }
        highScore.SetText(string.Format("单次旅程最高名望：{0}", data.highestFame));
        //selectBtn.SetVisible(data.selectable);
        selection_btn.gameObject.SetActive(data.curFameLevel > 0);
        more_btn.gameObject.SetActive(data.curStoryLevel>0);
        gameObject.SetActive(true);
    }
}
