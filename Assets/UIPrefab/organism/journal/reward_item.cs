﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class reward_item : ui_icon_text
{
    public void SetRewardPreview(string journalRewardUID)
    {
        if (journalRewardUID == dConstants.JournalConst.CASE_OPTIONAL_REWARD)
        {
            icon.SetIcon("token_question");
            majorStr.SetText("可选奖励");
            minorStr.SetText("");
        }
        else if (journalRewardUID == dConstants.JournalConst.CASE_RANDOMIZED_REWARD)
        {
            icon.SetIcon("token_question");
            majorStr.SetText("随机奖励");
            minorStr.SetText("");
        }
        else
        {
            JournalReward reward = dDataHub.hub.dJournalReward.GetFromUID(journalRewardUID);
            switch (reward.rewardType)
            {
                case "random_equipment":
                    icon.SetIcon("token_random_equipment");
                    majorStr.SetText(reward.desc);
                    minorStr.SetText("");
                    break;
                case "currency":
                    Currency currency = dDataHub.hub.currencyDict.GetFromUID(reward.rewardUID);
                    icon.SetIcon(currency.artAsset.objectArt);
                    icon.frame.gameObject.SetActive(currency.UID != "currency_star");
                    majorStr.SetText(reward.desc);
                    if (reward.tags.Contains("slot_coin"))
                    {
                        minorStr.SetText("来自游记类型");
                    }
                    else if (reward.tags.Contains("coin"))
                    {
                        minorStr.SetText("来自游记星级");
                    }
                    else
                    {
                        minorStr.SetText("");
                    }
                    break;
                case "time":
                    icon.SetIcon("icon_time");
                    majorStr.SetText(reward.desc);
                    minorStr.SetText(reward.title);
                    break;
                default:
                    Debug.LogError(string.Format("Invalid reward type {0} found", reward.rewardType));
                    break;
            }
        }
        gameObject.SetActive(true);
    }
    public void SetReward(RewardItem rewardItem)
    {
        switch (rewardItem.type)
        {
            case RewardItem.RewardType.currency:
                Currency cur = dDataHub.hub.currencyDict.GetFromUID(rewardItem.rewardUID);
                icon.SetIcon(cur.artAsset.objectArt);
                majorStr.SetText(string.Format("{0}", rewardItem.modifiedAmount));
                minorStr.SetText(rewardItem.source);
                break;
            case RewardItem.RewardType.equipment:
                Equipment equ = dDataHub.hub.equipmentDict.GetFromUID(rewardItem.rewardUID);
                icon.SetIcon(equ.art.objectArt);
                majorStr.SetText(string.Format("{0}", equ.title));
                minorStr.SetText(rewardItem.source);
                break;
            default:
                break;
        }
        gameObject.SetActive(true);
    }
}
