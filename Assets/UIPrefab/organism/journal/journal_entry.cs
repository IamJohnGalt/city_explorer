﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class journal_entry : MonoBehaviour
{
    [Header("Data")]
    [SerializeField] dataJournal JournalData;

    [Header("Children Objects")]
    [SerializeField] ui_icon icon;
    [SerializeField] ui_text desc;
    [SerializeField] ui_star star;

    [Header("Event")]
    [SerializeField] GameEvent JournalPageToggle;

    public void UIUpdate()
    {
        //icon.SetIcon
        icon.SetIcon(JournalData.journalBgArt != "" ? JournalData.journalBgArt : "transparent");
        string statusTxt = "";
        switch (JournalData.gameplayStatus)
        {
            case dConstants.JournalConst.JournalStatus.selection:
                statusTxt = "思考中...";
                star.gameObject.SetActive(false);
                desc.SetText(string.Format("{0}", statusTxt));
                break;
            case dConstants.JournalConst.JournalStatus.journal_working:
                statusTxt = "书写中...";
                star.SetText(JournalData.totalStars);
                star.gameObject.SetActive(true);
                desc.SetText(string.Format("{0}<br>{1}/{2}", statusTxt, JournalData.CurTokenCount, JournalData.oneParkJournal ? "不限" : JournalData.curComboTokenCount.ToString()));
                break;
            case dConstants.JournalConst.JournalStatus.journal_full:
                statusTxt = "书写中...";
                star.SetText(JournalData.totalStars);
                star.gameObject.SetActive(true);
                desc.SetText(string.Format("{0}<br>{1}/{2}", statusTxt, JournalData.CurTokenCount, JournalData.oneParkJournal ? "不限" : JournalData.curComboTokenCount.ToString()));
                break;
            case dConstants.JournalConst.JournalStatus.reward:
                statusTxt = "发表奖励！";
                star.SetText(JournalData.totalStars);
                star.gameObject.SetActive(true);
                desc.SetText(string.Format("{0}", statusTxt));
                break;
            default:
                break;
        }       
    }
    public void journal_icon_hit()
    {
        JournalPageToggle.Raise();
    }
    public void Anim_AcquireCollectible()
    {
        icon.transform.localScale = Vector3.one * 1.2f;
        Sequence seq = DOTween.Sequence();
        seq.Append(icon.transform.DOScale(1f, 0.3f).From());
        seq.Append(icon.transform.DOScale(1f, 0.3f));
    }
}
