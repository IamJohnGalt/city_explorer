﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class journal_slot : MonoBehaviour
{
    public delegate void btn_hit_func(int index);
    public btn_hit_func ref_token_selected_func;
    public btn_hit_func ref_token_delete_func;

    [Header("Data")]
    [SerializeField] ColorPalette frameColor;

    [Header("Children Objects")]
    [SerializeField] Image text_sprite;
    [SerializeField] Image frame_sprite;
    [SerializeField] ui_button delete_btn;
    [SerializeField] ui_collectible collectible;

    public dConstants.JournalConst.SlotStatus status = dConstants.JournalConst.SlotStatus.hidden;
    public string statusStr;

    public void OnSlotClicked(int index)
    {
        ref_token_selected_func?.Invoke(index);
    }

    public void OnDeleteClicked(int index)
    {
        ref_token_delete_func?.Invoke(index);
    }

    void SetTextBg(bool isEmpty)
    {
        Color clr = text_sprite.color;
        text_sprite.color = new Color(clr.r, clr.g, clr.b, isEmpty ? 0.3f : 1f);
    }
    public void Anim_FadeIn()
    {
        Debug.Log("fade animation needed");
    }
    public void Anim_FadeOut()
    {
        Debug.Log("fade animation needed");
    }
    public void SetSlot(dConstants.JournalConst.SlotStatus _status = dConstants.JournalConst.SlotStatus.hidden)
    {
        status = _status;
        if (_status == dConstants.JournalConst.SlotStatus.hidden)
        {
            statusStr = "hidden";
        }
        else if (_status == dConstants.JournalConst.SlotStatus.selected)
        {
            statusStr = "selected";
        }
        else if (_status == dConstants.JournalConst.SlotStatus.filled)
        {
            statusStr = "filled";
            SetTextBg(false);
        }
        else
        {
            statusStr = "empty";
            SetTextBg(true);
        }
        gameObject.SetActive(status != dConstants.JournalConst.SlotStatus.hidden);
        frame_sprite.color = frameColor.GetAsset(statusStr);
        //delete_btn.gameObject.SetActive(status == dConstants.JournalConst.SlotStatus.selected && collectible.gameObject.activeSelf);
    }
}
