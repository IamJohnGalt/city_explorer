﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Generic/Confirmation Popup")]
public class dataConfirmationPopup : ScriptableObject
{
    public enum ButtonColorStatus { positive, neutral, negative};
    public string title_icon;
    public string title_text;
    public string desc_text;
    public bool btn_no_available;
    public ButtonColorStatus btn_no_status;
    public string btn_no_text;
    public bool btn_yes_available;
    public ButtonColorStatus btn_yes_status;
    public string btn_yes_text;

    public void Reset()
    {
        title_icon = "EMPTY";
        title_text = "";
        desc_text = "";
        btn_no_available = false;
        btn_no_text = "再想想";
        btn_no_status = ButtonColorStatus.positive;
        btn_yes_available = false;
        btn_yes_text = "确认";
        btn_yes_status = ButtonColorStatus.negative;
    }
}
