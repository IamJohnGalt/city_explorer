﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class uiPopupConfirmation : uiElement
{
    public static uiPopupConfirmation controller;

    [SerializeField] dataConfirmationPopup data;

    public delegate void btn_hit_func();
    public btn_hit_func confirm_func;

    [Header("DisplayGroup")]
    [SerializeField] GameObject content;

    [Header("Children Objects")]
    [SerializeField] ui_icon_text title;
    [SerializeField] ui_text desc;
    [SerializeField] ui_button btn_no;
    [SerializeField] ui_button btn_yes;

    private void Awake()
    {
        if (controller == null)
        {
            controller = this;
        }
        else if (controller != null)
        {
            Destroy(gameObject);
        }
        UIClose();
    }
    public void btn_yes_hit()
    {
        confirm_func.Invoke();
        UIClose();
    }
    public void btn_no_hit()
    {
        UIClose();
    }
    public override void UIClose()
    {
        content.SetActive(false);
    }
    public override void UIInit()
    {
        UIUpdate();
        content.SetActive(true);
    }
    public override void UIUpdate()
    {
        title.icon.SetIcon(data.title_icon);
        title.majorStr.SetText(data.title_text);
        desc.SetText(data.desc_text);
        btn_no.gameObject.SetActive(data.btn_no_available);
        btn_no.majorStr.SetText(data.btn_no_text);
        btn_yes.gameObject.SetActive(data.btn_yes_available);
        btn_yes.majorStr.SetText(data.btn_yes_text);
        //to do implement button status UI change
    }
    public void SetUpPopup(System.Action execute_func)
    {
        confirm_func = () => execute_func();
        UIInit();
    }
    public dataConfirmationPopup AccessPopupData()
    {
        return data;
    }
}
