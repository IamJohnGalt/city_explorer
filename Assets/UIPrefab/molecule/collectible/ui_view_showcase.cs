﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ui_view_showcase : MonoBehaviour
{
    private float stayDuration = 6f;
    private float transitionDuration = 2f;
    private List<string> availableViewNames;
    private List<string> currentViews = new List<string>();
    [SerializeField] Image art1;
    private List<string> viewsInUse = new List<string>();
    [SerializeField] Image art2;
    private string viewName2;
    [SerializeField] Image frame;

    public void StartPPT(List<string> iconNames)
    {
        //setup view list
        availableViewNames = iconNames;
        currentViews = new List<string>(availableViewNames);
        //init both view arts;
        viewsInUse.Add(TakeIcon());
        art2.sprite = uAssetReader.controller.GetByName(viewsInUse[viewsInUse.Count - 1]);
        viewsInUse.Add(TakeIcon());
        art1.sprite = uAssetReader.controller.GetByName(viewsInUse[viewsInUse.Count - 1]);
        //display art1
        art1.color = Color.white;
        //hide art2
        art2.color = new Color(1, 1, 1, 0);
        //set up animation
        SlideStartFadeOut(art1);
        SlideStartFadeIn(art2);
    }
    void SlideStartFadeOut(Image art)
    {
        art.transform.SetAsFirstSibling();
        art.DOFade(0f, transitionDuration).SetDelay(stayDuration).SetEase(Ease.OutSine).OnComplete(() => SlideStartFadeIn(art));
    }
    void SlideStartFadeIn(Image art)
    {
        viewsInUse.Add(TakeIcon());
        ReturnIcon(viewsInUse[0]);
        viewsInUse.RemoveAt(0);
        art.sprite = uAssetReader.controller.GetByName(viewsInUse[viewsInUse.Count - 1]);
        art.DOFade(1f, transitionDuration).SetDelay(stayDuration).SetEase(Ease.OutSine).OnComplete(() => SlideStartFadeOut(art));
    }
    string TakeIcon()
    {
        int rng = Random.Range(0, currentViews.Count);
        string iconName = currentViews[rng];
        currentViews.RemoveAt(rng);
        return iconName;
    }
    void ReturnIcon(string iconName)
    {
        currentViews.Add(iconName);
    }
}
