﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ui_collectible : MonoBehaviour
{
    public delegate void btn_hit_func(int index);
    public btn_hit_func ref_token_selected_func;

    [Header("Data")]
    public string itemUID;
    public dConstants.ParkConst.TokenStatus status;
    [SerializeField] ColorPalette namePallete;

    [Header("Children Objects")]
    [SerializeField] GameObject interactiveZone;
    [SerializeField] Image art;
    [SerializeField] Image art_frame;
    [SerializeField] Image mask;
    [SerializeField] ui_text collectibleName;
    [SerializeField] ui_star star;
    [SerializeField] List<ui_tag> tags;
    private void OnEnable()
    {
        HoverOff();
    }
    public void SetAsViewByUID(string _viewUID, dConstants.ParkConst.TokenStatus _status = dConstants.ParkConst.TokenStatus.hidden)
    {
        if (_viewUID == "")
        {
            gameObject.SetActive(false);
            return;
        }
        else if (!dDataHub.hub.dView.Contains(_viewUID))
        {
            gameObject.SetActive(false);
            Debug.LogError(string.Format("token({0}) is set to invisible due to missing token data by its UID({1})", gameObject.name, _viewUID));
            return;
        }
        itemUID = _viewUID;
        status = _status;
        View view = dDataHub.hub.dView.GetFromUID(itemUID);
        collectibleName.gameObject.SetActive(false);
        //collectibleName.SetTextColor(namePallete.GetAsset(view.tier.ToString()));
        if (_status == dConstants.ParkConst.TokenStatus.preview)
        {
            art.sprite = DeterminePreviewSprite(view.preview_keywords);
            /*if (view.starValue > 0)
            {
                star.SetText("?");
                star.SetFade(1f);
                star.gameObject.SetActive(true);
            }
            else
            {
                star.gameObject.SetActive(false);
            }*/
            star.gameObject.SetActive(false);
            //collectibleName.SetText("未知风景");
            for (int i = 0; i < tags.Count; i++)
            {

                if (i < view.preview_keywords.Count && i < dConstants.ParkConst.COLLECTIBLE_PREVIEW_COUNT)
                {
                    tags[i].SetTag(view.preview_keywords[i]);
                    tags[i].SetFade(1f);
                    tags[i].gameObject.SetActive(true);
                }
                else
                {
                    tags[i].gameObject.SetActive(false);
                }
            }

        }
        else
        {
            art.sprite = uAssetReader.controller.GetByName(view.artAsset.objectArt);
            star.gameObject.SetActive(false);
            //collectibleName.SetText(view.title);
            //collectibleName.gameObject.SetActive(view.artAsset.objectArt == "collectible_temp");
            for (int i = 0; i < tags.Count; i++)
            {
                tags[i].gameObject.SetActive(false);
            }
        }
        interactiveZone.SetActive(status == dConstants.ParkConst.TokenStatus.playable);
        gameObject.SetActive(status != dConstants.ParkConst.TokenStatus.hidden);
    }
    public void SetAsViewByArt(string artPath)
    {
        if (artPath == "")
        {
            gameObject.SetActive(false);
            return;
        }
        itemUID = "";
        status = dConstants.ParkConst.TokenStatus.preview;
        art.sprite = uAssetReader.controller.GetByName(artPath);
        for (int i = 0; i < tags.Count; i++)
        {
            tags[i].gameObject.SetActive(false);
        }
        collectibleName.SetText("");
        star.gameObject.SetActive(false);
        interactiveZone.SetActive(false);
        gameObject.SetActive(true);
    }
    public void HoverOn()
    {
        mask.gameObject.SetActive(true);
        SFXPlayer.mp3.PlaySFX("button_hover");
    }
    public void HoverOff()
    {
        mask.gameObject.SetActive(false);
    }
    public void SetAsCollectibleByUID(string _collectibleUID, dConstants.ParkConst.TokenStatus _status = dConstants.ParkConst.TokenStatus.hidden)
    {
        if (_collectibleUID == "")
        {
            gameObject.SetActive(false);
            return;
        }
        else if (!dDataHub.hub.dCollectible.Contains(_collectibleUID))
        {
            gameObject.SetActive(false);
            Debug.LogError(string.Format("token({0}) is set to invisible due to missing token data by its UID({1})", gameObject.name, _collectibleUID));
            return;
        }

        itemUID = _collectibleUID;
        status = _status;
        collectibleName.gameObject.SetActive(true);
        Collectible collectible = dDataHub.hub.dCollectible.GetFromUID(itemUID);
        //collectibleName.SetTextColor(namePallete.GetAsset(collectible.tier.ToString()));
        if (_status == dConstants.ParkConst.TokenStatus.preview)
        {
            art.sprite = uAssetReader.controller.GetByName("collectible_temp");
            if (collectible.starValue > 0)
            {
                star.SetText("?");
                star.SetFade(1f);
                star.gameObject.SetActive(true);
            }
            else
            {
                star.gameObject.SetActive(false);
            }
            collectibleName.gameObject.SetActive(false);
            for (int i = 0; i < tags.Count; i++)
            {

                if (i < collectible.keywords.Count && i < dConstants.ParkConst.COLLECTIBLE_PREVIEW_COUNT)
                {
                    tags[i].SetTag(collectible.keywords[i]);
                    tags[i].SetFade(1f);
                    tags[i].gameObject.SetActive(true);
                }
                else
                {
                    tags[i].gameObject.SetActive(false);
                }
            }
        }
        else if (_status == dConstants.ParkConst.TokenStatus.inactive)
        {
            art.sprite = uAssetReader.controller.GetByName(collectible.artAsset.objectArt);
            if (collectible.starValue > 0)
            {
                star.SetText(collectible.starValue);
                star.SetFade(0.5f);
                star.gameObject.SetActive(true);
            }
            else
            {
                star.gameObject.SetActive(false);
            }
            collectibleName.SetText(collectible.title);
            //collectibleName.gameObject.SetActive(collectible.artAsset.objectArt == "collectible_temp");
            for (int i = 0; i < tags.Count; i++)
            {

                if (i < collectible.keywords.Count)
                {
                    tags[i].SetTag(collectible.keywords[i]);
                    tags[i].SetFade(0.5f);
                    tags[i].gameObject.SetActive(true);
                }
                else
                {
                    tags[i].gameObject.SetActive(false);
                }
            }
        }
        else
        {
            art.sprite = uAssetReader.controller.GetByName(collectible.artAsset.objectArt);
            if (collectible.starValue > 0)
            {
                star.SetText(collectible.starValue);
                star.SetFade(1f);
                star.gameObject.SetActive(true);
            }
            else
            {
                star.gameObject.SetActive(false);
            }
            collectibleName.SetText(collectible.title);
            //collectibleName.gameObject.SetActive(collectible.artAsset.objectArt == "collectible_temp");
            for (int i = 0; i < tags.Count; i++)
            {

                if (i < collectible.keywords.Count)
                {
                    tags[i].SetTag(collectible.keywords[i]);
                    tags[i].SetFade(1f);
                    tags[i].gameObject.SetActive(true);
                }
                else
                {
                    tags[i].gameObject.SetActive(false);
                }
            }
        }
        //HoverOff();
        interactiveZone.SetActive(status == dConstants.ParkConst.TokenStatus.playable);
        gameObject.SetActive(status != dConstants.ParkConst.TokenStatus.hidden);
        
    }
    public void OnTokenClicked(int index)
    {
        if (status == dConstants.ParkConst.TokenStatus.playable)
        {
            ref_token_selected_func?.Invoke(index);
        }
    }
    public void SetFade(float targetAlpha, float transitionTime = 0f)
    {
        if(transitionTime == 0)
        {
            Color artColor = new Color(art.color.r, art.color.g, art.color.b, targetAlpha);
            Color frameColor = new Color(art_frame.color.r, art_frame.color.g, art_frame.color.b, targetAlpha);
            art.color = artColor;
            art_frame.color = frameColor;
            collectibleName.SetFade(targetAlpha, transitionTime);
            star.SetFade(targetAlpha, transitionTime);
            for (int i = 0; i < tags.Count; i++)
            {
                tags[i].SetFade(targetAlpha, transitionTime);
            }
        }
        else
        {
            art.DOFade(targetAlpha, transitionTime);
            art_frame.DOFade(targetAlpha, transitionTime);
            collectibleName.SetFade(targetAlpha, transitionTime);
            star.SetFade(targetAlpha, transitionTime);
            for (int i = 0; i < tags.Count; i++)
            {
                tags[i].SetFade(targetAlpha, transitionTime);
            }
        }
    }
    public void SetViewFade(float targetAlpha, float transitionTime = 0f)
    {
        if (transitionTime == 0)
        {
            Color artColor = new Color(art.color.r, art.color.g, art.color.b, targetAlpha);
            Color frameColor = new Color(art_frame.color.r, art_frame.color.g, art_frame.color.b, targetAlpha);
            art.color = artColor;
            art_frame.color = frameColor;
            collectibleName.SetFade(targetAlpha, transitionTime);
        }
        else
        {
            art.DOFade(targetAlpha, transitionTime);
            art_frame.DOFade(targetAlpha, transitionTime);
            collectibleName.SetFade(targetAlpha, transitionTime);
        }
    }
    public void SetTagFade(float targetAlpha, float transitionTime = 0f)
    {
        if (transitionTime == 0)
        {
            star.SetFade(targetAlpha, transitionTime);
            for (int i = 0; i < tags.Count; i++)
            {
                tags[i].SetFade(targetAlpha, transitionTime);
            }
        }
        else
        {
            star.SetFade(targetAlpha, transitionTime);
            for (int i = 0; i < tags.Count; i++)
            {
                tags[i].SetFade(targetAlpha, transitionTime);
            }
        }
    }
    public void ShutOffInteraction()
    {
        interactiveZone.gameObject.SetActive(false);
    }
    public void ResumeInteraction()
    {
        interactiveZone.gameObject.SetActive(true);
    }

    Sprite DeterminePreviewSprite(List<string> allTags)
    {
        if (allTags.Count == 0)
        {
            return uAssetReader.controller.GetByName("view_preview_generic");
        }
        else if (allTags[0] == "natural")
        {
            return uAssetReader.controller.GetByName("view_preview_natural");
        }
        else if (allTags[0] == "animal")
        {
            return uAssetReader.controller.GetByName("view_preview_animal");
        }
        else if (allTags[0] == "structure")
        {
            return uAssetReader.controller.GetByName("view_preview_structure");
        }
        else
        {
            return uAssetReader.controller.GetByName("view_preview_generic");
        }
    }
}
