﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ui_tag : ui_label
{
    [SerializeField] StringAsset loc_cn_keywords;
    [SerializeField] SpriteAsset sprite_keyword_tags;
    public void SetTag(string keyword)
    {
        SetText(loc_cn_keywords.GetAsset(keyword));
        SetFrame(sprite_keyword_tags.GetAsset(keyword));
        gameObject.SetActive(true);
    }

}
