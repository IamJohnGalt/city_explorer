﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ui_star : MonoBehaviour
{
    [Header("Children Objects")]
    [SerializeField] ui_text value;
    [SerializeField] Image star;
    [SerializeField] Image neg_star;

    public void SetText(int v)
    {
        value.SetText(v.ToString());
        star.gameObject.SetActive(v >= 0);
        neg_star.gameObject.SetActive(v < 0);
    }
    public void SetText(string str)
    {
        value.SetText(str);
    }
    public void SetFade(float alpha, float transitionTime = 0f)
    {
        if (transitionTime == 0f)
        {
            Color clr = star.color;
            star.color = new Color(clr.r, clr.g, clr.b, alpha);
        }
        else
        {
            star.DOFade(alpha, transitionTime);
        }
        value.SetFade(alpha, transitionTime);
    }
}
