﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ui_counter : MonoBehaviour
{
    [Header("Data")]
    private int curCounter = 0;
    private int maxCounter = 0;

    [Header("Asset")]
    [SerializeField] SpriteAsset counterFrames;
    [SerializeField] Sprite counterIcon;

    [Header("Children Objects")]
    [SerializeField] ui_text text;
    [SerializeField] List<ui_icon> counters;

    private Sequence anim_seq_flashing;

    public void SetText(string txt)
    {
        text.SetText(txt);
    }
    public void SetCounter(int curCount, int maxCount)
    {
        curCounter = curCount;
        maxCounter = maxCount;
        CalibrateCounter();
    }
    void CalibrateCounter()
    {
        for (int i = 0; i < counters.Count; i++)
        {
            if (i < curCounter)
            {
                counters[i].SetIcon(counterIcon);
                counters[i].SetFrame(counterFrames.GetAsset("filled"));
                counters[i].gameObject.SetActive(true);
            }
            else if(i < maxCounter)
            {
                counters[i].SetIcon();
                counters[i].SetFrame(counterFrames.GetAsset("empty"));
                counters[i].gameObject.SetActive(true);
            }
            else
            {
                counters[i].gameObject.SetActive(false);
            }
        }
    }
    public void PreviewDelta(int previewCount, float flashingTime)
    {
        Debug.Log("preview counter with a delta of " + previewCount);
        PreviewStop();
        for (int i = 0;i < counters.Count; i++)
        {
            if(previewCount > 0)
            {
                if (i < curCounter + previewCount)
                {
                    counters[i].SetIcon(counterIcon);
                    counters[i].Anim_IconFlashing(flashingTime);
                }
            }
            else if (previewCount < 0)
            {
                if (i >= curCounter + previewCount && i < curCounter)
                {
                    counters[i].SetIcon(counterIcon);
                    counters[i].Anim_IconFlashing(flashingTime);
                }
            }
        }
    }
    public void PreviewStop()
    {
        for (int i = 0; i < counters.Count; i++)
        {
            DOTween.Kill(counters[i].icon);
            counters[i].SetFade(1f);
        }
        CalibrateCounter();
    }
}
