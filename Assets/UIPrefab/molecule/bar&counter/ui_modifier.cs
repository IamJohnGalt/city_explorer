﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ui_modifier : MonoBehaviour
{
    [Header("Children Objects")]
    [SerializeField] RectTransform content;
    [SerializeField] ui_text text;
    private bool isShowing;

    public void ResetModifier()
    {
        content.transform.localPosition = Vector3.zero;
        text.SetText("");
    }
    public void SetModifier(string label_text)
    {
        bool show = label_text != "";
        if (isShowing != show)
        {
            isShowing = show;
            if (show)
            {
                Anim_SlideDown();
            }
            else
            {
                Anim_SlideUp();
            }
        }
        text.SetText(label_text);
    }
    public void Anim_SlideDown()
    {
        content.DOAnchorPosY(-20f, 1f);
    }

    public void Anim_SlideUp()
    {
        content.DOAnchorPosY(0f, 1f);
    }
}
