﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class ui_bar : MonoBehaviour
{
    [Header("Data")]
    public float curValue;
    public float targetValue;
    public float previewValue;
    public bool showMax;
    [Header("Children Objects")]
    public ui_icon icon;
    [SerializeField] List<Image> fills;
    [SerializeField] List<Image> fills_preview;
    [SerializeField] ui_icon_text cur;
    [SerializeField] ui_icon_text preview;
    public ui_icon_text max;
    [SerializeField] FocusEffect focus;
    [SerializeField] ui_label arrival_label;

    private Sequence PreviewAnimSeq;
    public void HideNumbers()
    {
        cur.gameObject.SetActive(false);
        max.gameObject.SetActive(false);
        showMax = false;
    }
    public void ResetBar(float startPct, string curLabel, string maxLabel)
    {
        cur.majorStr.SetText(curLabel);
        max.majorStr.SetText(maxLabel);
        cur.gameObject.SetActive(true);
        max.gameObject.SetActive(true);
        showMax = true;
        curValue = Mathf.Max(Mathf.Min(startPct, 1f), 0f); ;
        targetValue = curValue;
        CalibrateBar();
        //DOTween.Kill(gameObject);
    }
    public void SetBarProgress(float progPct, string curLabel, string maxLabel)
    {
        CalibrateBar();
        targetValue = Mathf.Max(Mathf.Min(progPct, 1f), 0f);
        cur.majorStr.SetText(curLabel);
        max.majorStr.SetText(maxLabel);
        //max.gameObject.SetActive(curValue < 1);
        Anim_ValueIncrease();
    }
    public void SetIcon(Sprite spt)
    {
        icon.SetIcon(spt);
    }
    public void SetColor(Color clr)
    {
        foreach(Image fill in fills)
        {
            fill.color = clr;
        }
        cur.icon.SetFrameColor(clr);
        max.icon.SetFrameColor(clr);
    }
    void CalibrateBar()
    {
        cur.GetComponent<RectTransform>().anchorMin = new Vector2(curValue, 1f);
        cur.GetComponent<RectTransform>().anchorMax = new Vector2(curValue, 1f);
        max.gameObject.SetActive(showMax && curValue < 1);
        foreach (Image fill in fills)
        {
            fill.fillAmount = curValue;
        }
    }
    public void Anim_ValueIncrease()
    {
        if (curValue != targetValue)
        {
            DOTween.To(() => curValue, x => curValue = x, targetValue, 1f).OnUpdate(() => CalibrateBar());
        }
    }
    public void PreviewDelta(float previewPct, string previewNumber)
    {
        previewValue = Mathf.Min(1f, previewPct);
        preview.majorStr.SetText(previewNumber);
        CalibratePreview();
        if (previewValue != curValue)
        {
            //Anim_PreviewFlashing(previewValue > curValue);
            Anim_StartPreview(previewValue > curValue);
        }
        if(previewValue == 1f)
        {
            focus.gameObject.SetActive(true);
            arrival_label.gameObject.SetActive(true);
        }
    }
    /*public void PreviewStop()
    {
        previewValue = curValue;
        for (int i = 0; i < fills_preview.Count; i++)
        {
            DOTween.Kill(fills_preview[i]);
            Image previewImg = fills_preview[i].GetComponent<Image>();
            previewImg.color = new Color(previewImg.color.r, previewImg.color.g, previewImg.color.b, 0);
        }
        for (int i = 0; i < fills.Count; i++)
        {
            DOTween.Kill(fills[i]);
            Image tokenImg = fills[i].GetComponent<Image>();
            tokenImg.color = new Color(tokenImg.color.r, tokenImg.color.g, tokenImg.color.b, 1f);
        }
        DOTween.Kill(preview.icon);
        DOTween.Kill(preview.majorStr);
        preview.icon.SetFade(0f);
        preview.majorStr.SetFade(0f);
        //previewAnimSeq.Rewind();
    }*/
    public void PreviewStopSequence()
    {
        previewValue = curValue;
        CalibratePreview();
        PreviewAnimSeq.Rewind();
        focus.gameObject.SetActive(false);
        arrival_label.gameObject.SetActive(false);
    }
    void CalibratePreview()
    {
        for (int i = 0; i < fills_preview.Count; i++)
        {
            fills_preview[i].GetComponent<Image>().fillAmount = previewValue;
        }
        preview.GetComponent<RectTransform>().anchorMin = new Vector2(previewValue, 1f);
        preview.GetComponent<RectTransform>().anchorMax = new Vector2(previewValue, 1f);
    }
    /*void Anim_PreviewFlashing(bool flashingPreview)
    {
        //previewAnimSeq = DOTween.Sequence();
        //previewAnimSeq.Pause();
        //set both bar fills to full color
        for (int i = 0; i < fills_preview.Count; i++)
        {
            DOTween.Kill(fills_preview[i]);
            Image previewImg = fills_preview[i].GetComponent<Image>();
            previewImg.color = new Color(previewImg.color.r, previewImg.color.g, previewImg.color.b, 1f);
        }
        for (int i = 0; i < fills.Count; i++)
        {
            DOTween.Kill(fills[i]);
            Image tokenImg = fills[i].GetComponent<Image>();
            tokenImg.color = new Color(tokenImg.color.r, tokenImg.color.g, tokenImg.color.b, 1f);
        }
        DOTween.Kill(preview.icon);
        DOTween.Kill(preview.majorStr);
        preview.icon.SetFade(1f);
        preview.majorStr.SetFade(1f);
        //flashing depends on which one is longer
        if (flashingPreview)
        {
            for (int i = 0; i < fills_preview.Count; i++)
            {
                Image previewImg = fills_preview[i].GetComponent<Image>();
                previewImg.DOFade(0f, 0.5f).SetLoops(-1, LoopType.Yoyo);
            }
            preview.icon.GetComponent<Image>().DOFade(0f, 0.5f).SetLoops(-1, LoopType.Yoyo);
            preview.majorStr.GetComponent<TextMeshProUGUI>().DOFade(0f, 0.5f).SetLoops(-1, LoopType.Yoyo);
        }
        else
        {
            for (int i = 0; i < fills.Count; i++)
            {
                Image tokenImg = fills[i].GetComponent<Image>();
                tokenImg.DOFade(0f, 0.5f).SetLoops(-1, LoopType.Yoyo);
            }
            preview.icon.GetComponent<Image>().DOFade(0f, 0.5f).SetLoops(-1, LoopType.Yoyo).SetDelay(0.5f);
            preview.majorStr.GetComponent<TextMeshProUGUI>().DOFade(0f, 0.5f).SetLoops(-1, LoopType.Yoyo).SetDelay(0.5f);
        }
        //previewAnimSeq.Restart();
    }*/
    void Anim_StartPreview(bool flashingPreview)
    {
        PreviewAnimSeq = DOTween.Sequence();
        if (flashingPreview)
        {
            for (int i = 0; i < fills_preview.Count; i++)
            {
                Image previewImg = fills_preview[i].GetComponent<Image>();
                PreviewAnimSeq.Join(previewImg.DOFade(1f, 1f).SetLoops(2, LoopType.Yoyo));
            }
            PreviewAnimSeq.Join(preview.icon.frame.GetComponent<Image>().DOFade(1f, 1f).SetLoops(2, LoopType.Yoyo));
            PreviewAnimSeq.Join(preview.majorStr.GetComponent<TextMeshProUGUI>().DOFade(1f, 1f).SetLoops(2, LoopType.Yoyo));
        }
        else
        {
            for (int i = 0; i < fills_preview.Count; i++)
            {
                Image previewImg = fills_preview[i].GetComponent<Image>();
                PreviewAnimSeq.Join(previewImg.DOFade(1f, 1f).SetLoops(1, LoopType.Yoyo));
            }
            PreviewAnimSeq.Join(preview.icon.frame.GetComponent<Image>().DOFade(1f, 1f).SetLoops(2, LoopType.Yoyo));
            PreviewAnimSeq.Join(preview.majorStr.GetComponent<TextMeshProUGUI>().DOFade(1f, 1f).SetLoops(2, LoopType.Yoyo));
        }
        PreviewAnimSeq.AppendCallback(() => Anim_StartPreview(flashingPreview));
    }
}
