﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ui_bar_stamina : MonoBehaviour
{
    const int countPerIcon = 5;
    const int countPerTick = 1;
    [Header("Data")]
    public int curValue;
    public int maxValue;
    public int targetValue;
    public int previewValue;

    [Header("Children Objects")]
    public RectTransform tokenGroup;
    public RectTransform previewGroup;
    public List<ui_icon> tokens;
    public List<ui_icon> tokens_preview;
    public ui_text number;
    public ui_label warning;

    private Sequence previewAnimSeq;
    private bool inAnim;
    private bool depleted;

    private void Awake()
    {
        inAnim = false;
    }
    public void ResetStamina(int cur, int max)
    {
        curValue = cur;
        targetValue = cur;
        maxValue = max;
        CalibrateBar();
        warning.gameObject.SetActive(false);
    }
    public void SetStamina(int tar)
    {
        targetValue = tar;
        if(tar > 0)
        {
            depleted = false;
        }
        //maxValue = max;
        //DOTween.Kill(gameObject);
        if (!inAnim)
        {
            Anim_ValueIncrease();
        }
    }
    public void SetFoodIcon(Sprite sprt)
    {
        for (int i = 0; i < tokens.Count; i++)
        {
            tokens[i].SetFrame(sprt);
            tokens[i].SetIcon(sprt);
            tokens_preview[i].SetIcon(sprt);
        }
    }
    void CalibrateBar()
    {
        int curTokenUnitLeft = curValue;
        int maxTokenUnitLeft = maxValue;
        for (int i = 0; i < tokens.Count; i++)
        {
            tokens[i].gameObject.SetActive(maxTokenUnitLeft > 0);
            tokens[i].SetFrameFill(Mathf.Max(0, Mathf.Min(1, (float)maxTokenUnitLeft / countPerIcon)));
            tokens[i].SetIconFill(Mathf.Max(0, Mathf.Min(1, (float)curTokenUnitLeft / countPerIcon)));
            maxTokenUnitLeft -= Mathf.Min(maxTokenUnitLeft, countPerIcon);
            curTokenUnitLeft -= Mathf.Min(curTokenUnitLeft, countPerIcon);
        }
        //LayoutRebuilder.ForceRebuildLayoutImmediate(tokenGroup);
        number.SetText(string.Format("{0}/{1}", curValue, maxValue));
    }
    public void Anim_ValueIncrease()
    {
        if (curValue != targetValue)
        {
            inAnim = true;
            int previousTokenIndex = Mathf.FloorToInt((float)curValue / countPerIcon);
            curValue += Mathf.Min(countPerTick, Mathf.Max(-countPerTick, (targetValue - curValue)));
            int currentTokenIndex = Mathf.FloorToInt((float)curValue / countPerIcon);
            //Debug.Log(string.Format("two indexes on anim value increase as {0} and {1}", previousTokenIndex, currentTokenIndex));
            CalibrateBar();
            
            int deltaTokenIndex = Mathf.Min(previousTokenIndex, currentTokenIndex);
            RectTransform targetRect = tokens[deltaTokenIndex].GetComponent<RectTransform>();
            
            Sequence seq = DOTween.Sequence();
            seq.Append(targetRect.DOShakeAnchorPos(0.05f, 5f));
            seq.AppendInterval(0.05f).OnComplete(() => Anim_ValueIncrease());
        }
        else
        {
            inAnim = false;
        }
    }
    public void PreviewDelta(int preview)
    {
        previewValue = preview;
        CalibratePreview();
        if(previewValue != curValue)
        {
            Anim_PreviewFlashing(previewValue > curValue);
        }
        if(previewValue < 0)
        {
            warning.gameObject.SetActive(true || depleted);
        }
    }
    public void PreviewStop()
    {
        previewValue = curValue;
        for (int i = 0; i < tokens_preview.Count; i++)
        {
            DOTween.Kill(tokens_preview[i].icon);
            Image previewImg = tokens_preview[i].icon.GetComponent<Image>();
            previewImg.color = new Color(1f, 1f, 1f, 0);
        }
        for (int i = 0; i < tokens.Count; i++)
        {
            DOTween.Kill(tokens[i].icon);
            Image tokenImg = tokens[i].icon.GetComponent<Image>();
            tokenImg.color = new Color(1f, 1f, 1f, 1f);
        }
        warning.gameObject.SetActive(false || depleted);
        CalibrateBar();
        //previewAnimSeq.Rewind();
    }
    public void StaminaDepleted()
    {
        depleted = true;
        warning.gameObject.SetActive(depleted);
    }
    void CalibratePreview()
    {
        int previewTokenUnitLeft = previewValue;
        int maxTokenUnitLeft = maxValue;
        for (int i = 0; i < tokens_preview.Count; i++)
        {
            tokens_preview[i].gameObject.SetActive(maxTokenUnitLeft > 0);
            //tokens_preview[i].SetFrameFill(Mathf.Max(0, Mathf.Min(1, (float)maxTokenUnitLeft / countPerIcon)));
            tokens_preview[i].SetIconFill(Mathf.Max(0, Mathf.Min(1, (float)previewTokenUnitLeft / countPerIcon)));
            maxTokenUnitLeft -= Mathf.Min(maxTokenUnitLeft, countPerIcon);
            previewTokenUnitLeft -= Mathf.Min(previewTokenUnitLeft, countPerIcon);
        }
        number.SetText(string.Format("{0}/{1}", Mathf.Min(maxValue, previewValue), maxValue));
    }
    void Anim_PreviewFlashing(bool flashingPreview)
    {
        //previewAnimSeq = DOTween.Sequence();
        //previewAnimSeq.Pause();

        //set both token and preview set into full color
        for (int i = 0; i < tokens_preview.Count; i++)
        {
            DOTween.Kill(tokens_preview[i].icon);
            Image previewImg = tokens_preview[i].icon.GetComponent<Image>();
            previewImg.color = new Color(1f, 1f, 1f, 1f);
        }
        for (int i = 0; i < tokens.Count; i++)
        {
            DOTween.Kill(tokens[i].icon);
            Image tokenImg = tokens[i].icon.GetComponent<Image>();
            tokenImg.color = new Color(1f, 1f, 1f, 1f);
        }
        //flashing depends on which one is longer
        if (flashingPreview)
        {
            for (int i = 0; i < tokens_preview.Count; i++)
            {
                Image previewImg = tokens_preview[i].icon.GetComponent<Image>();
                previewImg.DOFade(0f, 0.5f).SetLoops(-1, LoopType.Yoyo);
            }
        }
        else
        {
            for (int i = 0; i < tokens.Count; i++)
            {
                Image tokenImg = tokens[i].icon.GetComponent<Image>();
                tokenImg.DOFade(0f, 0.5f).SetLoops(-1, LoopType.Yoyo);
            }
        }
        //previewAnimSeq.Restart();
    }
}
