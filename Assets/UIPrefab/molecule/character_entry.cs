﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class character_entry : MonoBehaviour
{
    [SerializeField] Image portrait;
    public void UpdateToCurrentAvatar()
    {
        portrait.sprite = CharacterManagement.controller.GetCurrentCharacterPortrait();
    }
    public void UpdateAvatar(Sprite sprt)
    {
        portrait.sprite = sprt;
    }
}
