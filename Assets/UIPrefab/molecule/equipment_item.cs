﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class equipment_item : MonoBehaviour
{
    public delegate void use_func_hit(int index);
    public use_func_hit ref_use_func_hit;
    
    //[SerializeField] ui_tooltip tooltip;
    [SerializeField] ui_icon icon;
    [SerializeField] ui_icon item_tag;

    public string equipmentUID;
    public bool activeEquipment;
    public bool depleted;
    public bool usable;
    [SerializeField] int equipmentListIndex;

    private int screenWidth = 1920;
    private int screenHeight = 1080;
    private int offset_x = 50;
    private void Awake()
    {
    }
    public void ShowEquipmentByUID(string uid)
    {
        if(uid == "")
        {
            gameObject.SetActive(false);
            return;
        }
        equipmentUID = uid;
        activeEquipment = false;
        depleted = true;
        usable = false;
        Equipment equ = dDataHub.hub.equipmentDict.GetFromUID(uid);
        //Sprite iconSpt = Resources.Load<Sprite>(string.Format("Art/token/{0}", ));
        icon.SetIcon(equ.art.objectArt);
        if(equ.art.backgroundArt != "")
        {
            item_tag.SetIcon(equ.art.backgroundArt);
        }
        else
        {
            item_tag.SetIcon();
        }
        //tooltip.SetToolTip(equ.title, equ.desc);
        if (!gameObject.activeSelf)
        {
            Anim_Popup();
        }
        gameObject.SetActive(true);
    }
    public void SetActiveEquipmentByUID(string uid, bool isDepleted, bool isActive)
    {
        if (uid == "")
        {
            gameObject.SetActive(false);
            return;
        }
        equipmentUID = uid;
        activeEquipment = true;
        depleted = isDepleted;
        usable = isActive;
        Equipment equ = dDataHub.hub.equipmentDict.GetFromUID(uid);
        icon.SetIcon(equ.art.objectArt);
        icon.SetFade(isDepleted ? 0.5f : 1f);
        if (equ.art.backgroundArt != "")
        {
            item_tag.SetIcon(equ.art.backgroundArt);
        }
        else
        {
            item_tag.SetIcon();
        }
        //tooltip.SetToolTip(equ.title, equ.desc);
        //tooltip.AddButton(isActive, "使用", equipment_use_hit);
        gameObject.SetActive(true);
    }
    public void ShowTooltip()
    {
        //Debug.Log(string.Format("showtooltip called"));
;       //tooltip.Anim_Popup();
        Equipment equ = dDataHub.hub.equipmentDict.GetFromUID(equipmentUID);
        tooltip_popup.controller.CallTooltip(transform.position, tooltip_popup.WindowType.horizontal, equ.title, equ.desc, activeEquipment, usable, "使用", equipment_use_hit);
    }
    public void equipment_use_hit()
    {
        ref_use_func_hit?.Invoke(equipmentListIndex);
        tooltip_popup.controller.DismissAllTooltips();
    }
    public void Anim_Popup()
    {
        transform.localScale = Vector3.one * 1.5f;
        Sequence seq = DOTween.Sequence();
        seq.Append(transform.DOScale(0f, 0.3f).From());
        seq.Append(transform.DOScale(1f, 0.3f));
    }
}
