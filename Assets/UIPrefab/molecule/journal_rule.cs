﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class journal_rule : MonoBehaviour
{
    [Header("Children Objects")]
    [SerializeField] ui_star star_;
    [SerializeField] ui_star _star;
    [SerializeField] ui_text prefix;
    [SerializeField] ui_text midfix;
    [SerializeField] ui_text suffix;
    [SerializeField] List<ui_tag> collectible_tags;
    [SerializeField] List<ui_tag> additional_tags;

    public void SetRule(bool StarOnTop, int _value, string _prefix, string _midfix, string _suffix, List<string> _keywords, List<string> _addition_keywords)
    {
        star_.SetText(_value);
        _star.SetText(_value);
        
        prefix.SetText(_prefix);
        midfix.SetText(_midfix);
        suffix.SetText(_suffix);
        for (int i = 0; i < collectible_tags.Count; i++)
        {
            if(i< _keywords.Count)
            {
                collectible_tags[i].SetTag(_keywords[i]);
                collectible_tags[i].gameObject.SetActive(true);
            }
            else
            {
                collectible_tags[i].gameObject.SetActive(false);
            }
        }
        for (int i = 0; i < additional_tags.Count; i++)
        {
            if (i < _addition_keywords.Count)
            {
                additional_tags[i].SetTag(_addition_keywords[i]);
                additional_tags[i].gameObject.SetActive(true);
            }
            else
            {
                additional_tags[i].gameObject.SetActive(false);
            }
        }
        star_.gameObject.SetActive(StarOnTop);
        _star.gameObject.SetActive(!StarOnTop);
        prefix.gameObject.SetActive(_prefix != "");
        midfix.gameObject.SetActive(_midfix != "");
        suffix.gameObject.SetActive(_suffix != "");
    }

    public void SetItem(StarBonusItem item)
    {
        SetRule(item.status == StarBonusItem.BonusItemStatus.activated, item.bonusStar, item.prefix, item.midfix, item.suffix, item.keywords, item.addition_keywords);
    }
}
