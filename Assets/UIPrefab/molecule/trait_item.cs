﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class trait_item : MonoBehaviour
{
    public string traitUID;
    [SerializeField] ui_icon icon;

    public void ShowTraitByUID(string uid = "")
    {
        if (uid == "")
        {
            gameObject.SetActive(false);
            return;
        }
        if(uid == "next_trait_slot")
        {
            traitUID = uid;
            icon.SetIcon();
            icon.SetFade(0.5f);
            gameObject.SetActive(true);
            return;
        }
        if (!gameObject.activeSelf || traitUID == "next_trait_slot")
        {
            Anim_Popup();
        }
        traitUID = uid;
        Trait trait = dDataHub.hub.traitDict.GetFromUID(traitUID);
        //Sprite iconSpt = Resources.Load<Sprite>(string.Format("Art/token/{0}", ));
        icon.SetIcon(trait.art.objectArt);
        icon.SetFade(1f);
        //tooltip.SetToolTip(equ.title, equ.desc);
        
        gameObject.SetActive(true);
    }
    public void ShowTooltip()
    {
        //Debug.Log(string.Format("showtooltip called"));
        //tooltip.Anim_Popup();
        if (dDataHub.hub.traitDict.Contains(traitUID))
        {
            Trait trait = dDataHub.hub.traitDict.GetFromUID(traitUID);
            tooltip_popup.controller.CallTooltip(transform.position, tooltip_popup.WindowType.horizontal, trait.title, trait.desc);
        }
        else
        {
            tooltip_popup.controller.CallTooltip(transform.position, tooltip_popup.WindowType.horizontal, "通过发表游记<br>或城镇活动获取", "");
        }
    }
    public void Anim_Popup()
    {
        transform.localScale = Vector3.one * 1.5f;
        Sequence seq = DOTween.Sequence();
        seq.Append(transform.DOScale(0f, 0.6f).From());
        seq.Append(transform.DOScale(1f, 0.3f));
    }
}
