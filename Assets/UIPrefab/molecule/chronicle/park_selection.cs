﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class park_selection : selection_base
{
    [Header("Data")]
    public string parkUID;
    //[SerializeField] StringAsset regionTxtTable;

    [Header("Children Objects")]
    [SerializeField] ui_text parkName;
    [SerializeField] Image art;
    [SerializeField] ui_text regionName;
    [SerializeField] ui_text parkSize;
    [SerializeField] List<ui_tag> terrains;
    [SerializeField] ui_text desc;

    [SerializeField] ui_icon_text coin_cost;
    [SerializeField] ui_icon_text time_cost;
    [SerializeField] ui_button confirm_btn;

    public bool SetParkByUID(string uid)
    {
        bool hasEnoughMoney = false;
        parkUID = uid;
        Park park = dDataHub.hub.dPark.GetFromUID(uid);
        parkName.SetText(park.title);
        art.sprite = uAssetReader.controller.GetByName(park.artAsset.objectArt);
        regionName.SetText(park.regionText);
        if (park.maxTier <= 2)
        {
            parkSize.SetText(string.Format("区域：{0}（推荐补给：{1}）", "小", dConstants.ChronicleConst.RECOMMENDED_STAMINA_S));
        }
        else if (park.maxTier <= 3)
        {
            parkSize.SetText(string.Format("区域：{0}（推荐补给：{1}）", "中", dConstants.ChronicleConst.RECOMMENDED_STAMINA_M));
        }
        else
        {
            parkSize.SetText(string.Format("区域：{0}（推荐补给：{1}）", "大", dConstants.ChronicleConst.RECOMMENDED_STAMINA_L));
        }
        //terrainDesc.SetText(string.Format("地势："));
        for (int i = 0; i < terrains.Count; i++)
        {
            if (i < park.terrains.Count)
            {
                terrains[i].SetTag(park.terrains[i]);
                terrains[i].gameObject.SetActive(true);
            }
            else
            {
                terrains[i].gameObject.SetActive(false);
            }
        }
        if (CurrencyManagement.wallet.CheckCurrency("currency_coin") < park.modifiedCoinCost && park.modifiedCoinCost > 0)
        {
            confirm_btn.DisableButton();
            confirm_btn.majorStr.SetText("没有足够金钱");
            //confirm_text.SetFade(0);
            confirm_btn.majorStr.SetTextColor(Color.red);
            hasEnoughMoney = false;
        }
        else
        {
            confirm_btn.EnableButton();
            confirm_btn.majorStr.SetText("出发");
            //confirm_text.SetFade(0);
            confirm_btn.majorStr.SetTextColor(Color.black);
            hasEnoughMoney = true;
        }
        //terrainTags.SetText(park.GetTerrainsString);
        desc.SetText(park.desc);
        coin_cost.majorStr.SetText(park.modifiedCoinCost.ToString());
        coin_cost.gameObject.SetActive(park.modifiedCoinCost > 0);
        time_cost.majorStr.SetText(string.Format("{0}月", park.modifiedTimeCost));
        gameObject.SetActive(true);
        return hasEnoughMoney;
    }
}
