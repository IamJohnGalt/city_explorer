﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class trait_selection : MonoBehaviour
{
    [Header("Data")]
    public string traitUID;
    [Header("Children Objects")]
    [SerializeField] ui_text traitName;
    [SerializeField] Image frame_art;
    [SerializeField] Image character_avatar;
    [SerializeField] ui_text desc;

    [Header("Exposed Objects")]
    public GameObject iconGroup;

    public void SetTraitByUID(string uid)
    {
        traitUID = uid;
        Trait trait = dDataHub.hub.traitDict.GetFromUID(uid);
        traitName.SetText(trait.title);
        frame_art.sprite = uAssetReader.controller.GetByName(trait.art.backgroundArt);
        character_avatar.sprite = CharacterManagement.controller.GetCurrentCharacter().avatar;
        desc.SetText(trait.desc);
    }
}
