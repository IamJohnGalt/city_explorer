﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class end_selection : selection_base
{
    [Header("Children Objects")]
    [SerializeField] Image art;
    [SerializeField] ui_text title;
    [SerializeField] ui_text desc;
    //[SerializeField] uipfb_label_v2 confirm_text;

    public void SetEnding(string _title, string _desc, Sprite _icon)
    {
        art.sprite = _icon;
        title.SetText(_title);
        desc.SetText(_desc);
        gameObject.SetActive(true);
    }
}
