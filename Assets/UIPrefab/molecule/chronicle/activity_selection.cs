﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class activity_selection : selection_base
{
    [Header("Data")]
    public string activityUID;
    [Header("Children Objects")]
    [SerializeField] ui_text activityName;
    [SerializeField] Image actionArt;
    [SerializeField] ui_text activityArtText;
    //[SerializeField] uipfb_label_v2 regionName;
    [SerializeField] ui_text desc;
    [SerializeField] ui_icon_text coin_cost;
    [SerializeField] ui_icon_text time_cost;
    [SerializeField] ui_button select_btn;
    [SerializeField] ui_button no_cost_btn;
    //[SerializeField] uipfb_label_v2 confirm_text;

    public bool SetActivityByUID(string uid)
    {
        activityUID = uid;
        bool hasEnoughMoney = false;
        ChronicleActivity act = dDataHub.hub.dChronicleActivity.GetFromUID(uid);
        activityName.SetText(act.title);
        actionArt.sprite = uAssetReader.controller.GetByName(act.artAsset.objectArt);
        if(act.artAsset.objectArt == "icon_activity_frame")
        {
            activityArtText.SetText(act.title);
            activityArtText.gameObject.SetActive(true);
        }
        else
        {
            activityArtText.gameObject.SetActive(false);
        }
        desc.SetText(act.desc);
        bool hasCost = false;
        if (act.modifiedCoinCost > 0)
        {
            coin_cost.majorStr.SetText(act.modifiedCoinCost.ToString());
            hasCost = true;
            coin_cost.gameObject.SetActive(true);
        }
        else
        {
            coin_cost.gameObject.SetActive(false);
        }
        if (act.modifiedTimeCost > 0)
        {
            time_cost.majorStr.SetText(string.Format("{0}月", act.modifiedTimeCost.ToString()));
            hasCost = true;
            time_cost.gameObject.SetActive(true);
        }
        else
        {
            time_cost.gameObject.SetActive(false);
        }
        select_btn.gameObject.SetActive(hasCost);
        no_cost_btn.gameObject.SetActive(!hasCost);

        if (CurrencyManagement.wallet.CheckCurrency("currency_coin") < act.modifiedCoinCost && act.modifiedCoinCost > 0)
        {
            select_btn.DisableButton();
            select_btn.majorStr.SetText("没有足够金钱");
            //confirm_text.SetFade(0);
            select_btn.majorStr.SetTextColor(Color.red);
            hasEnoughMoney = false;
        }
        else
        {
            select_btn.EnableButton();
            select_btn.majorStr.SetTextColor(Color.black);
            select_btn.majorStr.SetText("前往");
            
            no_cost_btn.EnableButton();
            no_cost_btn.majorStr.SetText("前往");
            //confirm_text.SetFade(0);
            
            hasEnoughMoney = true;
        }
        //time_cost.SetText(act.timeCost.ToString());
        gameObject.SetActive(true);
        return hasEnoughMoney;
    }
}
