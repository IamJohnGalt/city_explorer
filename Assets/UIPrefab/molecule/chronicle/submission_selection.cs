﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class submission_selection : selection_base
{
    [Header("Data")]
    public string submissionOptionUID;
    [Header("Children Objects")]
    [SerializeField] ui_text submissionName;
    [SerializeField] Image art;
    [SerializeField] ui_star totalStar;
    [SerializeField] ui_text descTxt;
    [SerializeField] ui_button submit_btn;

    public void SetSubmissionOption(string journalName, Sprite journalArt, int totalS, string desc)
    {
        art.sprite = journalArt;
        submissionName.SetText(journalName);
        //submissionArt.SetIcon(icon);
        if (totalS > 0)
        {
            totalStar.SetText(totalS);
            totalStar.gameObject.SetActive(true);
        }
        else
        {
            totalStar.gameObject.SetActive(false);
        }
        descTxt.SetText(desc);

    }
}
