﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class chronicle_log : MonoBehaviour
{
    [Header("Data")]
    [SerializeField] SpriteAsset logLineSprites;
    private int lineSpriteIndex = -1;

    [Header("Children Objects")]
    [SerializeField] ui_label date;
    [SerializeField] ui_text log;
    [SerializeField] List<ui_icon> tokens;
    [SerializeField] List<ui_collectible> views;
    [SerializeField] uipfb_mask mask;
    [SerializeField] Image line_bg;



    public void SetLog(ChronicleActionLog logData, bool hiddenDate = false)
    {
        date.SetText(logData.date.DateToString());
        date.gameObject.SetActive(!hiddenDate);
        log.SetText(logData.desc);
        for (int i = 0; i < tokens.Count; i++)
        {
            if (i < logData.tokenPaths.Count)
            {
                tokens[i].SetIcon(logData.tokenPaths[i]);
                tokens[i].gameObject.SetActive(true);
            }
            else
            {
                tokens[i].gameObject.SetActive(false);
            }
        }
        for (int i = 0; i < views.Count; i++)
        {
            if (i < logData.viewPaths.Count)
            {
                views[i].SetAsViewByArt(logData.viewPaths[i]);
                views[i].gameObject.SetActive(true);
            }
            else
            {
                views[i].gameObject.SetActive(false);
            }
        }
    }
    public void ShowLineBackground()
    {
        if(lineSpriteIndex == -1)
        {
            int rng = Random.Range(0, logLineSprites.Assets.Count);
            lineSpriteIndex = rng;
        }
        line_bg.sprite = logLineSprites.Assets[lineSpriteIndex];
        line_bg.gameObject.SetActive(true);
    }
    public void SetToHidden()
    {
        mask.SetFade(1);
        transform.localScale = Vector3.zero;
    }
    public void Anim_Appear(float duration)
    {
        //Debug.Log(string.Format("item_appear called"));
        Sequence seq = DOTween.Sequence();
        seq.Append(transform.DOScale(1f, duration * 0.5f));
        seq.AppendCallback(() => mask.Anim_FadeTo(0, duration * 0.5f));
        //seq.Play();
        //sfx
        SFXPlayer.mp3.PlaySFX("writing");
    }
    public void Anim_FadeTo(float mask_alpha, float duration)
    {
        //Debug.Log(string.Format("item_fadein called"));
        mask.Anim_FadeTo(mask_alpha, duration);
    }
    public void SetTo_FadeOut()
    {
        mask.SetFade(1);
    }
    public void SetTo_Appear()
    {
        transform.localScale = Vector3.one;
        mask.SetFade(0);
    }
}
