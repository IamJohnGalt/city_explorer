﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ui_journal_reward_claim : selection_base
{
    [Header("Data")]
    [SerializeField] RewardItem item;
    //[SerializeField] StringAsset regionTxtTable;

    [Header("Children Objects")]
    [SerializeField] ui_text journalName;
    [SerializeField] Image art;
    [SerializeField] ui_text flavorDesc;
    [SerializeField] ui_text rewardDesc;
    [SerializeField] ui_icon_text reward;
    [SerializeField] ui_button confirm_btn;

    private string curLog;
    private string curLogIcon;

    public void SetClaimReward(RewardItem _rewardItem, string _journalArt, string _title)
    {
        art.sprite = uAssetReader.controller.GetByName(_journalArt);
        journalName.SetText(_title);
        curLog = "";
        curLogIcon = "";
        switch (_rewardItem.rewardUID)
        {
            case "currency_star":
                flavorDesc.SetText(string.Format("于{0}发表，为{1}所收录为第{2}篇，得以流传于世。",
                    CalendarManagement.calendar.GetCurDate().DateToString(),
                    dConstants.MASTER_JOURNAL_NAME,
                    MasterJournalManagement.controller.TotalJournalCount() + 1
                    //Mathf.RoundToInt(CharacterManagement.controller.GetCurrentCharacter().GetTotalFameCompletion*100)
                ));
                rewardDesc.SetText(string.Format("为{0} - {1}篇积累进度：", dConstants.MASTER_JOURNAL_NAME, CharacterManagement.controller.GetCurrentCharacterTitle()));
                reward.icon.SetFrame("icon_star");
                reward.icon.SetIcon();
                reward.minorStr.SetText("累积星级");
                reward.majorStr.SetText(_rewardItem.amount.ToString());

                curLog = string.Format("为{0} - {1}篇积累星级{2}", dConstants.MASTER_JOURNAL_NAME, CharacterManagement.controller.GetCurrentCharacterTitle(), _rewardItem.amount);
                curLogIcon = "icon_star";

                break;
            case "currency_trait":
                flavorDesc.SetText(string.Format("{0}的传作过程让{1}的心智得到了历练。",
                        _title,
                        CharacterManagement.controller.GetCurrentCharacterTitle()
                        ));
                rewardDesc.SetText("获得一个特质用以强化当前旅行：");

                reward.icon.SetFrame();
                reward.icon.SetIcon(CharacterManagement.controller.GetCurrentCharacter().avatar);
                reward.minorStr.SetText("");
                reward.majorStr.SetText(string.Format("选择{0}个特质", _rewardItem.amount));

                curLog = string.Format("让{0}的心智得到了历练，获得一个特质。", CharacterManagement.controller.GetCurrentCharacterTitle());
                curLogIcon = CharacterManagement.controller.GetCurrentCharacter().avatar.name;

                break;
            case "currency_coin":
                flavorDesc.SetText(string.Format("{0}的发表让{1}获得了一些来源于江湖人士的资助。",
                        _title,
                        CharacterManagement.controller.GetCurrentCharacterTitle()
                        ));
                rewardDesc.SetText("获得铜钱资助用以继续当前旅行：");

                reward.icon.SetFrame("icon_l");
                reward.icon.SetIcon("icon_coin");
                reward.minorStr.SetText("获得资助");
                reward.majorStr.SetText(_rewardItem.amount.ToString());

                curLog = string.Format("让{0}获得了一些来源于江湖人士的资助以继续当前旅行。铜钱 +{1}", CharacterManagement.controller.GetCurrentCharacterTitle(), _rewardItem.amount);
                curLogIcon = "icon_coin";

                break;
            case "travel_time":
                flavorDesc.SetText(string.Format("{0}的发表让{1}更加坚定了自己旅行与创作的决心。",
                        _title,
                        CharacterManagement.controller.GetCurrentCharacterTitle()
                        ));
                rewardDesc.SetText("延后了当前旅行的结束时间：");

                reward.icon.SetFrame("icon_l");
                reward.icon.SetIcon("icon_time");
                reward.minorStr.SetText("延长时间");
                reward.majorStr.SetText(string.Format("{0}月",_rewardItem.amount));

                curLog = string.Format("让{0}更加坚定了自己旅行与创作的决心。旅行时间 +{1}月", CharacterManagement.controller.GetCurrentCharacterTitle(), _rewardItem.amount);
                curLogIcon = "icon_time";

                break;
            default:
                if(_rewardItem.type == RewardItem.RewardType.equipment)
                {
                    flavorDesc.SetText(string.Format("朋友在{0}发表后，送来了礼物以表恭贺、敬仰之心。",
                        _title));
                    rewardDesc.SetText("获得额外装备用于接下来的游览：");

                    Equipment equ = dDataHub.hub.equipmentDict.GetFromUID(_rewardItem.rewardUID);

                    reward.icon.SetIcon(equ.art.objectArt);
                    reward.icon.SetFrame("icon_square_m");
                    reward.minorStr.SetText("");
                    reward.majorStr.SetText(equ.title);

                    curLog = string.Format("朋友送来了礼物以表恭贺、敬仰之心。获得装备 {0}", equ.title);
                    curLogIcon = "";
                }
                break;
        }
        //reward icon and flavor to do
        confirm_btn.gameObject.SetActive(true);
    }
    public string GetActionLog()
    {
        if (curLog == "")
        {
            Debug.LogError("cur log at ui_journal_reward is empty");
        }
        return curLog;
    }
    public string GetActionLogIcon()
    {
        if (curLogIcon == "")
        {
            Debug.LogError("cur log icon at ui_journal_reward is empty");
        }
        return curLogIcon;
    }
}
