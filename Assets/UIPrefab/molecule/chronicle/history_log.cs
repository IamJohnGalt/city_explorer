﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class history_log : MonoBehaviour
{
    [Header("Data")]
    [SerializeField] ui_text log;
    [SerializeField] ui_icon_text star;

    public void SetLog(string desc, int starValue)
    {
        log.SetText(desc);
        star.majorStr.SetText(starValue.ToString());
        star.gameObject.SetActive(starValue > 0);
    }
}
