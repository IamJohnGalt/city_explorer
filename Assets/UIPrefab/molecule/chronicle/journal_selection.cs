﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class journal_selection : selection_base
{
    [Header("Data")]
    public string journalUID;
    [Header("Children Objects")]
    [SerializeField] ui_text journalName;
    [SerializeField] Image frame_art;
    [SerializeField] ui_text minCount;
    [SerializeField] ui_text maxCount;
    [SerializeField] ui_text minCoin;
    [SerializeField] ui_text desc;
    //[SerializeField] List<ui_icon> rewardTypes;
    //[SerializeField] journal_rule hint_rule;
    /*
    public void SetJournalByUID(string uid)
    {
        journalUID = uid;
        JournalStyle journal = dDataHub.hub.dJournalStyle.GetFromUID(uid);
        journalName.SetText(journal.title);
        frame_art.sprite = uAssetReader.controller.GetByName(journal.artAsset.backgroundArt);
        minCount.SetText(journal.modifiedComboTokenCount.ToString());
        maxCount.SetText(journal.modifiedComboTokenCount.ToString());
        //check reward types
        /*List<string> tokenFileNames = new List<string>();
        if (journal.fameModifier > 0 || journal.fameBase > 0)
        {
            tokenFileNames.Add("icon_fame");
        }
        if (journal.coinModifier > 0 || journal.coinBase > 0)
        {
            tokenFileNames.Add("icon_coin");
        }
        if (journal.xpModifier > 0 || journal.xpBase > 0)
        {
            tokenFileNames.Add("token_xp");
        }
        if (journal.equipmentModifier > 0 || journal.equipmentBase > 0)
        {
            tokenFileNames.Add("token_random_equipment");
        }
        for (int i = 0; i < rewardTypes.Count; i++)
        {
            if (i < tokenFileNames.Count)
            {
                rewardTypes[i].SetIcon(tokenFileNames[i]);
                rewardTypes[i].gameObject.SetActive(true);
            }
            else
            {
                rewardTypes[i].gameObject.SetActive(false);
            }
        }
        //Debug.Log("Journal Special Rule: " + journal.additionalRuleset[0] + "previewing");
        StarBonusItem preview = gJournal.controller.PreviewRule(journal.additionalRuleset[0]);
        hint_rule.SetItem(preview);
        //Debug.Log("Journal Special Rule needs to be displayed");
        //hint_rule.gameObject.SetActive(false);
        gameObject.SetActive(true);
    }
    */
    public void SetJournalOptionByUID(string uid)
    {
        journalUID = uid;
        JournalOption option = dDataHub.hub.dJournalOption.GetFromUID(uid);
        journalName.SetText(option.title);
        frame_art.sprite = uAssetReader.controller.GetByName(option.artAsset.objectArt);
        minCount.SetText(string.Format("{0}",option.slotRange.minInt.ToString()));
        maxCount.SetText(string.Format("{0}", option.slotRange.maxInt.ToString()));
        minCoin.SetText(option.minCoin);
        desc.SetText(option.desc);
        gameObject.SetActive(true);
    }
}
