﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class uiRepeatPath : MonoBehaviour
{
    [SerializeField] List<ui_icon> locations;
    public List<string> curLocationIcons = new List<string>();
    public void SetLocations(List<string> iconNames)
    {
        curLocationIcons = iconNames;
        for (int i = 0; i < locations.Count; i++)
        {
            if (i < iconNames.Count)
            {
                locations[i].SetIcon(iconNames[i]);
            }
        }
    }
    
}
