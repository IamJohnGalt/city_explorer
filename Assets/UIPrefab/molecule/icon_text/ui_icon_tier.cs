﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ui_icon_tier : MonoBehaviour
{
    [SerializeField] ui_icon_text icon_text;
    [SerializeField] ColorPalette tier_palette;

    public void SetTier(int tier)
    {
        icon_text.majorStr.SetText(tier.ToString());
        icon_text.icon.SetFrameColor(tier_palette.GetAsset(tier.ToString()));
    }
}
