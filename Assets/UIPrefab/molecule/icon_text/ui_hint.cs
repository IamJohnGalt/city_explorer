﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ui_hint : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] string hintText;
    [SerializeField] tooltip_popup.WindowType type = tooltip_popup.WindowType.vertical_down;

    public void ShowHintTooltip()
    {
        tooltip_popup.controller.CallTooltipBasedOnMouse(Input.mousePosition, type, "", hintText);
    }
}
