﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class ui_icon_text : MonoBehaviour
{
    public ui_icon icon;
    public ui_text majorStr;
    public ui_text minorStr;

    [Header("Animation Constants")]
    static private float valueShiftTime = 0.05f;
    static private float animationTimePerTick = 0.3f;
    private Sequence anim_icon;
    private Sequence anim_text;
    public void Anim_ValueEarn(int ticks)
    {
        DOTween.Kill(anim_icon);
        anim_icon = DOTween.Sequence();
        for(int i = 0; i < ticks; i++)
        {
            anim_icon.AppendCallback(() => icon.Anim_Earn());
            anim_icon.AppendInterval(animationTimePerTick);
        }
        DOTween.Kill(anim_text);
        anim_text = DOTween.Sequence();
        int valueShiftTimes = Mathf.CeilToInt((animationTimePerTick * ticks) / valueShiftTime);
        for (int i = 0; i < valueShiftTimes; i++)
        {
            anim_text.AppendCallback(() => majorStr.Anim_SetToRandomNumber());
            anim_text.AppendInterval(valueShiftTime);
        }
        anim_text.AppendCallback(() => majorStr.ResetText());
    }
    public void Anim_ValueLose(int ticks)
    {
        anim_icon.Kill();
        anim_icon = DOTween.Sequence();
        for (int i = 0; i < ticks; i++)
        {
            anim_icon.AppendCallback(() => icon.Anim_Lose());
            anim_icon.AppendInterval(animationTimePerTick);
        }
        anim_text.Kill();
        anim_text = DOTween.Sequence();
        int valueShiftTimes = Mathf.CeilToInt((animationTimePerTick * ticks) / valueShiftTime);
        for (int i = 0; i < valueShiftTimes; i++)
        {
            anim_text.AppendCallback(() => majorStr.Anim_SetToRandomNumber());
            anim_text.AppendInterval(valueShiftTime);
        }
        anim_text.AppendCallback(() => majorStr.ResetText());
    }
    void Anim_SetToRandomNumber(int digit)
    { 
    }
}
