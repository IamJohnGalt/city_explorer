﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class stat_item : MonoBehaviour
{
    public string statUID;
    [SerializeField] ui_icon icon;
    [SerializeField] ui_text token_level;

    public void ShowStatByUID(string uid = "")
    {
        if (uid == "")
        {
            gameObject.SetActive(false);
            return;
        }
        statUID = uid;
        CharacterStat stat = dDataHub.hub.dCharacterStats.GetFromUID(statUID);
        //Sprite iconSpt = Resources.Load<Sprite>(string.Format("Art/token/{0}", ));
        icon.SetIcon(CharacterManagement.controller.GetCurrentCharacter().token);
        //icon.SetFade(1f);
        token_level.SetText(CharacterManagement.controller.GetCurrentCharacter().GetStoryLevelText(stat.level));
        //tooltip.SetToolTip(equ.title, equ.desc);
        gameObject.SetActive(true);
    }
    public void ShowTooltip()
    {
        //Debug.Log(string.Format("showtooltip called"));
        //tooltip.Anim_Popup();
        if (dDataHub.hub.dCharacterStats.Contains(statUID))
        {
            CharacterStat stat = dDataHub.hub.dCharacterStats.GetFromUID(statUID);
            tooltip_popup.controller.CallTooltip(transform.position, tooltip_popup.WindowType.horizontal, stat.title, stat.desc);
        }
    }
    public void Anim_Highlight()
    {
        Sequence seq = DOTween.Sequence();
        seq.Append(transform.DOScale(1.3f, 0.5f));
        seq.AppendInterval(0.3f);
        seq.Append(transform.DOScale(1f, 0.5f));
    }
}
