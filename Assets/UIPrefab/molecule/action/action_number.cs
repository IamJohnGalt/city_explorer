﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class action_number : MonoBehaviour
{
    [Header("Children")]
    [SerializeField] GameObject content;
    [SerializeField] ui_text shortNumnber;
    [SerializeField] ui_text longNumnber;
    [SerializeField] Image shortFrame;
    [SerializeField] Image longFrame;
    [SerializeField] Image shortShape;
    [SerializeField] Image longShape;
    [SerializeField] action_indicator shortIndicator;
    [SerializeField] action_indicator longIndicator;
    [Header("Stat Attributes")]
    [SerializeField] bool isPositiveStat;

    private bool isShort = false;
    private string numberStr = "";
    private Sequence anim_icon;
    private Sequence anim_text;

    public void SetNumber(string numberTxt, float delta)
    {

        string oldString = numberStr;
        numberStr = numberTxt;
        if (numberTxt.Length < 3)
        {
            isShort = true;
            shortNumnber.SetText(numberTxt);
            shortFrame.gameObject.SetActive(true);
            longFrame.gameObject.SetActive(false);
            shortIndicator.SetIndicator(isPositiveStat, delta);
        }
        else
        {
            isShort = false;
            longNumnber.SetText(numberTxt);
            shortFrame.gameObject.SetActive(false);
            longFrame.gameObject.SetActive(true);
            longIndicator.SetIndicator(isPositiveStat, delta);
        }
        if (oldString != "" && oldString != numberTxt)
        {
            Anim_ValueChanged();
        }
        gameObject.SetActive(numberStr != "");
    }
    public void SetWaterMark(string iconName)
    {
        shortShape.sprite = uAssetReader.controller.GetByName(iconName);
        longShape.sprite = uAssetReader.controller.GetByName(iconName);
    }
    public void SetColor(Color clr)
    {
        shortFrame.color = clr;
        longFrame.color = clr;
    }

    public void Anim_ValueChanged()
    {
        //Debug.Log("Anim_ValueChanged Change Animation called on action number");
        
        anim_icon.Kill();
        anim_icon = DOTween.Sequence();
        anim_icon.Append(content.transform.DOScale(1.3f, 0.3f));
        anim_icon.Append(content.transform.DOScale(1f, 0.3f));

        anim_text.Kill();
        anim_text = DOTween.Sequence();
        int valueShiftTimes = 6;
        for (int i = 0; i < valueShiftTimes; i++)
        {
            anim_text.AppendCallback(() => shortNumnber.Anim_SetToRandomNumber());
            anim_text.AppendCallback(() => longNumnber.Anim_SetToRandomNumber());
            anim_text.AppendInterval(0.05f);
        }
        anim_text.AppendCallback(() => shortNumnber.ResetText());
        anim_text.AppendCallback(() => longNumnber.ResetText());
    }
}
