﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ui_dice : ui_icon
{
    [SerializeField] SpriteAsset diceAssets;
    private static int ROLL_ANIM_COUNT = 20;
    private static float ROLL_ANIM_DURATION = 0.05f;

    public void Anim_Roll()
    {
        //Debug.Log("dice rolling effect");
        Sequence seq = DOTween.Sequence();
        for (int i = 0; i < ROLL_ANIM_COUNT; i++)
        {
            seq.AppendCallback(SetRandomDice);
            seq.AppendInterval(ROLL_ANIM_DURATION);
        }
        seq.AppendCallback(ResetDice);
    }
    public void ResetDice()
    {
        SetIcon(diceAssets.GetAsset(0));
    }
    void SetRandomDice()
    {
        int rng = Random.Range(1, diceAssets.Assets.Count);
        SetIcon(diceAssets.GetAsset(rng));
    }
}
