﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class action_indicator : MonoBehaviour
{
    [SerializeField] SpriteAsset signSprites;
    [SerializeField] ui_icon indicatorIcon;
    public void SetIndicator(bool isPositiveStat, float delta)
    {
        if (delta == 0)
        {
            indicatorIcon.gameObject.SetActive(false);
            return;
        }
        else
        {
            indicatorIcon.gameObject.SetActive(true);
        }
        if(isPositiveStat && delta > 0)
        {
            indicatorIcon.SetIcon(signSprites.GetAsset("pos_plus"));
        }
        else if(isPositiveStat && delta < 0)
        {
            indicatorIcon.SetIcon(signSprites.GetAsset("neg_minus"));
        }
        else if (!isPositiveStat && delta > 0)
        {
            indicatorIcon.SetIcon(signSprites.GetAsset("neg_plus"));
        }
        else if (!isPositiveStat && delta < 0)
        {
            indicatorIcon.SetIcon(signSprites.GetAsset("pos_minus"));
        }
        else
        {
            Debug.LogError("invalid status on action_indicator");
        }
    }
}
