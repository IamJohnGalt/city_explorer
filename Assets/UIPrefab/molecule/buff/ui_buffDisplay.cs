﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ui_buffDisplay : MonoBehaviour
{
    [Header("Data")]
    [SerializeField] BuffDisplayItem curItem;
    [Header("Children Objects")]
    [SerializeField] ui_icon buff_icon;
    [SerializeField] ui_text buff_desc;

    public void SetBuffDisplay(BuffDisplayItem item)
    {
        curItem = item;
        if(item.operatorName!= "")
        {
            buff_icon.SetFrame(item.operatorName);
        }
        else
        {
            buff_icon.SetFrame();
        }
        buff_icon.SetIcon(item.iconName);
        buff_desc.SetText(item.desc);
    }
}
