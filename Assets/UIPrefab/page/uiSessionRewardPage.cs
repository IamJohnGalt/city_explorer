﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class uiSessionRewardPage : uiElement
{
    [Header("Data")]
    public CurrencyList CurrencyData;
    public dataEndSession EndSessionData;
    public dataStats StatsData;

    [Header("Groups")]
    [SerializeField] GameObject group_page;
    [SerializeField] GameObject group_stats;
    [SerializeField] GameObject group_fame;
    [SerializeField] GameObject group_xp;
    [SerializeField] GameObject group_token;

    [Header("Children Objects")]
    [SerializeField] ui_text endingDesc;
    [SerializeField] ui_text score;
    //[SerializeField] uipfb_icon_v2 portrait;
    [SerializeField] ui_icon_text time;
    [SerializeField] ui_icon_text journal;
    [SerializeField] ui_icon_text park;
    [SerializeField] ui_icon_text fame;
    [SerializeField] ui_icon_text xp;
    [SerializeField] List<ui_icon_text> memos;
    //[SerializeField] List<uiCurrencyItem> currencies;
    [SerializeField] ui_button mainmenu_btn;
    [SerializeField] ui_button again_btn;
    [SerializeField] character_entry portrait_1;
    [SerializeField] character_entry portrait_2;

    //[Header("GameEvent")]
    //[SerializeField] GameEvent ClaimPermReward;
    private void Awake()
    {
        group_page.SetActive(false);
    }

    public void btn_confirm()
    {
        UIClose();
        EndSession.controller.ClaimPermReward();
        EndSession.controller.ShowNewContent();
    }
    public override void UIInit()
    {
        UIUpdate();
        Anim_Popup();
        if (CharacterManagement.controller.GetCurrentCharacter().characterUID == "character_1" && CharacterManagement.controller.GetCurrentCharacter().curStoryLevel >= 4)
        {
            CharacterPersona.controller.Persona_End_Session_More_XP();
        }
        group_page.SetActive(true);
    }
    public override void UIUpdate()
    {
        endingDesc.SetText(string.Format("{0}在经历了{1}年的旅行与创作后，<br>逐渐淡出了人们的视野。<br>而他的游记在{2}的收录下，得以流传于世......",
            CharacterManagement.controller.GetCurrentCharacter().characterName,
            CalendarManagement.calendar.calendarData.pastYears,
            dConstants.MASTER_JOURNAL_NAME));
        //portrait.SetIcon(CharacterManagement.controller.GetCurrentCharacterPortrait());
        score.SetText(string.Format("最终得分：{0}", EndSessionData.finalSum));
        time.majorStr.SetText(CalendarManagement.calendar.calendarData.TotalTravelTimeString);
        journal.majorStr.SetText(string.Format("{0}篇", StatsData.inSessionJournalLogs.Count));
        park.majorStr.SetText(string.Format("{0}处", StatsData.inSessionParks.Count));
        fame.majorStr.SetText(CurrencyManagement.wallet.CheckCurrency("currency_star").ToString());
        xp.majorStr.SetText((CurrencyManagement.wallet.CheckCurrency("currency_star") * CurrencyManagement.wallet.CheckCurrency("currency_xp")).ToString());
        for(int i = 0; i < memos.Count; i++)
        {
            int memoEarned = CurrencyManagement.wallet.CheckCurrency(string.Format("currency_token_charac{0}", i + 1));
            Currency cur = dDataHub.hub.currencyDict.GetFromUID(string.Format("currency_token_charac{0}", i + 1));
            if (memoEarned > 0)
            {
                memos[i].minorStr.SetText(cur.CurrencyName);
                memos[i].majorStr.SetText(memoEarned.ToString());
                memos[i].icon.SetIcon(cur.artAsset.objectArt);
                memos[i].gameObject.SetActive(true);
            }
            else
            {
                memos[i].gameObject.SetActive(false);
            }
        }
        mainmenu_btn.gameObject.SetActive(true);
        again_btn.gameObject.SetActive(true);
        portrait_1.UpdateToCurrentAvatar();
        portrait_2.UpdateToCurrentAvatar();
    }
    public override void UIClose()
    {
        group_page.SetActive(false);
    }
    void Anim_Popup()
    {
        RectTransform rect = GetComponent<RectTransform>();
        rect.localScale = Vector3.one;
        rect.DOScale(Vector3.zero, 0.3f).From().SetEase(Ease.OutSine);
    }
}
