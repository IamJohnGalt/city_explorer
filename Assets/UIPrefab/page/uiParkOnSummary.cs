﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class uiParkOnSummary : uiElement
{
    [Header("Data")]
    public dataParkVisit data;
    public gParkVisit parkGameplayScript;
    public uiParkOnMap mapUI;

    [Header("Events")]
    [SerializeField] GameEvent ParkVisitEnd;

    [Header("Display Groups")]
    [SerializeField] GameObject group_page;
    [SerializeField] GameObject group_header;
    [SerializeField] GameObject group_stats;
    [SerializeField] GameObject group_collectible;
    //[SerializeField] GameObject group_character;

    [Header("Main")]
    [SerializeField] ui_text end_title;
    [SerializeField] ui_icon_text day;
    [SerializeField] ui_icon_text node;
    [SerializeField] ui_icon_text progress;
    [SerializeField] List<ui_collectible> collectibles;
    [SerializeField] ui_button btn_confirm;
    //[SerializeField] character_entry portrait;
    public ui_icon_text endReason;

    public void btn_confirm_hit()
    {
        UIClose();
        FocusPointEffect.controller.StartFocusOn(mapUI.map.character.transform, 20f, 3f, 1f, 1.5f, 0.5f, 0.5f, 0.5f, ()=>parkGameplayScript.ExitPark());
        SFXPlayer.mp3.PlaySFX("guqin_parkend");
    }
    public void Start()
    {
        group_page.SetActive(false);
    }
    public override void UIInit()
    {
        //portrait.UpdateToCurrentAvatar();
        UIUpdate();
        group_page.SetActive(true);
        Anim_Popup();
        if (data.inFinalNode)
        {
            Sequence seq = DOTween.Sequence();
            seq.AppendInterval(1f);
            seq.AppendCallback(() => gIntroParkScript.controller.SetFinalNodeIntro());
        }
        else if (data.staminaDepleted && !data.inFinalNode)
        {
            Sequence seq = DOTween.Sequence();
            seq.AppendInterval(1f);
            seq.AppendCallback(() => gIntroParkScript.controller.SetOutOfStaminaIntro());
        }
        
    }
    public override void UIClose()
    {
        group_page.SetActive(false);
    }
    public override void UIUpdate()
    {
        //ClearGroups();
        end_title.SetText(string.Format("{0}从{1}返回", CalendarManagement.calendar.GetCurDate().DateToString(), data.parkName));
        day.majorStr.SetText(string.Format("{0}月", data.totalMonths));
        node.majorStr.SetText(string.Format("{0}个节点", data.totalNodes-1));
        //progress.majorStr.SetText(string.Format("{0}里", data.totalModifiedProgress));
        progress.majorStr.SetText(string.Format("{0}里", data.totalProgress));
        for(int i = 0; i < collectibles.Count; i++)
        {
            if(i < data.acquiredCollectibles.Count)
            {
                collectibles[i].SetAsCollectibleByUID(data.acquiredCollectibles[data.acquiredCollectibles.Count - 1 - i], dConstants.ParkConst.TokenStatus.active);
                collectibles[i].gameObject.SetActive(true);
            }
            else
            {
                collectibles[i].gameObject.SetActive(false);
            }
            collectibles[i].transform.localScale = Vector3.one * (data.acquiredCollectibles.Count > 4 ? 0.8f : 1f);
        }
        if (data.inFinalNode)
        {
            ParkNode node = dDataHub.hub.dParkNode.GetFromUID(data.curNodeDataUID);
            endReason.icon.SetIcon(node.artAsset.objectArt);
            if (data.staminaReserved > 0)
            {
                endReason.majorStr.SetText(string.Format("到达终点（额外保留补给：{0}）", data.staminaReserved));
            }
            else
            {
                endReason.majorStr.SetText(string.Format("到达终点"));
            }
                
        }
        else if(!data.staminaDepleted && data.staminaReserved >= 0)
        {
            endReason.icon.SetIcon("icon_gate");
            endReason.majorStr.SetText(string.Format("主动返回（额外保留补给：{0}）", data.staminaReserved));
        }
        else
        {
            endReason.icon.SetIcon("dynamic_current_stamina");
            endReason.majorStr.SetText("补给耗尽");
        }
        btn_confirm.gameObject.SetActive(true);
    }
    public void Anim_Popup()
    {
        //group_header.GetComponent<RectTransform>().DOAnchorPosY(300f, 1f).From(true).SetEase(Ease.OutBack);
        group_page.GetComponent<RectTransform>().DOScale(Vector3.zero, 0.6f).From().SetEase(Ease.OutBack);
    }
}
