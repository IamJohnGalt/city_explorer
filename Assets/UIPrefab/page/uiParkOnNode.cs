﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class uiParkOnNode : uiElement
{
    [Header("Data")]
    public string targetNodePfbUID;
    public gParkVisit parkGameplayScript;
    [SerializeField] dataParkVisit data;
    [SerializeField] uiParkOnMap masterUIPage;

    [Header("Display Groups")]
    [SerializeField] GameObject group_page;
    [SerializeField] GameObject group_main;
    [SerializeField] GameObject group_bg;
    [SerializeField] GameObject group_node;
    [SerializeField] GameObject group_reward;
    [SerializeField] GameObject group_view;
    [SerializeField] GameObject group_collectible;

    [Header("Children Objects")]
    [SerializeField] ui_icon node_icon;
    [SerializeField] ui_text node_title;
    [SerializeField] ui_text node_desc;
    [SerializeField] ui_text reward_desc;
    [SerializeField] ui_text collectible_desc;
    [SerializeField] ui_button btn_exit;
    [SerializeField] ui_button btn_skip;
    [SerializeField] ui_button btn_confirm;
    [SerializeField] List<ui_reward> rewards;
    [SerializeField] List<ui_collectible> views;
    [SerializeField] List<ui_collectible> collectibles;

    [Header("Special Objects")]
    [SerializeField] GameObject group_2ndreward;
    [SerializeField] ui_button btn_2ndreward;
    [SerializeField] GameObject group_redrawcol;
    [SerializeField] ui_button btn_redrawcol;

    [Header("Event")]
    [SerializeField] GameEvent StaminaChanged;

    static private float viewTransitionTime = 0.5f;
    //[SerializeField] int selectedViewIndex = -1;
    //[SerializeField] int selectedCollectibleIndex = -1;

    private uiParkNode nodeObj;
    private bool isCurrentNode { get { return targetNodePfbUID == data.curNodePfbUID; } }

    private void Awake()
    {
        UIClose();
        for (int i = 0; i < rewards.Count; i++)
        {
            rewards[i].ref_action_hit_func = btn_reward_hit;
        }
        for (int i = 0; i < views.Count; i++)
        {
            views[i].ref_token_selected_func = btn_view_hit;
        }
        for (int i=0;i < collectibles.Count; i++)
        {
            collectibles[i].ref_token_selected_func = btn_collectible_hit;
        }
    }
    public void btn_reward_hit(int index)
    {
        if(rewards[index].status == dConstants.ParkConst.TokenStatus.playable)
        {
            parkGameplayScript.CardPlayedOnRewardPool(index);
            //sfx
            SFXPlayer.mp3.PlaySFX("button_click");
        }
        UIUpdate();
        if (data.parkUID == dConstants.ParkConst.INTRO_PARK_UID)
        {
            Sequence seq = DOTween.Sequence();
            seq.AppendInterval(4f).OnComplete(() => gIntroParkScript.controller.SetDialogAndMask());
        }
    }
    public void btn_view_hit(int index)
    {
        if (views[index].status == dConstants.ParkConst.TokenStatus.playable)
        {
            //selectedViewIndex = index;
            parkGameplayScript.CardPlayedOnViewPool(index);
            //sfx
            SFXPlayer.mp3.PlaySFX("button_click");
        }
        Anim_InitCollectible();
        Anim_View2Collectible();
        //UIUpdate();
        //Animation Need
    }
    public void btn_collectible_hit(int index)
    {
        if (collectibles[index].status == dConstants.ParkConst.TokenStatus.playable)
        {
            //selectedCollectibleIndex = index;
            parkGameplayScript.CardPlayedOnCollectiblePool(index);
            /*collectibles[index].ShutOffInteraction();
            collectibles[index].HoverOff();
            CollectEffect.Collect(collectibles[index].gameObject, 1, new Vector2(1f, 0f), UICanvas.canvas.EffectLayer, collectibles[index].transform, UIKeyElements.locator.Journal);
            collectibles[index].ResumeInteraction();
            */
        }
        if (data.parkUID == dConstants.ParkConst.INTRO_PARK_UID)
        {
            Sequence seq = DOTween.Sequence();
            seq.AppendInterval(4f).OnComplete(() => gIntroParkScript.controller.SetDialogAndMask());
        }
        UIUpdate();
        //Animation Need
    }
    public void btn_exit_hit()
    {
        UIClose();
    }
    public void btn_skip_hit()
    {
        parkGameplayScript.SkipCollectible();
        UIUpdate();
    }
    public void btn_confirm_hit()
    {
        UIClose();
    }
    public void btn_2nd_reward_hit()
    {
        parkGameplayScript.Enable2ndRewardOnRewardPool();
        StaminaChanged.Raise();
        UIUpdate();
    }
    public void btn_redraw_collectible_hit()
    {
        parkGameplayScript.RedrawCollectibleByAbility();
        UIUpdate();
    }
    public override void UIInit()
    {
        if (masterUIPage.OnNodeStartQueued)
        {
            return;
        }
        //selectedViewIndex = -1;
        //selectedCollectibleIndex = -1;
        bool found = false;
        for(int i = 0; i < data.currentMap.nodes.Count; i++)
        {
            if (data.currentMap.nodes[i].onClick)
            {
                targetNodePfbUID = data.currentMap.nodes[i].pfbUID;
                found = true;
                break;
            }
        }
        //if not found, default to current node
        if (!found)
        {
            targetNodePfbUID = data.curNodePfbUID;
        }
        if (!data.currentNodeViewed)
        {
            for(int i = 0; i < rewards.Count; i++)
            {
                rewards[i].ResetUsage();
            }
        }
        parkGameplayScript.NodeVisitedOnMap(targetNodePfbUID);
        //btn_exit.HoverOff();
        //btn_skip.HoverOff();
        //btn_confirm.HoverOff();
        UIUpdate();
        //UICollectibleUpdate();
        if (data.parkUID == dConstants.ParkConst.INTRO_PARK_UID)
        {
            Sequence seq = DOTween.Sequence();
            seq.AppendInterval(0.5f).OnComplete(() => gIntroParkScript.controller.SetDialogAndMask());
        }
        Anim_Popup();
        group_page.SetActive(true); 
    }
    public override void UIClose()
    {
        group_page.SetActive(false);
        nodeObj = null;
    }
    public override void UIUpdate()
    {
        //Debug.Log("calling UIUpdate()");
        nodeObj = data.currentMap.RetrieveNodeByPfbUID(targetNodePfbUID);
        if (nodeObj == null)
        {
            Debug.LogError("Cannot find a valid node obj and data to popup");
            return;
        }
        ParkNode nodeData = dDataHub.hub.dParkNode.GetFromUID(nodeObj.nodeUID);
        node_title.SetText(nodeData.title);
        node_icon.SetIcon(nodeData.artAsset.objectArt);
        node_desc.SetText(nodeData.desc);
        //group_reward.SetActive(nodeObj.status == uiParkNode.NodeStatus.current || nodeObj.status == uiParkNode.NodeStatus.visited);
        //group_collectible.SetActive(true);
        for(int i = 0; i < rewards.Count; i++)
        {
            rewards[i].gameObject.SetActive(false);
            if (i< nodeObj.rewardUIDs.Count)
            {
                //special case for skip collectible reward
                if(nodeObj.rewardUIDs[i] == "parkreward_fangkong" || nodeObj.rewardUIDs[i] == "parkreward_dengdai")
                {
                    bool collectibleClaimed = false;
                    for(int j=0;j< nodeObj.collectibleClaimed.Count; j++)
                    {
                        if (nodeObj.collectibleClaimed[j])
                        {
                            collectibleClaimed = true;
                        }
                    }
                    if (collectibleClaimed)
                    {
                        rewards[i].SetTokenByUID(nodeObj.rewardUIDs[i], dConstants.ParkConst.TokenStatus.inactive);
                    }
                    else
                    {
                        rewards[i].SetTokenByUID(nodeObj.rewardUIDs[i], DetermineRewardStatus(isCurrentNode, nodeObj.rewardClaimed[i], nodeObj.status, data.RewardClaimed));
                    }
                }
                //regular cases
                else
                {
                    rewards[i].SetTokenByUID(nodeObj.rewardUIDs[i], DetermineRewardStatus(isCurrentNode, nodeObj.rewardClaimed[i], nodeObj.status, data.RewardClaimed));
                }
            }
        }
        bool viewClaimed = false;
        for(int i=0;i< nodeObj.viewClaimed.Count; i++)
        {
            if (nodeObj.viewClaimed[i])
            {
                viewClaimed = true;
                break;
            }
        }
        if(viewClaimed)
        {
            for (int i = 0; i < views.Count; i++)
            {
                views[i].gameObject.SetActive(false);
            }
            bool collectibleClaimed = false;
            for (int i = 0; i < nodeObj.collectibleClaimed.Count; i++)
            {
                if (nodeObj.collectibleClaimed[i])
                {
                    collectibleClaimed = true;
                }
            }
            for (int i = 0; i < collectibles.Count; i++)
            {
                collectibles[i].gameObject.SetActive(false);
                if (i < nodeObj.collectibleUIDs.Count)
                {
                    //each needs to be revealed by anim script
                    collectibles[i].SetAsCollectibleByUID(nodeObj.collectibleUIDs[i], DetermineCollectibleStatus(isCurrentNode, collectibleClaimed, nodeObj.status));
                    collectibles[i].SetViewFade(1f);
                    collectibles[i].SetTagFade(1f);
                }
            }
            //group_view.SetActive(true);
            collectible_desc.SetText("选择一种方式记录...");
            //btn_skip.gameObject.SetActive(true);
        }
        else
        {
            for (int i = 0; i < views.Count; i++)
            {
                views[i].gameObject.SetActive(false);
                if (i < nodeObj.viewUIDs.Count)
                {
                    //each needs to be hidden by anim script later
                    views[i].SetAsViewByUID(nodeObj.viewUIDs[i], dConstants.ParkConst.TokenStatus.playable);
                    views[i].SetFade(1f);
                }
            }
            for (int i = 0; i < collectibles.Count; i++)
            {
                collectibles[i].gameObject.SetActive(false);
            }
            //group_collectible.SetActive(true);
            collectible_desc.SetText("欣赏美景...");
            //btn_skip.gameObject.SetActive(false);
        }
        //second reward button
        if(data.Allow2ndReward && data.RewardClaimed)
        {
            group_2ndreward.gameObject.SetActive(true);
            if(CurrencyManagement.wallet.CheckCurrency("currency_coin") >= dConstants.ParkConst.SECOND_REWARD_COIN_COST_VILLAGE)
            {
                btn_2ndreward.EnableButton();
                btn_2ndreward.majorStr.SetText("店铺（选择额外奖励）");
                btn_2ndreward.majorStr.SetTextColor(Color.black);
            }
            else
            {
                btn_2ndreward.DisableButton();
                btn_2ndreward.majorStr.SetText("铜钱不足");
                btn_2ndreward.majorStr.SetTextColor(Color.red);
            }
        }
        else
        {
            group_2ndreward.gameObject.SetActive(false);
        }
        //redraw collectible button
        if (data.AllowCollectibleRedraw && !data.CollectibleResolved)
        {
            group_redrawcol.gameObject.SetActive(true);
            if (data.staminaCur >= dConstants.ParkConst.REDRAW_COLLECTIBLE_COST)
            {
                btn_redrawcol.EnableButton();
                btn_redrawcol.majorStr.SetText("耐心（重新抽取风景）");
                btn_redrawcol.majorStr.SetTextColor(Color.black);
            }
            else
            {
                btn_redrawcol.DisableButton();
                btn_redrawcol.majorStr.SetText("补给不足");
                btn_redrawcol.majorStr.SetTextColor(Color.red);
            }
        }
        else
        {
            group_redrawcol.gameObject.SetActive(false);
        }

        btn_exit.gameObject.SetActive(true);
        btn_confirm.gameObject.SetActive(data.CollectibleResolved && data.RewardClaimed);
    }
    
    dConstants.ParkConst.TokenStatus DetermineRewardStatus(bool isCurrent, bool isPlayed, uiParkNode.NodeStatus nodeStatus, bool rewardClaimed)
    {
        if (isCurrent && !isPlayed && nodeStatus == uiParkNode.NodeStatus.current && !rewardClaimed)
        {
            return dConstants.ParkConst.TokenStatus.playable;
        }
        else if (isCurrent && !isPlayed && nodeStatus == uiParkNode.NodeStatus.current && rewardClaimed)
        {
            return dConstants.ParkConst.TokenStatus.inactive;
        }
        else if (isPlayed)
        {
            return dConstants.ParkConst.TokenStatus.played;
        }
        else if (nodeStatus == uiParkNode.NodeStatus.visited || nodeStatus == uiParkNode.NodeStatus.passed)
        {
            return dConstants.ParkConst.TokenStatus.hidden;
        }
        else
        {
            return dConstants.ParkConst.TokenStatus.preview;
        }
    }
    dConstants.ParkConst.TokenStatus DetermineCollectibleStatus(bool isCurrent, bool isPlayed, uiParkNode.NodeStatus nodeStatus)
    {
        if(isCurrent && !isPlayed && nodeStatus == uiParkNode.NodeStatus.current)
        {
            return dConstants.ParkConst.TokenStatus.playable;
        }
        else
        {
            return dConstants.ParkConst.TokenStatus.hidden;
        }
        /*else if(nodeStatus == uiParkNode.NodeStatus.visited || nodeStatus == uiParkNode.NodeStatus.passed)
        {
            return dConstants.ParkConst.TokenStatus.hidden;
        }
        else
        {
            return dConstants.ParkConst.TokenStatus.preview;
        }*/
    }
    void ClearGroups()
    {
        group_reward.SetActive(false);
    }
    /*void Anim_View2Collectible()
    {
        //Debug.Log("calling Anim_View2Collectible()");
        nodeObj = data.currentMap.RetrieveNodeByPfbUID(targetNodePfbUID);
        Sequence seq = DOTween.Sequence();
        ui_collectible selectedView = views[selectedViewIndex];
        //fade all unselected view
        if(nodeObj.viewUIDs.Count > 1)
        {
            seq.AppendInterval(viewTransitionTime);
            for (int i = 0; i < views.Count; i++)
            {
                ui_collectible item = views[i];
                if (i != selectedViewIndex)
                {
                    item.SetFade(0f, viewTransitionTime);
                }
            }
        }
        //seq.AppendCallback(() => selectedView.SetFade(0f, viewTransitionTime * 2));
        seq.AppendCallback(() => Anim_RevealCollectibleTag());
        seq.AppendInterval(viewTransitionTime * 2f);
        seq.AppendCallback(UIUpdate);
    }*/
    void Anim_InitCollectible()
    {
        //Debug.Log("calling UICollectibleUpdate()");
        for (int i = 0; i < views.Count; i++)
        {
            views[i].gameObject.SetActive(false);
        }
        for (int i = 0; i < collectibles.Count; i++)
        {
            collectibles[i].gameObject.SetActive(false);
            if (i < nodeObj.collectibleUIDs.Count)
            {
                //each needs to be revealed by anim script
                collectibles[i].SetAsCollectibleByUID(nodeObj.collectibleUIDs[i], DetermineCollectibleStatus(isCurrentNode, nodeObj.collectibleClaimed[i], nodeObj.status));
                collectibles[i].SetViewFade(1f);
                collectibles[i].SetTagFade(0f);
            }
        }
        //group_collectible.SetActive(true);
    }
    void Anim_RevealCollectibleTag()
    {
        //Debug.Log("calling Anim_RevealAllCollectibles");
        for (int i = 0; i < nodeObj.collectibleUIDs.Count; i++)
        {
            ui_collectible item = collectibles[i];
            item.SetTagFade(1f, viewTransitionTime * 2);
            //item.transform.DOLocalRotate(new Vector3(0,0,-5), 0.05f).SetLoops(-1, LoopType.Yoyo);
        }
        collectible_desc.SetText("选择一种方式记录...");
        //btn_skip.gameObject.SetActive(true);
    }
    void Anim_View2Collectible()
    {
        nodeObj = data.currentMap.RetrieveNodeByPfbUID(targetNodePfbUID);
        //hide all view instantly
        for (int i = 0; i < views.Count; i++)
        {
            ui_collectible item = views[i];
            item.SetFade(0f);
        }
        //show all collectible in the view location without tags
        for (int i = 0; i < collectibles.Count; i++)
        {
            ui_collectible item = collectibles[i];
            item.SetViewFade(1f);
            item.SetTagFade(0f);
            //item.transform.DOLocalRotate(new Vector3(0,0,-5), 0.05f).SetLoops(-1, LoopType.Yoyo);
        }
        if(nodeObj.collectibleUIDs.Count > 1)
        {
            group_collectible.GetComponent<HorizontalLayoutGroup>().spacing = -260f;
            DOTween.To(() => group_collectible.GetComponent<HorizontalLayoutGroup>().spacing,
            x => group_collectible.GetComponent<HorizontalLayoutGroup>().spacing = x,
            30f, viewTransitionTime).OnComplete(Anim_RevealCollectibleTag);
        }
        else
        {
            ui_collectible item = collectibles[1];
            item.ShutOffInteraction();
            item.SetViewFade(0f);
            item.SetTagFade(0f);
            item.gameObject.SetActive(true);

            group_collectible.GetComponent<HorizontalLayoutGroup>().spacing = -260f;
            DOTween.To(() => group_collectible.GetComponent<HorizontalLayoutGroup>().spacing,
            x => group_collectible.GetComponent<HorizontalLayoutGroup>().spacing = x,
            -100f, viewTransitionTime).OnComplete(Anim_RevealCollectibleTag);
        }
    }
    public void Anim_Popup()
    {
        //group_header.GetComponent<RectTransform>().DOAnchorPosY(300f, 1f).From(true).SetEase(Ease.OutBack);
        group_page.GetComponent<RectTransform>().localScale = Vector3.one;
        group_page.GetComponent<RectTransform>().DOScale(Vector3.zero, 0.6f).From().SetEase(Ease.OutBack);
    }
}
