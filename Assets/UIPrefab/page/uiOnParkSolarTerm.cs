﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class uiOnParkSolarTerm : uiElement
{
    [Header("Data")]
    [SerializeField] dataParkVisit gameplayData;
    [SerializeField] gParkVisit gameplayScript;

    [Header("Display Groups")]
    [SerializeField] GameObject group_page;
    [SerializeField] GameObject group_main;

    [Header("Children Objects")]
    [SerializeField] ui_text title;
    [SerializeField] ui_icon_text solar_term;
    [SerializeField] ui_icon_text max_stamina;
    [SerializeField] ui_icon_text total_node;

    [Header("Game Event")]
    [SerializeField] GameEvent EventReleaseCaptions;

    private Vector2 StartingAnchor = new Vector2(0.5f, 0.5f);
    private Vector2 StartingScale = Vector3.one;

    public void btn_confirm_hit()
    {
        //UIClose();
        Anim_Shrink();
        //gameplayScript.SkipOnCollectiblePopup();
        SFXPlayer.mp3.PlaySFX("button_click");
        if(gameplayData.parkUID == dConstants.ParkConst.INTRO_PARK_UID)
        {
            gIntroParkScript.controller.SetDialogAndMask();
        }
        
    }
    public void Start()
    {
        group_page.SetActive(false);
    }
    public override void UIInit()
    {
        ResetPos();
        UIUpdate();
        group_page.SetActive(true);
        //Anim_Popup();
    }
    public override void UIClose()
    {
        group_page.SetActive(false);
        EventReleaseCaptions.Raise();
    }
    public override void UIUpdate()
    {
        
        title.SetText(string.Format("{0}游于{1}", CalendarManagement.calendar.GetCurDate().DateToString(), gameplayData.parkName));

        if (gameplayData.curSolarTermUID != "")
        {
            ParkSolarTerm term = dDataHub.hub.dParkSolarTerm.GetFromUID(gameplayData.curSolarTermUID);
            solar_term.minorStr.SetText(term.title);
            solar_term.majorStr.SetText(term.desc);
            solar_term.icon.SetIcon(term.artAsset.objectArt);
            solar_term.gameObject.SetActive(true);
        }
        else
        {
            solar_term.gameObject.SetActive(false);
        }    

        max_stamina.icon.SetIcon("dynamic_current_stamina");
        max_stamina.majorStr.SetText(gameplayData.staminaMax.ToString());

        total_node.majorStr.SetText((gameplayData.currentMap.nodes.Count-1).ToString());
    }
    public void Anim_Popup()
    {
        //group_header.GetComponent<RectTransform>().DOAnchorPosY(300f, 1f).From(true).SetEase(Ease.OutBack);
        RectTransform anchoredNode = gameplayData.currentMap.GetStartNode().GetComponent<RectTransform>();
        group_page.GetComponent<RectTransform>().DOAnchorMin(new Vector2((anchoredNode.anchoredPosition.x + 300) / 1920, (1080 + anchoredNode.anchoredPosition.y) / 1080), 0.6f).From().SetEase(Ease.OutSine);
        group_page.GetComponent<RectTransform>().DOAnchorMax(new Vector2((anchoredNode.anchoredPosition.x + 300) / 1920, (1080 + anchoredNode.anchoredPosition.y) / 1080), 0.6f).From().SetEase(Ease.OutSine);
        group_page.GetComponent<RectTransform>().DOScale(Vector3.zero, 0.6f).From().SetEase(Ease.OutSine);
        //Debug.Log("anim_popup called at term popup");
    }
    public void Anim_Shrink()
    {
        //group_header.GetComponent<RectTransform>().DOAnchorPosY(300f, 1f).From(true).SetEase(Ease.OutBack);
        RectTransform anchoredNode = gameplayData.currentMap.GetStartNode().GetComponent<RectTransform>();
        //Debug.Log(string.Format("relativePos: {0}, {1}", relativePos.x, relativePos.y));
        group_page.GetComponent<RectTransform>().DOAnchorMin(new Vector2((anchoredNode.anchoredPosition.x + 300) / 1920, (1080 + anchoredNode.anchoredPosition.y) / 1080), 0.6f).SetEase(Ease.OutSine);
        group_page.GetComponent<RectTransform>().DOAnchorMax(new Vector2((anchoredNode.anchoredPosition.x + 300) / 1920, (1080 + anchoredNode.anchoredPosition.y) / 1080), 0.6f).SetEase(Ease.OutSine);
        group_page.GetComponent<RectTransform>().DOScale(Vector3.zero, 0.6f).SetEase(Ease.OutSine).OnComplete(UIClose);
        //group_page.GetComponent<RectTransform>().DOAnchorMin(new Vector2(0.95f, 0.95f), 0.6f).SetEase(Ease.OutSine);
        //group_page.GetComponent<RectTransform>().DOAnchorMax(new Vector2(0.95f, 0.95f), 0.6f).SetEase(Ease.OutSine);
        //Debug.Log("anim_shrink called at term popup");
    }
    void ResetPos()
    {
        group_page.GetComponent<RectTransform>().localScale = StartingScale;
        group_page.GetComponent<RectTransform>().anchorMin = StartingAnchor;
        group_page.GetComponent<RectTransform>().anchorMax = StartingAnchor;
    }
}
