﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class uiParkOnRoute : uiElement
{
    [Header("Data")]
    public dataParkVisit data;
    public gParkVisit parkGameplayScript;
    public uiParkHeader headerUI;
    [SerializeField] int curStep;
    [SerializeField] onRouteStructureSet structureSetRoute;
    [SerializeField] onRouteStructureSet structureSetArrival;
    [SerializeField] SpriteAsset onRouteBgAssets;

    [Header("Animation Script")]
    [SerializeField] ZoomEffect zoom;
    [SerializeField] uiParkActionToken latestPlayedAction;

    [Header("Display Groups")]
    [SerializeField] GameObject group_page;
    //[SerializeField] GameObject group_header;
    [SerializeField] GameObject group_main;
    //[SerializeField] GameObject group_character;
    //[SerializeField] GameObject group_bottom;
    [SerializeField] GameObject group_bg;

    [Header("Main")]
    public uiParkOnRoute_Structure curStructure;
    [SerializeField] uiParkOnRoute_Structure nextStructure;
    [SerializeField] Image onRouteBg;
    /*
    [Header("Bottom")]
    [SerializeField] uipfb_label danger_chance_pct;
    [SerializeField] uipfb_bar progress_bar;
    */
    [Header("Event")]
    [SerializeField] GameEvent onMapResume;
    //[SerializeField] GameEvent parkVisitEnd;
    [SerializeField] GameEvent collectiblePopup;
    

    [Header("Sister UI Elements")]
    [SerializeField] List<uiElement> sisters;

    private string previousStructureUID;

    public void Start()
    {
        group_page.SetActive(false);
    }
    public void stay_card_hover_on(int index)
    {
        parkGameplayScript.CardHoverOnStayPool(index);
        //SFXPlayer.mp3.PlaySFX("button_hover");
    }
    public void forward_card_hover_on(int index)
    {
        parkGameplayScript.CardHoverOnForwardPool(index);
        //SFXPlayer.mp3.PlaySFX("button_hover");
    }
    public void danger_card_hover_on(int index)
    {
        parkGameplayScript.CardHoverOnDangerPool(index);
        //SFXPlayer.mp3.PlaySFX("button_hover");
    }
    public void card_hover_off()
    {
        parkGameplayScript.CardHoverOff();
    }
    public void btn_stay_card_take_hit(int index)
    {
        parkGameplayScript.CardPlayedOnStayPool(index);
        latestPlayedAction = curStructure.stay_tokens[index];
        curStructure.character.Anim_MoveTo(curStructure.stay_tokens[index], UIUpdate);
        //exit route
        if (data.curStatus == dConstants.ParkConst.ParkStatus.onReward || data.curStatus == dConstants.ParkConst.ParkStatus.nextNode)
        {
            //Anim_MoveOutRoute();
            //Anim_NextSceneUp();
        }
        headerUI.UpdateStamina();
        //sfx
        SFXPlayer.mp3.PlaySFX("stepshort");
        //SFXPlayer.mp3.PlaySFX("button_click");
        if (data.parkUID == dConstants.ParkConst.INTRO_PARK_UID && data.curStatus != dConstants.ParkConst.ParkStatus.onReward)
        {
            Sequence seq = DOTween.Sequence();
            seq.AppendInterval(1f).OnComplete(() => gIntroParkScript.controller.SetDialogAndMask());
        }
    }
    public void btn_forward_card_take_hit(int index)
    {
        parkGameplayScript.CardPlayedOnForwardPool(index);
        latestPlayedAction = curStructure.forward_tokens[index];
        curStructure.character.Anim_MoveTo(curStructure.forward_tokens[index], UIUpdate);
        headerUI.UpdateStamina();
        //sfx
        SFXPlayer.mp3.PlaySFX("stepaway");
        //SFXPlayer.mp3.PlaySFX("button_click");
    }
    public void btn_danger_card_take_hit(int index)
    {
        parkGameplayScript.CardPlayedOnDangerPool(index);
        latestPlayedAction = curStructure.danger_tokens[index];
        curStructure.character.Anim_MoveTo(curStructure.danger_tokens[index], UIUpdate);
        headerUI.UpdateStamina();
        //sfx
        SFXPlayer.mp3.PlaySFX("stepshort");
        //SFXPlayer.mp3.PlaySFX("button_click");
    }
    
    public override void UIInit()
    {
        //parkGameplayScript = parkGameplayScript ?? tAvatar.Avatar.curNode.GetComponent<gParkVisit>();
        //data = data ?? parkGameplayScript.gameplayData;

        curStep = data.totalScenes;
        onRouteBg.sprite = onRouteBgAssets.GetAsset(data.parkXLSData.terrains[0]);
        //progress_bar.ResetBar(data.CurrentProgressPercent, data.progressCur.ToString(), data.progressMax.ToString());
        MainInit();
        MainSwap();

        UIUpdate();
        Anim_Init();
        
        group_page.SetActive(true);
    }
    public override void UIClose()
    {
        group_page.SetActive(false);
    }
    public override void UIUpdate()
    {
        ReleaseActionPlayEffect();
        if (curStep != data.totalScenes)
        {
            curStep = data.totalScenes;
            SceneTransition();
            //BottomUpdate();
        }
        else
        {
            switch (data.curStatus)
            {
                case dConstants.ParkConst.ParkStatus.onRoute:
                    //ReleaseActionPlayEffect();
                    MainUpdate();
                    CheckPopupCollectible();
                    //BottomUpdate();
                    break;
                case dConstants.ParkConst.ParkStatus.inDanger:
                    //ReleaseActionPlayEffect();
                    MainUpdate();
                    CheckPopupCollectible();
                    //BottomUpdate();
                    break;
                case dConstants.ParkConst.ParkStatus.onArrival:
                    //ReleaseActionPlayEffect();
                    MainUpdate();
                    CheckPopupCollectible();
                    //BottomUpdate();
                    break;
                default:
                    if (group_page.activeSelf)
                    {
                        onMapResume.Raise();
                        Anim_MoveOutRoute();
                    }
                    break;
            }
        }
        foreach (uiElement ui in sisters)
        {
            ui.UIUpdate();
        }
    }
    public void Anim_Init()
    {
        //group_header.GetComponent<RectTransform>().DOAnchorPosY(300f, 1f).From(true).SetEase(Ease.OutBack);
        //group_bottom.GetComponent<RectTransform>().DOAnchorPosY(-300f, 1f).From(true).SetEase(Ease.OutBack).SetDelay(0.5f);

        //zoom effect
        //Vector2 focusPoint = curStructure.stay_tokens[index].GetComponent<RectTransform>().anchoredPosition;
        //Vector2 mySize = GetComponent<RectTransform>().sizeDelta;
        Vector2 pivotPoint = new Vector2(0.5f, 0.5f);
        zoom.FocusOn(pivotPoint);
        zoom.Anim_ZoomIn_From(0f);
        curStructure.character.Anim_MoveIn();
        Sequence seq = DOTween.Sequence();
        seq.AppendInterval(0.1f).OnComplete(() => headerUI.Anim_ProgressSlideIn());

        if (data.parkUID == dConstants.ParkConst.INTRO_PARK_UID)
        {
            Sequence seq2 = DOTween.Sequence();
            seq2.AppendInterval(1.2f).OnComplete(() => gIntroParkScript.controller.SetDialogAndMask());
        }
        if(EquipmentManagement.equipment.ActiveEquipmentCount() > 0)
        {
            Sequence seq3 = DOTween.Sequence();
            seq3.AppendInterval(1.3f).OnComplete(() => gIntroParkScript.controller.SetEquipmentIntro());
        }
    }
    public void Anim_MoveOutRoute()
    {
        Vector2 pivotPoint = new Vector2(0.5f, 0.5f);
        zoom.FocusOn(pivotPoint);
        zoom.Anim_ZoomOut(0f);
        Sequence seq = DOTween.Sequence();
        seq.AppendInterval(1f).OnComplete(() => UIClose());
    }
    public void Anim_ShowArrival()
    {
        //Debug.Log("ShowArrival aniamtion start");
        CollectEffect.Collect(UIKeyElements.locator.NextNodeArrival.gameObject, 1, new Vector2(1.2f, 0f), UICanvas.canvas.EffectLayer, UIKeyElements.locator.NextNodeArrival, curStructure.stay_tokens[0].transform);
        //UIKeyElements.locator.NextNodeArrival.DOScale(1.2f, 0.3f).SetEase(Ease.OutBack).SetLoops(2, LoopType.Yoyo);
        curStructure.stay_tokens[0].transform.DOScale(0f, 0.5f).SetEase(Ease.OutBack).From().SetDelay(1.2f);
    }
    public void Anim_PopupAllActions()
    {
        for (int i = 0; i < curStructure.stay_tokens.Count; i++)
        {
            if (curStructure.stay_tokens[i].status == uiParkActionToken.ActionTokenStatus.playable)
            {
                curStructure.stay_tokens[i].transform.DOScale(0f, 0.5f).SetEase(Ease.OutBack).From().SetDelay(Random.value * 0.5f);
            }

        }
        for (int i = 0; i < curStructure.forward_tokens.Count; i++)
        {
            if (curStructure.forward_tokens[i].status == uiParkActionToken.ActionTokenStatus.playable)
            {
                curStructure.forward_tokens[i].transform.DOScale(0f, 0.5f).SetEase(Ease.OutBack).From().SetDelay(Random.value * 0.5f + 0.5f);
            }
        }
    }
    public void Anim_NextSceneUp()
    {
        //Debug.Log("new scene aniamtion start");
        curStructure.character.Anim_MoveIn();
        if (data.curStatus == dConstants.ParkConst.ParkStatus.onArrival)
        {
            Anim_ShowArrival();
        }
        else
        {
            Anim_PopupAllActions();
        }
        if(data.parkUID == dConstants.ParkConst.INTRO_PARK_UID)
        {
            Sequence seq = DOTween.Sequence();
            seq.AppendInterval(1f).OnComplete(() => gIntroParkScript.controller.SetDialogAndMask());
        }
    }
    public void Anim_CurSceneEnd()
    {
        //Debug.Log("end scene animation start");
        for (int i = 0; i < curStructure.stay_tokens.Count; i++)
        {
            if (curStructure.stay_tokens[i].status == uiParkActionToken.ActionTokenStatus.playable)
            {
                uiParkActionToken token = curStructure.stay_tokens[i];
                token.transform.DOScale(0f, 0.5f).SetEase(Ease.OutSine).SetDelay(Random.value * 0.3f);
            }

        }
        for (int i = 0; i < curStructure.forward_tokens.Count; i++)
        {
            if (curStructure.forward_tokens[i].status == uiParkActionToken.ActionTokenStatus.playable)
            {
                uiParkActionToken token = curStructure.forward_tokens[i];
                token.transform.DOScale(0f, 0.5f).SetEase(Ease.OutSine).SetDelay(Random.value * 0.5f + 0.3f);
            }
        }
    }
    public void SceneTransition()
    {
        Sequence seq = DOTween.Sequence();
        seq.AppendInterval(0.5f)
            .AppendCallback(() => Anim_CurSceneEnd())
            .AppendInterval(1f)
            .AppendCallback(() => MainInit())
            .AppendCallback(() => MainSwap())
            .AppendCallback(() => UIUpdate())
            .AppendCallback(() => Anim_NextSceneUp());
    }
    /*
    void BottomUpdate()
    {
        if(data.curStatus != dConstants.ParkConst.ParkStatus.onReward && data.curStatus != dConstants.ParkConst.ParkStatus.nextNode)
        {
            progress_bar.SetBarProgress(data.CurrentProgressPercent, data.progressCur.ToString(), data.progressMax.ToString());
        }
        danger_chance_pct.SetText(data.CurrentDangerAdditionString);
    }
    */
    void MainInit()
    {
        //Debug.Log("new scene generated");
        onRouteStructureOption structureOption = null;
        if (data.curStatus == dConstants.ParkConst.ParkStatus.onArrival)
        {
            structureOption = structureSetArrival.DrawValidStructure();
        }
        else
        {
            structureOption = structureSetRoute.DrawValidStructure(previousStructureUID);
        }
        previousStructureUID = structureOption.structureUID;
        nextStructure = Instantiate(structureOption.pfb_structure, group_main.transform).GetComponent<uiParkOnRoute_Structure>();
    }
    void MainSwap()
    {
        //Debug.Log("old scene destroyed w/ new scene swapped in");
        Destroy(curStructure.gameObject);
        curStructure = nextStructure;
        curStructure.character.SetPortrait(CharacterManagement.controller.GetCurrentCharacterPortrait());
        //curStructure.ref_stay_hit = btn_stay_card_take_hit;
        //curStructure.ref_forward_hit = btn_forward_card_take_hit;
        for (int i = 0; i < curStructure.stay_tokens.Count; i++)
        {
            curStructure.stay_tokens[i].ref_action_hit_func = btn_stay_card_take_hit;
            curStructure.stay_tokens[i].ref_action_hover_on = stay_card_hover_on;
            curStructure.stay_tokens[i].ref_action_hover_off = card_hover_off;
        }
        for (int i = 0; i < curStructure.forward_tokens.Count; i++)
        {
            curStructure.forward_tokens[i].ref_action_hit_func = btn_forward_card_take_hit;
            curStructure.forward_tokens[i].ref_action_hover_on = forward_card_hover_on;
            curStructure.forward_tokens[i].ref_action_hover_off = card_hover_off;
        }
        for (int i = 0; i < curStructure.danger_tokens.Count; i++)
        {
            curStructure.danger_tokens[i].ref_action_hit_func = btn_danger_card_take_hit;
            curStructure.danger_tokens[i].ref_action_hover_on = danger_card_hover_on;
            curStructure.danger_tokens[i].ref_action_hover_off = card_hover_off;
        }
        for (int i = 0; i < curStructure.major_danger_tokens.Count; i++)
        {
            curStructure.major_danger_tokens[i].ref_action_hit_func = btn_danger_card_take_hit;
            curStructure.major_danger_tokens[i].ref_action_hover_on = danger_card_hover_on;
            curStructure.major_danger_tokens[i].ref_action_hover_off = card_hover_off;
        }
    }
    void MainUpdate()
    {
        for (int i = 0; i < curStructure.stay_tokens.Count; i++)
        {
            curStructure.stay_tokens[i].SetHidden();
            if (i < data.stayActionPool.Count)
            {
                curStructure.stay_tokens[i].SetStatus(data.stayActionPlayed[i], data.curStatus == dConstants.ParkConst.ParkStatus.inDanger);
                curStructure.stay_tokens[i].SetTokenByUID(data.stayActionPool[i]);
            }
            if(i < curStructure.stay_paths.Count)
            {
                curStructure.stay_paths[i].SetActive(curStructure.stay_tokens[i].status != uiParkActionToken.ActionTokenStatus.hidden);
            }
            
        }
        for (int i = 0; i < curStructure.forward_tokens.Count; i++)
        {
            curStructure.forward_tokens[i].SetHidden();
            if (i < data.forwardActionPool.Count)
            {
                curStructure.forward_tokens[i].SetStatus(data.forwardActionPlayed[i], data.curStatus == dConstants.ParkConst.ParkStatus.inDanger);
                curStructure.forward_tokens[i].SetTokenByUID(data.forwardActionPool[i]);
            }
            if (i < curStructure.forward_paths.Count)
            {
                curStructure.forward_paths[i].SetActive(curStructure.forward_tokens[i].status != uiParkActionToken.ActionTokenStatus.hidden);
            }
        }
        for (int i = 0; i < curStructure.danger_tokens.Count; i++)
        {
            curStructure.danger_tokens[i].SetHidden();
            if (i < data.dangerActionPool.Count)
            {
                ParkPathAction danger = dDataHub.hub.dParkDangerAction.GetFromUID(data.dangerActionPool[i]);
                if(danger.tier != 0)
                {
                    curStructure.danger_tokens[i].SetStatus(data.dangerActionPlayed[i], data.curStatus == dConstants.ParkConst.ParkStatus.inDanger && data.dangerActionPlayed[i]);
                    curStructure.danger_tokens[i].SetTokenByUID(data.dangerActionPool[i]);
                }
            }
        }
        for (int i = 0; i < curStructure.major_danger_tokens.Count; i++)
        {
            curStructure.major_danger_tokens[i].SetHidden();
            if (i < data.dangerActionPool.Count)
            {
                ParkPathAction danger = dDataHub.hub.dParkDangerAction.GetFromUID(data.dangerActionPool[i]);
                if (danger.tier == 0)
                {
                    curStructure.major_danger_tokens[i].SetStatus(data.dangerActionPlayed[i], data.curStatus == dConstants.ParkConst.ParkStatus.inDanger && data.dangerActionPlayed[i]);
                    curStructure.major_danger_tokens[i].SetTokenByUID(data.dangerActionPool[i]);
                }
            }
        }
        //CheckPopupCollectible();
        group_main.SetActive(true);
    }
    void CheckPopupCollectible()
    {
        if (data.collectible2Collect)
        {
            collectiblePopup.Raise();
        }
    }
    /*
    void ReleaseActionPlayEffect_Delayed()
    {
        if (latestPlayedAction == null)
        {
            return;
        }
        bool hasProgress = false;
        bool hasCollectible = false;
        bool hasNegative = false;
        if (data.staminaDelta > 0)
        {
            //Debug.Log("add staminaa effect triggered");
            latestPlayedAction.Anim_StaminaAdded(data.staminaDelta);
        }
        if (data.progressDelta > 0)
        {
            latestPlayedAction.Anim_ProgressAdded(data.progressDelta);
            hasProgress = true;
        }
        if (data.dangerTokenDelta > 0)
        {
            latestPlayedAction.Anim_DangerTokenAcquire(data.dangerTokenDelta);
        }
        if (data.collectibleTokenDelta > 0)
        {
            latestPlayedAction.Anim_CollectibleTokenAcquire(data.collectibleTokenDelta);
            hasCollectible = true;
            
        }
        if (data.addedDangerActionIndex >= 0)
        {
            //add card play action
            ParkPathAction danger = dDataHub.hub.dParkDangerAction.GetFromUID(data.dangerActionPool[data.addedDangerActionIndex]);
            if (danger.tier != 0)
            {
                curStructure.danger_tokens[data.addedDangerActionIndex].Anim_DangerAppear(latestPlayedAction.transform);
            }
            else
            {
                curStructure.major_danger_tokens[data.addedDangerActionIndex].Anim_DangerAppear(latestPlayedAction.transform);
            }
            hasNegative = true;
            
        }
        if (hasNegative)
        {
            SFXPlayer.mp3.PlaySFX("guqin_warning");
        }
        else if (hasProgress)
        {
            //SFXPlayer.mp3.PlaySFX("guqin_continue");
        }
        else if (data.curStatus == dConstants.ParkConst.ParkStatus.onReward)
        {
            SFXPlayer.mp3.PlaySFX("guqin_node_arrival");
        }
        if (hasCollectible)
        {
            SFXPlayer.mp3.PlaySFX("dizi_short");
        }
        parkGameplayScript.ResetLatestActionResult();
        latestPlayedAction = null;
    }
    */
    void ReleaseActionPlayEffect()
    {
        
        if(latestPlayedAction == null)
        {
            return;
        }
        bool hasProgress = false;
        bool hasCollectible = false;
        bool hasNegative = false;
        if (data.staminaDelta > 0)
        {
            //Debug.Log("add staminaa effect triggered");
            latestPlayedAction.Anim_StaminaAdded(data.staminaDelta);
        }
        if (data.staminaDelta < 0)
        {
            //Debug.Log("add staminaa effect triggered");
            latestPlayedAction.Anim_StaminaConsumed(-data.staminaDelta);
        }
        if (data.progressDelta > 0)
        {
            latestPlayedAction.Anim_ProgressAdded(data.progressDelta);
            hasProgress = true;
        }
        if (data.dangerTokenDelta > 0)
        {
            latestPlayedAction.Anim_DangerTokenAcquire(data.dangerTokenDelta);
        }
        if (data.addedDangerActionIndex >= 0)
        {
            //add card play action
            ParkPathAction danger = dDataHub.hub.dParkDangerAction.GetFromUID(data.dangerActionPool[data.addedDangerActionIndex]);
            if (danger.tier != 0)
            {
                latestPlayedAction.Anim_DangerTokenPop();
                curStructure.danger_tokens[data.addedDangerActionIndex].Anim_DangerAppear(latestPlayedAction.danger_base.transform);
                Sequence seq = DOTween.Sequence();
                seq.AppendInterval(1f);
                seq.AppendCallback(()=>gIntroParkScript.controller.SetDangerDialogAndMask());
            }
            else
            {
                latestPlayedAction.Anim_DangerTokenPop();
                curStructure.major_danger_tokens[data.addedDangerActionIndex].Anim_DangerAppear(latestPlayedAction.danger_base.transform);
            }
            hasNegative = true;
        }
        else if (data.dangerRolled)
        {
            latestPlayedAction.Anim_DangerTokenCheck();
        }
        if (data.collectibleTokenDelta > 0)
        {
            latestPlayedAction.Anim_CollectibleTokenAcquire(data.collectibleTokenDelta);
            hasCollectible = true;
            gIntroParkScript.controller.SetCollectibleDialogAndMask();
        }
        else if (data.collectibleRolled)
        {
            latestPlayedAction.Anim_CollectibleTokenCheck();
        }
        if (hasNegative)
        {
            SFXPlayer.mp3.PlaySFX("guqin_warning");
        }
        else if(hasProgress)
        {
            //SFXPlayer.mp3.PlaySFX("guqin_continue");
        }
        else if(data.curStatus == dConstants.ParkConst.ParkStatus.onReward)
        {
            SFXPlayer.mp3.PlaySFX("guqin_node_arrival");
        }
        if (hasCollectible)
        {
            SFXPlayer.mp3.PlaySFX("dizi_short");
        }
        parkGameplayScript.ResetLatestActionResult();
        latestPlayedAction = null;
    }
}
