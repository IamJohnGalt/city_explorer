﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class uiPopupCollectible : uiElement
{

    [Header("Data")]
    [SerializeField] dataParkVisit gameplayData;
    [SerializeField] gParkVisit gameplayScript;

    [Header("Display Groups")]
    [SerializeField] GameObject group_page;
    [SerializeField] GameObject group_main;
    [SerializeField] GameObject group_view;
    [SerializeField] GameObject group_collectible;

    [Header("Children Objects")]
    //[SerializeField] List<GameObject> slots;
    [SerializeField] List<ui_collectible> views;
    [SerializeField] List<ui_collectible> collectibles;
    [SerializeField] ui_button skip_btn;

    static private float viewTransitionTime = 0.5f;

    public void btn_skip_hit()
    {
        UIClose();
        gameplayScript.SkipOnCollectiblePopup();
    }
    public void btn_collectible_hit(int index)
    {
        UIClose();
        gameplayScript.CardPlayedOnCollectiblePopop(index);
        //collectibles[index].ShutOffInteraction();
        //collectibles[index].HoverOff();
        //CollectEffect.Collect(collectibles[index].gameObject, 1, new Vector2(1f, 0f), UICanvas.canvas.EffectLayer, collectibles[index].transform, UIKeyElements.locator.Journal);
        //collectibles[index].ResumeInteraction();
    }
    public void btn_view_hit(int index)
    {
        gameplayScript.CardPlayedOnViewPopop(index);
        Anim_InitCollectible();
        Anim_View2Collectible();
    }
    public void Awake()
    {
        group_page.SetActive(false);
        for (int i = 0; i < views.Count; i++)
        {
            views[i].ref_token_selected_func = btn_view_hit;
        }
        for (int i = 0; i < collectibles.Count; i++)
        {
            collectibles[i].ref_token_selected_func = btn_collectible_hit;
        }
    }
    public override void UIInit()
    {
        for (int i = 0; i < views.Count; i++)
        {
            ui_collectible item = views[i];
            item.SetFade(1f);
        }
        UIUpdate();
        group_page.SetActive(true);
        Anim_Popup();
        
    }
    public override void UIClose()
    {
        group_page.SetActive(false);
    }
    public override void UIUpdate()
    {
        //Debug.Log("UIUpdate on uiPpoupCollectible. collectible count: " + (gameplayData.popupCollectibles.Count).ToString());
        //Debug.Log("UIUpdate on uiPpoupCollectible. view count: " + (gameplayData.popupViews.Count).ToString());
        if ((gameplayData.popupCollectibles.Count + gameplayData.popupViews.Count) > 0)
        {
            if (gameplayData.popupCollectibles.Count > 0)
            {
                //hide views, show collectibles
                for (int i = 0; i < views.Count; i++)
                {
                    views[i].gameObject.SetActive(false);
                }
                for (int i = 0; i < collectibles.Count; i++)
                {
                    collectibles[i].gameObject.SetActive(false);
                    if (i < gameplayData.popupCollectibles.Count)
                    {
                        collectibles[i].SetAsCollectibleByUID(gameplayData.popupCollectibles[i], dConstants.ParkConst.TokenStatus.playable);
                        collectibles[i].gameObject.SetActive(true);
                    }
                }
                skip_btn.gameObject.SetActive(false);
            }
            else
            {
                //hide collectibles, show views
                for (int i = 0; i < views.Count; i++)
                {
                    views[i].gameObject.SetActive(false);
                    if (i < gameplayData.popupViews.Count)
                    {
                        views[i].SetAsViewByUID(gameplayData.popupViews[i], dConstants.ParkConst.TokenStatus.playable);
                        views[i].gameObject.SetActive(true);
                    }
                }
                for (int i = 0; i < collectibles.Count; i++)
                {
                    collectibles[i].gameObject.SetActive(false);
                }
                skip_btn.gameObject.SetActive(false);
            }
        }
        else
        {
            UIClose();
        }
    }
    public void Anim_Popup()
    {
        //group_header.GetComponent<RectTransform>().DOAnchorPosY(300f, 1f).From(true).SetEase(Ease.OutBack);
        group_page.GetComponent<RectTransform>().DOScale(Vector3.zero, 0.6f).From().SetEase(Ease.OutBack);
        //Debug.Log("anim_popup called at popup collectible");
    }
    void Anim_InitCollectible()
    {
        //Debug.Log("calling UICollectibleUpdate()");
        for (int i = 0; i < views.Count; i++)
        {
            views[i].gameObject.SetActive(false);
        }
        for (int i = 0; i < collectibles.Count; i++)
        {
            collectibles[i].gameObject.SetActive(false);
            if (i < gameplayData.popupCollectibles.Count)
            {
                //each needs to be revealed by anim script
                collectibles[i].SetAsCollectibleByUID(gameplayData.popupCollectibles[i], dConstants.ParkConst.TokenStatus.playable);
                collectibles[i].SetViewFade(1f);
                collectibles[i].SetTagFade(0f);
            }
        }
        //group_collectible.SetActive(true);
    }
    void Anim_RevealCollectibleTag()
    {
        //Debug.Log("calling Anim_RevealAllCollectibles");
        for (int i = 0; i < collectibles.Count; i++)
        {
            ui_collectible item = collectibles[i];
            item.SetTagFade(1f, viewTransitionTime * 2);
            //item.transform.DOLocalRotate(new Vector3(0,0,-5), 0.05f).SetLoops(-1, LoopType.Yoyo);
        }
    }
    void Anim_View2Collectible()
    {
        //hide all view instantly
        for (int i = 0; i < views.Count; i++)
        {
            ui_collectible item = views[i];
            item.SetFade(0f);
        }
        //show all collectible in the view location without tags
        for (int i = 0; i < collectibles.Count; i++)
        {
            ui_collectible item = collectibles[i];
            item.SetViewFade(1f);
            item.SetTagFade(0f);
            //item.transform.DOLocalRotate(new Vector3(0,0,-5), 0.05f).SetLoops(-1, LoopType.Yoyo);
        }
        if (gameplayData.popupCollectibles.Count > 1)
        {
            group_collectible.GetComponent<HorizontalLayoutGroup>().spacing = -260f;
            DOTween.To(() => group_collectible.GetComponent<HorizontalLayoutGroup>().spacing,
            x => group_collectible.GetComponent<HorizontalLayoutGroup>().spacing = x,
            30f, viewTransitionTime).OnComplete(Anim_RevealCollectibleTag);
        }
        else
        {
            Anim_RevealCollectibleTag();
        }
        skip_btn.gameObject.SetActive(false);
    }
}
