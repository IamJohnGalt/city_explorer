﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class UnlockedItem
{
    public Sprite icon;
    public string title;
    public string desc;

}
[CreateAssetMenu(menuName = "Character/Content Unlocks")]
public class dataContentUnlocked : ScriptableObject
{
    public List<UnlockedItem> unlocks;
    public bool isEndGame = false;

    public void Reset()
    {
        unlocks = new List<UnlockedItem>();
        isEndGame = false;
    }
}
