﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class uiParkOnMap : uiElement
{
    [Header("Data")]
    public gParkVisit parkGameplayScript;
    public uiParkHeader headerUI;
    [SerializeField] dataParkVisit data;
    //[SerializeField] dataPopupBox popupData;
    [SerializeField] onMapStructureSet mapSet;

    [Header("Animation")]
    [SerializeField] ZoomEffect zoom;
    [SerializeField] cloud_generator cloud;

    [Header("Display Groups")]
    [SerializeField] GameObject group_page;
    [SerializeField] GameObject group_main;
    [SerializeField] GameObject group_cloud;
    //[SerializeField] GameObject group_bg;

    [Header("Main")]
    public uiParkOnMap_Structure map;

    [Header("Event")]
    [SerializeField] GameEvent OnRouteStart;
    [SerializeField] GameEvent OnNodeStart;
    [SerializeField] GameEvent popupStart;

    [Header("Sister UI Elements")]
    [SerializeField] List<uiElement> sisters;

    private string previousStructureUID;
    public bool OnNodeStartQueued;


    void Awake()
    {
        group_page.SetActive(false);
    }
    public void btn_reward_hit(int index)
    {
        //Debug.Log(string.Format("reward index ({0}) is selected", index));
        parkGameplayScript.CardPlayedOnRewardPool(index);
        UIUpdate();
    }
    
    public void btn_exit_hit()
    {
        //deprecated function
        dataConfirmationPopup data = uiPopupConfirmation.controller.AccessPopupData();
        data.Reset();
        data.title_icon = "icon_gate";
        data.title_text = "返回城镇";
        data.desc_text = "确定要提前返回城镇么？<br>为下个地图保留30%的剩余补给";
        data.btn_no_available = true;
        data.btn_no_text = "再想想";
        data.btn_yes_available = true;
        data.btn_yes_text = "确认返回";
        uiPopupConfirmation.controller.SetUpPopup(()=>parkGameplayScript.FinishPark());
        //parkGameplayScript.FinishPark();
        UIUpdate();
    }
    public void btn_start_hit()
    {
        parkGameplayScript.ViewSolarTerm();
        //UIUpdate();
    }
    public void EnterRouteTransition()
    {
        uiParkRoute routeObj = map.RetrievePathByPfbUID(data.curPathPfbUID);
        //zoom effect test
        Vector2 focusPoint = routeObj.GetComponent<RectTransform>().anchoredPosition;
        Vector2 mySize = GetComponent<RectTransform>().sizeDelta;
        Vector2 pivotPoint = new Vector2(focusPoint.x / mySize.x, 1 + focusPoint.y / mySize.y);
        zoom.FocusOn(pivotPoint);
        zoom.Anim_ZoomIn(0.5f);
        //route effect
        CollectEffect.Collect(routeObj.label_sign.gameObject, 1, new Vector2(1f, 1f), UICanvas.canvas.EffectLayer, routeObj.label_sign.transform, UIKeyElements.locator.Progress, 0f, 1.3f);
        //chcarcter move
        map.character.Anim_MoveTo(map.RetrievePathByPfbUID(data.curPathPfbUID).GetComponent<RectTransform>().anchoredPosition, OnRouteStart.Raise);
        UIUpdate();
    }
    public void MapStructureInit()
    {
        Destroy(map.gameObject);
        onMapStructureOption mapStructure = mapSet.DrawValidStructure(data.parkXLSData.terrains[0], data.parkXLSData.maxTier);
        map = Instantiate(mapStructure.pfb_structure, group_main.transform).GetComponent<uiParkOnMap_Structure>();
        map.InitAllActiveChildren();
        for (int i = 0; i < map.nodes.Count; i++)
        {
            map.nodes[i].ref_reward_hit_func = btn_reward_hit;
        }
        //route hit mnoved into OnNextNode widget
        /*for (int i = 0; i < map.routes.Count; i++)
        {
            map.routes[i].ref_path_hit_func = btn_route_hit;
        }*/
        map.SetBackground(data.parkXLSData.terrains);
        map.ref_first_node_hit_func = btn_start_hit;
        data.currentMap = map;
        data.curNodePfbUID = map.nodes[0].pfbUID;
        data.curNodeDataUID = map.nodes[0].nodeUID;
        map.character.SetPortrait(CharacterManagement.controller.GetCurrentCharacterPortrait());
    }
    public override void UIInit()
    {
        //DebugStartPopup();
        cloud.InitClouds();
        UIUpdate();
        FocusPointEffect.controller.StartFocusOn(map.RetrieveNodeByPfbUID("pfb_parkNode_t0_1").transform, 1f, 1.5f, 20f, 0.5f, 1.5f, 1f, 0f);
        group_page.SetActive(true);
    }
    public override void UIClose()
    {
        //DebugEndPopup();
        group_page.SetActive(false);
    }
    public override void UIUpdate()
    {
        //Debug.Log("onMapUIUpdate");
        switch (data.curStatus)
        {
            case dConstants.ParkConst.ParkStatus.onReward:
                MainUpdate();
                if (OnNodeStartQueued)
                {
                    OnNodeStartQueued = false;
                    OnNodeStart.Raise();
                }
                //CurrentRewardUpdate();
                break;
            case dConstants.ParkConst.ParkStatus.nextNode:
                MainUpdate();
                break;
            default:
                //UIClose();
                break;
        }
        foreach (uiElement ui in sisters)
        {
            ui.UIUpdate();
        }
    }
    public void Anim_MoveInNode()
    {
        //Debug.Log("Anim_MoveInNode()");
        //zoom effect
        Vector2 focusPoint = map.RetrieveNodeByPfbUID(data.curNodePfbUID).GetComponent<RectTransform>().anchoredPosition;
        Vector2 mySize = GetComponent<RectTransform>().sizeDelta;
        Vector2 pivotPoint = new Vector2(focusPoint.x / mySize.x, (mySize.y + focusPoint.y) / mySize.y);
        zoom.FocusOn(pivotPoint);
        zoom.Anim_ZoomOut_From(2f);

        OnNodeStartQueued = true;
        Sequence seq = DOTween.Sequence();
        seq.AppendInterval(2f).OnComplete(() => map.character.Anim_MoveTo(map.RetrieveNodeByPfbUID(data.curNodePfbUID).GetComponent<RectTransform>().anchoredPosition, UIUpdate));
        //map.character.Anim_MoveTo(map.RetrieveNodeByPfbUID(data.curNodePfbUID).GetComponent<RectTransform>().anchoredPosition, UIUpdate);
    }
    public void Anim_MapPopup()
    {
        
    }
    public void SceneTransition()
    {
       
    }
    void MainUpdate()
    {
        
        //update nodes
        for (int i = 0; i < map.nodes.Count; i++)
        {
            map.nodes[i].SetStatus(data.curNodePfbUID, data.visitedNodePfb, data.curStatus);
            map.nodes[i].StatusUpdate();
        }
        //update routes
        for (int i = 0; i < map.routes.Count; i++)
        {
            //map.routes[i].SetPath(map.routes[i].pathUID);
            map.routes[i].SetStatus(data.curNodePfbUID, data.curPathPfbUID, data.visitedNodePfb, data.visitedPathPfb, data.curStatus);
            map.routes[i].StatusUpdate();
        }
        //update props
        List<string> visitNodePfbs = new List<string>(data.visitedNodePfb);
        visitNodePfbs.Add(data.curNodePfbUID);
        int totalPropsVisible = 0;
        for (int i = 0; i < map.props.Count; i++)
        {
            map.props[i].SetStatus(visitNodePfbs);
            if (map.props[i].discovered)
            {
                if(totalPropsVisible < data.acquiredCollectibles.Count)
                {
                    map.props[i].SetCollectible(data.acquiredCollectibles[totalPropsVisible]);
                    totalPropsVisible += 1;
                }
            }
            map.props[i].StatusUpdate();
        }
        group_main.SetActive(true);
        
    }
}
