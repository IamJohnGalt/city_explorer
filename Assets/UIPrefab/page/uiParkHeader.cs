﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class uiParkHeader : uiElement
{
    [Header("Data")]
    public dataParkVisit data;
    public dataPlayerEquipment equipmentData;
    public gParkVisit parkGameplayScript;

    [Header("Display Groups")]
    [SerializeField] GameObject group_page;
    [SerializeField] GameObject group_progress;

    [Header("Header")]
    public ui_counter collectible_counter;
    [SerializeField] ui_counter danger_counter;
    public ui_bar_stamina stamina_bar;
    [SerializeField] ui_icon_text solar_term;
    public ui_bar progress_bar;
    [SerializeField] ui_icon next_node;
    [SerializeField] ui_icon_text vision;
    [SerializeField] ui_label check_status;

    [Header("Modifier")]
    [SerializeField] ui_modifier action_stamina;
    [SerializeField] ui_modifier progress_flat;
    [SerializeField] ui_modifier progress_percent;
    [SerializeField] ui_modifier danger_chance;
    [SerializeField] ui_modifier collectible_chance;

    [Header("Bottom")]
    [SerializeField] ui_label park_name;
    [SerializeField] ui_button btn_end_early;


    [Header("Equipment")]
    public List<equipment_item> equipments;

    public void btn_equipment_hit(int index)
    {
        if(data.StatusIsOnRoute)
        {
            parkGameplayScript.EquipmentPlayed(index);
        }
    }
    public void btn_exit_hit()
    {
        dataConfirmationPopup data = uiPopupConfirmation.controller.AccessPopupData();
        data.Reset();
        data.title_icon = "icon_gate";
        data.title_text = "返回城镇";
        data.desc_text = "确定要提前返回城镇么？<br>为下个地图保留30%的剩余补给";
        data.btn_no_available = true;
        data.btn_no_text = "再想想";
        data.btn_yes_available = true;
        data.btn_yes_text = "确认返回";
        uiPopupConfirmation.controller.SetUpPopup(() => parkGameplayScript.FinishPark());
        //parkGameplayScript.FinishPark();
        UIUpdate();
    }
    public void btn_solar_term_hit()
    {
        parkGameplayScript.ViewSolarTerm();
    }
    public void Start()
    {
        group_page.SetActive(false);
        for(int i = 0; i < equipments.Count; i++)
        {
            equipments[i].ref_use_func_hit = btn_equipment_hit;
        }
    }
    public override void UIInit()
    {
        //Solar Term initialize once
        //ParkSolarTerm term = dDataHub.hub.dParkSolarTerm.GetFromUID(data.curSolarTermUID);
        //solar_term.icon.SetIcon(term.artAsset.objectArt);
        //solar_term.majorStr.SetText(string.Format("{0}", term.title));

        park_name.SetText(data.parkName);
        //stamina_bar.SetIcon(CharacterManagement.controller.GetCurrentCharacter().food);
        //stamina_bar.ResetBar(data.CurrentStaminaPercent, data.staminaCur.ToString(), data.staminaMax.ToString());
        stamina_bar.SetFoodIcon(CharacterManagement.controller.GetCurrentCharacter().food);
        stamina_bar.ResetStamina(data.staminaCur, data.staminaMax);

        UIUpdate();
        //Anim_StaticUISlideIn();
        group_page.SetActive(true);
    }
    public override void UIClose()
    {
        group_page.SetActive(false);
    }
    public override void UIUpdate()
    {
        HeaderUpdate();
        EquipmentUpdate();
        BottomUpdate();
        group_page.SetActive(true);
    }
    public void Anim_ProgressSlideIn()
    {
        group_progress.SetActive(true);
        //group_progress.GetComponent<RectTransform>().DOAnchorPosY(300f, 0.5f).From(true).SetEase(Ease.OutBack);
        Image img = group_progress.GetComponent<Image>();
        img.fillAmount = 0;
        DOTween.To(() => img.fillAmount, x => img.fillAmount = x, 1f, 0.5f);
    }
    public void Anim_StaticUISlideIn()
    {
        Debug.Log("deprecated animation shouldn't be used");
        //group_page.GetComponent<RectTransform>().DOAnchorPosY(300f, 1f).From(true).SetEase(Ease.OutBack);
    }
    void EquipmentUpdate()
    {
        for(int i = 0; i < equipments.Count; i++)
        {
            if (i < dConstants.Avatar.MAX_ACTIVE_EQUIPMENT_SLOT && i < data.equipmentCount.Count)
            {
                if(data.equipmentCount[i] == -1)
                {
                    equipments[i].gameObject.SetActive(false);
                }
                else
                {
                    equipments[i].SetActiveEquipmentByUID(equipmentData.activeEquipment[i], data.equipmentCount[i] == 0, data.equipmentCount[i] > 0 && data.StatusIsOnRoute);
                    equipments[i].gameObject.SetActive(true);
                }
                
            }
        }
    }
    void HeaderUpdate()
    {
        //stamina_bar.SetBarProgress(data.CurrentStaminaPercent, data.staminaCur.ToString(), data.staminaMax.ToString());
        UpdateStamina();
        UpdateProgress();
        UpdateCounters();
        check_status.gameObject.SetActive(data.parkUID != "park_session_start");
    }
    void BottomUpdate()
    {
        btn_end_early.gameObject.SetActive(data.curStatus == dConstants.ParkConst.ParkStatus.nextNode && data.totalNodes > 1);
    }
    public void UpdateStamina()
    {
        stamina_bar.SetStamina(data.staminaCur);
        if (data.staminaDepleted)
        {
            stamina_bar.StaminaDepleted();
        }
        action_stamina.SetModifier(data.CurrentActionStaminaAdditionString);
    }
    public void UpdateProgress()
    {
        if (progress_bar.gameObject.activeSelf)
        {
            progress_bar.SetBarProgress(data.CurrentProgressPercent, data.progressCur.ToString(), data.progressMax.ToString());
        }
        else
        {
            progress_bar.ResetBar(data.CurrentProgressPercent, data.progressCur.ToString(), data.progressMax.ToString());
        }
        if (data.StatusIsOnRoute)
        {
            ParkNode nextNode = dDataHub.hub.dParkNode.GetFromUID(data.nextNodeDataUID);
            next_node.SetIcon(nextNode.artAsset.objectArt);
            progress_bar.gameObject.SetActive(true);
            //next_node.gameObject.SetActive(true);
        }
        else
        {
            group_progress.SetActive(false);
            progress_bar.gameObject.SetActive(false);
            //next_node.gameObject.SetActive(false);
        }
        if (data.hideProgress)
        {
            progress_bar.HideNumbers();
        }
        progress_flat.SetModifier(data.CurrentProgressAdditionFlatString);
        progress_percent.SetModifier(data.CurrentProgressAdditionPercentString);
    }
    public void UpdateCounters()
    {
        danger_counter.SetCounter(data.curDangerToken, data.maxDangerToken);
        //danger_counter.SetText(data.CurrentDangerAdditionString);
        danger_chance.SetModifier(data.CurrentDangerAdditionString);
        collectible_counter.SetCounter(data.curCollectibleToken, data.maxCollectibleToken);
        //collectible_counter.SetText(data.CurrentCollectibleAdditionString);
        collectible_chance.SetModifier(data.CurrentCollectibleAdditionString);

        vision.majorStr.SetText(data.ModifiedActionDrawCount.ToString());
    }
    public void ShowCardPlayPreview()
    {
        stamina_bar.PreviewDelta(data.staminaPreview);
        if (!data.hideProgress)
        {
            progress_bar.PreviewDelta((float)data.progressPreview / data.progressMax, Mathf.Min(data.progressMax, data.progressPreview).ToString());
        }
        //danger_counter.PreviewDelta(1, 0.5f);
        //collectible_counter.PreviewDelta(-1, 1f);
    }
    public void StopCardPlayPreview()
    {
        stamina_bar.PreviewStop();
        progress_bar.PreviewStopSequence();
        //danger_counter.PreviewStop();
        //collectible_counter.PreviewStop();
    }
    public void EnterRoute()
    {
        //
    }
    public void ExitRoute()
    {
        //progress_bar.gameObject.SetActive(false);
    }
    public void show_stamina_tooltip()
    {
        tooltip_popup.controller.CallTooltipBasedOnMouse(Input.mousePosition, tooltip_popup.WindowType.vertical_down, "补给", "补给耗尽会结束景点的游览");
    }
    public void show_danger_tooltip()
    {
        tooltip_popup.controller.CallTooltipBasedOnMouse(Input.mousePosition, tooltip_popup.WindowType.vertical_down, "危险", "累计3个危险标记会触发灾难");
    }
    public void show_collectible_tooltip()
    {
        tooltip_popup.controller.CallTooltipBasedOnMouse(Input.mousePosition, tooltip_popup.WindowType.vertical_down, "风景", "累计3个风景标记会偶遇风景");
    }
    public void show_vision_tooltip()
    {
        tooltip_popup.controller.CallTooltipBasedOnMouse(Input.mousePosition, tooltip_popup.WindowType.vertical_down, "探索", "当前场景抽取的行动卡数量");
    }
    public void show_progress_tooltip()
    {
        tooltip_popup.controller.CallTooltipBasedOnMouse(Input.mousePosition, tooltip_popup.WindowType.vertical_down, "距离", 
            string.Format("前进足够距离以到达下一个节点(还剩{0})", data.progressMax - data.progressCur));
    }
}
