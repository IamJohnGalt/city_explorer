﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class uiParkNextNode : uiElement
{
    [Header("Data")]
    public string targetNodePfbUID;
    public gParkVisit parkGameplayScript;
    public uiParkOnMap masterUIPage;
    [SerializeField] dataParkVisit data;

    [Header("Display Groups")]
    [SerializeField] GameObject content_group;
    [SerializeField] GameObject group_node;
    [SerializeField] GameObject group_path;
    [SerializeField] GameObject group_collectible;

    [Header("Children Objects")]
    [SerializeField] ui_icon node_icon;
    [SerializeField] ui_text node_title;
    [SerializeField] ui_text path_title;
    [SerializeField] ui_text path_flavor;
    [SerializeField] ui_text path_desc;
    [SerializeField] ui_icon_text progress_req;
    [SerializeField] ui_icon_tier view_tier;
    [SerializeField] List<ui_collectible> collectibles;
    [SerializeField] ui_button confirm_btn;
    [SerializeField] ui_button close_btn;

    [Header("Game Event")]
    [SerializeField] GameEvent ClearPathHighlight;

    private uiParkNode targetNodeObj;
    private uiParkNode curNodeObj;
    private uiParkRoute routeObj;
    private void Awake()
    {
        UIClose();
    }
    public void btn_path_taken()
    {
        if (data.curStatus == dConstants.ParkConst.ParkStatus.nextNode)
        {
            parkGameplayScript.RoutePfbTaken(routeObj.pfbUID);
            masterUIPage.EnterRouteTransition();
            //sfx
            SFXPlayer.mp3.PlaySFX("stepaway");
            SFXPlayer.mp3.PlaySFX("guqin_node_start");
        }
        //UIClose();
    }
    public override void UIInit()
    {
        if (masterUIPage.OnNodeStartQueued)
        {
            return;
        }
        targetNodeObj = null;
        curNodeObj = null;
        routeObj = null;

        for (int i = 0; i < data.currentMap.nodes.Count; i++)
        {
            if (data.currentMap.nodes[i].onClick)
            {
                targetNodePfbUID = data.currentMap.nodes[i].pfbUID;
                break;
            }
        }
        parkGameplayScript.NodePreviewOnMap(targetNodePfbUID);
        if(data.parkUID == dConstants.ParkConst.INTRO_PARK_UID)
        {
            gIntroParkScript.controller.ClearDialogAndMask();
        }
        //confirm_btn.HoverOff();
        //close_btn.HoverOff();
        UIUpdate();
        Reposition();
        Expand();
        content_group.SetActive(true);
    }
    public override void UIClose()
    {
        ClearPathHighlight.Raise();
        content_group.SetActive(false);
        targetNodeObj = null;
        curNodeObj = null;
        routeObj = null;
        confirm_btn.HoverOff();
        close_btn.HoverOff();
    }
    public override void UIUpdate()
    {
        targetNodeObj = data.currentMap.RetrieveNodeByPfbUID(targetNodePfbUID);
        curNodeObj = data.CurrentNodeObj;
        for(int i = 0; i < curNodeObj.outRoutes.Count; i++)
        {
            if(curNodeObj.outRoutes[i].outNode == targetNodeObj)
            {
                routeObj = curNodeObj.outRoutes[i];
                break;
            }
        }
        ParkNode nodeData = dDataHub.hub.dParkNode.GetFromUID(targetNodeObj.nodeUID);
        node_title.SetText(nodeData.title);
        node_icon.SetIcon(nodeData.artAsset.objectArt);
        group_node.SetActive(true);

        if(targetNodeObj.viewUIDs.Count > 0)
        {
            View viewData = dDataHub.hub.dView.GetFromUID(targetNodeObj.viewUIDs[0]);
            view_tier.SetTier(viewData.tier);
        }
        for(int i = 0; i < collectibles.Count; i++)
        {
            
            if (i < targetNodeObj.viewUIDs.Count)
            {
                collectibles[i].SetAsViewByUID(targetNodeObj.viewUIDs[0], dConstants.ParkConst.TokenStatus.preview);
                collectibles[i].gameObject.SetActive(true);
            }
            else
            {
                collectibles[i].gameObject.SetActive(false);
            }
        }
        
        if (routeObj == null)
        {
            Debug.Log("unable to find a route connect target and cur node");
            group_path.SetActive(false);
            confirm_btn.gameObject.SetActive(false);
        }
        else
        {
            ParkPath pathData = dDataHub.hub.dParkPath.GetFromUID(routeObj.pathUID);
            path_title.SetText(string.Format("路径 - {0}", pathData.title));
            path_flavor.SetText(pathData.flavor);
            if (pathData.randomProgress)
            {
                progress_req.majorStr.SetText(string.Format("???"));
            }
            else
            {
                progress_req.majorStr.SetText(string.Format("{0}", pathData.modifiedProgressReq));
            }
            
            path_desc.SetText(pathData.desc);
            group_path.SetActive(true);
            confirm_btn.gameObject.SetActive(true);
        }
    }
    void Reposition()
    {
        uiParkNode node = data.currentMap.RetrieveNodeByPfbUID(targetNodePfbUID);
        transform.position = node.popup_pos.position;
    }
    public void Expand()
    {
        //SetVisible(true);
        RectTransform rect = GetComponent<RectTransform>();
        rect.localScale = Vector3.one;
        rect.DOScale(Vector3.zero, 0.3f).From().SetEase(Ease.OutSine);
    }
    public void Collapse()
    {
        RectTransform rect = GetComponent<RectTransform>();
        rect.DOScale(Vector3.zero, 0.3f).SetEase(Ease.OutSine);
    }
}
