﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class uiSessionMenu : uiElement
{
    [Header("Data")]
    [SerializeField] dataChronicle dataChronicle;
    [SerializeField] dataCalendar dataCalendar;
    [SerializeField] CurrencyList dataCurrency;
    [SerializeField] dataPlayerEquipment dataEquipment;
    [SerializeField] dataPlayerTrait dataTrait;
    [SerializeField] dataPlayerLevel dataPlayerLevel;
    [Header("Group")]
    [SerializeField] GameObject group_page;
    [SerializeField] GameObject group_currency;
    [SerializeField] GameObject group_plevel;
    [SerializeField] GameObject group_trait;
    [SerializeField] GameObject group_equipment;
    [SerializeField] GameObject group_journal;
    [Header("Children Objects")]
    [SerializeField] ui_icon_text date;
    [SerializeField] ui_icon_text timeLeft;
    [SerializeField] ui_icon_text coin;
    [SerializeField] ui_icon_text star;
    [SerializeField] ui_icon_text staminaBonus;
    [SerializeField] ui_text progressMeter;
    [SerializeField] List<equipment_item> equipmentItems;
    [SerializeField] List<trait_item> traitItems;
    //[SerializeField] trait_item traitItemRare;
    [SerializeField] journal_entry journal;
    [SerializeField] character_entry portrait;
    [SerializeField] List<stat_item> stats;


    public override void UIInit()
    {
        timeLeft.gameObject.SetActive(false);
        coin.gameObject.SetActive(false);
        group_plevel.SetActive(false);
        UIUpdate();
        CalendarUpdate();
    }
    public override void UIUpdate()
    {
        portrait.UpdateToCurrentAvatar();
        dataCharacter curCharc = CharacterManagement.controller.GetCurrentCharacter();
        for (int i = 0; i < stats.Count; i++)
        {
            if(i + 2 <= curCharc.curStoryLevel)
            {
                stats[i].ShowStatByUID(string.Format("{0}_persona_{1}", curCharc.characterUID, i+2));
            }
            else
            {
                stats[i].gameObject.SetActive(false);
            }
        }
        CurrencyUpdate();
        //progressLeft.icon.SetIcon(CharacterManagement.controller.GetCurrentCharacter().food);
        if (dataPlayerLevel.currentLevel < dConstants.Avatar.MAX_PLAYER_LEVEL)
        {
            progressMeter.SetText(string.Format("据下次提升：{0} / {1}", CurrencyManagement.wallet.CheckCurrency("currency_xp").ToString(), dataPlayerLevel.GetNextLvXPCost));
        }
        else
        {
            
            progressMeter.SetText(string.Format("已达到最大值"));
        }
        staminaBonus.icon.SetIcon("dynamic_current_stamina");
        TotalBuffValueSet bvs = ActiveBuffs.controller.RetrieveValueSetByAction("max_stamina");
        int maxSta = ActiveBuffs.ModifyByBVS(dConstants.ParkConst.INITAL_STAMINA, bvs);
        //staminaBonus.majorStr.SetText(string.Format("{0}", PlayerLevelManagement.controller.GetCurrentStaminaBonus()));
        staminaBonus.majorStr.SetText(string.Format("{0}", maxSta));
        //equipment group update
        for (int i = 0; i < equipmentItems.Count; i++)
        {
            if (i < dataEquipment.activeEquipment.Count && dataEquipment.activeEquipment[i] != "")
            {
                equipmentItems[i].ShowEquipmentByUID(dataEquipment.activeEquipment[i]);
                equipmentItems[i].gameObject.SetActive(true);
            }
            else
            {
                equipmentItems[i].gameObject.SetActive(false);
            }
        }
        //trait group update
        for (int i = 0; i < traitItems.Count; i++)
        {
            if (i < dataTrait.activeTrait.Count && dataTrait.activeTrait[i] != "")
            {
                traitItems[i].ShowTraitByUID(dataTrait.activeTrait[i]);
                traitItems[i].gameObject.SetActive(true);
            }
            else if(i == dataTrait.activeTrait.Count)
            {
                traitItems[i].ShowTraitByUID("next_trait_slot");
                traitItems[i].gameObject.SetActive(true);
            }
            else
            {
                traitItems[i].gameObject.SetActive(false);
            }
        }
        /*if(dataTrait.activeTraitRare.Count > 0)
        {
            traitItemRare.ShowTraitByUID(dataTrait.activeTraitRare[0]);
            traitItemRare.gameObject.SetActive(true);
        }
        else
        {
            traitItemRare.gameObject.SetActive(false);
        }*/
    }
    public void CalendarUpdate()
    {
        date.majorStr.SetText(CalendarManagement.calendar.GetCurDate().DateToString());
        date.minorStr.SetText(CalendarManagement.calendar.SeasonToString());
        date.icon.SetIcon(CalendarManagement.calendar.GetSeasonSprite());
    }
    public void TimeUpdate()
    {
        if(CalendarManagement.calendar.GetMonthLeft() > 0)
        {
            timeLeft.majorStr.SetText(string.Format("{0}月", CalendarManagement.calendar.GetMonthLeft()));
            timeLeft.majorStr.SetTextColor(Color.black);
            timeLeft.minorStr.SetText(string.Format("据旅程结束"));
        }
        else
        {
            timeLeft.majorStr.SetText(string.Format("时间耗尽", CalendarManagement.calendar.GetMonthLeft()));
            timeLeft.majorStr.SetTextColor(Color.red);
            timeLeft.minorStr.SetText(string.Format("返回城镇后结束"));
        }
        
        if (dataCalendar.latestMonthChanged > 0)
        {
            timeLeft.Anim_ValueEarn(1);
        }
        else if(dataCalendar.latestMonthChanged < 0)
        {
            timeLeft.Anim_ValueLose(1);
        }
        dataCalendar.latestMonthChanged = 0;
        if(dataChronicle.status != dConstants.ChronicleConst.ChronicleStatus.inPark)
        {
            CalendarUpdate();
        }
    }
    public void CurrencyUpdate()
    {
        coin.majorStr.SetText(CurrencyManagement.wallet.CheckCurrency("currency_coin").ToString());
        int amountChanged = CurrencyManagement.wallet.CheckCurrencyLatestChange("currency_coin");
        if (amountChanged > 0)
        {
            coin.Anim_ValueEarn(Mathf.CeilToInt(Mathf.Abs(amountChanged) / 50f));
        }
        else if (amountChanged < 0)
        {
            coin.Anim_ValueLose(Mathf.CeilToInt(Mathf.Abs(amountChanged) / 50f));
        }
        star.majorStr.SetText(CurrencyManagement.wallet.CheckCurrency("currency_star").ToString());
        amountChanged = CurrencyManagement.wallet.CheckCurrencyLatestChange("currency_star");
        if (amountChanged > 0)
        {
            star.Anim_ValueEarn(Mathf.CeilToInt(Mathf.Abs(amountChanged) / 3f));
        }
        else if (amountChanged < 0)
        {
            star.Anim_ValueLose(Mathf.CeilToInt(Mathf.Abs(amountChanged) / 3f));
        }
    }
    public void RevealTime()
    {
        timeLeft.gameObject.SetActive(true);
    }
    public void RevealCoin()
    {
        coin.gameObject.SetActive(true);
    }
    public void RevealProgress()
    {
        group_plevel.SetActive(true);
    }
    public void character_hit()
    {
        //in-session character details up
        CharacterManagement.controller.ViewCurrentTraveller();
    }

    public void show_season_tooltip()
    {
        tooltip_popup.controller.CallTooltipBasedOnMouse(Input.mousePosition, tooltip_popup.WindowType.horizontal, "季节", "不同季节会出现不同的风景");
    }
    public void show_time_tooltip()
    {
        tooltip_popup.controller.CallTooltipBasedOnMouse(Input.mousePosition, tooltip_popup.WindowType.horizontal, "旅行时间", "旅行时间耗尽，<br>则游戏结束");
    }
    public void show_coin_tooltip()
    {
        tooltip_popup.controller.CallTooltipBasedOnMouse(Input.mousePosition, tooltip_popup.WindowType.horizontal, "铜钱", "有钱能使鬼推磨");
    }
    public void show_stamina_tooltip()
    {
        tooltip_popup.controller.CallTooltipBasedOnMouse(Input.mousePosition, tooltip_popup.WindowType.horizontal, "补给", "旅者的补给<br>随路程增加");
    }
    public void show_star_tooltip()
    {
        tooltip_popup.controller.CallTooltipBasedOnMouse(Input.mousePosition, tooltip_popup.WindowType.horizontal, "星级", string.Format("收集星级以完成{0}",dConstants.MASTER_JOURNAL_NAME));
    }
}
