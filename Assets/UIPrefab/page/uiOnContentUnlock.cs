﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class uiOnContentUnlock : uiElement
{
    [Header("Gameplay")]
    [SerializeField] dataContentUnlocked unlockData;

    [Header("Groups")]
    [SerializeField] GameObject page;

    [Header("Children Objects")]
    [SerializeField] ui_text title;
    [SerializeField] List<ui_icon_text> unlockedItems;
    // Start is called before the first frame update
    private void Awake()
    {
        page.SetActive(false);
    }
    public void btn_confirm_hit()
    {
        UIInit();
    }
    public override void UIInit()
    {
        if(unlockData.unlocks.Count > 0)
        {
            UIUpdate();
            Anim_Popup();
            page.SetActive(true);
        }
        else
        {
            if (unlockData.isEndGame)
            {
                SessionExit();
            }
            else
            {
                UIClose();
            }
        }
    }
    public override void UIUpdate()
    {
        int totalDisplayedCount = 0;
        if (unlockData.isEndGame)
        {
            title.SetText("新解锁内容");
        }
        else
        {
            title.SetText("来自上次旅程的解锁内容");
        }
        for(int i = 0; i < unlockedItems.Count; i++)
        {
            if(i < unlockData.unlocks.Count)
            {
                unlockedItems[i].icon.SetIcon(unlockData.unlocks[i].icon);
                unlockedItems[i].minorStr.SetText(unlockData.unlocks[i].title);
                unlockedItems[i].majorStr.SetText(unlockData.unlocks[i].desc);
                unlockedItems[i].gameObject.SetActive(true);
                totalDisplayedCount += 1;
            }
            else
            {
                unlockedItems[i].gameObject.SetActive(false);
            }
        }
        unlockData.unlocks.RemoveRange(0, totalDisplayedCount);
    }
    public override void UIClose()
    {
        page.SetActive(false);
    }
    void SessionExit()
    {
        EndSession.controller.RestartGame();
    }
    void Anim_Popup()
    {
        RectTransform rect = GetComponent<RectTransform>();
        rect.localScale = Vector3.one;
        rect.DOScale(Vector3.zero, 0.3f).From().SetEase(Ease.OutSine);
    }
}
