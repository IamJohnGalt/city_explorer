﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class uiJournalPage : uiElement
{
    [Header("Data")]
    [SerializeField] gJournal gameplayScript;
    //[SerializeField] gChronicle gameplayScript;
    [SerializeField] JournalBonusHub bonusHub;
    [SerializeField] dataJournal gameplayData;
    [SerializeField] dataChronicle chronicleData;
    [SerializeField] JournalSet journalSet;
    [SerializeField] StringAsset rewardDegrees;

    [Header("Display Groups")]
    [SerializeField] GameObject group_page;
    [SerializeField] GameObject group_expand;
    [SerializeField] GameObject group_bg;
    [SerializeField] GameObject group_selection;
    [SerializeField] GameObject group_journal;
    //[SerializeField] GameObject group_wip_info;
    [SerializeField] GameObject group_reward;
    [SerializeField] GameObject group_leftover;

    [Header("Selection Page")]
    [SerializeField] List<GameObject> journalOptionSlots;
    [SerializeField] List<uiJournalToken> journalOptions;

    [Header("Journal Page")]
    public uiJournalStructure journalStructure;
    [SerializeField] List<int> selectedSlotIndex;

    [Header("Reward Page")]
    [SerializeField] ui_label journalTitle;
    [SerializeField] ui_text submissionDesc;
    [SerializeField] ui_star totalStar;
    //[SerializeField] ui_text fameDesc;
    [SerializeField] List<reward_item> fameRewards;
    [SerializeField] ui_text coinDesc;
    [SerializeField] List<reward_item> coinRewards;
    [SerializeField] GameObject equipmentSection;
    [SerializeField] ui_text equipmentDesc;
    [SerializeField] List<reward_item> equipmentRewards;
    public ui_button btn_confirm;
    
    //[SerializeField] List<uiJournalReward> optionalRewards;

    [Header("Leftover Page")]
    public List<journal_slot> leftoverSlots;
    public List<ui_collectible> leftoverCollectibles;

    //[Header("Toggle Btn")]
    //[SerializeField] uipfb_label_v2 journalIcon;
    private bool expanded;

    //[Header("Event")]
    //[SerializeField] GameEvent OnRouteStart;
    //[SerializeField] GameEvent popupStart;
    private void Awake()
    {
        //group_page.SetActive(false);
    }
    public void btn_journal_selection_hit(int index)
    {
        //gameplayScript.JournalSelected(index);
        UIUpdate();
    }
    public void btn_max_cur_star()
    {
        gameplayScript.Rearrange4HighestStar();
        SFXPlayer.mp3.PlaySFX("button_click");
        UIUpdate();
    }
    public void btn_journal_token_hit_on_main(int index)
    {
        int modifiedIndex = index;
        if (selectedSlotIndex.Contains(modifiedIndex))
        {
            selectedSlotIndex.Remove(modifiedIndex);
        }
        else
        {
            selectedSlotIndex.Add(modifiedIndex);
        }
        if(selectedSlotIndex.Count == 2)
        {
            //do swap
            gameplayScript.SwapToken(selectedSlotIndex[0], selectedSlotIndex[1]);
            selectedSlotIndex.Clear();
        }
        SFXPlayer.mp3.PlaySFX("button_click");
        UIUpdate();
    }
    public void btn_journal_token_hit_on_extra(int index)
    {
        int modifiedIndex = index + gameplayData.curComboTokenCount;
        if (selectedSlotIndex.Contains(modifiedIndex))
        {
            selectedSlotIndex.Remove(modifiedIndex);
        }
        else
        {
            selectedSlotIndex.Add(modifiedIndex);
        }
        if (selectedSlotIndex.Count == 2)
        {
            //do swap
            gameplayScript.SwapToken(selectedSlotIndex[0], selectedSlotIndex[1]);
            selectedSlotIndex.Clear();
        }
        SFXPlayer.mp3.PlaySFX("button_click");
        UIUpdate();
    }
    public void btn_journal_token_remove_on_main(int index)
    {
        int modifiedIndex = index;
        gameplayScript.RemoveToken(modifiedIndex);
        selectedSlotIndex.Clear();
        UIUpdate();
    }
    public void btn_journal_token_remove_on_extra(int index)
    {
        int modifiedIndex = index + gameplayData.curComboTokenCount;
        gameplayScript.RemoveToken(modifiedIndex);
        selectedSlotIndex.Clear();
        UIUpdate();
    }

    public void btn_journal_submit_hit()
    {
        //old
        gameplayScript.JournalSubmit();

        //new
        JournalViewToggle();
        ResetJournalLayout();

        //
        SFXPlayer.mp3.PlaySFX("paper_send");
        UIUpdate();
    }
    public void btn_reward_confirm_hit()
    {
        //deprecated
        gameplayScript.RewardSelected();
        //Anim_Page_Slide();
        JournalViewToggle();
        ResetJournalLayout();
        SFXPlayer.mp3.PlaySFX("button_click");
        UIUpdate();
    }
    public override void UIInit()
    {
        //reset ui
        selectedSlotIndex = new List<int>();
        group_expand.GetComponent<RectTransform>().localScale = Vector3.zero;
        expanded = false;
        group_page.SetActive(true);
        
    }
    public override void UIClose()
    {
        //DebugEndPopup();
        group_page.SetActive(false);
    }
    public override void UIUpdate()
    {
        ClearGroup();
        switch (gameplayData.gameplayStatus)
        {
            case dConstants.JournalConst.JournalStatus.selection:
                //SelectionUpdate();
                LeftoverUpdate();
                break;
            case dConstants.JournalConst.JournalStatus.journal_working:
                JournalUpdate();
                break;
            case dConstants.JournalConst.JournalStatus.journal_full:
                JournalUpdate();
                break;
            case dConstants.JournalConst.JournalStatus.reward:
                RewardUpdate();
                break;
            default:
                //UIClose();
                break;
        }
    }
    public void JournalViewToggle(bool forceExpand = false)
    {
        if (expanded && !forceExpand)
        {
            Anim_Page_Collapse();
        }
        else
        {
            Anim_Page_Expand();
        }
    }
    public void CreateNewJournalStructure()
    {
        //init journal structure
        //Debug.Log("curJournal main slot count: " + gameplayData.curComboTokenCount);
        Destroy(journalStructure.gameObject);
        journalStructure = Instantiate(journalSet.GetStructureBySlot(gameplayData.curComboTokenCount), group_journal.transform).GetComponent<uiJournalStructure>();
        //setup slot click actions
        for (int i = 0; i < journalStructure.mainSlots.Count; i++)
        {
            journalStructure.mainSlots[i].ref_token_selected_func = btn_journal_token_hit_on_main;
            //mainCollectibles[i].ref_token_selected_func = btn_journal_token_hit_on_main;
            journalStructure.mainSlots[i].ref_token_delete_func = btn_journal_token_remove_on_main;
        }
        for (int i = 0; i < journalStructure.extraSlots.Count; i++)
        {
            journalStructure.extraSlots[i].ref_token_selected_func = btn_journal_token_hit_on_extra;
            //journalStructure.extraCollectibles[i].ref_token_selected_func = btn_journal_token_hit_on_extra;
            journalStructure.extraSlots[i].ref_token_delete_func = btn_journal_token_remove_on_extra;
        }
        journalStructure.ref_submit_func = btn_journal_submit_hit;
        journalStructure.ref_max_func = btn_max_cur_star;
        UIUpdate();
    }
    void Anim_Page_Expand()
    {
        UIUpdate();
        group_expand.GetComponent<RectTransform>().DOScale(1f, 0.5f).SetEase(Ease.OutSine).OnComplete(()=>gIntroParkScript.controller.SetJournalBasicIntro());
        expanded = true;
        SFXPlayer.mp3.PlaySFX("paper_expand");
    }
    void Anim_Page_Collapse()
    {
        group_expand.GetComponent<RectTransform>().DOScale(0f, 0.5f).SetEase(Ease.InSine);
        expanded = false;
        SFXPlayer.mp3.PlaySFX("paper_expand");
    }
    void ClearGroup()
    {
        group_selection.SetActive(false);
        group_journal.SetActive(false);
        group_reward.SetActive(false);
        group_leftover.SetActive(false);
    }
    void SelectionUpdate()
    {
        //group_selection.SetActive(true);
        //journalIcon.SetText("思考中");
    }
    void JournalUpdate()
    {
        JournalStyle style = dDataHub.hub.dJournalStyle.GetFromUID(gameplayData.journalUID);
        //journal name
        journalStructure.journalName.SetText(gameplayData.journalName);
        //token slots
        for(int i=0;i< journalStructure.mainSlots.Count; i++)
        {
            if (i < gameplayData.curComboTokenCount)
            {
                journalStructure.mainCollectibles[i].SetAsCollectibleByUID(gameplayData.tokens[i], DetermineCollectibleStatus(true, true));
                journalStructure.mainSlots[i].SetSlot(DetermineSlotStatus(true, gameplayData.tokens[i] != "", selectedSlotIndex.Contains(i)));
                journalStructure.mainSlots[i].gameObject.SetActive(!gameplayData.oneParkJournal || gameplayData.tokens[i] != "");
            }
            else
            {
                journalStructure.mainSlots[i].gameObject.SetActive(false);
            }
        }
        int extraSloTIndexOffset = gameplayData.curComboTokenCount;
        bool exceedMainSlots = gameplayData.CurTokenCount > gameplayData.curComboTokenCount;
        //Debug.Log(string.Format("exceedMainSlots: {0}", exceedMainSlots));
        for (int i = 0; i < journalStructure.extraSlots.Count; i++)
        {
            if (i < gameplayData.CurTokenCount - gameplayData.curComboTokenCount)
            {
                journalStructure.extraCollectibles[i].SetAsCollectibleByUID(gameplayData.tokens[i + extraSloTIndexOffset], DetermineCollectibleStatus(true, false));
                journalStructure.extraSlots[i].SetSlot(DetermineSlotStatus(exceedMainSlots, gameplayData.tokens[i + extraSloTIndexOffset] != "", selectedSlotIndex.Contains(i + extraSloTIndexOffset)));
                journalStructure.extraSlots[i].gameObject.SetActive(true);
            }
            else
            {
                journalStructure.extraSlots[i].gameObject.SetActive(false);
            }
        }

        //journalStructure.subG_extra.SetActive(exceedMainSlots);
        journalStructure.subG_extra.SetActive(true);
        //new star bonus item group, display all bonus stars - content, quest, combo
        for (int i=0;i< journalStructure.bonusItems.Count; i++)
        {
            if(i < gameplayData.starBonusItems.Count)
            {
                journalStructure.bonusItems[i].SetItem(gameplayData.starBonusItems[i]);
                journalStructure.bonusItems[i].gameObject.SetActive(true);
            }
            else
            {
                journalStructure.bonusItems[i].gameObject.SetActive(false);
            }
        }
        for (int i = 0; i < journalStructure.wipItems.Count; i++)
        {
            if (i < gameplayData.starBonusWIPs.Count)
            {
                journalStructure.wipItems[i].SetItem(gameplayData.starBonusWIPs[i]);
                journalStructure.wipItems[i].gameObject.SetActive(true);
            }
            else
            {
                journalStructure.wipItems[i].gameObject.SetActive(false);
            }
        }
        for (int i = 0; i < journalStructure.hintItems.Count; i++)
        {
            if (i < gameplayData.starBonusQuests.Count)
            {
                journalStructure.hintItems[i].SetItem(gameplayData.starBonusQuests[i]);
                journalStructure.hintItems[i].gameObject.SetActive(true);
            }
            else
            {
                journalStructure.hintItems[i].gameObject.SetActive(false);
            }
        }
        journalStructure.wipDivider.SetActive(gameplayData.starBonusWIPs.Count > 0);
        journalStructure.hintDivider.SetActive(gameplayData.starBonusQuests.Count > 0);
        journalStructure.totalStar.SetText(gameplayData.totalStars);
        //preview reward
        for(int i=0;i< journalStructure.previews.Count; i++)
        {
            if (i < gameplayData.curRewardPreviews.Count)
            {
                journalStructure.previews[i].SetRewardPreview(gameplayData.curRewardPreviews[i]);
                journalStructure.previews[i].transform.localScale = Vector3.one * (gameplayData.curRewardPreviews.Count > 5 ? 0.8f : 1f);
                journalStructure.previews[i].gameObject.SetActive(true);
            }
            else
            {
                journalStructure.previews[i].gameObject.SetActive(false);
            }
        }
        //submit button
        if (gameplayData.journalIsSubmittable && chronicleData.status == dConstants.ChronicleConst.ChronicleStatus.journalSubmission || gameplayData.forcedSubmission)
        {
            journalStructure.submit_btn.EnableButton();
            journalStructure.submit_btn.minorStr.gameObject.SetActive(false);
            journalStructure.submit_btn.majorStr.gameObject.SetActive(true);
        }
        else if (gameplayData.journalIsSubmittable && chronicleData.status != dConstants.ChronicleConst.ChronicleStatus.journalSubmission)
        {
            journalStructure.submit_btn.DisableButton();
            journalStructure.submit_btn.minorStr.SetText(string.Format("回到城镇后以", gameplayData.CurTokenCount, gameplayData.curComboTokenCount));
            journalStructure.submit_btn.minorStr.gameObject.SetActive(true);
            journalStructure.submit_btn.majorStr.gameObject.SetActive(true);
        }
        else
        {
            if(gameplayData.CurTokenCount < gameplayData.curComboTokenCount)
            {
                journalStructure.submit_btn.DisableButton();
                journalStructure.submit_btn.minorStr.SetText(string.Format("收集更多风景({0}/{1})以", gameplayData.CurTokenCount, gameplayData.curComboTokenCount));
                journalStructure.submit_btn.minorStr.gameObject.SetActive(true);
                journalStructure.submit_btn.majorStr.gameObject.SetActive(true);
            }
            else if(gameplayData.totalStars <= 0)
            {
                journalStructure.submit_btn.DisableButton();
                journalStructure.submit_btn.minorStr.SetText(string.Format("获得足够星级以", gameplayData.CurTokenCount, gameplayData.curComboTokenCount));
                journalStructure.submit_btn.minorStr.gameObject.SetActive(true);
                journalStructure.submit_btn.majorStr.gameObject.SetActive(true);
            }
            else
            {
                Debug.LogError("invalid status of unsubmittable journal!");
            }
        }
        if(gameplayData.CurTokenCount <= gameplayData.curComboTokenCount)
        {
            journalStructure.max_btn.DisableButton();
        }
        else
        {
            journalStructure.max_btn.EnableButton();
        }
        
        group_journal.SetActive(true);
    }
    void RewardUpdate()
    {
        //journalStructure.starBonusGroup.total_stars.SetText(gameplayData.totalStars.ToString());

        journalTitle.SetText(gameplayData.journalName);
        if (!gameplayData.sessionStartJournal)
        {
            submissionDesc.SetText(string.Format("于{0}发表，为{1}所收录为第{2}篇，得以流传于世。",
            CalendarManagement.calendar.GetCurDate().DateToString(),
            dConstants.MASTER_JOURNAL_NAME,
            MasterJournalManagement.controller.TotalJournalCount() + 1
            //Mathf.RoundToInt(CharacterManagement.controller.GetCurrentCharacter().GetTotalFameCompletion*100)
            ));
        }
        else
        {
            submissionDesc.SetText(string.Format("于{0}完成。",
            CalendarManagement.calendar.GetCurDate().DateToString()));
        }
        
        /*
        totalStar.SetText(gameplayData.totalStars);
        
        //set reward by category
        int nextFameRewardIndex = 0;
        int nextCoinRewardIndex = 0;
        int nextEquipmentRewardIndex = 0;

        int totalFame = 0;
        int totalCoin = 0;
        int totalEquipment = 0;

        for (int i = 0; i < gameplayData.constantRewards.Count; i++)
        {
            if(gameplayData.constantRewards[i].type == RewardItem.RewardType.currency)
            {
                if (gameplayData.constantRewards[i].rewardUID == "currency_star")
                {
                    if(nextFameRewardIndex < fameRewards.Count)
                    {
                        fameRewards[nextFameRewardIndex++].SetReward(gameplayData.constantRewards[i]);
                        totalFame += gameplayData.constantRewards[i].amount;
                    }
                    else
                    {
                        Debug.LogError("not enough fame reward ui items");
                    }
                }
                else if (gameplayData.constantRewards[i].rewardUID == "currency_coin")
                {
                    if (nextCoinRewardIndex < coinRewards.Count)
                    {
                        coinRewards[nextCoinRewardIndex++].SetReward(gameplayData.constantRewards[i]);
                        totalCoin += gameplayData.constantRewards[i].amount;
                    }
                    else
                    {
                        Debug.LogError("not enough coin reward ui items");
                    }
                }
            }
            else if(gameplayData.constantRewards[i].type == RewardItem.RewardType.equipment)
            {
                if (nextEquipmentRewardIndex < equipmentRewards.Count)
                {
                    equipmentRewards[nextEquipmentRewardIndex++].SetReward(gameplayData.constantRewards[i]);
                    totalEquipment += 1;
                }
                else
                {
                    Debug.LogError("not enough equipment reward ui items");
                }
            }
        }
        for(int i = 0; i < fameRewards.Count; i++)
        {
            if(i>= nextFameRewardIndex)
            {
                fameRewards[i].gameObject.SetActive(false);
            }
        }
        for (int i = 0; i < coinRewards.Count; i++)
        {
            if (i >= nextCoinRewardIndex)
            {
                coinRewards[i].gameObject.SetActive(false);
            }
        }
        for (int i = 0; i < equipmentRewards.Count; i++)
        {
            if (i >= nextEquipmentRewardIndex)
            {
                equipmentRewards[i].gameObject.SetActive(false);
            }
        }
        if (totalFame > 0)
        {
            fameDesc.SetText(string.Format("{0}的发表在当时引起了{1}的反响，为文人墨客们所认可：",
            gameplayData.journalName,
            rewardDegrees.GetAsset(totalFame >= 8 ? 2 : totalFame >= 4 ? 1 : 0)));
        }
        else
        {
            fameDesc.SetText(string.Format("{0}并没有通过{1}的发表得到当时的文人墨客们所认可{2}。", 
                CharacterManagement.controller.GetCurrentCharacterTitle(), 
                gameplayData.journalName,
                "，但这并未打消他/她对于游记写作的热情"));
        }
        if (totalCoin > 0)
        {
            coinDesc.SetText(string.Format("{0}的发表也让{1}获得了{2}的资助：",
            gameplayData.journalName,
            CharacterManagement.controller.GetCurrentCharacterTitle(),
            rewardDegrees.GetAsset(totalCoin >= 180 ? 2 : totalCoin >= 80 ? 1 : 0)));
        }
        else
        {
            coinDesc.SetText(string.Format("{0}并没有从{1}的发表中获得任何额外的资助，但这并未打消他/她对于游记写作的热情。", 
                CharacterManagement.controller.GetCurrentCharacterTitle(), 
                gameplayData.journalName));
        }
        equipmentDesc.SetText(string.Format("朋友在{0}发表后，送来了礼物，以表恭贺、敬仰之情：", 
            gameplayData.journalName));
        equipmentSection.SetActive(totalEquipment > 0);
        */
        group_journal.SetActive(true);
        group_reward.SetActive(true);

        //display the final journal visual
        SetJournalLayoutForReward();
    }
    void LeftoverUpdate()
    {
        for (int i = 0; i < leftoverSlots.Count; i++)
        {
            //leftoverSlots[i].gameObject.SetActive(true);
            if (i < gameplayData.previousTokens.Count)
            {
                leftoverCollectibles[i].SetAsCollectibleByUID(gameplayData.previousTokens[i], DetermineCollectibleStatus(true, false));
                leftoverSlots[i].SetSlot(DetermineSlotStatus(true, gameplayData.previousTokens[i] != "", false));
                leftoverCollectibles[i].gameObject.SetActive(true);
            }
            else
            {
                leftoverSlots[i].SetSlot(DetermineSlotStatus(true, true, false));
                leftoverCollectibles[i].gameObject.SetActive(false);
            }
        }
        group_leftover.SetActive(true);
    }
    void SetJournalLayoutForReward()
    {
        //group_journal.GetComponent<RectTransform>().localScale = Vector3.one * 0.5f;
        journalStructure.subG_collectible.SetActive(true);
        journalStructure.subG_wip.SetActive(false);
        //group_journal.SetActive(true);
    }
    void ResetJournalLayout()
    {
        //group_journal.GetComponent<RectTransform>().localScale = Vector3.one;
        journalStructure.subG_collectible.SetActive(true);
        journalStructure.subG_wip.SetActive(true);
        //journalStructure.subG_wip.SetActive(true);
    }
    dConstants.ParkConst.TokenStatus DetermineCollectibleStatus(bool isAvailable, bool isActive)
    {
        if (!isAvailable)
        {
            return dConstants.ParkConst.TokenStatus.hidden;
        }
        else if (isActive)
        {
            return dConstants.ParkConst.TokenStatus.active;
        }
        else
        {
            return dConstants.ParkConst.TokenStatus.inactive;
        }
    }
    dConstants.JournalConst.SlotStatus DetermineSlotStatus(bool isAvailable, bool hasToken, bool isSelected)
    {
        if (!isAvailable)
        {
            return dConstants.JournalConst.SlotStatus.hidden;
        }
        else if (isSelected)
        {
            return dConstants.JournalConst.SlotStatus.selected;
        }
        else if (hasToken)
        {
            return dConstants.JournalConst.SlotStatus.filled;
        }
        else
        {
            return dConstants.JournalConst.SlotStatus.empty;
        }
    }

   
}
