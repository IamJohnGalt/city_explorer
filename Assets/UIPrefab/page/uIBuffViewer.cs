﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class uIBuffViewer : uiElement
{
    [Header("Data")]
    [SerializeField] dataBuffDisplay displayList;
    //[SerializeField] ui_buffDisplay itemTemplate;

    [Header("Display Groups")]
    [SerializeField] GameObject content_group;

    [Header("Children Objects")]
    //[SerializeField] Transform buffContainer;
    [SerializeField] List<ui_buffDisplay> buffItems;
    [SerializeField] ui_button close_btn;

    private void Awake()
    {
        UIClose();
    }
    public override void UIInit()
    {
        Anim_Expand();
        content_group.SetActive(true);
    }
    public override void UIUpdate()
    {
        for (int i = 0; i < buffItems.Count; i++)
        {
            if(i < displayList.buffDisplays.Count)
            {
                buffItems[i].SetBuffDisplay(displayList.buffDisplays[i]);
                buffItems[i].gameObject.SetActive(true);
            }
            else
            {
                buffItems[i].gameObject.SetActive(false);
            }
        }
    }
    public override void UIClose()
    {
        content_group.SetActive(false);
        close_btn.HoverOff();
    }
    public void Anim_Expand()
    {
        RectTransform rect = GetComponent<RectTransform>();
        rect.localScale = Vector3.one;
        rect.DOScale(Vector3.zero, 0.3f).From().SetEase(Ease.OutSine);
    }
    public void Anim_Collapse()
    {
        RectTransform rect = GetComponent<RectTransform>();
        rect.DOScale(Vector3.zero, 0.3f).SetEase(Ease.OutSine);
    }

}
