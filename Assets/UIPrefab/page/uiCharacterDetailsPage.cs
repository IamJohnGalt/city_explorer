﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class uiCharacterDetailsPage : uiElement
{
    [Header("Data")]
    public dataCharacterManagement data;

    [Header("Groups")]
    [SerializeField] GameObject group_page;
    [SerializeField] GameObject group_title;
    [SerializeField] GameObject group_fame;
    [SerializeField] GameObject group_token;

    [Header("Children Objects")]
    [SerializeField] ui_text charatcer_name;
    [SerializeField] ui_text charatcer_class;
    [SerializeField] ui_text charatcer_food_desc;
    [SerializeField] ui_icon charatcer_food_icon;

    [SerializeField] character_entry portrait;
    //[SerializeField] ui_icon token_icon_l;
    

    [SerializeField] ui_bar fame_bar;
    [SerializeField] ui_text fameTitle;
    [SerializeField] List<ui_text> curFameDetails;
    [SerializeField] ui_text fame_acquire;
    

    [SerializeField] ui_bar token_bar;
    [SerializeField] ui_text tokenTitle;
    [SerializeField] List<ui_text> curTokenDetails;
    //[SerializeField] List<uiPersonaNHelp> storyByLevels;
    [SerializeField] ui_icon token_icon;
    [SerializeField] ui_text story_acquire;
    [SerializeField] ui_text story_acquire_2;
    [SerializeField] ui_text collect_method;
    [SerializeField] ui_text collect_reward;

    private void Awake()
    {
        group_page.SetActive(false);
    }
    public override void UIInit()
    {
        dataCharacter selected = data.selectedCharacter;
        fame_bar.ResetBar((float)selected.GetTotalFameCompletion, selected.GetFameAcquired.ToString(), selected.GetFameNeeded.ToString());
        token_bar.ResetBar((float)selected.curToken / selected.GetCurTokenThreshold, selected.curToken.ToString(), selected.GetCurTokenThreshold.ToString());
        UIUpdate();
        group_page.SetActive(true);
    }
    public override void UIUpdate()
    {
        dataCharacter selected = data.selectedCharacter;

        charatcer_name.SetText(selected.characterName);
        charatcer_class.SetText(selected.characterClass);
        charatcer_food_desc.SetText(string.Format("最爱的食物：{0}", selected.foodName));
        charatcer_food_icon.SetIcon(selected.food);

        portrait.UpdateAvatar(selected.GetPortrait);
        //token_icon_l.SetIcon(selected.token);
        token_icon.SetIcon(selected.token);

        if(selected.curFameLevel == dConstants.Character.MAX_CHARACTER_LEVEL)
        {
            fame_bar.ResetBar(1f, "满", "");
        }
        else
        {
            fame_bar.ResetBar((float)selected.GetTotalFameCompletion, selected.GetFameAcquired.ToString(), selected.GetFameNeeded.ToString());
        }
        fameTitle.SetText(string.Format("游记完成度：{0}%", Mathf.RoundToInt(selected.GetTotalFameCompletion * 100)));
        fame_acquire.SetText(string.Format("使用{0}进行游戏，发表游记，累积星级，以完成{1}的一部分。", selected.characterClass, dConstants.MASTER_JOURNAL_NAME));
        for (int i=0;i < curFameDetails.Count; i++)
        {
            if(i < selected.curFameLevel)
            {
                CharacterStat stat = dDataHub.hub.dCharacterStats.GetFromUID(string.Format("{0}_level_{1}", selected.characterUID, i+1));
                curFameDetails[i].SetText(string.Format("(累计星级{0}){1}：{2}", selected.GetFameNeededByLevel(i), stat.title, stat.desc));
                curFameDetails[i].SetTextColor(Color.black);
                curFameDetails[i].gameObject.SetActive(stat.desc != "");
            }
            else if(i == selected.curFameLevel && i < dConstants.Character.MAX_CHARACTER_LEVEL)
            {
                CharacterStat stat = dDataHub.hub.dCharacterStats.GetFromUID(string.Format("{0}_level_{1}", selected.characterUID, i + 1));
                curFameDetails[i].SetText(string.Format("(下一等级){0}：{1}", stat.title, stat.desc));
                curFameDetails[i].SetTextColor(Color.gray);
                curFameDetails[i].gameObject.SetActive(stat.desc != "");
            }
            else
            {
                curFameDetails[i].gameObject.SetActive(false);
            }
        }
        token_bar.SetIcon(selected.token);
        token_bar.SetColor(selected.mainColor);
        if (selected.curStoryLevel == dConstants.Character.MAX_STORY_LEVEL)
        {
            token_bar.ResetBar(1f, "满", "");
        }
        else
        {
            token_bar.ResetBar((float)selected.curToken / selected.GetCurTokenThreshold, selected.curToken.ToString(), selected.GetCurTokenThreshold.ToString());
        }
            
        tokenTitle.SetText(string.Format("信物等级：{0}", selected.GetStoryLevelText(selected.curStoryLevel)));
        story_acquire.SetText(string.Format("使用其他角色，以收集{0}的{1}", selected.characterClass, selected.tokenName));
        story_acquire_2.SetText(string.Format("使用其他角色进行游戏，可收集到{0}的{1}", selected.characterClass, selected.tokenName));
        for (int i = 0; i < curTokenDetails.Count; i++)
        {
            if (i < selected.curStoryLevel)
            {
                CharacterStat stat = dDataHub.hub.dCharacterStats.GetFromUID(string.Format("{0}_persona_{1}", selected.characterUID, i + 1));
                curTokenDetails[i].SetText(string.Format("({0}){1}：{2}", selected.GetStoryLevelText(i+1), stat.title, stat.desc));
                curTokenDetails[i].SetTextColor(Color.black);
                curTokenDetails[i].gameObject.SetActive(true);
            }
            else if (i == selected.curStoryLevel && i < dConstants.Character.MAX_STORY_LEVEL)
            {
                CharacterStat stat = dDataHub.hub.dCharacterStats.GetFromUID(string.Format("{0}_persona_{1}", selected.characterUID, i + 1));
                curTokenDetails[i].SetText(string.Format("(下一等级){0}：{1}", stat.title, stat.desc));
                curTokenDetails[i].SetTextColor(Color.gray);
                curTokenDetails[i].gameObject.SetActive(true);
            }
            else
            {
                curTokenDetails[i].gameObject.SetActive(false);
            }
        }
        List<string> collect_desc = new List<string>();
        for(int i = 0; i < dConstants.Character.MAX_STORY_LEVEL+1; i++)
        {
            CharacterStat stat = dDataHub.hub.dCharacterStats.GetFromUID(string.Format("{0}_help_{1}", selected.characterUID, i));
            if(stat.desc != "")
            {
                if (stat.level <= selected.curStoryLevel)
                {
                    collect_desc.Add(stat.desc);
                }
                else
                {
                    collect_desc.Add(string.Format("[达到{0}解锁]", selected.GetStoryLevelText(i)));
                }
            }
        }
        collect_method.SetText(string.Format("收集方式：<br>{0}", string.Join(",<br>", collect_desc)));
        //collect_reward.SetText(string.Format("收集奖励：{0}", selected.token_reward));
        //collect_method.SetText();
        //collect_reward.SetText();
    }
    public override void UIClose()
    {
        group_page.SetActive(false);
    }
}
