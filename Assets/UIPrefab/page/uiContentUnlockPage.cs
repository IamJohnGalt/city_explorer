﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class uiContentUnlockPage : uiElement
{
    [Header("Data")]
    public dataContentStore ContentData;
    [SerializeField] dataStats StatsData;
    [SerializeField] content_meter meterTemplate;

    [Header("Groups")]
    [SerializeField] GameObject group_page;
    [SerializeField] GameObject group_stats;
    [SerializeField] GameObject group_xp_progress;
    [SerializeField] GameObject group_bar;

    [Header("Children Objects")]
    //[SerializeField] List<character_selection> characters;
    [SerializeField] ui_icon_text totalFame;
    [SerializeField] ui_icon_text totalSession;
    [SerializeField] ui_icon_text totalJournal;
    [SerializeField] uiScrollableGroupHorizontal scrollControl;

    [SerializeField] ui_icon_text curXP;
    [SerializeField] Image xp_bar_fill;

    private List<content_meter> meters = new List<content_meter>();
    private int StartingPosX = 400;
    private int StepPosX = 800;
    private int MaxPoxX = 40000;

    private void Awake()
    {
        group_page.SetActive(false);
    }
    public override void UIInit()
    {
        UIUpdate();
        group_page.SetActive(true);
    }
    public override void UIUpdate()
    {
        totalFame.majorStr.SetText(string.Format("{0}", StatsData.totalFame));
        totalSession.majorStr.SetText(string.Format("{0}次", StatsData.totalSession));
        totalJournal.majorStr.SetText(string.Format("{0}篇", StatsData.totalJournal));

        int LastItemIndex = -1;
        int NextItemIndex = -1;
        bool NextItemLocked = false;
        //create meters
        List<ContentItem> AllItems = dDataHub.hub.dContentItem.GetFullList();
        for(int i = 0; i < AllItems.Count; i++)
        {
            if (i >= meters.Count)
            {
                meters.Add(Instantiate(meterTemplate, group_bar.transform));
            }
            meters[i].GetComponent<RectTransform>().anchoredPosition = new Vector2(StartingPosX + i * StepPosX, 0);
            meters[i].SetMeterByUID(AllItems[i].UID);
            if (!NextItemLocked && AllItems[i].xp_req > ContentData.XPEarned)
            {
                LastItemIndex = i - 1;
                NextItemIndex = i;
                NextItemLocked = true;
            }
        }
        if (!NextItemLocked)
        {
            LastItemIndex = AllItems.Count - 2;
            NextItemIndex = AllItems.Count - 1;
        }
        //Debug.Log(string.Format("lastIndex {0}, nextIndex {1}", LastItemIndex, NextItemIndex));
        //set bar element in right pos, set bar in the current pos
        float lastPosX = Mathf.Max(0, (StartingPosX + LastItemIndex * StepPosX));
        float nextPosX = StartingPosX + NextItemIndex * StepPosX;
        float nodeDistance = nextPosX - lastPosX;
        float lastXP = LastItemIndex >= 0 ? AllItems[LastItemIndex].xp_req : 0;
        float nextXP = AllItems[NextItemIndex].xp_req;
        float barPctg = (lastPosX + (nodeDistance * (ContentData.XPEarned - lastXP) / (nextXP - lastXP))) / MaxPoxX;
        xp_bar_fill.fillAmount = barPctg;
        curXP.GetComponent<RectTransform>().anchorMin = new Vector2(barPctg, 0.5f);
        curXP.GetComponent<RectTransform>().anchorMax = new Vector2(barPctg, 0.5f);
        curXP.majorStr.SetText(string.Format("{0}", ContentData.XPEarned));
        //to do set teh view to current
        group_bar.GetComponent<RectTransform>().anchoredPosition = new Vector2(-(lastPosX - 3200f), 200f);
        scrollControl.SetTo(-(lastPosX - 400f));
    }
    public override void UIClose()
    {
        group_page.SetActive(false);
    }
}
