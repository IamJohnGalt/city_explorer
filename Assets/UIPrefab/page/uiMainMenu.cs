﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class uiMainMenu : MonoBehaviour
{
    //private float scrollingSpeed = 30f;
    //private int iconCountPerUnit = 2;
    //private float maxPosY = -2400f;
    //private List<string> currentIconList = new List<string>();
    //[SerializeField] List<uiRepeatPath> pathUnits;
    [SerializeField] List<string> availableIconNames;
    //[SerializeField] GameEvent EventCharacterSelectionStart;
    //[SerializeField] GameObject EndlessPath;
    //[SerializeField] GameObject Cloud1;
    //[SerializeField] GameObject Cloud2;
    [SerializeField] ui_view_showcase viewShow;

    public void InitMainMenu()
    {
        //Cloud1.GetComponent<RectTransform>().DOLocalMoveY(-10, 4.3f).SetRelative().SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutSine);
        //Cloud2.GetComponent<RectTransform>().DOLocalMove(Vector2.one * -7, 7.1f).SetRelative().SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutSine);
        //InitEndlessPath();
        //seq.Play();
        availableIconNames = dDataHub.hub.dView.GetALLViewIcons();
        viewShow.StartPPT(availableIconNames);
    }
    /*
    public void InitEndlessPath()
    {
        currentIconList = new List<string>(availableIconNames);
        //seq = DOTween.Sequence();
        for (int i=0;i< pathUnits.Count; i++)
        {
            uiRepeatPath path = pathUnits[i];
            List<string> useIcons = new List<string>();
            for(int j=0;j< iconCountPerUnit; j++)
            {
                useIcons.Add(TakeIcon());
            }
            path.SetLocations(useIcons);
            float startPosY = path.GetComponent<RectTransform>().anchoredPosition.y;
            float moveTime = Mathf.Abs((maxPosY - startPosY)) / scrollingSpeed;
            path.GetComponent<RectTransform>().DOAnchorPosY(maxPosY, moveTime).SetEase(Ease.Linear).OnComplete(() => ResetPathUnit(path));
        }
    }
    void ResetPathUnit(uiRepeatPath unit)
    {
        //Debug.Log("reset Path unit called on unit name: " + unit.name);
        List<string> returnIcons = unit.curLocationIcons;
        for(int i=0;i< returnIcons.Count; i++)
        {
            ReturnIcon(returnIcons[i]);
        }
        List<string> useIcons = new List<string>();
        for (int i = 0; i < iconCountPerUnit; i++)
        {
            useIcons.Add(TakeIcon());
        }
        unit.SetLocations(useIcons);
        unit.GetComponent<RectTransform>().anchoredPosition = Vector2.zero;
        unit.GetComponent<RectTransform>().DOAnchorPosY(maxPosY, Mathf.Abs(maxPosY / scrollingSpeed)).SetEase(Ease.Linear).OnComplete(() => ResetPathUnit(unit));
    }
    string TakeIcon()
    {
        int rng = Random.Range(0, currentIconList.Count);
        string iconName = currentIconList[rng];
        currentIconList.RemoveAt(rng);
        return iconName;
    }
    void ReturnIcon(string iconName)
    {
        currentIconList.Add(iconName);
    }
    */
    public void btn_newgame_hit()
    {
        gameObject.SetActive(false);
        SFXPlayer.mp3.PlaySFX("button_click");
        //EventCharacterSelectionStart.Raise();
        //DOTween.PauseAll(gameObject);
    }
    public void btn_exit_hit()
    {
        Application.Quit();
    }
}
