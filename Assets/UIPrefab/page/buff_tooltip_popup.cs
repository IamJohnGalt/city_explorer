﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class buff_tooltip_popup : MonoBehaviour
{
    [SerializeField] Canvas masterCanvas;
    //private float screenWidth = 1920;
    //private float screenHeight = 1080;

    public static buff_tooltip_popup controller;
    private void Awake()
    {
        if (controller == null)
        {
            controller = this;
        }
        else if (controller != null)
        {
            Destroy(gameObject);
        }
    }
    [SerializeField] ui_buff_tooltip buff_tooltip;

    public void CallTooltip(Vector3 targetV3, List<string> buffUIDs)
    {
        //Debug.Log(string.Format("Call Buff Tooltip by {0}", targetV3));
        DismissAllTooltips();
        float screenWidth = masterCanvas.GetComponent<RectTransform>().sizeDelta.x;
        float screenHeight = masterCanvas.GetComponent<RectTransform>().sizeDelta.y;
        //Debug.Log(string.Format("current canvas size x {0}, y {1}", screenWidth, screenHeight));
        Vector3 relativePos = Camera.main.WorldToViewportPoint(targetV3);
        Vector2 WinPos = new Vector2(
            relativePos.x * screenWidth,
            relativePos.y * screenHeight);
        buff_tooltip.SetBuffTooptips(buffUIDs);
        buff_tooltip.GetComponent<RectTransform>().anchoredPosition = WinPos;
        buff_tooltip.Anim_FadeIn();
        buff_tooltip.gameObject.SetActive(gameObject);
    }
    public void DismissAllTooltips()
    {
        buff_tooltip.gameObject.SetActive(false);
    }
}
