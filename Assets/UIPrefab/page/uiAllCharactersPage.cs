﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class uiAllCharactersPage : uiElement
{
    [Header("Data")]
    public dataCharacterManagement CharacterData;
    [SerializeField] dataContentStore ContentUnlockData;
    [SerializeField] dataHistoryLog HistoryData;
    [SerializeField] history_log logTemplate;

    [Header("Groups")]
    [SerializeField] GameObject group_page;
    [SerializeField] GameObject group_content;
    [SerializeField] GameObject group_details;
    [SerializeField] GameObject group_history;

    [Header("Children Objects")]
    [SerializeField] List<ui_characterbook> books;
    [SerializeField] character_entry selectedCharacter;
    [SerializeField] ui_text feature;
    [SerializeField] ui_icon_text journalCompletion;
    [SerializeField] ui_icon_text tokenLevel;
    [SerializeField] ui_text highest_star;

    [SerializeField] ui_button btn_confirm;
    [SerializeField] ui_button btn_locked;

    [SerializeField] List<history_log> historyLogs = new List<history_log>();
    //[SerializeField] ui_bar nextUnlock;
    //[SerializeField] ui_icon_text totalFame;
    //[SerializeField] ui_icon_text totalSession;

    [Header("Game Event")]
    [SerializeField] GameEvent ContentPageUp;

    private int currentSelectedCharacter = -1;

    public void btn_content_hit()
    {
        ContentPageUp.Raise();
    }
    //deprecated function
    /*
    public void btn_character_select(int index)
    {
        CharacterManagement.controller.SelectTraveller(index);
        SFXPlayer.mp3.PlaySFX("guqin_single");
    }
    */
    public void btn_character_confirm()
    {
        SFXPlayer.mp3.StopAllSFX();
        CharacterManagement.controller.SelectTraveller(currentSelectedCharacter);
        SFXPlayer.mp3.PlaySFX("button_click");
    }
    public void btn_character_detail_view()
    {
        CharacterManagement.controller.ViewTraveller(currentSelectedCharacter);
    }
    public void btn_character_preview(int index)
    {
        if (currentSelectedCharacter == -1)
        {
            Anim_CameraDown();
        }
        currentSelectedCharacter = index;
        
        UIUpdate();
    }
    public void btn_character_hover_on(int index)
    {
        if(currentSelectedCharacter != index)
        {
            books[index].Anim_BookOut(true);
        }
    }
    public void btn_character_hover_off(int index)
    {
        if (currentSelectedCharacter != index)
        {
            books[index].Anim_BookIn();
        }
    }
    public override void UIInit()
    {
        for (int i = 0; i < books.Count; i++)
        {
            books[i].SetCharacter(CharacterData.allCharacters[i].GetPortrait);
        }
        currentSelectedCharacter = -1;
        CameraReset();
        UIUpdate();
        group_page.SetActive(true);
    }
    public override void UIUpdate()
    {
        if(currentSelectedCharacter < 0)
        {
            group_details.SetActive(false);
        }
        else
        {
            dataCharacter selected = CharacterData.allCharacters[currentSelectedCharacter];
            selectedCharacter.UpdateAvatar(selected.GetPortrait);
            feature.SetText(selected.characterClass);
            journalCompletion.majorStr.SetText(string.Format("游记完成度：{0}%", Mathf.RoundToInt(selected.GetTotalFameCompletion * 100)));
            tokenLevel.icon.SetIcon(selected.token);
            if (selected.curStoryLevel > 0)
            {
                tokenLevel.majorStr.SetText(string.Format("信物等级 {0}", selected.GetStoryLevelText(selected.curStoryLevel)));
                btn_confirm.majorStr.SetText("出发");
                btn_confirm.EnableButton();
                btn_confirm.gameObject.SetActive(true);
                btn_locked.gameObject.SetActive(false);
            }
            else
            {
                CharacterStat stat = dDataHub.hub.dCharacterStats.GetFromUID(string.Format("{0}_help_{1}", selected.characterUID, 0));
                string tokenAcquisitionMethod = stat.desc;
                tokenLevel.majorStr.SetText(string.Format("通过[{0}]以收集信物（{1}/{2}）", tokenAcquisitionMethod, selected.curToken, selected.GetCurTokenThreshold));
                btn_locked.majorStr.SetText("收集更多信物以解锁");
                btn_locked.DisableButton();
                btn_confirm.gameObject.SetActive(false);
                btn_locked.gameObject.SetActive(true);
            }
            if(selected.highestFame > 0)
            {
                highest_star.SetText(string.Format("旅行最高星级：{0}", selected.highestFame));
                highest_star.gameObject.SetActive(true);
            }
            else
            {
                highest_star.gameObject.SetActive(false);
            }
            group_details.SetActive(true);
        }
        //btn_confirm.gameObject.SetActive(currentSelectedCharacter >= 0);
        /*nextUnlock.ResetBar(
            (ContentUnlockData.XPEarned - ContentUnlockData.GetLastItemXPReq()) / (ContentUnlockData.GetNextItemXPReq() - ContentUnlockData.XPEarned),
            ContentUnlockData.XPEarned.ToString(),
            ContentUnlockData.GetNextItemXPReq().ToString());
        */
        //history update
        for (int i = 0; i < HistoryData.history.Count; i++)
        {
            if (i < historyLogs.Count)
            {
                historyLogs[i].SetLog(string.Format("{0}，{1}", HistoryData.history[i].dateText, HistoryData.history[i].log), HistoryData.history[i].starValue);
            }
            else
            {
                historyLogs.Add(Instantiate<history_log>(logTemplate, group_history.transform));
                historyLogs[i].SetLog(string.Format("{0}，{1}", HistoryData.history[i].dateText, HistoryData.history[i].log), HistoryData.history[i].starValue);
            }
        }
    }
    void CameraReset()
    {
        group_content.GetComponent<RectTransform>().anchoredPosition = new Vector2(0f, -360f);
    }
    void Anim_CameraDown()
    {
        group_content.GetComponent<RectTransform>().DOAnchorPos(new Vector2(0f, 0f), 1.5f).SetEase(Ease.OutSine);
    }
    public override void UIClose()
    {
        group_page.SetActive(false);
    }
}
