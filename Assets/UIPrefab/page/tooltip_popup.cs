﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class tooltip_popup : MonoBehaviour
{
    [SerializeField] Canvas masterCanvas;
    private float screenWidth = 1920;
    private float screenHeight = 1080;
    private float screenScale = 1f;
    private int offset_x = 50;
    private int offset_y = 20;

    public static tooltip_popup controller;
    private void Awake()
    {
        if (controller == null)
        {
            controller = this;
        }
        else if (controller != null)
        {
            Destroy(gameObject);
        }
    }
    [SerializeField] ui_tooltip tooltip_hor;
    [SerializeField] ui_tooltip tooltip_ver_down;
    [SerializeField] ui_tooltip tooltip_ver_up;

    public enum WindowType { horizontal, vertical_down, vertical_up};
    public void CallTooltip(Vector3 targetV3, WindowType type, string title, string desc, bool showBtn = false, bool activeBtn = false, string btn_desc = "", System.Action use_func = null)
    {
        DismissAllTooltips();
        screenWidth = masterCanvas.GetComponent<RectTransform>().sizeDelta.x;
        screenHeight = masterCanvas.GetComponent<RectTransform>().sizeDelta.y;
        //Debug.Log(string.Format("current canvas size x {0}, y {1}", screenWidth, screenHeight));
        Vector3 relativePos = Camera.main.WorldToViewportPoint(targetV3);
        Vector2 WinPos = new Vector2(
            relativePos.x * screenWidth + (type == WindowType.horizontal ? offset_x : 0f), 
            relativePos.y * screenHeight + (type == WindowType.vertical_down ? -offset_y : type == WindowType.vertical_up ? offset_y : 0f));
        ui_tooltip tooltip = tooltip_hor;
        switch (type)
        {
            case WindowType.horizontal:
                tooltip = tooltip_hor;
                break;
            case WindowType.vertical_down:
                tooltip = tooltip_ver_down;
                break;
            case WindowType.vertical_up:
                tooltip = tooltip_ver_up;
                break;
            default:
                tooltip = tooltip_hor;
                break;
        }
        tooltip.SetToolTip(title, desc);
        if (showBtn) 
        {
            tooltip.AddButton(activeBtn, btn_desc, use_func);
        }
        tooltip.GetComponent<RectTransform>().anchoredPosition = WinPos;
        tooltip.Anim_Popup();
    }
    public void CallTooltipBasedOnMouse(Vector3 mousePosition, WindowType type, string title, string desc, bool showBtn = false, bool activeBtn = false, string btn_desc = "", System.Action use_func = null)
    {
        DismissAllTooltips();
        screenWidth = masterCanvas.GetComponent<RectTransform>().sizeDelta.x;
        screenHeight = masterCanvas.GetComponent<RectTransform>().sizeDelta.y;
        //screenScale = masterCanvas.GetComponent<RectTransform>().localScale.x;
        //Debug.Log(string.Format("current canvas size x {0}, y {1}", screenWidth, screenHeight));
        Vector3 relativePos = Camera.main.ScreenToViewportPoint(mousePosition);
        //Debug.Log(string.Format("relative Pos ({0}, {1})", relativePos.x, relativePos.y));
        Vector2 WinPos = new Vector2(
            relativePos.x * screenWidth,
            relativePos.y * screenHeight);
        ui_tooltip tooltip = tooltip_hor;
        switch (type)
        {
            case WindowType.horizontal:
                tooltip = tooltip_hor;
                break;
            case WindowType.vertical_down:
                tooltip = tooltip_ver_down;
                break;
            case WindowType.vertical_up:
                tooltip = tooltip_ver_up;
                break;
            default:
                tooltip = tooltip_hor;
                break;
        }
        tooltip.SetToolTip(title, desc);
        if (showBtn)
        {
            tooltip.AddButton(activeBtn, btn_desc, use_func);
        }
        tooltip.GetComponent<RectTransform>().anchoredPosition = WinPos;
        tooltip.Anim_Popup();
    }
    public void DismissAllTooltips()
    {
        tooltip_hor.gameObject.SetActive(false);
        tooltip_ver_down.gameObject.SetActive(false);
        tooltip_ver_up.gameObject.SetActive(false);
    }
}
