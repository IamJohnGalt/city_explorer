﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class uiChronicle : uiElement
{
    [Header("Data")]
    [SerializeField] gChronicle gameplayScript;
    [SerializeField] dataChronicle gameplayData;
    [SerializeField] dataJournal journalData;
    [SerializeField] dataEndSession endingData;
    [SerializeField] chronicle_log logItemTemplate;
    
    [Header("Static Groups")]
    [SerializeField] GameObject group_page;
    [SerializeField] GameObject group_bg;
    [SerializeField] GameObject group_main;
    [SerializeField] GameObject group_past;
    [SerializeField] GameObject group_now;

    [Header("Dynamic Groups")]
    [SerializeField] GameObject group_destination;
    [SerializeField] GameObject group_activity;
    [SerializeField] GameObject group_journal;
    [SerializeField] GameObject group_trait;
    [SerializeField] GameObject group_submission;
    [SerializeField] GameObject group_end;
    [SerializeField] GameObject group_rewardclaim;

    [Header("Children Objects")]
    [SerializeField] List<chronicle_log> pastLogs;
    [SerializeField] chronicle_log curLog;
    [SerializeField] uipfb_mask curOptionMask;
    [SerializeField] List<park_selection> destinations;
    [SerializeField] List<activity_selection> activities;
    [SerializeField] GameObject btn_skip;
    [SerializeField] ui_icon_text cost_skip;
    [SerializeField] List<journal_selection> journals;
    [SerializeField] List<trait_selection> traits;
    [SerializeField] List<submission_selection> submissions;
    [SerializeField] List<end_selection> endings;
    [SerializeField] ui_journal_reward_claim journal_reward;
    [SerializeField] Image journalIcon;

    //animation specific data
    [SerializeField] int displayIndexOnPastLog = 0;
    private static float displayTimePerLog = 1f;


    public void btn_destination_hit(int index)
    {
        gameplayScript.ParkSelected(index);
        //sfx
        SFXPlayer.mp3.PlaySFX("cart_away");
        UIUpdate();
    }
    public void btn_activity_hit(int index)
    {
        gameplayScript.ActiivitySelected(index);
        //SFXPlayer.mp3.PlaySFX("default");
        //sfx
        SFXPlayer.mp3.PlaySFX("button_click");
        UIUpdate();
    }
    public void btn_activity_skip()
    {
        gameplayScript.ActiivitySkipped();
        //sfx
        SFXPlayer.mp3.PlaySFX("button_click");
        UIUpdate();
    }
    public void btn_journal_hit(int index)
    {
        gameplayScript.JournalSelected(index);
        journalIcon.sprite = uAssetReader.controller.GetByName(journalData.journalBgArt);
        CollectEffect.Collect(journalIcon.gameObject, 1, new Vector2(1f, 0f), UICanvas.canvas.EffectLayer, journals[index].transform, UIKeyElements.locator.Journal);
        //SFXPlayer.mp3.PlaySFX("default");
        //sfx
        SFXPlayer.mp3.PlaySFX("button_click");
        UIUpdate();
    }
    public void btn_trait_hit(int index)
    {
        gameplayScript.TraitSelected(index);

        CollectEffect.Collect(traits[index].iconGroup, 1, new Vector2(1f, 0f), UICanvas.canvas.EffectLayer, traits[index].transform, UIKeyElements.locator.TraitMenu);
        //sfx
        SFXPlayer.mp3.PlaySFX("button_click");
        UIUpdate();
    }
    public void btn_submission_hit(int index)
    {
        gameplayScript.SubmissionSelected(index);
        //SFX to do paper expand
        UIUpdate();
    }
    public void btn_reward_claim_hit()
    {
        //adjust chronicle log item based on UI
        gameplayScript.CreateDescForCurrentAction(journal_reward.GetActionLog());
        gameplayScript.AddArtForCurrentAction(journal_reward.GetActionLogIcon());
        //claim reward
        gameplayScript.JournalRewardClaim();
        //sfx
        SFXPlayer.mp3.PlaySFX("button_click");
        UIUpdate();
    }
    public void btn_sessionend_hit()
    {
        gameplayScript.EndConfirmed();
        UIUpdate();
    }
    private void Start()
    {
        ResetDisplay();
    }
    void ResetDisplay()
    {
        displayIndexOnPastLog = 0;
    }
    public override void UIInit()
    {
        
    }
    public override void UIUpdate()
    {
        //past options
        for(int i=0;i< gameplayData.pastActions.Count; i++)
        {
            bool hiddenDate = false;
            if (i > 0)
            {
                hiddenDate = gameplayData.pastActions[i].date.isSameMonth(gameplayData.pastActions[i - 1].date);
            }
            if (i < pastLogs.Count)
            {
                
                pastLogs[i].SetLog(gameplayData.pastActions[i], hiddenDate);
            }
            else
            {
                pastLogs.Add(Instantiate<chronicle_log>(logItemTemplate, group_past.transform));
                pastLogs[i].SetLog(gameplayData.pastActions[i], hiddenDate);
                pastLogs[i].ShowLineBackground();
                pastLogs[i].SetToHidden();
            }
        }
        //current options
        ClearAllGroups();
        switch (gameplayData.status)
        {
            case dConstants.ChronicleConst.ChronicleStatus.destination:
                DestinationUpdate();
                break;
            case dConstants.ChronicleConst.ChronicleStatus.activity:
                ActivityUpdate();
                break;
            case dConstants.ChronicleConst.ChronicleStatus.journalSelection:
                JournalUpdate();
                break;
            case dConstants.ChronicleConst.ChronicleStatus.journalSubmission:
                SubmnissionUpdate();
                break;
            case dConstants.ChronicleConst.ChronicleStatus.sessionEnd:
                SessionEndUpdate();
                break;
            case dConstants.ChronicleConst.ChronicleStatus.inTrait:
                TraitUpdate();
                break;
            case dConstants.ChronicleConst.ChronicleStatus.inJournalReward:
                JournalRewardUpdate();
                break;
            default:
                break;
        }
        //current log
        curLog.SetLog(gameplayData.currentAction);
        
        //check if there is queued items
        if (displayIndexOnPastLog < pastLogs.Count)
        {
            curLog.SetTo_FadeOut();
            curOptionMask.SetFade(1f);
            //Debug.Log("Calling to start anim show queue item sequence on uiChronicle");
            Anim_ShowQueuedItems();
        }
    }
    void ClearAllGroups()
    {
        group_destination.SetActive(false);
        group_activity.SetActive(false);
        group_journal.SetActive(false);
        group_submission.SetActive(false);
        group_trait.SetActive(false);
        group_end.SetActive(false);
        group_rewardclaim.SetActive(false);
        btn_skip.SetActive(false);
    }
    void DestinationUpdate()
    {
        bool atLeastOneParkAvailable = false;
        for (int i = 0; i < destinations.Count; i++)
        {
            if (i < gameplayData.currentOptions.Count)
            {
                //AtLeastOnePark = AtLeastOnePark || destinations[i].SetParkByUID(gameplayData.currentOptions[i]);
                bool available = destinations[i].SetParkByUID(gameplayData.currentOptions[i]);
                atLeastOneParkAvailable = atLeastOneParkAvailable || available;
                //additional UI resize
                destinations[i].transform.localScale = Vector3.one * (gameplayData.currentOptions.Count > 3 ? 0.7f : 1f);
                destinations[i].gameObject.SetActive(true);
            }
            else
            {
                destinations[i].gameObject.SetActive(false);
            }
        }
        btn_skip.SetActive(!atLeastOneParkAvailable);
        cost_skip.gameObject.SetActive(true);
        cost_skip.majorStr.SetText(string.Format("{0}月", dConstants.ChronicleConst.SKIP_PARK_TIME_COST));
        group_destination.SetActive(true);
    }
    void ActivityUpdate()
    {
        bool atLeastOneActivityAvailable = false;
        for (int i = 0; i < activities.Count; i++)
        {
            if (i < gameplayData.currentOptions.Count)
            {
                //AtLeastOneActivity = AtLeastOneActivity || activities[i].SetActivityByUID(gameplayData.currentOptions[i]);
                bool available = activities[i].SetActivityByUID(gameplayData.currentOptions[i]);
                atLeastOneActivityAvailable = atLeastOneActivityAvailable || available;
                //additional UI resize
                activities[i].transform.localScale = Vector3.one * (gameplayData.currentOptions.Count > 3 ? 0.7f : 1f);
                activities[i].gameObject.SetActive(true);
            }
            else
            {
                activities[i].gameObject.SetActive(false);
            }
        }
        btn_skip.SetActive(!atLeastOneActivityAvailable);
        cost_skip.gameObject.SetActive(true);
        cost_skip.majorStr.SetText(string.Format("{0}月", dConstants.ChronicleConst.SKIP_JOURNAL_TIME_COST));
        group_activity.SetActive(true);
    }
    void TraitUpdate()
    {
        for (int i = 0; i < traits.Count; i++)
        {
            if (i < gameplayData.currentOptions.Count)
            {
                traits[i].SetTraitByUID(gameplayData.currentOptions[i]);
                //additional UI resize
                traits[i].transform.localScale = Vector3.one * (gameplayData.currentOptions.Count > 3 ? 0.7f : 1f);
                traits[i].gameObject.SetActive(true);
            }
            else
            {
                traits[i].gameObject.SetActive(false);
            }
        }
        btn_skip.SetActive(false);
        //cost_skip.majorStr.SetText(string.Format("{0}月", dConstants.ChronicleConst.SKIP_ACTIVITY_TIME_COST));
        group_trait.SetActive(true);
    }
    void JournalUpdate()
    {
        for (int i = 0; i < journals.Count; i++)
        {
            if (i < gameplayData.currentOptions.Count)
            {
                journals[i].SetJournalOptionByUID(gameplayData.currentOptions[i]);
                //additional UI resize
                journals[i].transform.localScale = Vector3.one * (gameplayData.currentOptions.Count > 3 ? 0.7f : 1f);
                journals[i].gameObject.SetActive(true);
            }
            else
            {
                journals[i].gameObject.SetActive(false);
            }
        }
        group_journal.SetActive(true);
    }
    void SubmnissionUpdate()
    {
        Sprite journalArt = uAssetReader.controller.GetByName(journalData.journalBgArt);
        submissions[0].SetSubmissionOption("收笔", journalArt, journalData.totalStars, string.Format("发表{0}", journalData.journalName, journalData.totalStars));
        //submissions[1].SetSubmissionOption("再等等", journalArt, 0, string.Format("继续书写<br>(收集更多风景以提高评级)"));
        submissions[0].gameObject.SetActive(journalData.journalIsSubmittable);
        btn_skip.SetActive(!CurrencyManagement.wallet.NotEnoughCoin() && !journalData.oneParkJournal);
        cost_skip.majorStr.SetText(string.Format("{0}月", 0));
        cost_skip.gameObject.SetActive(true);
        //cost_skip.gameObject.SetActive(false);
        //submissions[1].gameObject.SetActive(!CurrencyManagement.wallet.NotEnoughCoin() && !journalData.oneParkJournal);
        group_submission.SetActive(true);
    }
    void JournalRewardUpdate()
    {
        int claimIndex = -1;
        for(int i = 0; i < journalData.constantRewards.Count; i++)
        {
            if (!journalData.constantRewards[i].claimed)
            {
                claimIndex = i;
                break;
            }
        }
        journal_reward.SetClaimReward(journalData.constantRewards[claimIndex], journalData.journalBgArt, journalData.journalName);
        group_rewardclaim.SetActive(true);
    }
    void SessionEndUpdate()
    {
        for (int i = 0; i < endings.Count; i++)
        {
            if (i < endingData.endingIndex.Count)
            {
                int endingIndex = endingData.endingIndex[i];
                endings[i].SetEnding(endingData.endTitle[endingIndex], endingData.endDesc[endingIndex], endingData.endIcon[endingIndex]);
                endings[i].gameObject.SetActive(true);
            }
            else
            {
                endings[i].gameObject.SetActive(false);
            }
        }
        group_end.SetActive(true);
    }
    public override void UIClose()
    {

    }
    public void Anim_SelectionConfirmed()
    {
        //temp for now
        curOptionMask.Anim_FadeTo(1f, displayTimePerLog);
        
    }
    void RevealCurOptions()
    {
        curOptionMask.Anim_FadeTo(0, displayTimePerLog);
        //sfx
        SFXPlayer.mp3.PlaySFX("writing_long");
    }
    void ForceLogUpdate()
    {
        LayoutRebuilder.ForceRebuildLayoutImmediate(group_past.GetComponent<RectTransform>());
    }
    public void Anim_ShowQueuedItems()
    {
        //DOTween.KillAll(gameObject);
        Sequence seq = DOTween.Sequence().OnUpdate(ForceLogUpdate);
        for (int i = displayIndexOnPastLog; i < gameplayData.pastActions.Count; i++)
        {
            //Debug.Log(string.Format("item ({0}) on past log added to seq", i));
            chronicle_log item = pastLogs[i];
            seq.AppendCallback(() => item.Anim_Appear(displayTimePerLog)).AppendInterval(displayTimePerLog);
        }
        //Debug.Log(string.Format("item on current log added to seq"));
        //seq.AppendCallback(() => pastLogs[0].Anim_Appear(displayTimePerLog)).AppendInterval(displayTimePerLog);
        seq.AppendCallback(() => curLog.Anim_FadeTo(0, displayTimePerLog)).AppendInterval(displayTimePerLog);
        seq.AppendCallback(RevealCurOptions);
        //seq.Play();
        displayIndexOnPastLog = gameplayData.pastActions.Count;
    }
}
