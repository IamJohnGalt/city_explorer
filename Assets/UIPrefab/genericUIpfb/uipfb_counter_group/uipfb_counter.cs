﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class uipfb_counter : MonoBehaviour
{
    [Header("Children Objects")]
    [SerializeField] uipfb_icon_v2 icon;
    [SerializeField] List<uipfb_counter_item> counters;

    public void SetIcon(Sprite sprt)
    {
        icon.SetIcon(sprt);
    }
    public void SetCounter(int curCount, int nextCount, int maxCount)
    {
        for(int i = 0; i < counters.Count; i++)
        {
            counters[i].SetStatus(i < maxCount, i < nextCount, i < curCount);
        }
    }
}
