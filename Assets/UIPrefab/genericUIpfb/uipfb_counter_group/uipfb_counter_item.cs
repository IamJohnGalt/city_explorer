﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class uipfb_counter_item : uipfb_atom
{
    enum CounterItemStatus { hidden, empty, next, filled};
    [Header("Status")]
    [SerializeField] CounterItemStatus curStatus;

    [Header("Children Objects")]
    [SerializeField] uipfb_art frame;
    [SerializeField] uipfb_art fill;


    public void SetColor(Color frameColor, Color fillColor)
    {
        //to do
    }
    public void SetStatus(bool activated, bool isNext, bool filled)
    {
        if (!activated)
        {
            curStatus = CounterItemStatus.hidden;
        }
        else if (filled)
        {
            curStatus = CounterItemStatus.filled;
        }
        else if (isNext)
        {
            curStatus = CounterItemStatus.next;
        }
        else
        {
            curStatus = CounterItemStatus.empty;
        }
        UpdateByCurStatus();
    }
    public void UpdateByCurStatus()
    {
        frame.SetVisible(curStatus != CounterItemStatus.hidden);
        fill.SetVisible(curStatus == CounterItemStatus.filled);
        //to do: support next status
    }
}
