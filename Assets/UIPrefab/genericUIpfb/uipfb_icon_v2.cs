﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class uipfb_icon_v2 : uipfb_atom
{
    public Image sprite;

    public void SetIcon(Sprite spt = null)
    {
        sprite.sprite = spt;
        SetVisible(spt != null);
    }
    public void SetIconColor(Color clr)
    {
        sprite.color = clr;
    }
    public void SetFade(float alpha)
    {
        Color curColor = sprite.color;
        Color clr = new Color(curColor.r, curColor.g, curColor.b, alpha);
        sprite.color = clr;
    }
    public void Anim_FadeTo(float alpha, float duration)
    {
        sprite.DOFade(alpha, duration);
    }
    public void Anim_Explode()
    {
        transform.DOScale(Vector3.one * 2f, 0.5f).SetEase(Ease.OutQuad);
        sprite.DOFade(0f, 0.5f).SetEase(Ease.OutQuad);
    }
    public void Anim_ScaleUpByValue(float endScale)
    {
        transform.DOScale(Vector3.one * endScale, 0.3f).SetEase(Ease.OutBack);
    }
    public void Anim_Highlighted()
    {
        transform.DOScale(Vector3.one * 1.1f, 0.8f).SetEase(Ease.InOutSine).SetLoops(-1, LoopType.Yoyo);
    }
}

