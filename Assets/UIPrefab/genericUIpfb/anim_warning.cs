﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class anim_warning : MonoBehaviour
{
    private void OnEnable()
    {
        transform.DOScale(Vector3.one * 1.2f, 0.5f).SetRelative().SetLoops(-1, LoopType.Yoyo);
    }
}
