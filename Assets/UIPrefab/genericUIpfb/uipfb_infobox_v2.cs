﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class uipfb_infobox_v2 : uipfb_atom
{
    [Header("Children Objects")]
    [SerializeField] Transform box;
    [SerializeField] Image frame_sprite;
    [SerializeField] Image icon_sprite;
    [SerializeField] TextMeshProUGUI title_tmp;
    [SerializeField] TextMeshProUGUI desc_tmp;

    public void SetInfoBox(string title = null, string desc = null, Sprite icon = null, Sprite frame = null)
    {
        title_tmp.SetText(title ?? "");
        title_tmp.gameObject.SetActive(title != "");
        desc_tmp.SetText(desc ?? "");
        desc_tmp.gameObject.SetActive(desc != "");
        icon_sprite.sprite = icon;
        icon_sprite.gameObject.SetActive(icon != null);
        frame_sprite.sprite = frame?? frame_sprite.sprite;
    }
    public void Anim_Expand()
    {
        box.DOScale(Vector3.one, 0.2f).SetEase(Ease.OutBack);
    }
    public void Anim_Collapse()
    {
        box.DOScale(Vector3.zero, 0.2f);
    }
}
