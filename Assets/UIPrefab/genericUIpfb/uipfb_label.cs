﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class uipfb_label : uipfb_atom
{
    [Header("Data")]
    public string curStr;
    [Header("Children Objects")]
    [SerializeField] TextMeshProUGUI tmp_str;

    public void SetText(string txt)
    {
        curStr = txt;
        tmp_str.text = txt;
        SetVisible(txt != "");
    }
    public void SetSprite(Sprite img)
    {
        GetComponent<Image>().sprite = img;
    }
    public void SetFade(float alpha)
    {
        Color curColor = GetComponent<Image>().color;
        Color txtColor = tmp_str.GetComponent<TextMeshProUGUI>().color;
        Color clr = new Color(curColor.r, curColor.g, curColor.b, alpha);
        GetComponent<Image>().color = clr;
        clr = new Color(txtColor.r, txtColor.g, txtColor.b, alpha);
        tmp_str.color = clr;
    }

    public void Anim_TextFadeIn()
    {
        tmp_str.DOFade(1f, 0.5f);
    }
    public void Anim_TextFadeOut()
    {
        tmp_str.DOFade(0f, 0.5f);
    }
    public void Anim_ScaleUp()
    {
        transform.DOScale(Vector3.one * 1.05f, 0.3f).SetEase(Ease.OutBack);
    }
    public void Anim_ScaleUpByValue(float endScale)
    {
        transform.DOScale(Vector3.one * endScale, 0.3f).SetEase(Ease.OutBack);
    }
    public void Anim_ScaleReset()
    {
        transform.DOScale(Vector3.one, 0.3f);
    }
}

