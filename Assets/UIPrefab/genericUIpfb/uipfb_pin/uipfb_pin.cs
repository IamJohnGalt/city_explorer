﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class uipfb_pin : MonoBehaviour
{
    [SerializeField] Sprite initial_sprite;
    [SerializeField] Sprite final_sprite;
    [SerializeField] uipfb_icon_v2 icon;
    void Awake()
    {
        ResetPin();
    }
    void OnArrival()
    {
        //Debug.Log("a pin animation is finished");
        icon.SetIcon(final_sprite);
    }
    public void Anim_Pin(float anim_time = 0.2f)
    {
        gameObject.SetActive(true);
        //Debug.Log("a pin animation is triggered");
        icon.SetIcon(initial_sprite);
        icon.GetComponent<RectTransform>().DOAnchorPos(new Vector2(-50f, 50f), anim_time).From().SetEase(Ease.OutSine).OnComplete(OnArrival);
    }
    public void SetPin()
    {
        OnArrival();
        gameObject.SetActive(true);
    }
    public void ResetPin()
    {
        gameObject.SetActive(false);
    }
}
