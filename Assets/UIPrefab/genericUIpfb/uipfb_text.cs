﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;

public class uipfb_text : uipfb_atom
{
    [SerializeField] TextMeshProUGUI str;

    public void Anim_TextFadeIn()
    {
        str.DOFade(1f, 0.5f);
    }
    public void Anim_TextFadeOut()
    {
        str.DOFade(0f, 0.5f);
    }
    public void SetText(string txt)
    {
        str.text = txt;
        SetVisible(txt != "");
    }
}
