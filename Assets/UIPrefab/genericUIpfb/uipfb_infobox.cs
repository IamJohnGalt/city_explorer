﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;

public class uipfb_infobox : uipfb_atom
{
    [Header("Data")]
    public string curStr;
    [Header("Children Objects")]
    [SerializeField] Transform box;
    [SerializeField] TextMeshProUGUI tmp_str;

    public void SetText(string txt)
    {
        curStr = txt;
        tmp_str.text = txt;
    }
    public void Anim_Expand()
    {
        box.DOScale(Vector3.one, 0.2f).SetEase(Ease.OutBack);
    }
    public void Anim_Collapse()
    {
        box.DOScale(Vector3.zero, 0.2f);
    }
}
