﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class uipfb_art : uipfb_atom
{
    [Header("Data")]
    public string spritePath;
    [Header("Children Objects")]
    public Image img;

    public void SetSprite(string filePath)
    {
        Sprite spt = Resources.Load<Sprite>(filePath);
        spritePath = filePath;
        img.sprite = spt;
        SetVisible(true);
    }
    public void SetSprite(Sprite sprite)
    {
        img.sprite = sprite;
        SetVisible(true);
    }
}
