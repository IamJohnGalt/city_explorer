﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class uipfb_icon : uipfb_atom
{
    [Header("Children Objects")]
    [SerializeField] Image icon_sprite;

    public void SetIcon(Sprite spt)
    {
        icon_sprite.sprite = spt;
    }
    public void SetIconColor(Color clr)
    {
        icon_sprite.color = clr;
    }
    public void SetFade(float alpha)
    {
        Color curColor = icon_sprite.color;
        Color clr = new Color(curColor.r, curColor.g, curColor.b, alpha);
        icon_sprite.color = clr;
    }
    public void Anim_ScaleUpByValue(float endScale)
    {
        transform.DOScale(Vector3.one * endScale, 0.3f).SetEase(Ease.OutBack);
    }
    public void Anim_ScaleReset()
    {
        transform.DOScale(Vector3.one, 0.3f);
    }
    public void Anim_Explode()
    {
        transform.DOScale(Vector3.one * 2f, 0.5f).SetEase(Ease.OutQuad);
        icon_sprite.DOFade(0f, 0.5f).SetEase(Ease.OutQuad);
    }
}
