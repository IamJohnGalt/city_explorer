﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ZoomEffect : MonoBehaviour
{
    [SerializeField] RectTransform rect;
    [SerializeField] Image mask;
    public void FocusOn(Vector2 pivotPoint)
    {
        //rect.anchorMin = pivotPoint;
        //rect.anchorMax = pivotPoint;
        rect.pivot = pivotPoint;
    }
    public void Anim_ZoomIn(float delay)
    {
        mask.DOFade(1f, 1f).SetEase(Ease.InQuad).SetDelay(delay);
        rect.DOScale(Vector3.one * 10f, 3f).SetEase(Ease.InQuad).SetDelay(delay).OnComplete(()=> ResetScale2One());
    }
    public void Anim_ZoomOut(float delay)
    {
        mask.DOFade(1f, 1f).SetEase(Ease.InQuad).SetDelay(delay);
        rect.DOScale(Vector3.one * 1f, 1f).SetEase(Ease.InQuad).SetDelay(delay).OnComplete(() => ResetScale2One());
    }
    public void Anim_ZoomIn_From(float delay)
    {
        mask.DOFade(1f, 1f).SetEase(Ease.InQuad).SetDelay(delay).From();
        rect.DOScale(Vector3.one * 0.1f, 1f).SetEase(Ease.OutQuad).SetDelay(delay).OnComplete(() => ResetScale2One()).From();
    }
    public void Anim_ZoomOut_From(float delay)
    {
        mask.DOFade(1f, 1f).SetEase(Ease.InQuad).SetDelay(delay).From();
        rect.DOScale(Vector3.one * 10f, 1f).SetEase(Ease.OutQuad).SetDelay(delay).OnComplete(() => ResetScale2One()).From();
    }
    void ResetScale2One()
    {
        rect.localScale = Vector3.one;
        mask.color = new Color(1f, 1f, 1f, 0f);
    }
}
