﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class uipfb_bar : uipfb_atom
{
    [Header("Data")]
    public float curValue;
    public float targetValue;
    [Header("Children Objects")]
    [SerializeField] uipfb_icon_v2 icon;
    [SerializeField] Image fill;
    [SerializeField] uipfb_label cur;
    [SerializeField] uipfb_label max;
    public void ResetBar(float startPct, string curLabel, string maxLabel)
    {
        cur.SetText(curLabel);
        max.SetText(maxLabel);
        curValue = startPct;
        targetValue = curValue;
        CalibrateBar();
    }
    public void SetBarProgress(float progPct, string curLabel, string maxLabel)
    {
        CalibrateBar();
        targetValue = Mathf.Max(Mathf.Min(progPct, 1f), 0f);
        cur.SetText(curLabel);
        max.SetText(maxLabel);
        Anim_ValueIncrease();
    }
    public void SetIcon(Sprite spt)
    {
        icon.SetIcon(spt);
    }
    public void SetColor(Color clr)
    {

    }
    void CalibrateBar()
    {
        cur.GetComponent<RectTransform>().anchorMin = new Vector2(curValue, 1f);
        cur.GetComponent<RectTransform>().anchorMax = new Vector2(curValue, 1f);
        fill.fillAmount = curValue;
    }
    public void Anim_ValueIncrease()
    {
        if(curValue != targetValue)
        {
            DOTween.To(() => curValue, x => curValue = x, targetValue, 1f).OnUpdate(() => CalibrateBar());
        }
    }
}
