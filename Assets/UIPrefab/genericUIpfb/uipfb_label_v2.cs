﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;

public class uipfb_label_v2 : uipfb_icon_v2
{
    [SerializeField] TextMeshProUGUI str;
    public string curStr;
    

    public void SetText(string txt)
    {
        curStr = txt;
        str.text = txt;
        SetVisible(txt != "");
    }
    public void SetTextColor(Color clr)
    {
        float curAlpha = str.color.a;
        str.color = new Color(clr.r, clr.g, clr.b, curAlpha);
    }
    public void SetFade(float alpha)
    {
        Color sptColor = sprite.color;
        Color txtColor = str.GetComponent<TextMeshProUGUI>().color;
        sprite.color = new Color(sptColor.r, sptColor.g, sptColor.b, alpha);
        str.color = new Color(txtColor.r, txtColor.g, txtColor.b, alpha);
    }

    public void Anim_TextFadeIn()
    {
        str.DOFade(1f, 0.5f);
    }
    public void Anim_TextFadeOut()
    {
        str.DOFade(0f, 0.5f);
    }
    public void Anim_Explode()
    {
        transform.DOScale(Vector3.one * 2f, 0.5f).SetEase(Ease.OutQuad);
        sprite.DOFade(0f, 0.5f).SetEase(Ease.OutQuad);
        str.DOFade(0f, 0.5f).SetEase(Ease.OutQuad);
    }
}
