﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class uipfb_mask : uipfb_atom
{
    public Image sprite;
    public void SetFade(float alpha)
    {
        Color curColor = sprite.color;
        Color clr = new Color(curColor.r, curColor.g, curColor.b, alpha);
        sprite.color = clr;
    }
    public void Anim_FadeTo(float alpha, float duration)
    {
        sprite.DOFade(alpha, duration);
    }
}
