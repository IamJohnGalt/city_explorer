﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class uipfb_atom : MonoBehaviour
{
    public void SetVisible(bool visibility)
    {
        gameObject.SetActive(visibility);
    }
    public void Anim_ScaleUp()
    {
        transform.DOScale(Vector3.one * 1.05f, 0.3f).SetEase(Ease.OutBack);
    }
    public void Anim_ScaleReset()
    {
        transform.DOScale(Vector3.one, 0.3f).SetEase(Ease.OutBack);
    }
}
